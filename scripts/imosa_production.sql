-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 12-09-2016 a las 16:41:01
-- Versión del servidor: 5.7.13-0ubuntu0.16.04.2
-- Versión de PHP: 7.0.10-2+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `imosa_production`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `brands`
--

INSERT INTO `brands` (`brand_id`, `description`) VALUES
(1, 'HONDA'),
(2, 'LONCIN'),
(3, 'CG'),
(4, 'YAMAHA'),
(5, 'GENESIS'),
(6, 'SHINERAY'),
(7, 'BRONCO'),
(8, 'SERPENTO'),
(9, 'SUZUKI'),
(10, 'KMF'),
(11, 'BAJAJ'),
(12, 'HERO'),
(13, 'ITALIKA'),
(14, 'ZMOTO'),
(15, 'YUMBO'),
(16, 'UM'),
(17, 'APACHE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cellars`
--

CREATE TABLE `cellars` (
  `cellar_id` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cellars`
--

INSERT INTO `cellars` (`cellar_id`, `description`) VALUES
(3, 'BODEGA 1'),
(4, 'BODEGA 2\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `rtn` varchar(255) NOT NULL,
  `web_site` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `company`
--

INSERT INTO `company` (`company_id`, `name`, `address`, `telephone`, `email`, `rtn`, `web_site`, `logo`) VALUES
(1, 'IMPORTA Y DISTRIBUIDORA DE MOTOPARTES S.A.', 'Res. Palma Real, Frente a Fuerza Aerea', '2203-4049', 'imosa.hn@gmail.com', '08019015808210', 'www.imosa.hn', 'uploads/thumb_logo.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company_invoice`
--

CREATE TABLE `company_invoice` (
  `company_invoice_id` int(11) NOT NULL,
  `cai` varchar(255) NOT NULL,
  `range_to` varchar(255) NOT NULL,
  `range_up` varchar(255) NOT NULL,
  `limit_date` varchar(20) NOT NULL,
  `isv` double NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `company_invoice`
--

INSERT INTO `company_invoice` (`company_invoice_id`, `cai`, `range_to`, `range_up`, `limit_date`, `isv`, `company_id`) VALUES
(2, '69D930B5B64B764FB8A80BBE7AB0198C', '00-001-01-00000001', '00-001-01-00000150', '27-07-17', 0.15, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `credits`
--

CREATE TABLE `credits` (
  `credit_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `balance` int(11) NOT NULL,
  `credit_condition` varchar(140) NOT NULL,
  `type` enum('Repuesto','Motocicleta') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `phone_number` varchar(140) NOT NULL,
  `address` varchar(255) NOT NULL,
  `full_name` varchar(140) NOT NULL,
  `RTN` varchar(14) DEFAULT NULL,
  `customer_type` int(11) NOT NULL,
  `customer_id` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `customers`
--

INSERT INTO `customers` (`customer_id`, `phone_number`, `address`, `full_name`, `RTN`, `customer_type`, `customer_id`, `email`) VALUES
(4, '32516392', 'DANLI, EL PARAISO', 'COMERCIAL Y VARIEDADES DE ORIENTE', '08019015808210', 3, '1', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers_type`
--

CREATE TABLE `customers_type` (
  `customer_type` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `customers_type`
--

INSERT INTO `customers_type` (`customer_type`, `description`) VALUES
(3, 'MAYOREO'),
(4, 'RUTA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `company_invoice` int(11) NOT NULL,
  `isv` varchar(20) NOT NULL,
  `subtotal` varchar(20) NOT NULL,
  `total` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `models`
--

CREATE TABLE `models` (
  `model_id` int(11) NOT NULL,
  `description` varchar(140) NOT NULL,
  `brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `models`
--

INSERT INTO `models` (`model_id`, `description`, `brand_id`) VALUES
(103, 'MAGNUM-150', 3),
(104, 'PULSAR-135', 11),
(105, 'PLATINA-100', 11),
(106, 'AX-100', 9),
(107, 'CT-100', 11),
(108, 'YBR-125', 4),
(109, 'DT-175', 4),
(110, 'CG-125', 3),
(111, 'CG-150', 3),
(112, 'CBF-125', 1),
(113, 'HUNK', 12),
(114, 'CBF-150', 1),
(115, 'HJ-125-7', 5),
(116, 'PLATINA-125', 11),
(117, 'PULSAR-180', 11),
(118, 'PULSAR-200NS', 11),
(119, 'SUPER SPLENDOR', 12),
(120, 'GLAMOUR', 12),
(121, 'DAKAR-200', 15),
(122, 'XM-200', 5),
(123, 'ZM-200L', 10),
(124, 'SPITZER-150', 2),
(125, 'CRUX-110', 4),
(126, 'DISCOVER-125', 11),
(127, 'DISCOVER-125ST', 11),
(128, 'DISCOVER-135', 11),
(129, 'FZ16', 4),
(130, 'DM-150', 13),
(131, 'GXT-200', 5),
(132, 'XL-200', 1),
(133, 'XTZ-125', 4),
(134, 'UNICORN-150', 1),
(135, 'CGL-125', 1),
(136, 'GXT-250', 5),
(137, 'LX-150', 5),
(138, 'XY-200', 6),
(139, 'XY-200 ST-6', 6),
(140, 'RT-180', 13),
(141, 'FT-180', 13),
(142, 'GY6', 13),
(143, 'GN-125', 9),
(144, 'BOA', 8),
(145, 'COBRA', 8),
(146, 'TITAN', 6),
(147, 'ZM-250', 10),
(148, 'ZM-250L', 10),
(149, 'INVICTA-150', 1),
(150, 'GS-150R', 9),
(151, 'XCD', 11),
(152, 'DISCOVER-100', 11),
(153, 'EN-125', 9),
(154, 'PULSAR-150', 11),
(155, 'XCD-125', 11),
(156, 'PULSAR-200', 11),
(157, 'GN-125H', 9),
(158, 'DISCOVER-150', 11),
(159, 'BOXER-150', 11),
(160, 'PULSAR-220', 11),
(161, 'PULSAR-135LS', 11),
(162, 'PULSAR-UG4', 11),
(163, 'XCD-135', 11),
(164, 'UNIVERSAL', 1),
(165, 'BYQ', 14),
(166, 'FT-125', 13),
(167, 'CORAL', 8),
(168, 'BOXER BM150', 11),
(169, 'BOA-160', 8),
(170, 'SERPENTO-125', 8),
(171, 'SERPENTO-150', 8),
(172, 'ZM-200', 10),
(173, 'CG-200', 1),
(174, 'DM-200', 13),
(175, 'PULSAR-125', 11),
(176, 'PULSAR-125LS', 11),
(177, 'KB-150', 10),
(178, 'CG', 3),
(179, 'JARA-200', 8),
(180, 'ZX-150L', 10),
(181, 'FT-150', 13),
(182, 'TX-200', 13),
(183, 'DSR-200', 16),
(184, 'GYA-200', 5),
(185, 'GS-200', 15),
(186, 'TVS-160', 17),
(187, 'BOXER-100', 11),
(188, 'RE 205', 11),
(189, 'AX-100-4', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `total` varchar(20) NOT NULL,
  `subtotal` varchar(20) NOT NULL,
  `isv` varchar(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) DEFAULT '1',
  `is_invoice` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments_history`
--

CREATE TABLE `payments_history` (
  `payments_history` int(11) NOT NULL,
  `credit_id` int(11) NOT NULL,
  `payment_amount` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providers`
--

CREATE TABLE `providers` (
  `provider_id` int(11) NOT NULL,
  `name` varchar(140) NOT NULL,
  `country` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `providers`
--

INSERT INTO `providers` (`provider_id`, `name`, `country`) VALUES
(1, 'VINI', 'CHINA'),
(2, 'IMBRA', 'COLOMBIA'),
(3, 'BJ', 'CHINA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_forma`
--

CREATE TABLE `pro_forma` (
  `proforma_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `total` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `replacements`
--

CREATE TABLE `replacements` (
  `replacement_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `bar_code` varchar(50) DEFAULT NULL,
  `description` varchar(140) DEFAULT NULL,
  `wholesale_price` double NOT NULL,
  `route_price` double NOT NULL,
  `picture` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `cellar_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `replacements`
--

INSERT INTO `replacements` (`replacement_id`, `code`, `bar_code`, `description`, `wholesale_price`, `route_price`, `picture`, `location`, `quantity`, `cellar_id`, `provider_id`) VALUES
(75, '7425602001041', NULL, 'FRICCIONES MAGNUM150\r\n', 55, 60.5, 'uploads/thumb_IMG-70.png', '-', 68, 3, 1),
(76, '7425602001065', NULL, 'FRICCIONES PULSAR 135\r\n', 70, 77, 'uploads/thumb_IMG-70.png', '-', 131, 3, 1),
(77, '7425602001089', NULL, 'FRICCIONES PLATINA 100/AX100/CT100\r\n', 60, 66, 'uploads/thumb_IMG-70.png', '-', 120, 3, 1),
(78, '7425602001027', NULL, 'FRICCIONES YBR125/CG125/150/DT 175\r\n', 70, 77, 'uploads/thumb_IMG-70.png', '-', 124, 3, 1),
(79, '7425602004417', NULL, 'CILINDRO COMPLETO CBF 125 STUNNER\r\n', 850, 935, 'uploads/thumb_IMG-8.png', '-', 19, 3, 1),
(80, '7425602004424', NULL, 'CILINDRO COMPLETO CBF150/HUNK\r\n', 775, 852.5, 'uploads/thumb_IMG-4.png', '-', 5, 3, 1),
(81, '7425602004455', NULL, 'CILINDRO COMPLETO CG150\r\n', 675, 742.5, 'uploads/thumb_IMG-11.png', '-', 8, 3, 1),
(82, '7425602004400', NULL, 'CILINDRO COMPLETO DAKAR 200\r\n', 900, 990, 'uploads/thumb_IMG-14.png', '-', 14, 3, 1),
(83, '7425602004479', NULL, 'CILINDRO COMPLETO HJ125-7\r\n', 600, 660, 'uploads/thumb_IMG-16.png', '-', 25, 3, 1),
(84, '7425602005452', NULL, 'CILINDRO COMPLETO PLATINA125\r\n', 850, 935, 'uploads/thumb_IMG-5.png', '-', 7, 3, 1),
(85, '7425602004394', NULL, 'CILINDRO COMPLETO PULSAR 180\r\n', 900, 990, 'uploads/thumb_IMG-7.png', '-', 4, 3, 1),
(86, '7425602004370', NULL, 'CILINDRO COMPLETO PULSAR200 NS\r\n', 1200, 1320, 'uploads/thumb_IMG-2.png', '-', 3, 3, 1),
(87, '7425602004387', NULL, 'CILINDRO COMPLETO SUPER SPLENDOR/GLAMOUR\r\n', 775, 852.5, 'uploads/thumb_IMG-9.png', '-', 6, 3, 1),
(88, '7425602004431', NULL, 'CILINDRO COMPLETO XM200', 780, 858, 'uploads/thumb_IMG-18.png', '-', 21, 3, 1),
(89, '7425602005773', NULL, 'CILINDRO DE MOTOR SPITZER150\r\n', 600, 660, 'uploads/thumb_IMG-12.png', '-', 12, 3, 1),
(90, '7425602005766', NULL, 'CILINDRO DE MOTOR ZM-200L\r\n', 800, 880, 'uploads/thumb_IMG-15.png', '-', 36, 3, 1),
(91, '7425602002116', NULL, 'KIT DE ANILLOS CG150 STD\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 40, 3, 1),
(92, '7425602002123', NULL, 'KIT DE ANILLOS CRUX110 STD\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 72, 3, 1),
(93, '7425602002130', NULL, 'KIT DE ANILLOS CT100 STD\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 98, 3, 1),
(94, '7425602002154', NULL, 'KIT DE ANILLOS PULSAR135/DISCOVER125 ST/XCD125/PLATINA125 STD\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-', 81, 3, 1),
(95, '7425602002161', NULL, 'KIT DE ANILLOS DISCOVER135 STD/FZ16 STD\r\n', 110, 121, 'uploads/thumb_IMG-1.png', '-', 93, 3, 1),
(96, '7425602002178', NULL, 'KIT DE ANILLOS DM150 STD/ENGINE BLACK\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-  ', 48, 3, 1),
(97, '7425602002208', NULL, 'KIT DE ANILLOS GXT200 STD\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-', 29, 3, 1),
(98, '7425602002253', NULL, 'KIT DE ANILLOS XL200 STD\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-', 51, 3, 1),
(99, '7425602002260', NULL, 'KIT DE ANILLOS XM200 STD\r\n', 110, 121, 'uploads/thumb_IMG-1.png', '-', 59, 3, 1),
(100, '7425602002277', NULL, 'KIT DE ANILLOS YBR125 ED/G STD\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 20, 3, 1),
(101, '7425602002284', NULL, 'KIT DE ANILLOS ZM-200L STD\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-', 100, 3, 1),
(102, '7425602002734', NULL, 'KIT DE ANILLOS GXT200 0.25\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 86, 3, 1),
(103, '7425602001911', NULL, 'KIT DE BIELA DE MOTOR YBR 125/XTZ125\r\n', 250, 275, 'uploads/thumb_IMG-0.png', '-', 17, 3, 1),
(104, '7425602001928', NULL, 'KIT DE BIELA DE MOTOR CRUX110\r\n', 250, 275, 'uploads/thumb_IMG-0.png', '-', 20, 3, 1),
(105, '7425602001935', NULL, 'KIT DE BIELA DE MOTOR FZ16\r\n', 325, 357.5, 'uploads/thumb_IMG-0.png', '-', 22, 3, 1),
(106, '7425602001942', NULL, 'KIT DE BIELA DE MOTOR DT 175\r\n', 230, 253, 'uploads/thumb_IMG-0.png', '-', 18, 3, 1),
(107, '7425602001997', NULL, 'KIT DE BIELA DE MOTOR CBF 125 STUNNER\r\n', 300, 330, 'uploads/thumb_IMG-0.png', '-', 24, 3, 1),
(108, '7425602002000', NULL, 'KIT DE BIELA DE MOTOR UNICORN 150/HUNK/CBF150\r\n', 285, 313.5, 'uploads/thumb_IMG-0.png', '-', 27, 3, 1),
(109, '7425602002017', NULL, 'KIT DE BIELA DE MOTOR PULSAR135\r\n', 300, 330, 'uploads/thumb_IMG-0.png', '-', 18, 3, 1),
(110, '7425602002048', NULL, 'KIT DE BIELA DE MOTOR PLATINA 125/DISCOVER 125 ST\r\n', 300, 330, 'uploads/thumb_IMG-0.png', '-', 13, 3, 1),
(111, '7425602002055', NULL, 'KIT DE BIELA DE MOTOR CG150\r\n', 230, 253, 'uploads/thumb_IMG-0.png', '-', 20, 3, 1),
(112, '7425602002062', NULL, 'KIT DE BIELA DE MOTOR CGL125/HJ125-7\r\n', 210, 231, 'uploads/thumb_IMG-0.png', '-', 24, 3, 1),
(113, '7425602005476', NULL, 'KIT DE BIELA DE MOTOR GXT250\r\n', 600, 660, 'uploads/thumb_IMG-0.png', '-', 25, 3, 1),
(114, '7425602001010', NULL, 'KIT DE BIELA DE MOTOR DM150\r\n', 300, 330, 'uploads/thumb_IMG-0.png', '-', 18, 3, 1),
(115, '7425602001119', NULL, 'PASTILLAS DE FRENO LX-150/XY-200 ST-6\r\n', 40, 44, 'uploads/thumb_IMG-45.png', '-', 115, 3, 1),
(116, '7425602001157', NULL, 'PASTILLAS DE FRENO PULSAR 200NS\r\n', 40, 44, 'uploads/thumb_IMG-44.png', '-', 46, 3, 1),
(117, '7425602001126', NULL, 'PASTILLAS DE FRENO RT-180/GY6/DM150/FT180\r\n', 35, 38.5, 'uploads/thumb_IMG-42.png', '-', 55, 3, 1),
(118, '7425602001102', NULL, 'PASTILLAS DE FRENO YBR 125\r\n', 40, 44, 'uploads/thumb_IMG-49.png', '- ', 150, 3, 1),
(119, '7425602001096', NULL, 'PASTILLAS DE FRENO PULSAR180/FZ-16/PULSAR135/DISCOVER125/135\r\n', 35, 38.5, 'uploads/thumb_IMG-47.png', '-', 45, 3, 1),
(120, '7425602001133', NULL, 'PASTILLAS DE FRENO HJ 125/GN 125\r\n', 33, 36.3, 'uploads/thumb_IMG-48.png', '-', 7, 3, 1),
(121, '7425602001140', NULL, 'PASTILLAS DE FRENO BOA/COBRA/TITAN\r\n', 33, 36.3, 'uploads/thumb_IMG-52.png', '-', 35, 3, 1),
(122, '7425602001874', NULL, 'CATARINA TRASERA YBR125  428-47T\r\n', 125, 137.5, 'uploads/thumb_IMG-91.png', '-', 22, 3, 1),
(123, '7425602001744', NULL, 'SPROCKET TRASERO GXT200 428-48T\r\n', 115, 126.5, 'uploads/thumb_IMG-87.png', '-', 70, 3, 1),
(124, '7425602001867', NULL, 'SPROCKET TRASERO CG125 428-45T\r\n', 105, 115.5, 'uploads/thumb_IMG-89.png', '-', 25, 3, 1),
(125, '7425602001898', NULL, 'CATARINA TRASERA ZM-250 520-38T\r\n', 165, 181.5, 'uploads/thumb_IMG-94.png', '-', 9, 3, 1),
(126, '7425602001782', NULL, 'CATARINA TRASERA ZM250L/KMF 520-36T\r\n', 175, 192.5, 'uploads/thumb_IMG-93.png', '-', 4, 3, 1),
(127, '7425602001843', NULL, 'SPROCKET TRASERO INVICTA 150 428-42T\r\n', 125, 137.5, 'uploads/thumb_IMG-88.png', '-', 15, 3, 1),
(128, '7425602001850', NULL, 'CATARINA TRASERA GS150R 428-42T\r\n', 110, 121, 'uploads/thumb_IMG-76.png', '-', 20, 3, 1),
(129, '7425602001812', NULL, 'SPROCKET TRASERO FZ16 428-40T\r\n', 95, 104.5, 'uploads/thumb_IMG-82.png', '-', 19, 3, 1),
(130, '7425602001799', NULL, 'SPROCKET TRASERO XCD/PLATINA100/CT100/DISCOVER100 428-42T\r\n', 115, 126.5, 'uploads/thumb_IMG-92.png', '-', 50, 3, 1),
(131, '7425602001713', NULL, 'CATARINA TRASERA EN125/GN125 428-45T\r\n', 110, 121, 'uploads/thumb_IMG-75.png', '-', 150, 3, 1),
(132, '7425602001805', NULL, 'CATARINA TRASERA PULSAR135 428-44T\r\n', 100, 110, 'uploads/thumb_IMG-90.png', '-', 23, 3, 1),
(133, '7425602001737', NULL, 'SPROCKET TRASERO YBR125  428-45T\r\n', 105, 115.5, 'uploads/thumb_IMG-95.png', '-', 30, 3, 1),
(134, '7425602001966', NULL, 'KIT DE BIELA DE MOTOR XM200\r\n', 320, 352, 'uploads/thumb_IMG-0.png', '-', 56, 3, 1),
(135, '7425602001751', NULL, 'CATARINA TRASERA YBR125 428-50T\r\n', 130, 143, 'uploads/thumb_IMG-85.png', '-', 36, 3, 1),
(136, '7425602001768', NULL, 'CATARINA TRASERA XTZ125/DT175 428-50T\r\n', 130, 143, 'uploads/thumb_IMG-85.png', '-', 23, 3, 1),
(137, '7425602004721', NULL, 'DISCOS DE CLUTCH YBR125 ED/G\r\n', 60, 66, 'uploads/thumb_IMG-58.png', '-', 120, 3, 1),
(138, '7425602004677', NULL, 'DISCOS DE CLUTCH CBF150/HUNK/HJ125-7\r\n', 70, 77, 'uploads/thumb_IMG-59.png', '-', 149, 3, 1),
(139, '7425602004660', NULL, 'DISCOS DE CLUTCH PULSAR 135/150/XCD125\r\n', 85, 93.5, 'uploads/thumb_IMG-60.png', '-', 61, 3, 1),
(140, '7425602004707', NULL, 'DISCOS DE CLUTCH PLATINA125/XCD125\r\n', 75, 82.5, 'uploads/thumb_IMG-61.png', '-', 75, 3, 1),
(141, '7425602004714', NULL, 'DISCOS DE CLUTCH XTZ125/CRUX110\r\n', 75, 82.5, 'uploads/thumb_IMG-62.png', '-', 147, 3, 1),
(142, '7425602004684', NULL, 'DISCOS DE CLUTCH FZ16\r\n', 150, 165, 'uploads/thumb_IMG-63.png', '-', 40, 3, 1),
(143, '7425602005742', NULL, 'DISCOS DE CLUTCH PULSAR180/200\r\n', 100, 110, 'uploads/thumb_IMG-64.png', '-', 71, 3, 1),
(144, '7425602000907', NULL, 'KIT DE TRACCION GXT200 428-47T/15T 428H-136\r\n', 290, 319, 'uploads/thumb_IMG-38.png', '-', 12, 3, 1),
(145, '7425602000921', NULL, 'KIT DE TRACCION YBR 125 428-45T/14T 428H-124\r\n', 275, 302.5, 'uploads/thumb_IMG-35.png', '-', 33, 3, 1),
(146, '7425602000914', NULL, 'KIT DE TRACCION HJ125 428-42T/15T 428H-124\r\n', 270, 297, 'uploads/thumb_IMG-37.png', '-', 31, 3, 1),
(147, '7425602000938', NULL, 'KIT DE TRACCION PULSAR 135 428-42T/14T 428H-124\r\n', 265, 291.5, 'uploads/thumb_IMG-36.png', '-', 8, 3, 1),
(148, '7425602005223', NULL, 'KIT DE BALANCINES PULSAR200 NS\r\n', 270, 297, 'uploads/thumb_IMG-24.png', '-', 38, 3, 1),
(149, '7425602005247', NULL, 'KIT DE BALANCINES DE ABAJO HJ125-7\r\n', 75, 82.5, 'uploads/thumb_IMG-25.png', '-', 97, 3, 1),
(150, '7425602005230', NULL, 'KIT DE BALANCINES YBR125 ED/G/CRUX110\r\n', 110, 121, 'uploads/thumb_IMG-26.png', '-', 65, 3, 1),
(151, '7425602005209', NULL, 'KIT DE BALANCINES FZ16\r\n', 250, 275, 'uploads/thumb_IMG-28.png', '-', 86, 3, 1),
(152, '7425602005216', NULL, 'KIT DE BALANCINES / GN125H\r\n', 100, 110, 'uploads/thumb_IMG-29.png', '-', 146, 3, 1),
(153, '7425602005186', NULL, 'VARILLA DE MOTOR CGL 125/HJ125-7\r\n', 50, 55, 'uploads/thumb_IMG-31.png', '-', 160, 3, 1),
(154, '7425602005469', NULL, 'VARILLAS DE MOTOR XM-200\r\n', 50, 55, 'uploads/thumb_IMG-31.png', '-', 360, 3, 1),
(155, '7425602005551', NULL, 'VALVULAS DE MOTOR GXT200\r\n', 150, 165, 'uploads/thumb_IMG-27.png', '-', 57, 3, 1),
(156, '7425602004868', NULL, 'VALVULAS DE MOTOR DISCOVER150/BOXER150\r\n', 115, 126.5, 'uploads/thumb_IMG-27.png', '-', 177, 3, 1),
(157, '7425602004875', NULL, 'VALVULAS DE MOTOR CBF 125 STUNNER\r\n', 150, 165, 'uploads/thumb_IMG-27.png', '-', 79, 3, 1),
(158, '7425602004882', NULL, 'VALVULAS DE MOTOR CBF150/HUNK\r\n', 135, 148.5, 'uploads/thumb_IMG-27.png', '-', 88, 3, 1),
(159, '7425602004899', NULL, 'VALVULAS DE MOTOR CG150\r\n', 120, 132, 'uploads/thumb_IMG-27.png', '-', 193, 3, 1),
(160, '7425602004905', NULL, 'VALVULAS DE MOTOR CRUX110\r\n', 100, 110, 'uploads/thumb_IMG-27.png', '-', 70, 3, 1),
(161, '7425602004936', NULL, 'VALVULAS DE MOTOR FZ16\r\n', 175, 192.5, 'uploads/thumb_IMG-27.png', '-', 180, 3, 1),
(162, '7425602004943', NULL, 'VALVULAS DE MOTOR GN125H\r\n', 140, 154, 'uploads/thumb_IMG-27.png', '-', 78, 3, 1),
(163, '7425602004967', NULL, 'VALVULAS DE MOTOR PLATINA125\r\n', 115, 126.5, 'uploads/thumb_IMG-27.png', '-', 173, 3, 1),
(164, '7425602004981', NULL, 'VALVULAS DE MOTOR PULSAR180\r\n', 135, 148.5, 'uploads/thumb_IMG-27.png', '-', 75, 3, 1),
(165, '7425602005001', NULL, 'VALVULAS DE MOTOR SUPER SPLENDOR/GLAMOUR\r\n', 125, 137.5, 'uploads/thumb_IMG-27.png', '-', 90, 3, 1),
(166, '7425602005018', NULL, 'VALVULAS DE MOTOR XL200\r\n', 120, 132, 'uploads/thumb_IMG-27.png', '-', 55, 3, 1),
(167, '7425602005025', NULL, 'VALVULAS DE MOTOR YBR125 ED/G/XTZ125\r\n', 100, 110, 'uploads/thumb_IMG-27.png', '-', 185, 3, 1),
(168, '7425602004912', NULL, 'VALVULAS DE MOTOR DISCOVER100/135\r\n', 130, 143, 'uploads/thumb_IMG-27.png', '-', 76, 3, 1),
(169, '7425602005346', NULL, 'CADENA DE TIEMPO CRUX110(90L)\r\n', 75, 82.5, 'uploads/thumb_IMG-22.png', '-', 82, 3, 1),
(170, '7425602005353', NULL, 'CADENA DE TIEMPO DISCOVER125/DISCOVER 135/PULSAR135( 94L)\r\n', 75, 82.5, 'uploads/thumb_IMG-22.png', '-', 66, 3, 1),
(171, '7425602005360', NULL, 'CADENA DE TIEMPO FZ16(96L)\r\n', 95, 104.5, 'uploads/thumb_IMG-22.png', '-', 202, 3, 1),
(172, '7425602005384', NULL, 'CADENA DE TIEMPO PLATINA125/XCD125(92L)\r\n', 75, 82.5, 'uploads/thumb_IMG-22.png', '-', 96, 3, 1),
(173, '7425602005391', NULL, 'CADENA DE TIEMPO PULSAR180(98L)\r\n', 75, 82.5, 'uploads/thumb_IMG-22.png', '-', 89, 3, 1),
(174, '7425602005407', NULL, 'CADENA DE TIEMPO YBR125 ED/G(88L)\r\n', 70, 77, 'uploads/thumb_IMG-22.png', '-', 71, 3, 1),
(175, '7425602005308', NULL, 'TENSOR CADENA DE TIEMPO PULSAR 200/220/ PULSAR135 LS/PULSAR UG4/XCD135\r\n', 100, 110, 'uploads/thumb_IMG-81.png', '-', 174, 3, 1),
(176, '7425602005315', NULL, 'TENSOR CADENA DE TIEMPO YBR125 ED/G/XTZ125\r\n', 90, 99, 'uploads/thumb_IMG-80.png', '-', 175, 3, 1),
(177, '7425602005292', NULL, 'TENSOR CADENA DE TIEMPO GXT200\r\n', 100, 110, 'uploads/thumb_IMG-35.png', '-', 151, 3, 1),
(178, '7425602000860', NULL, 'CADENA DE TRACCION 428H-136L\r\n', 120, 132, 'uploads/thumb_IMG-57.png', '-', 45, 3, 1),
(179, '7425602000846', NULL, 'CADENA DE TRACCION 428H-136L DORADA\r\n', 170, 187, 'uploads/thumb_IMG-57.png', '-', 219, 3, 1),
(180, '7425602000549', NULL, 'BASE DE CATARINA XM-200/SHINEVAY200/BYQ\r\n', 190, 209, 'uploads/thumb_IMG-55.png', '-', 76, 3, 1),
(181, '7425602000532', NULL, 'BASE DE CATARINA YBR/FT-125/BOA/CORAL/COBRA/CYUX\r\n', 160, 176, 'uploads/thumb_IMG-34.png', '-', 92, 3, 1),
(182, '7425602005759', NULL, 'SELLOS DE VALVULA XCD/PULSR135/PULSAR180/DISCOVER125/135\r\n', 12, 13.2, 'uploads/thumb_IMG-33.png', '-', 894, 3, 1),
(183, '7425602004547', NULL, 'KIT DE EMPAQUES DE MOTOR GXT200\r\n', 80, 88, 'uploads/thumb_IMG-41.png', '-', 112, 3, 1),
(184, '7425602005131', NULL, 'EJE DE LEVAS GXT200\r\n', 250, 275, 'uploads/thumb_IMG-73.png', '-', 68, 3, 1),
(185, '7425602005162', NULL, 'EJE DE LEVAS XL200\r\n', 250, 275, 'uploads/thumb_IMG-67.png', '-', 26, 3, 1),
(186, '7425602005117', NULL, 'EJE DE LEVAS FZ16\r\n', 350, 385, 'uploads/thumb_IMG-68.png', '-', 56, 3, 1),
(187, '7425602005155', NULL, 'EJE DE LEVAS PULSAR135\r\n', 240, 264, 'uploads/thumb_IMG-78.png', '-', 44, 3, 1),
(188, '7425602005179', NULL, 'EJE DE LEVAS YBR125 ED/G\r\n', 265, 291.5, 'uploads/thumb_IMG-79.png', '-', 34, 3, 1),
(189, '7425602001430', NULL, 'ESPEJOS GXT200\r\n', 80, 88, 'uploads/thumb_IMG-66.png', '-', 138, 3, 1),
(190, '7425602005056', NULL, 'CIGÜEÑAL HJ125-7\r\n', 850, 935, 'uploads/thumb_IMG-43.png', '-', 4, 3, 1),
(191, '7425602005049', NULL, 'CIGÜEÑAL GXT200\r\n', 1300, 1430, 'uploads/thumb_IMG-74.png', '-', 18, 3, 1),
(192, '7425602003465', NULL, 'KIT DE PISTON AX100 0.25\r\n', 190, 209, 'uploads/thumb_IMG-19.png', '-', 28, 3, 1),
(193, '7425602003229', NULL, 'KIT DE PISTON AX100 STD\r\n', 190, 209, 'uploads/thumb_IMG-19.png', '-', 27, 3, 1),
(194, '7425602003236', NULL, 'KIT DE PISTON BOXER BM150 STD\r\n', 225, 247.5, 'uploads/thumb_IMG-20.png', '-', 26, 3, 1),
(195, '7425602003137', NULL, 'KIT DE PISTON BOA 160 STD\r\n', 250, 275, 'uploads/thumb_IMG-20.png', '-', 18, 3, 1),
(196, '7425602003243', NULL, 'KIT DE PISTON CBF125 STUNNER STD\r\n', 200, 220, 'uploads/thumb_IMG-20.png', '-', 60, 3, 1),
(197, '7425602003267', NULL, 'KIT DE PISTON CG150 STD\r\n', 200, 220, 'uploads/thumb_IMG-20.png', '-', 37, 3, 1),
(198, '7425602003274', NULL, 'KIT DE PISTON CRUX110 STD\r\n', 200, 220, 'uploads/thumb_IMG-21.png', '-', 28, 3, 1),
(199, '7425602003281', NULL, 'KIT DE PISTON CT100 STD\r\n', 200, 220, 'uploads/thumb_IMG-21.png', '-', 48, 3, 1),
(200, '742560200969', NULL, 'KIT DE PISTON DISCOVER 150 STD\r\n', 250, 275, 'uploads/thumb_IMG-20.png', '-', 46, 3, 1),
(201, '7425602003120', NULL, 'KIT DE PISTON DM150 STD(ENGINE BLACK)\r\n', 215, 236.5, 'uploads/thumb_IMG-20.png', '-', 21, 3, 1),
(202, '7425602003335', NULL, 'KIT DE PISTON FZ16 STD\r\n', 225, 247.5, 'uploads/thumb_IMG-21.png', '-', 40, 3, 1),
(203, '7425602003359', NULL, 'KIT DE PISTON GXT200 STD\r\n', 275, 302.5, 'uploads/thumb_IMG-20.png', '-', 33, 3, 1),
(204, '7425602003366', NULL, 'KIT DE PISTON HJ125-7 STD\r\n', 185, 203.5, 'uploads/thumb_IMG-20.png', '-', 44, 3, 1),
(205, '7425602003373', NULL, 'KIT DE PISTON PLATINA125 STD\r\n', 220, 242, 'uploads/thumb_IMG-21.png', '-', 41, 3, 1),
(206, '7425602003380', NULL, 'KIT DE PISTON PULSAR135 STD\r\n', 215, 236.5, 'uploads/thumb_IMG-21.png', '-', 31, 3, 1),
(207, '7425602003410', NULL, 'KIT DE PISTON SUPER PLENDOR/GLAMOUR STD\r\n', 200, 220, 'uploads/thumb_IMG-20.png', '-', 50, 3, 1),
(208, '7425602003113', NULL, 'KIT DE PISTON XCD125 STD\r\n', 200, 220, 'uploads/thumb_IMG-20.png', '-', 48, 3, 1),
(209, '7425602003434', NULL, 'KIT DE PISTON XM200 STD\r\n', 265, 291.5, 'uploads/thumb_IMG-20.png', '-', 41, 3, 1),
(210, '7425602003441', NULL, 'KIT DE PISTON YBR125 ED/G STD\r\n', 190, 209, 'uploads/thumb_IMG-20.png', '-', 14, 3, 1),
(211, '7425602003458', NULL, 'KIT DE PISTON ZM-200L STD\r\n', 250, 275, 'uploads/thumb_IMG-20.png', '-', 5, 3, 1),
(212, '7425602001294', NULL, 'BALINERAS DE BUFA 6202 2RS\r\n', 19, 20.9, 'uploads/thumb_IMG-6.png', '-', 0, 3, 1),
(213, '7425602001287', NULL, 'BALINERAS DE BUFA 6004 2RS\r\n', 25, 27.5, 'uploads/thumb_IMG-6.png', '-', 120, 3, 1),
(214, '7425602001317', NULL, 'BALINERAS DE BUFA 6301 2RS\r\n', 22, 24.2, 'uploads/thumb_IMG-6.png', '-', 200, 3, 1),
(215, '7425602001324', NULL, 'BALINERAS DE BUFA 6302 2RS\r\n', 24, 26.4, 'uploads/thumb_IMG-6.png', '-', 220, 3, 1),
(216, '7425602001300', NULL, 'BALINERAS DE BUFA 6204 2RS\r\n', 28, 30.8, 'uploads/thumb_IMG-6.png', '-', 170, 3, 1),
(217, '7425602004585', NULL, 'BALINERA DE MOTOR 63/28\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 72, 3, 1),
(218, '7425602001690', NULL, 'SPROCKET DELANTERO GXT200/KMF 428-15T\r\n', 30, 33, 'uploads/thumb_IMG-9.png', '-', 88, 3, 1),
(219, '7425602001638', NULL, 'SPROCKET DELANTERO HUNK/SERPENTO125/150  428-15T\r\n', 30, 33, 'uploads/thumb_IMG-10.png', '-', 125, 3, 1),
(220, '7425602001645', NULL, 'SPROCKET DELANTERO CT100/PLATINA125/PULSAR135/XCD/DISCOVER150 428-15T\r\n', 30, 33, 'uploads/thumb_IMG-11.png', '-', 282, 3, 1),
(221, '7425602001706', NULL, 'SPROCKET DELANTERO EN125/GN125 428-14T\r\n', 30, 33, 'uploads/thumb_IMG-12.png', '-', 112, 3, 1),
(222, '7425602001669', NULL, 'SPROCKET DELANTERO XTZ/YBR 428-14T\r\n', 28, 30.8, 'uploads/thumb_IMG-13.png', '-', 161, 3, 1),
(223, '7425602001836', NULL, 'SPROCKET DELANTERO ZM 200L 520-13T\r\n', 35, 38.5, 'uploads/thumb_IMG-14.png', '-', 131, 3, 1),
(224, '7425602001652', NULL, 'SPROCKET DELANTERO CG125 428-15T\r\n', 28, 30.8, 'uploads/thumb_IMG-15.png', '-', 201, 3, 1),
(225, '7425602001683', NULL, 'SPROCKET DELANTERO GXT200/KMF 520-12T\r\n', 35, 38.5, 'uploads/thumb_IMG-16.png', '-', 97, 3, 1),
(226, '7425602000150', NULL, 'BOBINA DE MAGNETO PULSAR135\r\n', 350, 385, 'uploads/thumb_IMG-26.png', '-', 25, 3, 1),
(227, '7425602000099', NULL, 'BOBINA DE MAGNETO GXT200/KMF 200\r\n', 375, 412.5, 'uploads/thumb_IMG-0.png', '-', 58, 3, 1),
(228, '7425602000167', NULL, 'BOBINA DE MAGNETO YBR125 ED/G/XTZ125\r\n', 350, 385, 'uploads/thumb_IMG-23.png', '-', 114, 3, 1),
(229, '7425602000082', NULL, 'BOBINA DE MAGNETO CG150/200/HJ125-7\r\n', 240, 264, 'uploads/thumb_IMG-24.png', '-', 112, 3, 1),
(230, '7425602000105', NULL, 'BOBINA DE MAGNETO YUMBO 200\r\n', 275, 302.5, 'uploads/thumb_IMG-25.png', '-', 60, 3, 1),
(231, '7425602004813', NULL, 'BENDIX GXT200/KMF200/XM200/DM200\r\n', 180, 198, 'uploads/thumb_IMG-7.png', '-', 148, 3, 1),
(232, '7425602004844', NULL, 'BENDIX PULSAR135\r\n', 250, 275, 'uploads/thumb_IMG-8.png', '-', 32, 3, 1),
(233, '7425602000464', NULL, 'KIT DE RAYOS TRASEROS GXT/KMF - 200\r\n', 90, 99, 'uploads/thumb_IMG-17.png', '-', 405, 3, 1),
(234, '7425602000594', NULL, 'MOTOR DE ARRANQUE GY6\r\n', 325, 357.5, 'uploads/thumb_IMG-21.png', '-', 15, 3, 1),
(235, '7425602000600', NULL, 'MOTOR DE ARRANQUE CG 125\r\n', 370, 407, 'uploads/thumb_IMG-28.png', '-', 13, 3, 1),
(236, '7425602000617', NULL, 'MOTOR DE ARRANQUE CBF125 STUNNER/CBF150/HUNK\r\n', 600, 660, 'uploads/thumb_IMG-20.png', '-', 14, 3, 1),
(237, '7425602000624', NULL, 'MOTOR DE ARRANQUE FZ16\r\n', 650, 715, 'uploads/thumb_IMG-18.png', '-', 11, 3, 1),
(238, '7425602000655', NULL, 'MOTOR DE ARRANQUE XCD125/PULSAR125 LS/DISCOVER125 ST\r\n', 700, 770, 'uploads/thumb_IMG-19.png', '-', 17, 3, 1),
(239, '7425602000662', NULL, 'MOTOR DE ARRANQUE YBR125 ED/G\r\n', 500, 550, 'uploads/thumb_IMG-30.png', '-', 6, 3, 1),
(240, '7425602000570', NULL, 'BUFA DE FRENO TRASERA KB 150/MAGNUM 150/CG\r\n', 450, 495, 'uploads/thumb_IMG-29.png', '-', 14, 3, 1),
(241, '7425602000563', NULL, 'BUFA TRASERA GXT200\r\n', 700, 770, 'uploads/thumb_IMG-27.png', '-', 30, 3, 1),
(242, '7425602000488', NULL, 'MONOSHOCK XM200/JARA200/KMF150L/UNIVERSAL\r\n', 850, 935, 'uploads/thumb_IMG-33.png', '-', 3, 3, 1),
(243, '7425602004615', NULL, 'BOMBA DE ACIETE HJ125-7\r\n', 100, 110, 'uploads/thumb_IMG-38.png', '-', 40, 3, 1),
(244, '7425602004622', NULL, 'BOMBA DE ACIETE YBR125 ED/G/XTZ125\r\n', 125, 137.5, 'uploads/thumb_IMG-36.png', '-', 56, 3, 1),
(245, '7425602001522', NULL, 'HULE DESLIZADOR DE CADENA GXT200\r\n', 75, 82.5, 'uploads/thumb_IMG-39.png', '-', 8, 3, 1),
(246, '7425602004745', NULL, 'PORTADISCO CRUX110\r\n', 180, 198, 'uploads/thumb_IMG-41.png', '-', 19, 3, 1),
(247, '7425602005414', NULL, 'CAJA DE CAMBIO CTA./ CBF150\r\n', 1400, 1540, 'uploads/thumb_IMG-40.png', '-', 7, 3, 1),
(248, '7425602005445', NULL, 'CAJA DE CAMBIO CTA. YBR125 ED/G\r\n', 800, 880, 'uploads/thumb_IMG-42.png', '-', 6, 3, 1),
(249, '7425602001614', NULL, 'VIAS GXT200\r\n', 75, 82.5, 'uploads/thumb_IMG-45.png', '-', 100, 3, 1),
(250, '7425602001539', NULL, 'STOP DE FRENO GXT200 / KMF - 200\r\n', 100, 110, 'uploads/thumb_IMG-46.png', '-', 30, 3, 1),
(251, '7425602001515', NULL, 'FOCO DELANTERO GXT200\r\n', 200, 220, 'uploads/thumb_IMG-47.png', '-', 36, 3, 1),
(252, '7425602004646', NULL, 'TRIANGULO DE CARTER GN125H/GXT200\r\n', 45, 49.5, 'uploads/thumb_IMG-48.png', '-', 155, 3, 1),
(253, '7425602001447', NULL, 'TORQUE DE CARBURADOR GXT200\r\n', 150, 165, 'uploads/thumb_IMG-49.png', '-', 170, 3, 1),
(254, '7425602001546', NULL, 'GUIA DE CADENA TRASERA GXT200\r\n', 55, 60.5, 'uploads/thumb_IMG-50.png', '-', 40, 3, 1),
(255, '7425602004639', NULL, 'TAPON MEDIDOR DE ACEITE HJ125-7/CG (UNIVERSAL)\r\n', 10, 11, 'uploads/thumb_IMG-60.png', '-', 180, 3, 1),
(256, '7425602001171', NULL, 'CABLE DE CLUTCH BOA/CG-150/FT-150\r\n', 35, 38.5, 'uploads/thumb_IMG-67.png', '-', 269, 3, 1),
(257, '7425602001188', NULL, 'CABLE DE CLUTCH DAKAR200/XL200\r\n', 30, 33, 'uploads/thumb_IMG-67.png', '-', 271, 3, 1),
(258, '7425602001195', NULL, 'CABLE DE CLUTCH FT125/MAGNUM-150/KB-150\r\n', 28, 30.8, 'uploads/thumb_IMG-67.png', '-', 269, 3, 1),
(259, '7425602001256', NULL, 'CABLE DE VELOCIMETRO BOA/ FT-150/TX-200\r\n', 30, 33, 'uploads/thumb_IMG-67.png', '-', 189, 3, 1),
(260, '7425602001263', NULL, 'CABLE DE VELOCIMETRO UM/DM/JARA/XM200\r\n', 35, 38.5, 'uploads/thumb_IMG-67.png', '-', 226, 3, 1),
(261, '7425602001270', NULL, 'CABLE DE ACELERADOR UM/JARA/XM2OO\r\n', 25, 27.5, 'uploads/thumb_IMG-67.png', '-', 220, 3, 1),
(262, '7425602001249', NULL, 'CABLE DE ACELERADOR FT125/150\r\n', 27, 29.7, 'uploads/thumb_IMG-67.png', '-', 239, 3, 1),
(263, '7425602004592', NULL, 'CARBURADOR HJ125-7\r\n', 300, 330, 'uploads/thumb_IMG-74.png', '-', 6, 3, 1),
(264, '7425602000686', NULL, 'JUEGO DE CARBONES GN125H\r\n', 35, 38.5, 'uploads/thumb_IMG-76.png', '-', 120, 3, 1),
(265, '7425602004806', NULL, 'KIT DE GUIA DE CADENA TIEMPO GN125H/GXT200 \r\n', 95, 104.5, 'uploads/thumb_IMG-79.png', '-', 152, 3, 1),
(266, '7425602000440', NULL, 'BOMBA DE FRENO SUPERIOR GXT200/UNIVERSAL (METALICA)\r\n', 200, 220, 'uploads/thumb_IMG-51.png', '-', 51, 3, 1),
(267, '7425602000761', NULL, 'PATA DE ARRANQUE YBR 125\r\n', 120, 132, 'uploads/thumb_IMG-55.png', '-', 85, 3, 1),
(268, '7425602000747', NULL, 'PATA DE ARRANQUE CGL125\r\n', 105, 115.5, 'uploads/thumb_IMG-52.png', '-', 32, 3, 1),
(269, '7425602000723', NULL, 'PATA DE ARRANQUE CG\r\n', 110, 121, 'uploads/thumb_IMG-53.png', '-', 15, 3, 1),
(270, '7425602000716', NULL, 'PATA DE ARRANQUE DT175\r\n', 125, 137.5, 'uploads/thumb_IMG-56.png', '-', 15, 3, 1),
(271, '7425602000778', NULL, 'PATA DE ARRANQUE AX100\r\n', 105, 115.5, 'uploads/thumb_IMG-54.png', '-', 12, 3, 1),
(272, '7425602000754', NULL, 'PATA DE ARRANQUE CG125/FT125\r\n', 110, 121, 'uploads/thumb_IMG-57.png', '-', 20, 3, 1),
(273, '7425602000709', NULL, 'PATA DE ARRANQUE GXT200/KMF200\r\n', 125, 137.5, 'uploads/thumb_IMG-58.png', '-', 73, 3, 1),
(274, '7425602000730', NULL, 'PATA DE ARRANQUE GYA200/XM200\r\n', 125, 137.5, 'uploads/thumb_IMG-59.png', '-', 163, 3, 1),
(275, '7425602000037', NULL, 'PATA DE CAMBIO HJ125-7/DOBLE\r\n', 120, 132, 'uploads/thumb_IMG-66.png', '-', 20, 3, 1),
(276, '7425602000051', NULL, 'PATA DE CAMBIO CG DOBLE\r\n', 60, 66, 'uploads/thumb_IMG-64.png', '-', 40, 3, 1),
(277, '7425602000013', NULL, 'PATA DE CAMBIO HJ125-7/SIMPLE\r\n', 125, 137.5, 'uploads/thumb_IMG-61.png', '-', 25, 3, 1),
(278, '7425602000044', NULL, 'PATA DE CAMBIO YBR 125\r\n', 50, 55, 'uploads/thumb_IMG-62.png', '-', 23, 3, 1),
(279, '7425602000020', NULL, 'PATA DE CAMBIO CG125/CG200\r\n', 45, 49.5, 'uploads/thumb_IMG-63.png', '-', 86, 3, 1),
(280, '7425602000365', NULL, 'REGULADOR DE VOLTAJE YBR125\r\n', 120, 132, 'uploads/thumb_IMG-114.png', '-', 57, 3, 1),
(281, '7425602000341', NULL, 'REGULADOR DE VOLTAJE CG125/CG150 4WINES ?MACHO?\r\n', 100, 110, 'uploads/thumb_IMG-97.png', '-', 58, 3, 1),
(282, '7425602000372', NULL, 'REGULADOR DE VOLTAJE GN125H\r\n', 150, 165, 'uploads/thumb_IMG-98.png', '-', 28, 3, 1),
(283, '7425602000426', NULL, 'REGULADOR DE VOLTAJE FZ16 \r\n', 150, 165, 'uploads/thumb_IMG-120.png', '-', 6, 3, 1),
(284, '7425602000389', NULL, 'REGULADOR DE VOLTAJE GY6 (SCOOTER)\r\n', 100, 110, 'uploads/thumb_IMG-119.png', '-', 29, 3, 1),
(285, '7425602000396', NULL, 'REGULADOR DE VOLTAJE CG WITH CONDITIONAL/DAKAR200/XM-200\r\n', 120, 132, 'uploads/thumb_IMG-119.png', '-', 18, 3, 1),
(286, '7425602000358', NULL, 'REGULADOR DE VOLTAJE CG125/CG150 4WINES ?HEMBRA?\r\n', 100, 110, 'uploads/thumb_IMG-115.png', '-', 56, 3, 1),
(287, '7425602000402', NULL, 'REGULADOR DE VOLTAJE GXT200/KMF200\r\n', 150, 165, 'uploads/thumb_IMG-121.png', '-', 73, 3, 1),
(288, '7425602000419', NULL, 'REGULADOR DE VOLTAJE CG150\r\n', 95, 104.5, 'uploads/thumb_IMG-111.png', '-', 45, 3, 1),
(289, '7425602000334', NULL, 'REGULADOR DE VOLTAJE YUMBO 200\r\n', 75, 82.5, 'uploads/thumb_IMG-104.png', '-', 69, 3, 1),
(290, '7425602000785', NULL, 'CHICHARRA DE ENCENDIDO CG\r\n', 80, 88, 'uploads/thumb_IMG-102.png', '-', 89, 3, 1),
(291, '7425602000808', NULL, 'CHICARRA DE ENCENDIDO GY6\r\n', 85, 93.5, 'uploads/thumb_IMG-107.png', '-', 99, 3, 1),
(292, '7425602000792', NULL, 'CHICARRA DE ENCENDIDO YBR 125\r\n', 95, 104.5, 'uploads/thumb_IMG-106.png', '-', 77, 3, 1),
(293, '7425602000839', NULL, 'CHICHARRA DE ENCENDIDO HJ125-7\r\n', 80, 88, 'uploads/thumb_IMG-103.png', '-', 95, 3, 1),
(294, '7425602000815', NULL, 'CHICHARRA DE ENCENDIDO GN125H\r\n', 95, 104.5, 'uploads/thumb_IMG-112.png', '-', 49, 3, 1),
(295, '7425602000822', NULL, 'CHICHARRA DE ENCENDIDO GXT200\r\n', 95, 104.5, 'uploads/thumb_IMG-105.png', '-', 96, 3, 1),
(296, '7425602000174', NULL, 'BOBINA DE ALTA TENSION GY6\r\n', 85, 93.5, 'uploads/thumb_IMG-113.png', '-', 47, 3, 1),
(297, '7425602000204', NULL, 'BOBINA DE ALTA TENSION HJ125-7\r\n', 85, 93.5, 'uploads/thumb_IMG-118.png', '-', 24, 3, 1),
(298, '7425602000181', NULL, 'BOBINA DE ALTA TENSION GN125H\r\n', 120, 132, 'uploads/thumb_IMG-110.png', '-', 125, 3, 1),
(299, '7425602000235', NULL, 'CDI PULSAR135\r\n', 300, 330, 'uploads/thumb_IMG-99.png', '-', 34, 3, 1),
(300, '7425602000259', NULL, 'CDI XTZ125 2009-2015\r\n', 600, 660, 'uploads/thumb_IMG-100.png', '-', 46, 3, 1),
(301, '7425602000273', NULL, 'CDI GY6 (SCOOTER)\r\n', 65, 71.5, 'uploads/thumb_IMG-101.png', '-', 38, 3, 1),
(302, '7425602000280', NULL, 'CDI CG/HJ125-7\r\n', 70, 77, 'uploads/thumb_IMG-108.png', '-', 37, 3, 1),
(303, '7425602000297', NULL, 'CDI CRUX110\r\n', 350, 385, 'uploads/thumb_IMG-109.png', '-', 51, 3, 1),
(304, '7425602000310', NULL, 'CDI UNIT GXT250\r\n', 400, 440, 'uploads/thumb_IMG-116.png', '-', 69, 3, 1),
(305, '7425602000303', NULL, 'CDI UNIT GXT200\r\n', 275, 302.5, 'uploads/thumb_IMG-117.png', '-', 68, 3, 1),
(306, '7425602000228', NULL, 'CDI PLATINA125\r\n', 350, 385, 'uploads/thumb_IMG-22.png', '-', 36, 3, 1),
(307, '560271307', NULL, 'RIN 1.85X17\r\n', 260, 286, 'uploads/thumb_IMG-82.png', '-', 45, 3, 1),
(308, '560271310', NULL, 'RIN 1.85X18\r\n', 260, 286, 'uploads/thumb_IMG-81.png', '-', 95, 3, 1),
(309, '560271311', NULL, 'RIN 1.60X21\r\n', 250, 275, 'uploads/thumb_IMG-84.png', '-', 20, 3, 1),
(310, '560271313', NULL, 'RIN 2.15X17 ', 275, 302.5, 'uploads/thumb_IMG-83.png', '-', 20, 3, 1),
(311, 'FTHJAZ', NULL, 'TANQUE DE COMBUSTIBLE(AZUL) HJ-125\r\n', 700, 770, 'uploads/thumb_IMG-73.png', '-', 6, 3, 1),
(312, 'FTHJNE', NULL, 'TANQUE DE COMBUSTIBLE(NEGRO) HJ-125\r\n', 700, 770, 'uploads/thumb_IMG-71.png', '-', 0, 3, 1),
(313, 'FTHJRO', NULL, 'TANQUE DE COMBUSTIBLE(ROJO) HJ-125\r\n', 700, 770, 'uploads/thumb_IMG-72.png', '-', 5, 3, 1),
(314, 'FTYBRAZ', NULL, 'TANQUE DE COMBUSTIBLE(AZUL) YBR 125\r\n', 750, 825, 'uploads/thumb_IMG-70.png', '-', 0, 3, 1),
(315, 'FTYBRNE', NULL, 'TANQUE DE COMBUSTIBLE(NEGRO) YBR 125\r\n', 750, 825, 'uploads/thumb_IMG-68.png', '-', 0, 3, 1),
(316, 'FTYBRRO', NULL, 'TANQUE DE COMBUSTIBLE(ROJO) YBR 125\r\n', 750, 825, 'uploads/thumb_IMG-69.png', '-', 0, 3, 1),
(317, '206160', NULL, 'DIAFRAGMA CARBURADOR BAJAJ PULSAR 180/200 - TVS 160RTR', 95, 104.5, 'uploads/thumb_IMG-55.png', '-', 6, 4, 2),
(318, '206192', NULL, 'DIAFRAGMA CARBURADOR DISCOVER 125 - 135\r\n', 110, 121, 'uploads/thumb_IMG-56.png', '-', 8, 4, 2),
(319, '206194', NULL, 'DIAFRAGMA CARBURADOR YAMAHA FZ 16\r\n', 100, 110, 'uploads/thumb_IMG-55.png', '-', 0, 4, 2),
(320, '206195', NULL, 'DIAFRAGMA CARBURADOR HONDA CBF 125\r\n', 130, 143, 'uploads/thumb_IMG-55.png', '-', 8, 4, 2),
(321, '206218', NULL, 'DIAFRAGMA CARBURADOR BAJAJ PULSAR 135 - DISCOVER 125ST\r\n', 110, 121, 'uploads/thumb_IMG-55.png', '-', 8, 3, 2),
(322, '101423', NULL, 'SEGURO DE SPROCKET BOXER CAL.2mm\r\n', 15, 16.5, 'uploads/thumb_IMG-59.png', '-', 10, 4, 2),
(323, '101645', NULL, 'SEGURO DE SPROCKET YBR(UNIVERSAL)', 45, 49.5, 'uploads/thumb_IMG-11.png', '-', 900, 4, 2),
(324, '7425602000853', NULL, 'CADENA DE TRACCION 428H-124L DORADA\r\n', 140, 154, 'uploads/thumb_IMG-40.png', '-', 103, 3, 1),
(325, 'KCA32', NULL, 'KIT COMPLETO DE EMPAQUES DISCOVER 125 ST M.N.\r\n', 65, 71.5, 'uploads/thumb_IMG-143.png', '-', 10, 4, 2),
(326, 'KCS32', NULL, 'KIT COMPLETO DE EMPAQUES AX 100-4 \r\n', 75, 82.5, 'uploads/thumb_IMG-142.png', '-', 10, 4, 2),
(327, 'KCA36', NULL, 'KIT COMPLETO  DE EMPAQUES MOTOTAXI RE 205\r\n', 90, 99, 'uploads/thumb_IMG-140.png', '-', 5, 4, 2),
(328, '210345', NULL, 'KIT COMPLETO DE EMPAQUES BOXER 100\r\n', 65, 71.5, 'uploads/thumb_IMG-141.png', '-', 10, 4, 2),
(329, 'KCH48', NULL, 'KIT COMPLETO DE EMPAQUES SPLENDOR NXG\r\n', 110, 121, 'uploads/thumb_IMG-146.png', '-', 6, 4, 2),
(330, '210349', NULL, 'KIT COMPLETO DE EMPAQUES PULSAR 180 SIN TAPA VALVULA\r\n', 85, 93.5, 'uploads/thumb_IMG-130.png', '-', 6, 4, 2),
(331, '210049', NULL, 'KIT COMPLETO DE EMPAQUES XL 200\r\n', 90, 99, 'uploads/thumb_IMG-132.png', '-', 17, 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `replacements_models`
--

CREATE TABLE `replacements_models` (
  `replacement_model` int(11) NOT NULL,
  `replacement_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `replacements_models`
--

INSERT INTO `replacements_models` (`replacement_model`, `replacement_id`, `model_id`) VALUES
(170, 76, 104),
(171, 77, 105),
(172, 77, 106),
(173, 77, 107),
(175, 78, 108),
(176, 78, 109),
(177, 78, 110),
(178, 78, 111),
(181, 80, 113),
(182, 80, 114),
(188, 86, 118),
(196, 92, 125),
(197, 93, 107),
(198, 94, 104),
(199, 94, 127),
(200, 94, 116),
(201, 95, 128),
(202, 95, 129),
(203, 96, 130),
(206, 97, 131),
(207, 98, 132),
(208, 99, 122),
(209, 100, 108),
(210, 101, 123),
(213, 102, 131),
(217, 104, 125),
(218, 105, 129),
(219, 106, 109),
(220, 107, 112),
(221, 108, 134),
(222, 108, 113),
(223, 108, 114),
(224, 109, 104),
(225, 110, 116),
(226, 110, 127),
(227, 111, 111),
(228, 112, 135),
(229, 112, 115),
(230, 113, 136),
(231, 114, 130),
(238, 116, 118),
(260, 123, 131),
(261, 124, 110),
(262, 125, 147),
(263, 126, 148),
(264, 127, 149),
(267, 130, 151),
(268, 130, 105),
(269, 130, 107),
(270, 130, 152),
(273, 131, 153),
(274, 131, 143),
(275, 132, 104),
(276, 133, 108),
(277, 134, 122),
(279, 135, 108),
(303, 148, 118),
(304, 149, 115),
(305, 150, 108),
(306, 150, 125),
(307, 151, 129),
(308, 152, 157),
(309, 153, 135),
(310, 153, 115),
(311, 154, 122),
(312, 155, 131),
(313, 156, 158),
(314, 156, 159),
(315, 157, 112),
(316, 158, 114),
(317, 158, 113),
(318, 159, 111),
(319, 160, 125),
(320, 161, 129),
(321, 162, 157),
(322, 163, 116),
(323, 164, 117),
(324, 165, 119),
(325, 165, 120),
(326, 166, 132),
(327, 167, 108),
(328, 167, 133),
(329, 168, 128),
(330, 168, 152),
(331, 169, 125),
(332, 170, 126),
(333, 170, 128),
(334, 170, 104),
(335, 171, 129),
(336, 172, 116),
(337, 172, 155),
(338, 173, 117),
(339, 174, 108),
(341, 176, 108),
(342, 176, 133),
(343, 175, 156),
(344, 175, 160),
(345, 175, 161),
(346, 175, 162),
(347, 175, 163),
(348, 177, 131),
(349, 178, 164),
(350, 179, 164),
(351, 180, 122),
(352, 180, 138),
(353, 180, 165),
(354, 181, 108),
(355, 181, 166),
(356, 181, 144),
(357, 181, 167),
(358, 181, 145),
(359, 181, 125),
(365, 183, 131),
(366, 182, 151),
(367, 182, 104),
(368, 182, 117),
(369, 182, 126),
(370, 182, 128),
(371, 184, 131),
(372, 185, 132),
(373, 186, 129),
(374, 187, 104),
(375, 188, 108),
(376, 189, 131),
(377, 190, 115),
(378, 191, 131),
(379, 192, 106),
(380, 193, 106),
(381, 194, 168),
(383, 196, 112),
(384, 197, 111),
(385, 198, 125),
(386, 199, 107),
(387, 200, 158),
(389, 201, 130),
(390, 202, 129),
(391, 203, 131),
(393, 205, 116),
(395, 207, 119),
(396, 207, 120),
(397, 208, 155),
(398, 206, 104),
(399, 204, 115),
(400, 195, 169),
(401, 209, 122),
(402, 210, 108),
(403, 211, 123),
(404, 212, 164),
(405, 213, 164),
(406, 214, 164),
(407, 215, 164),
(408, 216, 164),
(409, 217, 164),
(411, 219, 113),
(412, 219, 170),
(413, 219, 171),
(414, 220, 107),
(415, 220, 116),
(416, 220, 104),
(417, 220, 151),
(418, 220, 158),
(419, 221, 153),
(420, 221, 143),
(421, 222, 133),
(422, 222, 108),
(423, 223, 123),
(424, 224, 110),
(427, 218, 131),
(428, 218, 172),
(429, 225, 131),
(430, 225, 172),
(431, 226, 104),
(432, 227, 131),
(433, 227, 172),
(434, 228, 108),
(435, 228, 133),
(438, 229, 111),
(439, 229, 173),
(440, 229, 115),
(441, 230, 121),
(442, 231, 131),
(443, 231, 172),
(444, 231, 122),
(445, 231, 174),
(446, 232, 104),
(447, 233, 131),
(448, 233, 172),
(449, 234, 142),
(450, 235, 110),
(451, 236, 112),
(452, 236, 114),
(453, 236, 113),
(454, 237, 129),
(455, 238, 155),
(456, 238, 176),
(457, 238, 127),
(458, 239, 108),
(459, 240, 177),
(460, 240, 103),
(461, 240, 178),
(462, 241, 131),
(463, 242, 179),
(464, 242, 122),
(465, 242, 164),
(466, 242, 180),
(467, 243, 115),
(468, 244, 108),
(469, 244, 133),
(470, 245, 131),
(471, 246, 125),
(472, 247, 114),
(473, 248, 108),
(474, 249, 131),
(475, 250, 131),
(476, 250, 172),
(477, 251, 131),
(478, 252, 157),
(479, 252, 131),
(480, 253, 131),
(481, 254, 131),
(482, 255, 115),
(483, 255, 164),
(484, 256, 144),
(485, 256, 111),
(486, 256, 181),
(487, 257, 121),
(488, 257, 132),
(489, 258, 166),
(490, 258, 103),
(491, 258, 177),
(492, 259, 144),
(493, 259, 181),
(494, 259, 182),
(502, 260, 179),
(503, 260, 122),
(504, 260, 174),
(505, 260, 183),
(506, 261, 183),
(507, 261, 179),
(508, 261, 122),
(509, 262, 166),
(510, 262, 181),
(511, 263, 115),
(512, 264, 157),
(513, 265, 157),
(514, 265, 131),
(515, 266, 131),
(516, 266, 164),
(517, 267, 108),
(518, 268, 135),
(519, 269, 178),
(520, 270, 109),
(521, 271, 106),
(522, 272, 110),
(523, 272, 166),
(524, 273, 131),
(525, 273, 172),
(526, 274, 122),
(527, 274, 184),
(528, 275, 115),
(529, 276, 178),
(530, 277, 115),
(531, 278, 108),
(532, 279, 110),
(533, 279, 173),
(534, 280, 108),
(535, 281, 110),
(536, 281, 111),
(537, 282, 157),
(538, 283, 129),
(539, 284, 142),
(540, 285, 178),
(541, 285, 121),
(542, 285, 122),
(543, 286, 110),
(544, 286, 111),
(545, 287, 131),
(546, 287, 172),
(547, 288, 111),
(549, 289, 121),
(550, 290, 178),
(551, 291, 142),
(552, 292, 108),
(553, 293, 115),
(554, 294, 157),
(555, 295, 131),
(556, 296, 142),
(557, 297, 115),
(558, 298, 157),
(559, 299, 104),
(560, 300, 133),
(561, 301, 142),
(562, 302, 115),
(563, 302, 178),
(564, 303, 125),
(565, 304, 136),
(566, 305, 131),
(567, 306, 116),
(570, 309, 164),
(571, 310, 164),
(572, 307, 164),
(573, 308, 164),
(574, 311, 115),
(575, 312, 115),
(576, 313, 115),
(577, 314, 108),
(578, 315, 108),
(579, 316, 108),
(599, 75, 103),
(601, 79, 112),
(602, 81, 111),
(603, 82, 121),
(604, 83, 115),
(605, 84, 116),
(606, 85, 117),
(607, 87, 119),
(608, 87, 120),
(609, 88, 122),
(610, 89, 124),
(611, 90, 123),
(612, 91, 111),
(613, 103, 108),
(614, 103, 133),
(615, 115, 137),
(616, 115, 139),
(617, 117, 140),
(618, 117, 130),
(619, 117, 141),
(620, 117, 142),
(621, 118, 108),
(622, 119, 117),
(623, 119, 129),
(624, 119, 104),
(625, 119, 126),
(626, 119, 128),
(627, 120, 143),
(628, 120, 115),
(629, 121, 144),
(630, 121, 145),
(631, 121, 146),
(633, 122, 108),
(634, 128, 150),
(635, 129, 129),
(636, 136, 133),
(637, 136, 109),
(638, 137, 108),
(639, 138, 114),
(640, 138, 113),
(641, 138, 115),
(642, 139, 104),
(643, 139, 154),
(644, 139, 155),
(645, 140, 116),
(646, 140, 155),
(647, 141, 133),
(648, 141, 125),
(649, 142, 129),
(650, 143, 117),
(651, 143, 156),
(652, 144, 131),
(653, 145, 108),
(654, 146, 115),
(655, 147, 104),
(656, 324, 164),
(668, 327, 188),
(669, 325, 127),
(670, 326, 189),
(671, 328, 187),
(672, 317, 117),
(673, 317, 156),
(674, 317, 186),
(675, 318, 126),
(676, 318, 128),
(677, 319, 129),
(678, 320, 112),
(679, 321, 104),
(680, 321, 127),
(681, 322, 187),
(682, 323, 108),
(683, 323, 164),
(684, 329, 119),
(687, 330, 117),
(689, 331, 132);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `replacements_orders`
--

CREATE TABLE `replacements_orders` (
  `replacement_order` int(11) NOT NULL,
  `replacement_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '2',
  `active` tinyint(1) DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `user_type`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'Erick Zelaya', 'erick@lynext.com', '$2y$10$p9DPLOVYMoHwtyATMBCH2OyU4REqWGr1tLIDI1AOnX9tM6b8aVGSK', 1, 1, 'hgWngEyiOKHYJKGTXalIQOa9JX39F23i8lHdCiGbS7a6lh99j2sZ3SK9IxW5', '2016-08-31 23:09:24', '2016-09-01 03:25:33'),
(7, 'Francisco Urrutia', 'francisco@imosa.hn', '$2y$10$1nJQeOMWuOjbZrSWHmAOEu.3XQhqOf5vr9SZXPrBkPJCRzGZsZaZ.', 1, 1, 'DDs7QHau9pHqCNjl6UNWkDgRAOBcBzfbb1j6eZbdVE7sryOoNUP0wmeUH6Fg', '2016-09-01 05:01:50', '2016-09-01 05:06:02'),
(8, 'Juan Orellana ', 'jorellana@imosa.hn', '$2y$10$.ZXPiFDzusYEVPX1Ze7.n.XHxSeiw/IlyThbL6UVfP0F6e6m.SJgi', 3, 1, 'aIbiCRXAQ4jpNfhROYE3GDZnhFNKm0Kfy8Kb9laKd48Jgw7PnYvcnDgY5rkh', '2016-09-02 20:39:55', '2016-09-02 22:01:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_type`
--

CREATE TABLE `users_type` (
  `user_type` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_type`
--

INSERT INTO `users_type` (`user_type`, `description`) VALUES
(1, 'admin'),
(2, 'bodega'),
(3, 'vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vaucher`
--

CREATE TABLE `vaucher` (
  `vaucher_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indices de la tabla `cellars`
--
ALTER TABLE `cellars`
  ADD PRIMARY KEY (`cellar_id`);

--
-- Indices de la tabla `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indices de la tabla `company_invoice`
--
ALTER TABLE `company_invoice`
  ADD PRIMARY KEY (`company_invoice_id`),
  ADD KEY `fk_company` (`company_id`);

--
-- Indices de la tabla `credits`
--
ALTER TABLE `credits`
  ADD PRIMARY KEY (`credit_id`),
  ADD KEY `credits_customerFK` (`customer_id`);

--
-- Indices de la tabla `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD KEY `customersFK` (`customer_type`);

--
-- Indices de la tabla `customers_type`
--
ALTER TABLE `customers_type`
  ADD PRIMARY KEY (`customer_type`);

--
-- Indices de la tabla `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`),
  ADD KEY `fk_order_invoice` (`order_id`),
  ADD KEY `fk_company_invoice` (`company_invoice`);

--
-- Indices de la tabla `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`model_id`),
  ADD KEY `modelbrandFK` (`brand_id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_customerFK` (`customer_id`);

--
-- Indices de la tabla `payments_history`
--
ALTER TABLE `payments_history`
  ADD PRIMARY KEY (`payments_history`);

--
-- Indices de la tabla `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`provider_id`);

--
-- Indices de la tabla `pro_forma`
--
ALTER TABLE `pro_forma`
  ADD PRIMARY KEY (`proforma_id`),
  ADD KEY `fk_order_pro` (`order_id`),
  ADD KEY `fk_company_pro` (`company_id`);

--
-- Indices de la tabla `replacements`
--
ALTER TABLE `replacements`
  ADD PRIMARY KEY (`replacement_id`),
  ADD KEY `fk_cellar` (`cellar_id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indices de la tabla `replacements_models`
--
ALTER TABLE `replacements_models`
  ADD PRIMARY KEY (`replacement_model`),
  ADD KEY `fk_model_repla` (`replacement_id`),
  ADD KEY `fk_repla_model` (`model_id`),
  ADD KEY `replacement_model` (`replacement_model`),
  ADD KEY `replacement_model_2` (`replacement_model`);

--
-- Indices de la tabla `replacements_orders`
--
ALTER TABLE `replacements_orders`
  ADD PRIMARY KEY (`replacement_order`),
  ADD KEY `fk_repla_order` (`replacement_id`),
  ADD KEY `fk_order_repla` (`order_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_type`
--
ALTER TABLE `users_type`
  ADD PRIMARY KEY (`user_type`);

--
-- Indices de la tabla `vaucher`
--
ALTER TABLE `vaucher`
  ADD PRIMARY KEY (`vaucher_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `cellars`
--
ALTER TABLE `cellars`
  MODIFY `cellar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `company_invoice`
--
ALTER TABLE `company_invoice`
  MODIFY `company_invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `credits`
--
ALTER TABLE `credits`
  MODIFY `credit_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `customers_type`
--
ALTER TABLE `customers_type`
  MODIFY `customer_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `models`
--
ALTER TABLE `models`
  MODIFY `model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `payments_history`
--
ALTER TABLE `payments_history`
  MODIFY `payments_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `providers`
--
ALTER TABLE `providers`
  MODIFY `provider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `pro_forma`
--
ALTER TABLE `pro_forma`
  MODIFY `proforma_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `replacements`
--
ALTER TABLE `replacements`
  MODIFY `replacement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=332;
--
-- AUTO_INCREMENT de la tabla `replacements_models`
--
ALTER TABLE `replacements_models`
  MODIFY `replacement_model` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=690;
--
-- AUTO_INCREMENT de la tabla `replacements_orders`
--
ALTER TABLE `replacements_orders`
  MODIFY `replacement_order` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `users_type`
--
ALTER TABLE `users_type`
  MODIFY `user_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `company_invoice`
--
ALTER TABLE `company_invoice`
  ADD CONSTRAINT `fk_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customersFK` FOREIGN KEY (`customer_type`) REFERENCES `customers_type` (`customer_type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_company_invoice` FOREIGN KEY (`company_invoice`) REFERENCES `company_invoice` (`company_invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_invoice` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `models`
--
ALTER TABLE `models`
  ADD CONSTRAINT `modelbrandFK` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `order_customerFK` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pro_forma`
--
ALTER TABLE `pro_forma`
  ADD CONSTRAINT `fk_company_pro` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_pro` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `replacements`
--
ALTER TABLE `replacements`
  ADD CONSTRAINT `fk_cellar` FOREIGN KEY (`cellar_id`) REFERENCES `cellars` (`cellar_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `replacements_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`provider_id`);

--
-- Filtros para la tabla `replacements_models`
--
ALTER TABLE `replacements_models`
  ADD CONSTRAINT `fk_model_repla` FOREIGN KEY (`replacement_id`) REFERENCES `replacements` (`replacement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_repla_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `replacements_orders`
--
ALTER TABLE `replacements_orders`
  ADD CONSTRAINT `fk_order_repla` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_repla_order` FOREIGN KEY (`replacement_id`) REFERENCES `replacements` (`replacement_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
