-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2016 at 11:32 AM
-- Server version: 5.7.13-0ubuntu0.16.04.2
-- PHP Version: 7.0.10-2+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imosa_production`
--
CREATE DATABASE IF NOT EXISTS `imosa_production` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `imosa_production`;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `description`) VALUES
(1, 'HONDA'),
(2, 'LONCIN'),
(3, 'CG'),
(4, 'YAMAHA'),
(5, 'GENESIS'),
(6, 'SHINERAY'),
(7, 'BRONCO'),
(8, 'SERPENTO'),
(9, 'SUZUKI'),
(10, 'KMF'),
(11, 'BAJAJ'),
(12, 'HERO'),
(13, 'ITALIKA'),
(14, 'ZMOTO'),
(15, 'YUMBO'),
(16, 'UM'),
(17, 'APACHE'),
(18, 'HAOJUE'),
(19, 'FREEDOM');

-- --------------------------------------------------------

--
-- Table structure for table `cellars`
--

CREATE TABLE `cellars` (
  `cellar_id` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cellars`
--

INSERT INTO `cellars` (`cellar_id`, `description`) VALUES
(3, 'BODEGA 1'),
(4, 'BODEGA 2\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `name`, `department_id`) VALUES
(1, 'MARCALA', 6),
(2, 'JUTICALPA', 2),
(3, 'CATACAMAS', 2),
(4, 'CAMPAMENTO', 2),
(5, 'NACAOME', 5),
(6, 'SAN LORENZO', 5),
(7, 'SAN MARCOS DE COLON', 7),
(8, 'CHOLUTECA', 7),
(9, 'TEUPASENTI', 1),
(10, 'DANLI', 1),
(11, 'EL PARAISO', 1),
(12, 'TROJES', 1),
(13, 'TEGUCIGALPA', 3),
(14, 'TALANGA', 3),
(15, 'GUAIMACA', 3),
(16, 'LA ESPERANZA', 4);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `rtn` varchar(255) NOT NULL,
  `web_site` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `name`, `address`, `telephone`, `email`, `rtn`, `web_site`, `logo`) VALUES
(1, 'IMPORTA Y DISTRIBUIDORA DE MOTOPARTES S.A.', 'Res. Palma Real, Frente a Fuerza Aerea', '2203-4049', 'imosa.hn@gmail.com', '08019015808210', 'www.imosa.hn', 'uploads/thumb_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `company_invoice`
--

CREATE TABLE `company_invoice` (
  `company_invoice_id` int(11) NOT NULL,
  `cai` varchar(255) NOT NULL,
  `range_to` varchar(255) NOT NULL,
  `range_up` varchar(255) NOT NULL,
  `limit_date` varchar(20) NOT NULL,
  `isv` double NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_invoice`
--

INSERT INTO `company_invoice` (`company_invoice_id`, `cai`, `range_to`, `range_up`, `limit_date`, `isv`, `company_id`) VALUES
(2, '69D930B5B64B764FB8A80BBE7AB0198C', '00-001-01-00000001', '00-001-01-00000150', '27-07-17', 0.15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `credits`
--

CREATE TABLE `credits` (
  `credit_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `credit_condition` varchar(140) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credits`
--

INSERT INTO `credits` (`credit_id`, `customer_id`, `amount`, `balance`, `credit_condition`, `created_at`, `order_id`) VALUES
(3, 4, 55, 55, '30', '2016-09-19 23:34:39', 13),
(4, 5, 60.5, 30.5, '30', '2016-09-20 17:10:34', 14),
(5, 4, 70, 20, '30', '2016-09-20 17:11:23', 15);

-- --------------------------------------------------------

--
-- Table structure for table `credits_replacement`
--

CREATE TABLE `credits_replacement` (
  `id` int(11) NOT NULL,
  `credit_id` int(11) NOT NULL,
  `replacement_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cronjob`
--

CREATE TABLE `cronjob` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `executed_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `phone_number` varchar(140) NOT NULL,
  `phone_number2` varchar(255) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `full_name` varchar(140) NOT NULL,
  `RTN` varchar(14) DEFAULT NULL,
  `customer_type` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `telephone` varchar(25) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `phone_number`, `phone_number2`, `address`, `full_name`, `RTN`, `customer_type`, `email`, `city_id`, `telephone`, `contact`, `user_id`) VALUES
(1, '3251-6392', '', 'CONTIGUO A FARMACIA KIELSA # 1, DANLI – EL PARAISO', 'COMERCIAL Y VARIEDADES DE ORIENTE', '08019015808210', 1, 'ralfaro@imosa.hn', 10, '2763-1951', 'ADAN MAIRENA', 12),
(2, '9782-0882', NULL, 'TEGUCIGALPA, BARRIO BELEN.', 'CENTRAL DE MOTOS', '08019010285311', 1, 'admon@centraldemotos.com', 13, '2223-7440', 'JOEL LOPEZ', 12),
(3, '3293-9787', '', 'BARRIO EL MANCHEN, CALLE SANPABLO UNA CUADRA ARRIBA DEL MERCADO SAN PABLO.', 'MOTOFIX EL CUBANO', '08902010017603', 1, '', 13, '2220-3787', 'YOEL DÍAZ', 12),
(4, '9750-7356', '', 'TEGUCIGALPA, ½ CUADRA ANTES DE CASA ALIANZA AL FINAL DEL PUENTE CARIAS.', 'BICIMOTOS DANIELAS', '', 1, '', 13, '2237-1251', 'DANIEL IGLESIAS', 12),
(5, '9696-8877', '', 'TEUPASENTI, EL PARAÍSO, BARRIO EL CENTRO.', 'INVERSIONES JALAN', '07151978006506', 2, 'alexanderrodriguez@live.com.ar', 9, '2793-8110', 'MELVIN ALEXANDER RODRÍGUEZ RAUDALES', 8),
(6, '9913-4814', '3336-3433', 'CERRO GRANDE, ZONA # 3 ', 'KVM MOTOR SHOP', '17011978008945', 1, '', 13, '', 'ELMER VILLALOBOS', 12),
(7, '3150-7501', '', 'TEGUCIGALPA, CIUDAD LEMPIRA.', 'INVERSIONES DE DIOS', '07081995003768', 1, '', 13, '', 'JUAN DE DIOS', 12),
(8, '3230-8665', '', '5 TA  AVENIDA COMAYAGUELA.', 'MOTO REPUESTOS SPEED ', '08019009210174', 1, '', 13, '', 'ALLAN MORAZAN', 12),
(9, '9595-7171', '3232-7171', 'LAS BRISAS, COMAYAGUELA.', 'TALLER DE MOTOS PAGUADA', '08011974085763', 1, 'tallerpaguada@gmail.com', 13, '2225-1059', 'CARLOS ALFREDO PAGUADA', 12),
(10, '3230-8665', '', '5 TA AVENIDA COMAYAGUELA.', 'VIP MOTO ACCESORIOS', '08019009210174', 1, '', 13, '', 'ALLAN MORAZAN', 12),
(11, '8983-4515', '', '1 AVENIDA, UNA CUADRA ANTES DE CRUZ ROJA HONDUREÑA, COMAYAGUELA.', 'VML MOTOR SHOP ', '05111945021186', 1, 'victor_2m9@hotmail.com', 13, '2225-2509', 'VICTOR LANZA', 12),
(12, '9774-8078', '', 'TEGUCIGALPA.', 'WILSON CASTILLO', '', 1, '', 13, '', 'WILSON CASTILLO', 12),
(13, '9901-0032', '', 'BARRIO LAS ACACIAS FRENTE AL PORTON DE SALIDA DE TRANSPORTES AURORA, JUTICALPA - OLANCHO', 'BICI PARTES BANEGAS', '15031952003696', 2, 'wendybb102351@yahoo.com', 2, '', 'WENDY YECENIA BANEGAS', 8),
(14, '9989-2959', '', 'BO. HACIA LAS CUEVAS DE TALGUA', 'BICI PARTES CHICO', '15011972007849', 2, 'jmatutefortin@yahoo.com', 3, '2799-2682', 'JOCE FRANCISCO MATUTE FORTIN', 8),
(15, '9872-1804', '', 'Bo. LA RIORA, CATACAMAS, 30 METROS AL PONIENTE DE BANCO DE OCCIDENTE.', 'BICI PARTES TURCIOS ', '', 2, '', 3, '2799-4795', 'VICTOR TURCIOS', 8),
(16, '9820-6030', '', 'GUAYMACA, FM. CALLE PRINCIPAL', 'CENTRO COMERCIAL BENYITA', '08061973000679', 2, '', 15, '2769-3585', 'ERICK SUAZO', 8),
(17, '9954-1358', '', 'CAMPAMENTO OLANCHO, BARRIO EL PINO CALLE PRINCIPAL.', 'INVERSIONES RAUDALES', '1515194000730', 2, '', 4, '2789-0291', 'CARLOS RAUDALES', 8),
(18, '9938-3131', '', 'TALANGA, MEDIA CUADRA A LA DERECHA DE LA DESPENSA FAMILIAR.', 'MOTO PARTES JOSETH', '08011973087211', 2, 'motopartesjoseth@gmail.com', 14, '2775-9059', 'DOUGLAS ROBERTO DIAZ MENDOZA', 8),
(19, '9678-8320', '', 'BARRIO EL CENTRO FRENTE A FARMACIA SANTA ISABEL, LA ESPERANZA, INTIBUCA', 'SUPER MOTOCROOS', '10011979002431', 2, 'supermotorsimr@gmail.com', 16, '2756-8650', 'OSCAR MURILLO', 8),
(20, '9862-8154', '', 'ARENALES, TROJES', 'ABNER LOPEZ', '07191992010391', 2, '', 12, '', 'ABNER LOPEZ', 12),
(21, '9996-3178', '', 'TROJES, EL PARAISO', 'MOTO REPUESTOS ELENA', '07031975027387', 2, '', 12, '', 'MARVIN REYES', 12),
(22, '3175-0262', '', 'DANLI, EL PARAISO', 'SUPER CROSS', '', 2, '', 10, '', 'HECTOR', 12),
(23, '9571-1260', '', 'SAN LORENZO, VALLE, SALIDA A CHOLUTECA CARRETERA PANAMERICANA, KM 104, 100 M ANTES DE OFICINA DE LA ENEE', 'MOTO CENTRO GUEVARA ', '17011984024689', 2, 'guevaramotocentro@yahoo.com', 6, '', 'DANI ESTRADA', 11),
(24, '9781-4112', '', 'BARRIO EL CENTRO UNA CUADRA ANTES DE LOS TRANSPORTES, SAN MARCOS DE COLON, CHOLUTECA.', 'MOTO PARTES GARCIA', '06151982005179', 2, '', 7, '', 'MARVIN ALEXIS GARCIA', 11);

-- --------------------------------------------------------

--
-- Table structure for table `customers_type`
--

CREATE TABLE `customers_type` (
  `customer_type` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers_type`
--

INSERT INTO `customers_type` (`customer_type`, `description`) VALUES
(1, 'LOCAL'),
(2, 'RUTA');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `name`) VALUES
(1, 'EL PARAISO'),
(2, 'OLANCHO'),
(3, 'FRANCISCO MORAZAN'),
(4, 'INTIBUCA'),
(5, 'VALLE'),
(6, 'LA PAZ'),
(7, 'CHOLUTECA'),
(8, 'COMAYAGUA'),
(9, 'CORTES'),
(10, 'SANTA BARBARA'),
(11, 'LEMPIRA'),
(12, 'OCOTEPEQUE'),
(13, 'COPAN'),
(14, 'ATLANTIDA'),
(15, 'COLON'),
(16, 'YORO'),
(17, 'GRACIAS A DIOS'),
(18, 'ISLAS DE LA BAHIA');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `company_invoice` int(11) NOT NULL,
  `isv` varchar(20) NOT NULL,
  `subtotal` varchar(20) NOT NULL,
  `total` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_09_15_042105_create_jobs_table', 1),
('2016_09_15_235122_create_cron_job_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `models`
--

CREATE TABLE `models` (
  `model_id` int(11) NOT NULL,
  `description` varchar(140) NOT NULL,
  `brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `models`
--

INSERT INTO `models` (`model_id`, `description`, `brand_id`) VALUES
(103, 'MAGNUM-150', 3),
(104, 'PULSAR-135', 11),
(105, 'PLATINA-100', 11),
(106, 'AX-100', 9),
(107, 'CT-100', 11),
(108, 'YBR-125', 4),
(109, 'DT-175', 4),
(110, 'CG-125', 3),
(111, 'CG-150', 3),
(112, 'CBF-125', 1),
(113, 'HUNK', 12),
(114, 'CBF-150', 1),
(115, 'HJ-125-7', 5),
(116, 'PLATINA-125', 11),
(117, 'PULSAR-180', 11),
(118, 'PULSAR-200NS', 11),
(119, 'SUPER SPLENDOR', 12),
(120, 'GLAMOUR', 12),
(121, 'DAKAR-200', 15),
(122, 'XM-200', 5),
(123, 'ZM-200L', 10),
(124, 'SPITZER-150', 2),
(125, 'CRUX-110', 4),
(126, 'DISCOVER-125', 11),
(127, 'DISCOVER-125ST', 11),
(128, 'DISCOVER-135', 11),
(129, 'FZ16', 4),
(130, 'DM-150', 13),
(131, 'GXT-200', 5),
(132, 'XL-200', 1),
(133, 'XTZ-125', 4),
(134, 'UNICORN-150', 1),
(135, 'CGL-125', 1),
(136, 'GXT-250', 5),
(137, 'LX-150', 5),
(138, 'XY-200', 6),
(139, 'XY-200 ST-6', 6),
(140, 'RT-180', 13),
(141, 'FT-180', 13),
(142, 'GY6', 13),
(143, 'GN-125', 9),
(144, 'BOA', 8),
(145, 'COBRA', 8),
(146, 'TITAN', 6),
(147, 'ZM-250', 10),
(148, 'ZM-250L', 10),
(149, 'INVICTA-150', 1),
(150, 'GS-150R', 9),
(151, 'XCD', 11),
(152, 'DISCOVER-100', 11),
(153, 'EN-125', 9),
(154, 'PULSAR-150', 11),
(155, 'XCD-125', 11),
(156, 'PULSAR-200', 11),
(157, 'GN-125H', 9),
(158, 'DISCOVER-150', 11),
(159, 'BOXER-150', 11),
(160, 'PULSAR-220', 11),
(161, 'PULSAR-135LS', 11),
(162, 'PULSAR-UG4', 11),
(163, 'XCD-135', 11),
(164, 'UNIVERSAL', 1),
(165, 'BYQ', 14),
(166, 'FT-125', 13),
(167, 'CORAL', 8),
(168, 'BOXER BM150', 11),
(169, 'BOA-160', 8),
(170, 'SERPENTO-125', 8),
(171, 'SERPENTO-150', 8),
(172, 'ZM-200', 10),
(173, 'CG-200', 1),
(174, 'DM-200', 13),
(175, 'PULSAR-125', 11),
(176, 'PULSAR-125LS', 11),
(177, 'KB-150', 10),
(178, 'CG', 3),
(179, 'JARA-200', 8),
(180, 'ZX-150L', 10),
(181, 'FT-150', 13),
(182, 'TX-200', 13),
(183, 'DSR-200', 16),
(184, 'GYA-200', 5),
(185, 'GS-200', 15),
(186, 'TVS-160', 17),
(187, 'BOXER-100', 11),
(188, 'RE 205', 11),
(189, 'AX-100-4', 9),
(190, 'XT-225', 4),
(191, 'TS-185', 9),
(192, 'XT-250', 4),
(193, 'XL-185', 1),
(194, 'XT-200', 4),
(195, 'AX-115', 9),
(196, 'THRILLER', 12),
(197, 'PULSAR-220NS', 11),
(198, 'DR-200', 9),
(199, 'DT-125', 4),
(200, 'GN', 9),
(201, 'POLARIS 125', 14),
(202, 'HJ-150', 18),
(203, 'R-15', 4),
(204, 'XR-150L', 1),
(205, 'GS-125', 9),
(207, 'RX-100', 4),
(208, 'RX-115', 4),
(209, 'DT-100', 4),
(210, 'KS-150', 1),
(211, 'GX-150', 5),
(212, 'AVATAR-200', 19),
(213, 'BROSS-125', 1),
(214, 'RAPTOR-700', 4),
(215, 'DR-350', 9),
(216, 'DR-650', 9),
(217, 'YZ-250', 4),
(218, 'ETX-250', 7),
(219, 'HJ-150-30A', 5),
(220, 'XL-125', 1),
(221, 'XTM-200', 15),
(222, 'CRYPTON-110', 4),
(223, 'CRYPTON-115', 4),
(224, 'XTZ-250', 4),
(225, 'F-15', 4),
(226, 'XLR-125', 1),
(227, 'YBR-125TT', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `total` varchar(20) NOT NULL,
  `subtotal` varchar(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) DEFAULT '1',
  `is_invoice` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `checked_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments_history`
--

CREATE TABLE `payments_history` (
  `payments_history` int(11) NOT NULL,
  `credit_id` int(11) NOT NULL,
  `payment_amount` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bank_id` int(11) NOT NULL,
  `document` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments_history`
--

INSERT INTO `payments_history` (`payments_history`, `credit_id`, `payment_amount`, `created_at`, `bank_id`, `document`) VALUES
(1, 5, 50, '2016-09-20 22:55:14', 0, ''),
(2, 4, 30, '2016-09-20 23:09:13', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `provider_id` int(11) NOT NULL,
  `name` varchar(140) NOT NULL,
  `country` varchar(140) NOT NULL,
  `company` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`provider_id`, `name`, `country`, `company`) VALUES
(1, 'VINI', 'CHINA', 'VINI CO. LTD'),
(2, 'IMBRA', 'COLOMBIA', 'INDUMELBRA'),
(3, 'BJ', 'CHINA', 'BJ GROUP'),
(4, 'KENDA', 'HONDURAS', 'AGENCIA LA MUNDIAL'),
(5, 'CASTROL', 'HONDURAS', 'ACAVISA'),
(6, 'MOTUL', 'HONDURAS', 'LA META'),
(7, 'AUTO', 'GUATEMALA', 'PRODINCA');

-- --------------------------------------------------------

--
-- Table structure for table `pro_forma`
--

CREATE TABLE `pro_forma` (
  `proforma_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `total` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `replacements`
--

CREATE TABLE `replacements` (
  `replacement_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `bar_code` varchar(50) DEFAULT NULL,
  `description` varchar(140) DEFAULT NULL,
  `wholesale_price` double NOT NULL,
  `route_price` double NOT NULL,
  `picture` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `cellar_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `replacements`
--

INSERT INTO `replacements` (`replacement_id`, `code`, `bar_code`, `description`, `wholesale_price`, `route_price`, `picture`, `location`, `quantity`, `cellar_id`, `provider_id`) VALUES
(75, '7425602001041', NULL, 'FRICCIONES MAGNUM150\r\n', 55, 60.5, 'uploads/thumb_IMG-70.png', '-', 67, 3, 1),
(76, '7425602001065', NULL, 'FRICCIONES PULSAR 135\r\n', 70, 77, 'uploads/thumb_IMG-70.png', '-', 130, 3, 1),
(77, '7425602001089', NULL, 'FRICCIONES PLATINA 100/AX100/CT100\r\n', 60, 66, 'uploads/thumb_IMG-70.png', '-', 120, 3, 1),
(78, '7425602001027', NULL, 'FRICCIONES YBR125/CG125/150/DT 175\r\n', 70, 77, 'uploads/thumb_IMG-70.png', '-', 120, 3, 1),
(79, '7425602004417', NULL, 'CILINDRO COMPLETO CBF 125 STUNNER\r\n', 850, 935, 'uploads/thumb_IMG-8.png', '-', 19, 3, 1),
(80, '7425602004424', NULL, 'CILINDRO COMPLETO CBF150/HUNK\r\n', 775, 852.5, 'uploads/thumb_IMG-4.png', '-', 5, 3, 1),
(81, '7425602004455', NULL, 'CILINDRO COMPLETO CG150\r\n', 675, 742.5, 'uploads/thumb_IMG-11.png', '-', 8, 3, 1),
(82, '7425602004400', NULL, 'CILINDRO COMPLETO DAKAR 200\r\n', 900, 990, 'uploads/thumb_IMG-14.png', '-', 14, 3, 1),
(83, '7425602004479', NULL, 'CILINDRO COMPLETO HJ125-7\r\n', 600, 660, 'uploads/thumb_IMG-16.png', '-', 25, 3, 1),
(84, '7425602005452', NULL, 'CILINDRO COMPLETO PLATINA125\r\n', 850, 935, 'uploads/thumb_IMG-5.png', '-', 7, 3, 1),
(85, '7425602004394', NULL, 'CILINDRO COMPLETO PULSAR 180\r\n', 900, 990, 'uploads/thumb_IMG-7.png', '-', 4, 3, 1),
(86, '7425602004370', NULL, 'CILINDRO COMPLETO PULSAR200 NS\r\n', 1200, 1320, 'uploads/thumb_IMG-2.png', '-', 3, 3, 1),
(87, '7425602004387', NULL, 'CILINDRO COMPLETO SUPER SPLENDOR/GLAMOUR\r\n', 775, 852.5, 'uploads/thumb_IMG-9.png', '-', 6, 3, 1),
(88, '7425602004431', NULL, 'CILINDRO COMPLETO XM200', 780, 858, 'uploads/thumb_IMG-18.png', '-', 21, 3, 1),
(89, '7425602005773', NULL, 'CILINDRO COMPLETO CG 150/ SPITZER150 (MOTOR NEGRO)', 600, 660, 'uploads/thumb_IMG-12.png', '-', 12, 3, 1),
(90, '7425602005766', NULL, 'CILINDRO COMPLETO ZM-200L\r\n', 800, 880, 'uploads/thumb_IMG-15.png', '-', 36, 3, 1),
(91, '7425602002116', NULL, 'KIT DE ANILLOS CG150 STD\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 40, 3, 1),
(92, '7425602002123', NULL, 'KIT DE ANILLOS CRUX110 STD\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 72, 3, 1),
(93, '7425602002130', NULL, 'KIT DE ANILLOS CT100 STD\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 98, 3, 1),
(94, '7425602002154', NULL, 'KIT DE ANILLOS PULSAR135/DISCOVER125 ST/XCD125/PLATINA125 STD\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-', 81, 3, 1),
(95, '7425602002161', NULL, 'KIT DE ANILLOS DISCOVER135 STD/FZ16 STD\r\n', 110, 121, 'uploads/thumb_IMG-1.png', '-', 93, 3, 1),
(96, '7425602002178', NULL, 'KIT DE ANILLOS DM150 STD/ENGINE BLACK\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-  ', 48, 3, 1),
(97, '7425602002208', NULL, 'KIT DE ANILLOS GXT200 STD\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-', 29, 3, 1),
(98, '7425602002253', NULL, 'KIT DE ANILLOS XL200 STD\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-', 51, 3, 1),
(99, '7425602002260', NULL, 'KIT DE ANILLOS XM200 STD\r\n', 110, 121, 'uploads/thumb_IMG-1.png', '-', 59, 3, 1),
(100, '7425602002277', NULL, 'KIT DE ANILLOS YBR125 ED/G STD\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 18, 3, 1),
(101, '7425602002284', NULL, 'KIT DE ANILLOS ZM-200L STD\r\n', 100, 110, 'uploads/thumb_IMG-1.png', '-', 100, 3, 1),
(102, '7425602002734', NULL, 'KIT DE ANILLOS GXT200 0.25\r\n', 90, 99, 'uploads/thumb_IMG-1.png', '-', 86, 3, 1),
(103, '7425602001911', NULL, 'KIT DE BIELA DE MOTOR YBR 125/XTZ125\r\n', 250, 275, 'uploads/thumb_IMG-0.png', '-', 13, 3, 1),
(104, '7425602001928', NULL, 'KIT DE BIELA DE MOTOR CRUX110\r\n', 250, 275, 'uploads/thumb_IMG-0.png', '-', 20, 3, 1),
(105, '7425602001935', NULL, 'KIT DE BIELA DE MOTOR FZ16\r\n', 325, 357.5, 'uploads/thumb_IMG-0.png', '-', 22, 3, 1),
(106, '7425602001942', NULL, 'KIT DE BIELA DE MOTOR DT 175\r\n', 230, 253, 'uploads/thumb_IMG-0.png', '-', 18, 3, 1),
(107, '7425602001997', NULL, 'KIT DE BIELA DE MOTOR CBF 125 STUNNER\r\n', 300, 330, 'uploads/thumb_IMG-0.png', '-', 24, 3, 1),
(108, '7425602002000', NULL, 'KIT DE BIELA DE MOTOR UNICORN 150/HUNK/CBF150\r\n', 285, 313.5, 'uploads/thumb_IMG-0.png', '-', 27, 3, 1),
(109, '7425602002017', NULL, 'KIT DE BIELA DE MOTOR PULSAR135\r\n', 300, 330, 'uploads/thumb_IMG-0.png', '-', 18, 3, 1),
(110, '7425602002048', NULL, 'KIT DE BIELA DE MOTOR PLATINA 125/DISCOVER 125 ST\r\n', 300, 330, 'uploads/thumb_IMG-0.png', '-', 13, 3, 1),
(111, '7425602002055', NULL, 'KIT DE BIELA DE MOTOR CG150\r\n', 230, 253, 'uploads/thumb_IMG-0.png', '-', 20, 3, 1),
(112, '7425602002062', NULL, 'KIT DE BIELA DE MOTOR CGL125/HJ125-7\r\n', 210, 231, 'uploads/thumb_IMG-0.png', '-', 24, 3, 1),
(113, '7425602005476', NULL, 'KIT DE BIELA DE MOTOR GXT250\r\n', 600, 660, 'uploads/thumb_IMG-0.png', '-', 25, 3, 1),
(114, '7425602001010', NULL, 'KIT DE BIELA DE MOTOR DM150\r\n', 300, 330, 'uploads/thumb_IMG-0.png', '-', 18, 3, 1),
(115, '7425602001119', NULL, 'PASTILLAS DE FRENO LX-150/XY-200 ST-6\r\n', 40, 44, 'uploads/thumb_IMG-45.png', '-', 115, 3, 1),
(116, '7425602001157', NULL, 'PASTILLAS DE FRENO PULSAR 200 NS', 40, 44, 'uploads/thumb_IMG-44.png', '-', 46, 3, 1),
(117, '7425602001126', NULL, 'PASTILLAS DE FRENO RT-180/GY6/DM150/FT180\r\n', 35, 38.5, 'uploads/thumb_IMG-42.png', '-', 55, 3, 1),
(118, '7425602001102', NULL, 'PASTILLAS DE FRENO YBR 125\r\n', 40, 44, 'uploads/thumb_IMG-49.png', '- ', 150, 3, 1),
(119, '7425602001096', NULL, 'PASTILLAS DE FRENO PULSAR180/FZ-16/PULSAR135/DISCOVER125/135\r\n', 35, 38.5, 'uploads/thumb_IMG-47.png', '-', 45, 3, 1),
(120, '7425602001133', NULL, 'PASTILLAS DE FRENO HJ 125/GN 125\r\n', 33, 36.3, 'uploads/thumb_IMG-48.png', '-', 7, 3, 1),
(121, '7425602001140', NULL, 'PASTILLAS DE FRENO BOA/COBRA/TITAN\r\n', 33, 36.3, 'uploads/thumb_IMG-52.png', '-', 35, 3, 1),
(122, '7425602001874', NULL, 'CATARINA TRASERA YBR125  428-47T\r\n', 125, 137.5, 'uploads/thumb_IMG-91.png', '-', 22, 3, 1),
(123, '7425602001744', NULL, 'CATARINA TRASERA GXT200 428-48T\r\n', 115, 126.5, 'uploads/thumb_IMG-87.png', '-', 70, 3, 1),
(124, '7425602001867', NULL, 'CATARINA TRASERA CG125 428-45T\r\n', 105, 115.5, 'uploads/thumb_IMG-89.png', '-', 25, 3, 1),
(125, '7425602001898', NULL, 'CATARINA TRASERA ZM-250 520-38T\r\n', 165, 181.5, 'uploads/thumb_IMG-94.png', '-', 9, 3, 1),
(126, '7425602001782', NULL, 'CATARINA TRASERA ZM250L/KMF 520-36T\r\n', 175, 192.5, 'uploads/thumb_IMG-93.png', '-', 4, 3, 1),
(127, '7425602001843', NULL, 'CATARINA TRASERA INVICTA 150 428-42T\r\n', 125, 137.5, 'uploads/thumb_IMG-88.png', '-', 15, 3, 1),
(128, '7425602001850', NULL, 'CATARINA TRASERA GS150R 428-42T\r\n', 110, 121, 'uploads/thumb_IMG-76.png', '-', 20, 3, 1),
(129, '7425602001812', NULL, 'CATARINA TRASERA FZ16 428-40T\r\n', 95, 104.5, 'uploads/thumb_IMG-82.png', '-', 19, 3, 1),
(130, '7425602001799', NULL, 'CATARINA TRASERA XCD/PLATINA100/CT100/DISCOVER100 428-42T\r\n', 115, 126.5, 'uploads/thumb_IMG-92.png', '-', 50, 3, 1),
(131, '7425602001713', NULL, 'CATARINA TRASERA EN125/GN125 428-45T\r\n', 110, 121, 'uploads/thumb_IMG-75.png', '-', 150, 3, 1),
(132, '7425602001805', NULL, 'CATARINA TRASERA PULSAR135 428-44T\r\n', 100, 110, 'uploads/thumb_IMG-90.png', '-', 23, 3, 1),
(133, '7425602001737', NULL, 'CATARINA TRASERA YBR125  428-45T\r\n', 105, 115.5, 'uploads/thumb_IMG-95.png', '-', 30, 3, 1),
(134, '7425602001966', NULL, 'KIT DE BIELA DE MOTOR XM200\r\n', 320, 352, 'uploads/thumb_IMG-0.png', '-', 56, 3, 1),
(135, '7425602001751', NULL, 'CATARINA TRASERA YBR125 428-50T\r\n', 130, 143, 'uploads/thumb_IMG-85.png', '-', 36, 3, 1),
(136, '7425602001768', NULL, 'CATARINA TRASERA XTZ125/DT175 428-50T\r\n', 130, 143, 'uploads/thumb_IMG-85.png', '-', 23, 3, 1),
(137, '7425602004721', NULL, 'DISCOS DE CLUTCH YBR125 ED/G\r\n', 60, 66, 'uploads/thumb_IMG-58.png', '-', 120, 3, 1),
(138, '7425602004677', NULL, 'DISCOS DE CLUTCH HJ-125-7/CBF150/HUNK\r\n', 70, 77, 'uploads/thumb_IMG-59.png', '-', 149, 3, 1),
(139, '7425602004660', NULL, 'DISCOS DE CLUTCH PULSAR 135/150/XCD125\r\n', 85, 93.5, 'uploads/thumb_IMG-60.png', '-', 61, 3, 1),
(140, '7425602004707', NULL, 'DISCOS DE CLUTCH PLATINA125/XCD125\r\n', 75, 82.5, 'uploads/thumb_IMG-61.png', '-', 75, 3, 1),
(141, '7425602004714', NULL, 'DISCOS DE CLUTCH XTZ125/CRUX110\r\n', 75, 82.5, 'uploads/thumb_IMG-62.png', '-', 147, 3, 1),
(142, '7425602004684', NULL, 'DISCOS DE CLUTCH FZ16\r\n', 150, 165, 'uploads/thumb_IMG-63.png', '-', 39, 3, 1),
(143, '7425602005742', NULL, 'DISCOS DE CLUTCH PULSAR180/200\r\n', 100, 110, 'uploads/thumb_IMG-64.png', '-', 71, 3, 1),
(144, '7425602000907', NULL, 'KIT DE TRACCION GXT200 428-47T/15T 428H-136L\r\n', 290, 319, 'uploads/thumb_IMG-38.png', '-', 12, 3, 1),
(145, '7425602000921', NULL, 'KIT DE TRACCION YBR 125 428-45T/14T 428H-124L\r\n', 275, 302.5, 'uploads/thumb_IMG-35.png', '-', 33, 3, 1),
(146, '7425602000914', NULL, 'KIT DE TRACCION HJ125 428-42T/15T 428H-124L\r\n', 270, 297, 'uploads/thumb_IMG-37.png', '-', 31, 3, 1),
(147, '7425602000938', NULL, 'KIT DE TRACCION PULSAR 135 428-42T/14T 428H-124L\r\n', 265, 291.5, 'uploads/thumb_IMG-36.png', '-', 8, 3, 1),
(148, '7425602005223', NULL, 'KIT DE BALANCINES PULSAR200 NS\r\n', 270, 297, 'uploads/thumb_IMG-24.png', '-', 38, 3, 1),
(149, '7425602005247', NULL, 'KIT DE BALANCINES DE ABAJO HJ125-7\r\n', 75, 82.5, 'uploads/thumb_IMG-25.png', '-', 97, 3, 1),
(150, '7425602005230', NULL, 'KIT DE BALANCINES YBR125 ED/G/CRUX110\r\n', 110, 121, 'uploads/thumb_IMG-26.png', '-', 65, 3, 1),
(151, '7425602005209', NULL, 'KIT DE BALANCINES FZ16\r\n', 250, 275, 'uploads/thumb_IMG-28.png', '-', 86, 3, 1),
(152, '7425602005216', NULL, 'KIT DE BALANCINES / GN125H\r\n', 100, 110, 'uploads/thumb_IMG-29.png', '-', 146, 3, 1),
(153, '7425602005186', NULL, 'VARILLA DE MOTOR CGL 125/HJ125-7\r\n', 50, 55, 'uploads/thumb_IMG-31.png', '-', 160, 3, 1),
(154, '7425602005469', NULL, 'VARILLAS DE MOTOR XM-200\r\n', 50, 55, 'uploads/thumb_IMG-31.png', '-', 360, 3, 1),
(155, '7425602005551', NULL, 'VALVULAS DE MOTOR GXT200\r\n', 150, 165, 'uploads/thumb_IMG-27.png', '-', 57, 3, 1),
(156, '7425602004868', NULL, 'VALVULAS DE MOTOR DISCOVER150/BOXER150\r\n', 115, 126.5, 'uploads/thumb_IMG-27.png', '-', 177, 3, 1),
(157, '7425602004875', NULL, 'VALVULAS DE MOTOR CBF 125 STUNNER\r\n', 150, 165, 'uploads/thumb_IMG-27.png', '-', 79, 3, 1),
(158, '7425602004882', NULL, 'VALVULAS DE MOTOR CBF150/HUNK\r\n', 135, 148.5, 'uploads/thumb_IMG-27.png', '-', 88, 3, 1),
(159, '7425602004899', NULL, 'VALVULAS DE MOTOR CG150\r\n', 120, 132, 'uploads/thumb_IMG-27.png', '-', 193, 3, 1),
(160, '7425602004905', NULL, 'VALVULAS DE MOTOR CRUX110\r\n', 100, 110, 'uploads/thumb_IMG-27.png', '-', 70, 3, 1),
(161, '7425602004936', NULL, 'VALVULAS DE MOTOR FZ16\r\n', 175, 192.5, 'uploads/thumb_IMG-27.png', '-', 180, 3, 1),
(162, '7425602004943', NULL, 'VALVULAS DE MOTOR GN125H\r\n', 140, 154, 'uploads/thumb_IMG-27.png', '-', 78, 3, 1),
(163, '7425602004967', NULL, 'VALVULAS DE MOTOR PLATINA125\r\n', 115, 126.5, 'uploads/thumb_IMG-27.png', '-', 173, 3, 1),
(164, '7425602004981', NULL, 'VALVULAS DE MOTOR PULSAR180\r\n', 135, 148.5, 'uploads/thumb_IMG-27.png', '-', 75, 3, 1),
(165, '7425602005001', NULL, 'VALVULAS DE MOTOR SUPER SPLENDOR/GLAMOUR\r\n', 125, 137.5, 'uploads/thumb_IMG-27.png', '-', 90, 3, 1),
(166, '7425602005018', NULL, 'VALVULAS DE MOTOR XL200\r\n', 120, 132, 'uploads/thumb_IMG-27.png', '-', 55, 3, 1),
(167, '7425602005025', NULL, 'VALVULAS DE MOTOR YBR125 ED/G/XTZ125\r\n', 100, 110, 'uploads/thumb_IMG-27.png', '-', 185, 3, 1),
(168, '7425602004912', NULL, 'VALVULAS DE MOTOR DISCOVER100/135\r\n', 130, 143, 'uploads/thumb_IMG-27.png', '-', 76, 3, 1),
(169, '7425602005346', NULL, 'CADENA DE TIEMPO CRUX110(90L)\r\n', 75, 82.5, 'uploads/thumb_IMG-22.png', '-', 82, 3, 1),
(170, '7425602005353', NULL, 'CADENA DE TIEMPO DISCOVER125/DISCOVER 135/PULSAR135(94L)\r\n', 75, 82.5, 'uploads/thumb_IMG-22.png', '-', 66, 3, 1),
(171, '7425602005360', NULL, 'CADENA DE TIEMPO FZ16(96L)\r\n', 95, 104.5, 'uploads/thumb_IMG-22.png', '-', 202, 3, 1),
(172, '7425602005384', NULL, 'CADENA DE TIEMPO PLATINA125/XCD125(92L)\r\n', 75, 82.5, 'uploads/thumb_IMG-22.png', '-', 96, 3, 1),
(173, '7425602005391', NULL, 'CADENA DE TIEMPO PULSAR180(98L)\r\n', 75, 82.5, 'uploads/thumb_IMG-22.png', '-', 89, 3, 1),
(174, '7425602005407', NULL, 'CADENA DE TIEMPO YBR125 ED/G(88L)\r\n', 70, 77, 'uploads/thumb_IMG-22.png', '-', 71, 3, 1),
(175, '7425602005308', NULL, 'TENSOR CADENA DE TIEMPO PULSAR 200/220/ PULSAR135 LS/PULSAR UG4/XCD135\r\n', 100, 110, 'uploads/thumb_IMG-81.png', '-', 174, 3, 1),
(176, '7425602005315', NULL, 'TENSOR CADENA DE TIEMPO YBR125 ED/G/XTZ125\r\n', 90, 99, 'uploads/thumb_IMG-80.png', '-', 175, 3, 1),
(177, '7425602005292', NULL, 'TENSOR CADENA DE TIEMPO GXT200\r\n', 100, 110, 'uploads/thumb_IMG-35 (2).png', '-', 151, 3, 1),
(178, '7425602000860', NULL, 'CADENA DE TRACCION 428H-136L\r\n', 120, 132, 'uploads/thumb_IMG-57.png', '-', 45, 3, 1),
(179, '7425602000846', NULL, 'CADENA DE TRACCION 428H-136L DORADA\r\n', 170, 187, 'uploads/thumb_IMG-40.png', '-', 219, 3, 1),
(180, '7425602000549', NULL, 'BASE DE CATARINA XM-200/SHINEVAY200/BYQ\r\n', 190, 209, 'uploads/thumb_IMG-55.png', '-', 76, 3, 1),
(181, '7425602000532', NULL, 'BASE DE CATARINA YBR/FT-125/BOA/CORAL/COBRA/CYUX\r\n', 160, 176, 'uploads/thumb_IMG-34.png', '-', 92, 3, 1),
(182, '7425602005759', NULL, 'SELLOS DE VALVULA XCD/PULSR135/PULSAR180/DISCOVER125/135\r\n', 12, 13.2, 'uploads/thumb_IMG-33.png', '-', 894, 3, 1),
(183, '7425602004547', NULL, 'KIT DE EMPAQUES DE MOTOR GXT200\r\n', 80, 88, 'uploads/thumb_IMG-41.png', '-', 112, 3, 1),
(184, '7425602005131', NULL, 'EJE DE LEVAS GXT200\r\n', 250, 275, 'uploads/thumb_IMG-73.png', '-', 68, 3, 1),
(185, '7425602005162', NULL, 'EJE DE LEVAS XL200\r\n', 250, 275, 'uploads/thumb_IMG-67.png', '-', 26, 3, 1),
(186, '7425602005117', NULL, 'EJE DE LEVAS FZ16\r\n', 350, 385, 'uploads/thumb_IMG-68.png', '-', 56, 3, 1),
(187, '7425602005155', NULL, 'EJE DE LEVAS PULSAR135\r\n', 240, 264, 'uploads/thumb_IMG-78.png', '-', 44, 3, 1),
(188, '7425602005179', NULL, 'EJE DE LEVAS YBR125 ED/G\r\n', 265, 291.5, 'uploads/thumb_IMG-79.png', '-', 34, 3, 1),
(189, '7425602001430', NULL, 'ESPEJOS GXT200\r\n', 80, 88, 'uploads/thumb_IMG-66.png', '-', 138, 3, 1),
(190, '7425602005056', NULL, 'CIGÜEÑAL HJ125-7\r\n', 850, 935, 'uploads/thumb_IMG-43.png', '-', 4, 3, 1),
(191, '7425602005049', NULL, 'CIGÜEÑAL GXT200\r\n', 1300, 1430, 'uploads/thumb_IMG-74.png', '-', 18, 3, 1),
(192, '7425602003465', NULL, 'KIT DE PISTON AX100 0.25\r\n', 190, 209, 'uploads/thumb_IMG-19.png', '-', 28, 3, 1),
(193, '7425602003229', NULL, 'KIT DE PISTON AX100 STD\r\n', 190, 209, 'uploads/thumb_IMG-19.png', '-', 27, 3, 1),
(194, '7425602003236', NULL, 'KIT DE PISTON BOXER BM150 STD\r\n', 225, 247.5, 'uploads/thumb_IMG-20.png', '-', 26, 3, 1),
(195, '7425602003137', NULL, 'KIT DE PISTON BOA 160 STD\r\n', 250, 275, 'uploads/thumb_IMG-20.png', '-', 18, 3, 1),
(196, '7425602003243', NULL, 'KIT DE PISTON CBF125 STUNNER STD\r\n', 200, 220, 'uploads/thumb_IMG-20.png', '-', 60, 3, 1),
(197, '7425602003267', NULL, 'KIT DE PISTON CG150 STD\r\n', 200, 220, 'uploads/thumb_IMG-20.png', '-', 37, 3, 1),
(198, '7425602003274', NULL, 'KIT DE PISTON CRUX110 STD\r\n', 200, 220, 'uploads/thumb_IMG-21.png', '-', 28, 3, 1),
(199, '7425602003281', NULL, 'KIT DE PISTON CT100 STD\r\n', 200, 220, 'uploads/thumb_IMG-21.png', '-', 48, 3, 1),
(200, '742560200969', NULL, 'KIT DE PISTON DISCOVER 150 STD\r\n', 250, 275, 'uploads/thumb_IMG-20.png', '-', 46, 3, 1),
(201, '7425602003120', NULL, 'KIT DE PISTON DM150 STD(ENGINE BLACK)\r\n', 215, 236.5, 'uploads/thumb_IMG-20.png', '-', 21, 3, 1),
(202, '7425602003335', NULL, 'KIT DE PISTON FZ16 STD\r\n', 225, 247.5, 'uploads/thumb_IMG-21.png', '-', 40, 3, 1),
(203, '7425602003359', NULL, 'KIT DE PISTON GXT200 STD\r\n', 275, 302.5, 'uploads/thumb_IMG-20.png', '-', 33, 3, 1),
(204, '7425602003366', NULL, 'KIT DE PISTON HJ125-7 STD\r\n', 185, 203.5, 'uploads/thumb_IMG-20.png', '-', 44, 3, 1),
(205, '7425602003373', NULL, 'KIT DE PISTON PLATINA125 STD\r\n', 220, 242, 'uploads/thumb_IMG-21.png', '-', 41, 3, 1),
(206, '7425602003380', NULL, 'KIT DE PISTON PULSAR135 STD\r\n', 215, 236.5, 'uploads/thumb_IMG-21.png', '-', 31, 3, 1),
(207, '7425602003410', NULL, 'KIT DE PISTON SUPER SPLENDOR/GLAMOUR STD\r\n', 200, 220, 'uploads/thumb_IMG-20.png', '-', 49, 3, 1),
(208, '7425602003113', NULL, 'KIT DE PISTON XCD125 STD\r\n', 200, 220, 'uploads/thumb_IMG-20.png', '-', 48, 3, 1),
(209, '7425602003434', NULL, 'KIT DE PISTON XM200 STD\r\n', 265, 291.5, 'uploads/thumb_IMG-20.png', '-', 41, 3, 1),
(210, '7425602003441', NULL, 'KIT DE PISTON YBR125 ED/G STD\r\n', 190, 209, 'uploads/thumb_IMG-20.png', '-', 14, 3, 1),
(211, '7425602003458', NULL, 'KIT DE PISTON ZM-200L STD\r\n', 250, 275, 'uploads/thumb_IMG-20.png', '-', 5, 3, 1),
(212, '7425602001294', NULL, 'BALINERAS DE BUFA 6202 2RS\r\n', 19, 20.9, 'uploads/thumb_IMG-6.png', '-', 0, 3, 1),
(213, '7425602001287', NULL, 'BALINERAS DE BUFA 6004 2RS\r\n', 25, 27.5, 'uploads/thumb_IMG-6.png', '-', 120, 3, 1),
(214, '7425602001317', NULL, 'BALINERAS DE BUFA 6301 2RS\r\n', 22, 24.2, 'uploads/thumb_IMG-6.png', '-', 199, 3, 1),
(215, '7425602001324', NULL, 'BALINERAS DE BUFA 6302 2RS\r\n', 24, 26.4, 'uploads/thumb_IMG-6.png', '-', 220, 3, 1),
(216, '7425602001300', NULL, 'BALINERAS DE BUFA 6204 2RS\r\n', 28, 30.8, 'uploads/thumb_IMG-6.png', '-', 170, 3, 1),
(217, '7425602004585', NULL, 'BALINERA DE MOTOR 63/28\r\n', 90, 99, 'uploads/thumb_IMG-1 (2).png', '-', 72, 3, 1),
(218, '7425602001690', NULL, 'SPROCKET DELANTERO GXT200/KMF 428-15T\r\n', 30, 33, 'uploads/thumb_IMG-9 (2).png', '-', 88, 3, 1),
(219, '7425602001638', NULL, 'SPROCKET DELANTERO HUNK/SERPENTO125/150  428-15T\r\n', 30, 33, 'uploads/thumb_IMG-10.png', '-', 125, 3, 1),
(220, '7425602001645', NULL, 'SPROCKET DELANTERO CT100/PLATINA125/PULSAR135/XCD/DISCOVER150 428-15T\r\n', 30, 33, 'uploads/thumb_IMG-11 (2).png', '-', 282, 3, 1),
(221, '7425602001706', NULL, 'SPROCKET DELANTERO EN125/GN125 428-14T\r\n', 30, 33, 'uploads/thumb_IMG-12 (2).png', '-', 112, 3, 1),
(222, '7425602001669', NULL, 'SPROCKET DELANTERO YBR/XTZ -125 428-14T\r\n', 28, 30.8, 'uploads/thumb_IMG-13.png', '-', 161, 3, 1),
(223, '7425602001836', NULL, 'SPROCKET DELANTERO ZM 200L 520-13T\r\n', 35, 38.5, 'uploads/thumb_IMG-14 (2).png', '-', 131, 3, 1),
(224, '7425602001652', NULL, 'SPROCKET DELANTERO CG125 428-15T\r\n', 28, 30.8, 'uploads/thumb_IMG-15 (2).png', '-', 201, 3, 1),
(225, '7425602001683', NULL, 'SPROCKET DELANTERO GXT200/KMF 520-12T\r\n', 35, 38.5, 'uploads/thumb_IMG-16 (2).png', '-', 97, 3, 1),
(226, '7425602000150', NULL, 'BOBINA DE MAGNETO PULSAR135\r\n', 350, 385, 'uploads/thumb_IMG-26 (2).png', '-', 25, 3, 1),
(227, '7425602000099', NULL, 'BOBINA DE MAGNETO GXT200/KMF 200\r\n', 375, 412.5, 'uploads/thumb_IMG-0 (2).png', '-', 58, 3, 1),
(228, '7425602000167', NULL, 'BOBINA DE MAGNETO YBR125 ED/G/XTZ125\r\n', 350, 385, 'uploads/thumb_IMG-23.png', '-', 114, 3, 1),
(229, '7425602000082', NULL, 'BOBINA DE MAGNETO CG150/200/HJ125-7\r\n', 240, 264, 'uploads/thumb_IMG-24 (2).png', '-', 112, 3, 1),
(230, '7425602000105', NULL, 'BOBINA DE MAGNETO YUMBO 200\r\n', 275, 302.5, 'uploads/thumb_IMG-25 (2).png', '-', 60, 3, 1),
(231, '7425602004813', NULL, 'BENDIX GXT200/KMF200/XM200/DM200\r\n', 180, 198, 'uploads/thumb_IMG-7 (2).png', '-', 148, 3, 1),
(232, '7425602004844', NULL, 'BENDIX PULSAR135\r\n', 250, 275, 'uploads/thumb_IMG-8 (2).png', '-', 32, 3, 1),
(233, '7425602000464', NULL, 'KIT DE RAYOS TRASEROS GXT/KMF - 200\r\n', 90, 99, 'uploads/thumb_IMG-17.png', '-', 405, 3, 1),
(234, '7425602000594', NULL, 'MOTOR DE ARRANQUE GY6\r\n', 325, 357.5, 'uploads/thumb_IMG-21 (2).png', '-', 15, 3, 1),
(235, '7425602000600', NULL, 'MOTOR DE ARRANQUE CG 125\r\n', 370, 407, 'uploads/thumb_IMG-28 (2).png', '-', 13, 3, 1),
(236, '7425602000617', NULL, 'MOTOR DE ARRANQUE CBF125 STUNNER/CBF150/HUNK\r\n', 600, 660, 'uploads/thumb_IMG-20 (2).png', '-', 14, 3, 1),
(237, '7425602000624', NULL, 'MOTOR DE ARRANQUE FZ16\r\n', 650, 715, 'uploads/thumb_IMG-18 (2).png', '-', 11, 3, 1),
(238, '7425602000655', NULL, 'MOTOR DE ARRANQUE XCD125/PULSAR125 LS/DISCOVER125 ST\r\n', 700, 770, 'uploads/thumb_IMG-19 (2).png', '-', 17, 3, 1),
(239, '7425602000662', NULL, 'MOTOR DE ARRANQUE YBR125 ED/G\r\n', 500, 550, 'uploads/thumb_IMG-30.png', '-', 6, 3, 1),
(240, '7425602000570', NULL, 'BUFA TRASERA KB 150/MAGNUM 150/CG\r\n', 450, 495, 'uploads/thumb_IMG-29 (2).png', '-', 14, 3, 1),
(241, '7425602000563', NULL, 'BUFA TRASERA GXT200\r\n', 700, 770, 'uploads/thumb_IMG-27 (2).png', '-', 30, 3, 1),
(242, '7425602000488', NULL, 'MONOSHOCK XM200/JARA200/KMF150L/UNIVERSAL\r\n', 850, 935, 'uploads/thumb_IMG-33 (2).png', '-', 3, 3, 1),
(243, '7425602004615', NULL, 'BOMBA DE ACIETE HJ125-7\r\n', 100, 110, 'uploads/thumb_IMG-38 (2).png', '-', 40, 3, 1),
(244, '7425602004622', NULL, 'BOMBA DE ACIETE YBR125 ED/G/XTZ125\r\n', 125, 137.5, 'uploads/thumb_IMG-36 (2).png', '-', 56, 3, 1),
(245, '7425602001522', NULL, 'HULE DESLIZADOR DE CADENA GXT200\r\n', 75, 82.5, 'uploads/thumb_IMG-39.png', '-', 8, 3, 1),
(246, '7425602004745', NULL, 'PORTADISCO CRUX110\r\n', 180, 198, 'uploads/thumb_IMG-41 (2).png', '-', 19, 3, 1),
(247, '7425602005414', NULL, 'CAJA DE CAMBIO COMPLETA CBF150\r\n', 1400, 1540, 'uploads/thumb_IMG-40 (2).png', '-', 7, 3, 1),
(248, '7425602005445', NULL, 'CAJA DE CAMBIO COMPLETA YBR125 ED/G\r\n', 800, 880, 'uploads/thumb_IMG-42 (2).png', '-', 6, 3, 1),
(249, '7425602001614', NULL, 'VIAS GXT200 (2 UNIDADES)\r\n', 75, 82.5, 'uploads/thumb_IMG-45 (2).png', '-', 100, 3, 1),
(250, '7425602001539', NULL, 'STOP DE FRENO GXT200 / KMF - 200\r\n', 100, 110, 'uploads/thumb_IMG-46 (2).png', '-', 30, 3, 1),
(251, '7425602001515', NULL, 'FOCO DELANTERO GXT200\r\n', 200, 220, 'uploads/thumb_IMG-47 (2).png', '-', 36, 3, 1),
(252, '7425602004646', NULL, 'TRIANGULO DE CARTER GXT200/GN125H\r\n', 45, 49.5, 'uploads/thumb_IMG-48 (2).png', '-', 155, 3, 1),
(253, '7425602001447', NULL, 'TORQUE DE CARBURADOR GXT200\r\n', 150, 165, 'uploads/thumb_IMG-49 (2).png', '-', 170, 3, 1),
(254, '7425602001546', NULL, 'GUIA DE CADENA TRASERA GXT200\r\n', 55, 60.5, 'uploads/thumb_IMG-50 (2).png', '-', 40, 3, 1),
(255, '7425602004639', NULL, 'TAPON MEDIDOR DE ACEITE HJ125-7/CG (UNIVERSAL)\r\n', 10, 11, 'uploads/thumb_IMG-60 (2).png', '-', 180, 3, 1),
(256, '7425602001171', NULL, 'CABLE DE CLUTCH BOA/CG-150/FT-150\r\n', 35, 38.5, 'uploads/thumb_IMG-67 (2).png', '-', 269, 3, 1),
(257, '7425602001188', NULL, 'CABLE DE CLUTCH DAKAR200/XL200\r\n', 30, 33, 'uploads/thumb_IMG-67 (2).png', '-', 271, 3, 1),
(258, '7425602001195', NULL, 'CABLE DE CLUTCH FT125/MAGNUM-150/KB-150\r\n', 28, 30.8, 'uploads/thumb_IMG-67 (2).png', '-', 269, 3, 1),
(259, '7425602001256', NULL, 'CABLE DE VELOCIMETRO BOA/ FT-150/TX-200\r\n', 30, 33, 'uploads/thumb_IMG-67 (2).png', '-', 189, 3, 1),
(260, '7425602001263', NULL, 'CABLE DE VELOCIMETRO UM/DM/JARA/XM200\r\n', 35, 38.5, 'uploads/thumb_IMG-67 (2).png', '-', 226, 3, 1),
(261, '7425602001270', NULL, 'CABLE DE ACELERADOR UM/JARA/XM2OO\r\n', 25, 27.5, 'uploads/thumb_IMG-67 (2).png', '-', 220, 3, 1),
(262, '7425602001249', NULL, 'CABLE DE ACELERADOR FT125/150\r\n', 27, 29.7, 'uploads/thumb_IMG-67 (2).png', '-', 239, 3, 1),
(263, '7425602004592', NULL, 'CARBURADOR HJ125-7\r\n', 300, 330, 'uploads/thumb_IMG-74 (2).png', '-', 6, 3, 1),
(264, '7425602000686', NULL, 'JUEGO DE CARBONES GXT/KMF200/GN125H\r\n', 35, 38.5, 'uploads/thumb_IMG-76 (2).png', '-', 120, 3, 1),
(265, '7425602004806', NULL, 'KIT DE GUIA DE CADENA TIEMPO GXT/KMF 200 GN125H\r\n', 95, 104.5, 'uploads/thumb_IMG-79 (2).png', '-', 152, 3, 1),
(266, '7425602000440', NULL, 'BOMBA DE FRENO SUPERIOR GXT200/UNIVERSAL (METALICA)\r\n', 200, 220, 'uploads/thumb_IMG-51 (2).png', '-', 51, 3, 1),
(267, '7425602000761', NULL, 'PATA DE ARRANQUE YBR 125\r\n', 120, 132, 'uploads/thumb_IMG-55 (2).png', '-', 85, 3, 1),
(268, '7425602000747', NULL, 'PATA DE ARRANQUE CGL125\r\n', 105, 115.5, 'uploads/thumb_IMG-52 (2).png', '-', 32, 3, 1),
(269, '7425602000723', NULL, 'PATA DE ARRANQUE CG\r\n', 110, 121, 'uploads/thumb_IMG-53 (2).png', '-', 15, 3, 1),
(270, '7425602000716', NULL, 'PATA DE ARRANQUE DT175\r\n', 125, 137.5, 'uploads/thumb_IMG-56 (2).png', '-', 15, 3, 1),
(271, '7425602000778', NULL, 'PATA DE ARRANQUE AX100\r\n', 105, 115.5, 'uploads/thumb_IMG-54 (2).png', '-', 12, 3, 1),
(272, '7425602000754', NULL, 'PATA DE ARRANQUE CG125/FT125\r\n', 110, 121, 'uploads/thumb_IMG-57 (2).png', '-', 20, 3, 1),
(273, '7425602000709', NULL, 'PATA DE ARRANQUE GXT200/KMF200\r\n', 125, 137.5, 'uploads/thumb_IMG-58 (2).png', '-', 73, 3, 1),
(274, '7425602000730', NULL, 'PATA DE ARRANQUE GYA200/XM200\r\n', 125, 137.5, 'uploads/thumb_IMG-59 (2).png', '-', 163, 3, 1),
(275, '7425602000037', NULL, 'PATA DE CAMBIO HJ125-7/DOBLE\r\n', 120, 132, 'uploads/thumb_IMG-66 (2).png', '-', 20, 3, 1),
(276, '7425602000051', NULL, 'PATA DE CAMBIO CG DOBLE\r\n', 60, 66, 'uploads/thumb_IMG-64 (2).png', '-', 40, 3, 1),
(277, '7425602000013', NULL, 'PATA DE CAMBIO HJ125-7/SIMPLE\r\n', 125, 137.5, 'uploads/thumb_IMG-61 (2).png', '-', 25, 3, 1),
(278, '7425602000044', NULL, 'PATA DE CAMBIO YBR 125\r\n', 50, 55, 'uploads/thumb_IMG-62 (2).png', '-', 23, 3, 1),
(279, '7425602000020', NULL, 'PATA DE CAMBIO CG125/CG200\r\n', 45, 49.5, 'uploads/thumb_IMG-63 (2).png', '-', 86, 3, 1),
(280, '7425602000365', NULL, 'REGULADOR DE VOLTAJE YBR125\r\n', 120, 132, 'uploads/thumb_IMG-114.png', '-', 57, 3, 1),
(281, '7425602000341', NULL, 'REGULADOR DE VOLTAJE CG125/CG150 4 LINEAS (MACHO)\r\n', 100, 110, 'uploads/thumb_IMG-97.png', '-', 58, 3, 1),
(282, '7425602000372', NULL, 'REGULADOR DE VOLTAJE GN125H\r\n', 150, 165, 'uploads/thumb_IMG-98.png', '-', 28, 3, 1),
(283, '7425602000426', NULL, 'REGULADOR DE VOLTAJE FZ16 \r\n', 150, 165, 'uploads/thumb_IMG-120.png', '-', 6, 3, 1),
(284, '7425602000389', NULL, 'REGULADOR DE VOLTAJE GY6 (SCOOTER)\r\n', 100, 110, 'uploads/thumb_IMG-119.png', '-', 29, 3, 1),
(285, '7425602000396', NULL, 'REGULADOR DE VOLTAJE CG CON CONDENSADOR DAKAR200/XM-200\r\n', 120, 132, 'uploads/thumb_IMG-119.png', '-', 18, 3, 1),
(286, '7425602000358', NULL, 'REGULADOR DE VOLTAJE CG125/CG150 4 LINEAS (HEMBRA)\r\n', 100, 110, 'uploads/thumb_IMG-115.png', '-', 56, 3, 1),
(287, '7425602000402', NULL, 'REGULADOR DE VOLTAJE GXT200/KMF200\r\n', 150, 165, 'uploads/thumb_IMG-121.png', '-', 73, 3, 1),
(288, '7425602000419', NULL, 'REGULADOR DE VOLTAJE CG150\r\n', 95, 104.5, 'uploads/thumb_IMG-111.png', '-', 45, 3, 1),
(289, '7425602000334', NULL, 'REGULADOR DE VOLTAJE YUMBO 200\r\n', 75, 82.5, 'uploads/thumb_IMG-104.png', '-', 69, 3, 1),
(290, '7425602000785', NULL, 'CHICHARRA DE ENCENDIDO CG\r\n', 80, 88, 'uploads/thumb_IMG-102.png', '-', 89, 3, 1),
(291, '7425602000808', NULL, 'CHICARRA DE ENCENDIDO  GY6 (SCOOTER)\r\n', 85, 93.5, 'uploads/thumb_IMG-107.png', '-', 99, 3, 1),
(292, '7425602000792', NULL, 'CHICARRA DE ENCENDIDO YBR 125\r\n', 95, 104.5, 'uploads/thumb_IMG-106.png', '-', 77, 3, 1),
(293, '7425602000839', NULL, 'CHICHARRA DE ENCENDIDO HJ125-7\r\n', 80, 88, 'uploads/thumb_IMG-103.png', '-', 95, 3, 1),
(294, '7425602000815', NULL, 'CHICHARRA DE ENCENDIDO GN125H\r\n', 95, 104.5, 'uploads/thumb_IMG-112.png', '-', 49, 3, 1),
(295, '7425602000822', NULL, 'CHICHARRA DE ENCENDIDO GXT/KMF 200\r\n', 95, 104.5, 'uploads/thumb_IMG-105.png', '-', 96, 3, 1),
(296, '7425602000174', NULL, 'BOBINA DE ALTA TENSION GY6\r\n', 85, 93.5, 'uploads/thumb_IMG-113.png', '-', 47, 3, 1),
(297, '7425602000204', NULL, 'BOBINA DE ALTA TENSION HJ125-7 (UNIVERSAL)\r\n', 85, 93.5, 'uploads/thumb_IMG-118.png', '-', 24, 3, 1),
(298, '7425602000181', NULL, 'BOBINA DE ALTA TENSION GN125H\r\n', 120, 132, 'uploads/thumb_IMG-110.png', '-', 125, 3, 1),
(299, '7425602000235', NULL, 'CDI PULSAR 135\r\n', 300, 330, 'uploads/thumb_IMG-99.png', '-', 34, 3, 1),
(300, '7425602000259', NULL, 'CDI XTZ125 2009-2015\r\n', 600, 660, 'uploads/thumb_IMG-100.png', '-', 46, 3, 1),
(301, '7425602000273', NULL, 'CDI GY6 (SCOOTER)\r\n', 65, 71.5, 'uploads/thumb_IMG-101.png', '-', 38, 3, 1),
(302, '7425602000280', NULL, 'CDI CG/HJ125-7\r\n', 70, 77, 'uploads/thumb_IMG-108.png', '-', 37, 3, 1),
(303, '7425602000297', NULL, 'CDI CRUX110\r\n', 350, 385, 'uploads/thumb_IMG-109.png', '-', 51, 3, 1),
(304, '7425602000310', NULL, 'CDI UNIT GXT250\r\n', 400, 440, 'uploads/thumb_IMG-116.png', '-', 69, 3, 1),
(305, '7425602000303', NULL, 'CDI UNIT GXT200\r\n', 275, 302.5, 'uploads/thumb_IMG-117.png', '-', 68, 3, 1),
(306, '7425602000228', NULL, 'CDI PLATINA125\r\n', 350, 385, 'uploads/thumb_IMG-22 (2).png', '-', 36, 3, 1),
(307, '560271307', NULL, 'RIN 1.85X17\r\n', 260, 286, 'uploads/thumb_IMG-82 (2).png', '-', 45, 3, 1),
(308, '560271310', NULL, 'RIN 1.85X18\r\n', 260, 286, 'uploads/thumb_IMG-81 (2).png', '-', 95, 3, 1),
(309, '560271311', NULL, 'RIN 1.60X21\r\n', 250, 275, 'uploads/thumb_IMG-84 (2).png', '-', 20, 3, 1),
(310, '560271313', NULL, 'RIN 2.15X17 ', 275, 302.5, 'uploads/thumb_IMG-83 (2).png', '-', 20, 3, 1),
(311, 'FTHJAZ', NULL, 'TANQUE DE COMBUSTIBLE(AZUL) HJ-125\r\n', 700, 770, 'uploads/thumb_IMG-73 (2).png', '-', 6, 3, 1),
(312, 'FTHJNE', NULL, 'TANQUE DE COMBUSTIBLE(NEGRO) HJ-125\r\n', 700, 770, 'uploads/thumb_IMG-71 (2).png', '-', 0, 3, 1),
(313, 'FTHJRO', NULL, 'TANQUE DE COMBUSTIBLE(ROJO) HJ-125\r\n', 700, 770, 'uploads/thumb_IMG-72 (2).png', '-', 5, 3, 1),
(314, 'FTYBRAZ', NULL, 'TANQUE DE COMBUSTIBLE(AZUL) YBR 125\r\n', 750, 825, 'uploads/thumb_IMG-70 (2).png', '-', 0, 3, 1),
(315, 'FTYBRNE', NULL, 'TANQUE DE COMBUSTIBLE(NEGRO) YBR 125\r\n', 750, 825, 'uploads/thumb_IMG-68 (2).png', '-', 0, 3, 1),
(316, 'FTYBRRO', NULL, 'TANQUE DE COMBUSTIBLE(ROJO) YBR 125\r\n', 750, 825, 'uploads/thumb_IMG-69 (2).png', '-', 0, 3, 1),
(317, '206160', NULL, 'DIAFRAGMA CARBURADOR BAJAJ PULSAR 180/200 - TVS 160RTR', 95, 104.5, 'uploads/thumb_IMG-55 (4).png', '-', 0, 4, 2),
(318, '206192', NULL, 'DIAFRAGMA CARBURADOR DISCOVER 125 - 135\r\n', 110, 121, 'uploads/thumb_IMG-56.png', '-', 2, 4, 2),
(319, '206194', NULL, 'DIAFRAGMA CARBURADOR YAMAHA FZ 16\r\n', 100, 110, 'uploads/thumb_IMG-55 (4).png', '-', 0, 4, 2),
(320, '206195', NULL, 'DIAFRAGMA CARBURADOR HONDA CBF 125\r\n', 130, 143, 'uploads/thumb_IMG-55 (4).png', '-', 8, 4, 2),
(321, '206218', NULL, 'DIAFRAGMA CARBURADOR PULSAR 135 - DISCOVER 125ST\r\n', 110, 121, 'uploads/thumb_IMG-55 (4).png', '-', 6, 3, 2),
(322, '101423', NULL, 'SEGURO DE SPROCKET BOXER CAL.2mm\r\n', 15, 16.5, 'uploads/thumb_IMG-59 (3).png', '-', 0, 4, 2),
(323, '101645', NULL, 'SEGURO DE SPROCKET YBR-125 / CG (UNIVERSAL)', 12, 13.2, 'uploads/thumb_IMG-11 (4).png', '-', 535, 4, 2),
(324, '7425602000853', NULL, 'CADENA DE TRACCION 428H-124L DORADA\r\n', 140, 154, 'uploads/thumb_IMG-40.png', '-', 103, 3, 1),
(325, 'KCA32', NULL, 'KIT COMPLETO DE EMPAQUES DISCOVER 125 ST M.N.\r\n', 65, 71.5, 'uploads/thumb_IMG-143.png', '-', 10, 4, 2),
(326, 'KCS32', NULL, 'KIT COMPLETO DE EMPAQUES AX 100-4 \r\n', 75, 82.5, 'uploads/thumb_IMG-142.png', '-', 10, 4, 2),
(327, 'KCA36', NULL, 'KIT COMPLETO  DE EMPAQUES MOTOTAXI RE 205\r\n', 90, 99, 'uploads/thumb_IMG-140.png', '-', 5, 4, 2),
(328, '210345', NULL, 'KIT COMPLETO DE EMPAQUES BOXER 100\r\n', 65, 71.5, 'uploads/thumb_IMG-141.png', '-', 10, 4, 2),
(329, 'KCH48', NULL, 'KIT COMPLETO DE EMPAQUES SUPER SPLENDOR NXG\r\n', 110, 121, 'uploads/thumb_IMG-146.png', '-', 6, 4, 2),
(330, '210349', NULL, 'KIT COMPLETO DE EMPAQUES PULSAR 180 SIN TAPA VALVULA\r\n', 85, 93.5, 'uploads/thumb_IMG-130.png', '-', 6, 4, 2),
(331, '210049', NULL, 'KIT COMPLETO DE EMPAQUES XL 200\r\n', 90, 99, 'uploads/thumb_IMG-132.png', '-', 17, 4, 2),
(332, 'KCA27', NULL, 'KIT COMPLETO DE EMPAQUES PLATINA 125\r\n', 65, 71.5, 'uploads/thumb_IMG-133.png', '-', 20, 4, 2),
(333, 'KCA37', NULL, 'KIT COMPLETO DE EMPAQUES DISCOVER 150\r\n', 65, 71.5, 'uploads/thumb_IMG-137.png', '-', 10, 4, 2),
(334, '210014', NULL, 'KIT COMPLETO DE EMPAQUES XT 225\r\n', 100, 110, 'uploads/thumb_IMG-144.png', '-', 10, 4, 2),
(335, '210018', NULL, 'KIT COMPLETO DE EMPAQUES CRUX-110\r\n', 70, 77, 'uploads/thumb_IMG-139.png', '-', 10, 4, 2),
(336, 'KCH33', NULL, 'KIT COMPLETO DE EMPAQUES CBF 125\r\n', 90, 99, 'uploads/thumb_IMG-135.png', '-', 10, 4, 2),
(337, '210029', NULL, 'KIT COMPLETO DE EMPAQUES TS 185 ER\r\n', 85, 93.5, 'uploads/thumb_IMG-150.png', '-', 10, 4, 2),
(338, 'KCY29', NULL, 'KIT COMPLETO DE EMPAQUES XT 250\r\n', 110, 121, 'uploads/thumb_IMG-152.png', '-', 10, 4, 2),
(339, 'KCA28', NULL, 'KIT COMPLETO DE EMPAQUES DISCOVER 100\r\n', 65, 71.5, 'uploads/thumb_IMG-148.png', '-', 10, 4, 2),
(340, 'KCA18', NULL, 'KIT COMPLETO DE EMPAQUES DISCOVER 135\r\n', 65, 71.5, 'uploads/thumb_IMG-145.png', '-', 6, 4, 2),
(341, 'KCA34', NULL, 'KIT COMPLETO DE EMPAQUES PULSAR 200 NS\r\n', 100, 110, 'uploads/thumb_IMG-154.png', '-', 3, 4, 2),
(342, 'KCY37', NULL, 'KIT COMPLETO DE EMPAQUES FZ 16\r\n', 90, 99, 'uploads/thumb_IMG-138.png', '-', 9, 4, 2),
(343, 'KCA14', NULL, 'KIT COMPLETO DE EMPAQUES DISCOVER 125\r\n', 65, 71.5, 'uploads/thumb_IMG-136.png', '-', 10, 4, 2),
(344, 'KCA23', NULL, 'KIT COMPLETO DE EMPAQUES XCD 125\r\n', 65, 71.5, 'uploads/thumb_IMG-155.png', '-', 10, 4, 2),
(345, 'KCA26', NULL, 'KIT COMPLETO DE EMPAQUES PULSAR 135\r\n', 65, 71.5, 'uploads/thumb_IMG-153.png', '-', 13, 4, 2),
(346, '210039', NULL, 'KIT COMPLETO DE EMPAQUES XL 185 \r\n', 90, 99, 'uploads/thumb_IMG-151.png', '-', 20, 4, 2),
(347, 'KMA26', NULL, 'KIT MEDIO DE EMPAQUES PULSAR 135\r\n', 35, 38.5, 'uploads/thumb_IMG-157.png', '-', 105, 4, 2),
(348, '210086', NULL, 'KIT MEDIO DE EMPAQUES YBR 125\r\n', 35, 38.5, 'uploads/thumb_IMG-169.png', '-', 50, 4, 2),
(349, 'KMA15', NULL, 'KIT MEDIO DE EMPAQUES BOXER PLATINA\r\n', 30, 33, 'uploads/thumb_IMG-159.png', '-', 50, 4, 2),
(350, 'KMS34', NULL, 'KIT MEDIO DE EMPAQUES AX-100- 4\r\n', 35, 38.5, 'uploads/thumb_IMG-167.png', '-', 10, 4, 2),
(351, '210083', NULL, 'KIT MEDIO DE EMPAQUES XT 200, XT 225\r\n', 65, 71.5, 'uploads/thumb_IMG-173.png', '-', 8, 4, 2),
(352, '210091', NULL, 'KIT MEDIO DE EMPAQUES AX 100 CON TORQUE Y COMPLEMETO', 30, 33, 'uploads/thumb_IMG-164.png', '-', 10, 4, 2),
(353, 'KMA32', NULL, 'KIT MEDIO DE EMPAQUES DISCOVER 125 ST M.N.\r\n', 35, 38.5, 'uploads/thumb_IMG-163.png', '-', 20, 4, 2),
(354, '210131', NULL, 'KIT MEDIO DE EMPAQUES PULSAR 180\r\n', 35, 38.5, 'uploads/thumb_IMG-156.png', '-', 44, 4, 2),
(355, '210116', NULL, 'KIT MEDIO DE EMPAQUES XL 200\r\n', 35, 38.5, 'uploads/thumb_IMG-166.png', '-', 24, 4, 2),
(356, 'KMA14', NULL, 'KIT MEDIO DE EMPAQUES DISCOVER 125\r\n', 35, 38.5, 'uploads/thumb_IMG-177.png', '-', 20, 4, 2),
(357, 'KMH37', NULL, 'KIT MEDIO DE EMPAQUES CBF 125\r\n', 45, 49.5, 'uploads/thumb_IMG-171.png', '-', 10, 4, 2),
(358, 'KMY32', NULL, 'KIT MEDIO DE EMPAQUES FZ 16\r\n', 37, 40.7, 'uploads/thumb_IMG-168.png', '-', 44, 4, 2),
(359, '210100', NULL, 'KIT MEDIO DE EMPAQUES GN 125\r\n', 33, 36.3, 'uploads/thumb_IMG-161.png', '-', 14, 4, 2),
(360, '210129', NULL, 'KIT MEDIO DE EMPAQUES BOXER 100\r\n', 30, 33, 'uploads/thumb_IMG-162.png', '-', 40, 4, 2),
(361, 'KMY22', NULL, 'KIT MEDIO DE EMPAQUES XT 250\r\n', 65, 71.5, 'uploads/thumb_IMG-172.png', '-', 10, 4, 2),
(362, 'KMA16', NULL, 'KIT MEDIO DE EMPAQUES DISCOVER 135\r\n\r\n', 35, 38.5, 'uploads/thumb_IMG-176.png', '-', 14, 4, 2),
(363, 'KMA33', NULL, 'KIT MEDIO DE EMPAQUES MOTOTAXI RE 205\r\n', 37, 40.7, 'uploads/thumb_IMG-158.png', '-', 31, 4, 2),
(364, 'KMS12', NULL, 'KIT MEDIO DE EMPAQUE TS 185 CON CONICO Y COMPLEMENTO', 40, 44, 'uploads/thumb_IMG-174.png', '-', 20, 4, 2),
(365, 'KMAK13', NULL, 'KIT MEDIO DE EMPAQUE CG 150', 35, 38.5, 'uploads/thumb_IMG-175.png', '-', 80, 4, 2),
(366, 'CIY', NULL, 'EMPAQUE DE CILINDRO DTK 175', 9, 9.9, 'uploads/thumb_IMG-178.png', '-', 50, 4, 2),
(367, 'V274', NULL, 'EMPAQUE DE TAPADERA DE VALVULA PLATINA 125\r\n', 33, 36.3, 'uploads/thumb_IMG-170.png', '-', 28, 4, 2),
(368, 'V260', NULL, 'EMPAQUE DE TAPADERA DE VALVULA PULSAR 135 -DISCOVER 100 - BOXER BM 150\r\n', 35, 38.5, 'uploads/thumb_IMG-165.png', '-', 15, 4, 2),
(369, 'CA22', NULL, 'EMPAQUE DE TAPADERA DE CLUTCH PULSAR 135\r\n', 15, 16.5, 'uploads/thumb_IMG-131.png', '-', 40, 4, 2),
(370, 'CY32', NULL, 'EMPAQUE DE TAPADERA DE CLUTCH FZ 16\r\n', 20, 22, 'uploads/thumb_IMG-147.png', '-', 20, 4, 2),
(371, 'CA21', NULL, 'EMPAQUE DE TAPADERA DE CLUTCH PLATINA 125\r\n', 15, 16.5, 'uploads/thumb_IMG-149.png', '-', 40, 4, 2),
(372, '213108', NULL, 'KIT DE RETENEDORES DISCOVER 125/135\r\n', 85, 93.5, 'uploads/thumb_IMG-66 (4).png', '-', 2, 4, 2),
(373, '213050', NULL, 'KIT DE RETENEDORES BOXER\r\n', 85, 93.5, 'uploads/thumb_IMG-68 (4).png', '-', 1, 4, 2),
(374, '213038', NULL, 'KIT DE RETENEDORES AX100-115\r\n', 95, 104.5, 'uploads/thumb_IMG-69 (4).png', '-', 3, 4, 2),
(375, '213079', NULL, 'KIT DE RETENEDORES YAMAHA YBR 125\r\n', 85, 93.5, 'uploads/thumb_IMG-71 (4).png', '-', 1, 4, 2),
(376, '213126', NULL, 'KIT DE RETENEDORES PULSAR 135 / XCD 125\r\n', 85, 93.5, 'uploads/thumb_IMG-72 (4).png', '-', 6, 4, 2),
(377, '213073', NULL, 'KIT DE RETENEDORES GN 125\r\n', 85, 93.5, 'uploads/thumb_IMG-2 (4).png', '-', 1, 4, 2),
(378, '213077', NULL, 'KIT DE RETENEDORES YAMAHA', 85, 93.5, 'uploads/thumb_IMG-67 (4).png', '-', 12, 4, 2),
(379, 'P10025', NULL, 'PASTILLA DE FRENO DELANTERA CBF 125\r\n', 90, 99, 'uploads/thumb_IMG-74 (4).png', '-', 0, 4, 2),
(380, 'P10051', NULL, 'PASTILLAS DE FRENO TRASERA HUNK TRILLER', 90, 99, 'uploads/thumb_IMG-3 (4).png', '-', 41, 4, 2),
(381, 'P10045', NULL, 'PASTILLA DE FRENO TRASERA PULSAR 200 NS', 90, 99, 'uploads/thumb_IMG-220.png', '-', 1, 4, 2),
(382, '207113', NULL, 'MANUBRIO CROMADO EN-125, UNIVERSAL \r\n\r\n', 140, 154, 'uploads/thumb_IMG-180.png', '-', 200, 4, 2),
(383, 'N110.90.17', NULL, ' NEUMATICO 110-90-17 BJ\r\n\r\n', 65, 71.5, 'uploads/thumb_IMG-38 (4).png', '-', 6046, 4, 3),
(384, 'N300.17', NULL, 'NEUMATICO 300-17 BJ\r\n', 45, 49.5, 'uploads/thumb_IMG-38 (4).png', '-', 5471, 4, 3),
(385, 'N300.18', NULL, 'NEUMATICO 300-18 BJ\r\n', 45, 49.5, 'uploads/thumb_IMG-38 (4).png', '-', 8571, 4, 3),
(386, 'N300.21', NULL, 'NEUMATICO 300-21 BJ\r\n', 60, 66, 'uploads/thumb_IMG-38 (4).png', '-', 1521, 4, 3),
(387, 'N410.18', NULL, 'NEUMATICO 410-18 BJ\r\n', 60, 66, 'uploads/thumb_IMG-38 (4).png', '-', 8996, 4, 3),
(388, '216018', NULL, 'FILTRO DE AIRE UNIVERSAL 60mm (INTEX)\r\n', 65, 71.5, 'uploads/thumb_IMG-114 (2).png', '-', 44, 4, 2),
(389, '216061', NULL, 'FILTRO DE AIRE GXT/KMF 200 DR 200\r\n', 50, 55, 'uploads/thumb_IMG-45 (4).png', '-', 9, 4, 2),
(390, '216005', NULL, 'FILTRO DE AIRE XTZ125\r\n', 75, 82.5, 'uploads/thumb_IMG-216.png', '-', 11, 4, 2),
(391, '216008', NULL, 'FILTRO DE AIRE YBR125\r\n', 85, 93.5, 'uploads/thumb_IMG-51 (4).png', '-', 21, 4, 2),
(392, '101188', NULL, 'TAPÓN DE ACEITE CG', 24, 26.4, 'uploads/thumb_IMG-218.png', '-', 0, 4, 2),
(393, '101204', NULL, 'TORNILLO REGULADOR DE CABLE DE FRENO DELANTERO UNIVERSAL', 24, 26.4, 'uploads/thumb_IMG-217.png', '-', 10, 4, 2),
(394, '101229', NULL, 'CORTADOR DE CADENA 428/520', 385, 423.5, 'uploads/thumb_IMG-209.png', '-', 0, 4, 2),
(395, '105216', NULL, 'KIT DE REPARACION DE MONOSHOCK FZ-16', 380, 418, 'uploads/thumb_IMG-206.png', '-', 1, 4, 2),
(396, '105214', NULL, 'GUIA DE VALVULA DE ADMISION FZ16', 100, 110, 'uploads/thumb_IMG-208.png', '-', 3, 4, 2),
(397, '105215', NULL, 'GUIA DE VALVULA DE ESCAPE FZ16', 100, 110, 'uploads/thumb_IMG-207.png', '-', 0, 4, 2),
(398, '301021', NULL, 'JUEGO DE LLAVES COMBINADAS DE 6/22mm (8 UNDS)\r\n', 250, 275, 'uploads/thumb_IMG-197.png', '-', 1, 3, 1),
(399, '301004', NULL, 'LLAVE EN T DE 6mm\r\n', 40, 44, 'uploads/thumb_IMG-196.png', '-', 0, 4, 2),
(400, '301005', NULL, 'LLAVE EN T DE 8mm\r\n', 50, 55, 'uploads/thumb_IMG-196.png', '-', 0, 4, 2),
(401, '301043', NULL, 'LLAVE EN T DE 7MM\r\n', 45, 49.5, 'uploads/thumb_IMG-196.png', '-', 0, 4, 2),
(402, '206072', NULL, 'HULE PARA BATERIA CG UNIVERSAL (PAQUETE  10 UNI)\r\n', 20, 22, 'uploads/thumb_IMG-195.png', '-', 54, 4, 2),
(403, '206073', NULL, 'HULE PARA BATERIA DT 125/175 (PAQUETE  10 UNI)\r\n', 17, 18.7, 'uploads/thumb_IMG-10 (3).png', '-', 14, 4, 2),
(404, '206315', NULL, 'VÍA IZQUIERDA CRUX 110\r\n', 45, 49.5, 'uploads/thumb_IMG-194.png', '-', 225, 4, 2),
(405, '206316', NULL, 'VIA DERECHA CRUX 110', 45, 49.5, 'uploads/thumb_IMG-194.png', '-', 223, 4, 2),
(406, '206245', NULL, 'VIA AX100-4\r\n', 45, 49.5, 'uploads/thumb_IMG-97 (2).png', '-', 59, 4, 2),
(407, '206277', NULL, 'VIA DERECHA DISCOVER 125ST\r\n', 60, 66, 'uploads/thumb_IMG-96 (2).png', '-', 80, 4, 2),
(408, '206276', NULL, 'VIA IZQUIERDA DISCOVER 125ST\r\n', 60, 66, 'uploads/thumb_IMG-96 (2).png', '-', 60, 4, 2),
(409, '206329', NULL, 'VIA LED IZQUIERDA UNIVERSAL', 55, 60.5, 'uploads/thumb_IMG-95 (2).png', '-', 90, 3, 1),
(410, '206328', NULL, 'VIA LED UNIVERSAL', 55, 60.5, 'uploads/thumb_IMG-95 (2).png', '-', 90, 4, 2),
(411, '206295', NULL, 'VIA DERECHA PULSAR 135LS\r\n', 55, 60.5, 'uploads/thumb_IMG-94 (2).png', '-', 20, 4, 2),
(412, '206287', NULL, 'VIA IZQUIERDA PULSAR 135LS\r\n', 55, 60.5, 'uploads/thumb_IMG-94 (2).png', '-', 20, 4, 2),
(413, '206066', NULL, 'VIA GN-125\r\n', 60, 66, 'uploads/thumb_IMG-15 (3).png', '-', 22, 4, 2),
(414, '206006', NULL, 'UNION DE ESCAPE DT 175 (PAQUETE  10 UNI)\r\n', 18, 19.8, 'uploads/thumb_IMG-193.png', '-', 40, 4, 2),
(415, '101269', NULL, 'EXTRACTOR DE VOLANTE DT 175', 115, 126.5, 'uploads/thumb_IMG-192.png', '-', 0, 4, 2),
(416, '206024', NULL, 'MANGUERA DE FILTRO DE AIRE DT-175, DT 125\r\n', 50, 55, 'uploads/thumb_IMG-191.png', '-', 16, 4, 2),
(417, '301042', NULL, 'LLAVE AJUSTABLE DE 6"\r\n', 48, 52.8, 'uploads/thumb_IMG-190.png', '-', 15, 4, 2),
(418, '301041', NULL, 'LLAVE AJUSTABLE DE 12"\r\n', 100, 110, 'uploads/thumb_IMG-189.png', '-', 2, 4, 2),
(419, '206140', NULL, 'HUESO DESLIZADOR DE CADENA DT 175\r\n', 45, 49.5, 'uploads/thumb_IMG-188.png', '-', 90, 4, 2),
(420, '101588', NULL, 'TAPÓN DE DRENAJE DE ACEITE CG-125 CG', 20, 22, 'uploads/thumb_IMG-187.png', '-', 10, 4, 2),
(421, '101589', NULL, 'TAPÓN DE VALVULA GXT/KMF - 200 UNIVERSAL\r\n', 18, 19.8, 'uploads/thumb_IMG-182.png', '-', 20, 4, 2),
(422, '206086', NULL, 'HULE DESLIZADOR DE CADENA DT125\r\n', 65, 71.5, 'uploads/thumb_IMG-186.png', '-', 40, 4, 2),
(423, '101463', NULL, 'PATA DE BUFA CG\r\n', 38, 41.8, 'uploads/thumb_IMG-185.png', '-', 31, 4, 2),
(424, '101498', NULL, 'PATA DE BUFA BOXER - 100\r\n', 50, 55, 'uploads/thumb_IMG-183.png', '-', 37, 4, 2),
(425, '101586', NULL, 'PATA DE BUFA CRUX 110 / YBR-125 ', 33, 36.6, 'uploads/thumb_IMG-184.png', '-', 1, 4, 2),
(426, '101351', NULL, 'VARILLA DE FRENO YBR 125\r\n', 33, 36.3, 'uploads/thumb_IMG-179.png', '-', 165, 4, 2),
(427, '206167', NULL, 'HULE DE CATARINA YBR 125 / CGL125 \r\n', 40, 44, 'uploads/thumb_IMG-115 (2).png', '-', 81, 4, 2),
(428, '201180', NULL, 'RESORTE DE APOYO LATERAL HJ-125-7 EN-125 GN-125 BYQ-150 (PAQUETE  10 UNI)', 12, 13.2, 'uploads/thumb_IMG-112 (2).png', '-', 360, 4, 2),
(429, '201283', NULL, 'RESORTE DE PATA DE EMBANQUE PULSAR 180/135 (PAQUETE  10 UNI)\r\n', 17, 18.7, 'uploads/thumb_IMG-111 (2).png', '-', 120, 4, 2),
(430, '201242', NULL, 'RESORTE DE PEDAL DE FRENO CG 125 (PAQUETE  10 UNI)', 9.5, 10.45, 'uploads/thumb_IMG-111 (2).png', '-', 384, 4, 2),
(431, '201207', NULL, 'RESORTE DE APOYO LATERAL YBR 125 (PAQUETE  10 UNI)\r\n', 20, 22, 'uploads/thumb_IMG-110 (2).png', '-', 60, 4, 2),
(432, '101466', NULL, 'KIT REPARACION DE VARILLA DE FRENO UNIVERSAL\r\n', 25, 27.5, 'uploads/thumb_IMG-109 (2).png', '-', 189, 4, 2),
(433, '101619', NULL, 'BUJE DE AMORTIGUADOR 12mm\r\n', 22, 24.2, 'uploads/thumb_IMG-106 (2).png', '-', 394, 4, 2),
(434, 'A10001  ', NULL, 'FRICCIÓN DE FRENO DE BUFA PEQUEÑA KB 150\r\n', 80, 88, 'uploads/thumb_IMG-105 (2).png', '-', 176, 4, 2),
(435, 'A10022', NULL, 'FRICCIÓN DE FRENO DE BUFA PEQUEÑA AX 100', 80, 88, 'uploads/thumb_IMG-104 (2).png', '-', 415, 4, 2),
(436, '105122', NULL, 'BUJE DE PORTA CATARINA PLATINA 125\r\n', 100, 110, 'uploads/thumb_IMG-103 (2).png', '-', 12, 4, 2),
(437, '101083', NULL, 'TORNILLO DE TAPADERA DE  MOTOR ORIGINAL M6X30 (PAQUETE  10 UNI)\r\n', 6.5, 7.15, 'uploads/thumb_IMG-102 (2).png', '-', 318, 4, 2),
(438, '105113', NULL, 'BUJE DE PORTA CATARINA GN 125 - HJ 125-7\r\n', 40, 44, 'uploads/thumb_IMG-101 (2).png', '-', 11, 4, 2),
(439, '101084', NULL, 'TORNILLO DE TAPADERA DE MOTOR ORIGINAL M6X35 (PAQUETE  10 UNI)\r\n', 7, 7.7, 'uploads/thumb_IMG-100 (2).png', '-', 298, 4, 2),
(440, '101266', NULL, 'EJE DE TAPADERA DE BUFA BOXER - 100\r\n', 55, 60.5, 'uploads/thumb_IMG-99 (2).png', '-', 6, 4, 2),
(441, '101079', NULL, 'EJE DE TAPADERA DE BUFA DT 125/175 \r\n', 50, 55, 'uploads/thumb_IMG-98 (2).png', '-', 16, 4, 2),
(442, '105172', NULL, 'BUJE DE TIJERA XM 200 JARA 200 - C/U\r\n', 45, 49.5, 'uploads/thumb_IMG-93 (2).png', '-', 286, 4, 2),
(443, '101643', NULL, 'KIT DE TORNILLOS DE CATARINA CGL\r\n', 40, 44, 'uploads/thumb_IMG-92 (2).png', '-', 93, 4, 2),
(444, '101375', NULL, 'KIT DE TORNILLOS DE CATARINA YBR 125, CRUX 110\r\n', 40, 44, 'uploads/thumb_IMG-91 (2).png', '-', 700, 4, 2),
(445, '105084', NULL, 'BUJE DE CATARINA CG / CG125 - C/U\r\n', 40, 44, 'uploads/thumb_IMG-90 (2).png', '-', 394, 4, 2),
(446, '105083', NULL, 'BUJE DE PORTA CATARINA PULSAR/DISCOVER 135 \r\n', 125, 137.5, 'uploads/thumb_IMG-89 (2).png', '-', 3, 4, 2),
(447, '101646', NULL, 'KIT DE TORNILLOS DE MONOSHOCK GTX 200 KMF 200', 45, 49.5, 'uploads/thumb_IMG-88 (2).png', '-', 887, 4, 2),
(448, '101640', NULL, 'EJE DE TAPADERA DE BUFA GN 125H\r\n', 35, 38.5, 'uploads/thumb_IMG-87 (2).png', '-', 10, 4, 2),
(449, '105099', NULL, 'BUJE DE PORTA CATARINA SUPER SPLENDOR\r\n', 45, 49.5, 'uploads/thumb_IMG-86 (2).png', '-', 7, 4, 2),
(450, '101236', NULL, 'TENSOR DE CADENA DE TRACCIÓN IZQ PULSAR 180 DISCOVER 135 \r\n', 45, 49.5, 'uploads/thumb_IMG-85 (2).png', '-', 5, 4, 2),
(451, '101235', NULL, 'TENSOR DE CADENA DE TRACCION DER. PULSAR 180 DISCOVER 135 \r\n', 45, 49.5, 'uploads/thumb_IMG-84 (3).png', '-', 34, 4, 2),
(452, '101073', NULL, 'VARILLA DE FRENO PEQUEÑA SUZUKI AX100/115\r\n', 33, 36.3, 'uploads/thumb_IMG-77 (4).png', '-', 120, 4, 2),
(453, '208002', NULL, 'BOBINA DE ENCENDIDO DT 175\r\n', 80, 88, 'uploads/thumb_IMG-64 (4).png', '-', 0, 4, 2),
(454, '208003', NULL, 'BOBINA DE ENCENDIDO ESPECIAL DT 125/175\r\n', 80, 88, 'uploads/thumb_IMG-63 (4).png', '-', 22, 4, 2),
(455, '208079', NULL, 'BOBINA DE ENCENDIDO BOXER\r\n', 100, 110, 'uploads/thumb_IMG-62 (4).png', '-', 6, 4, 2),
(456, '208022', NULL, 'BOBINA DE LUCES Y CARGA DT 125-175 ESPECIAL\r\n', 95, 104.5, 'uploads/thumb_IMG-61 (4).png', '-', 0, 4, 2),
(457, '216035', NULL, 'FILTRO DE AIRE CGL 125 CBF 125\r\n', 85, 93.5, 'uploads/thumb_IMG-60 (4).png', '-', 0, 4, 2),
(458, '206028', NULL, 'POLVERAS DE BARRA SUPER ANCHA GXT/KMF 200 \r\n', 125, 137.5, 'uploads/thumb_IMG-54 (4).png', '-', 39, 4, 2),
(459, '206226', NULL, 'MANGUERA DE FILTRO DE AIRE GN/HJ 125\r\n', 60, 66, 'uploads/thumb_IMG-53 (4).png', '-', 4, 4, 2),
(460, '101087', NULL, 'ARANDELA PLANA DE 6MM\r\n(PAQUETE 100 UNI)', 0.35, 0.39, 'uploads/thumb_IMG-52 (4).png', '-', 3800, 4, 2),
(461, '206017', NULL, 'MANGUERA DE COMBUSTIBLE 3/16 X PIE (PAQUETE DE 100 PIES)\r\n', 8.5, 9.35, 'uploads/thumb_IMG-50 (4).png', '-', 1090, 4, 2),
(462, '216059', NULL, 'FILTRO DE AIRE AX 100/4\r\n', 85, 93.5, 'uploads/thumb_IMG-49 (4).png', '-', 0, 4, 2),
(463, '216036', NULL, 'FILTRO DE AIRE XM 200', 75, 82.5, 'uploads/thumb_IMG-48 (4).png', '-', 0, 4, 2),
(464, '216051', NULL, 'FILTRO DE AIRE PULSAR 200 NS\r\n', 100, 110, 'uploads/thumb_IMG-47 (4).png', '-', 0, 4, 2),
(465, '206199', NULL, 'HULES DE ESTRIBO DELANTERO PULSAR\r\n', 50, 55, 'uploads/thumb_IMG-46 (4).png', '-', 10, 4, 2),
(466, '206141', NULL, 'HULE DE CATARINA BOXER - 100 DISCOVER - 100 PLATINA - 125\r\n', 40, 44, 'uploads/thumb_IMG-44 (4).png', '-', 30, 4, 2),
(467, '201153', NULL, 'RESORTE DE PEDAL DE FRENO DT 125 (PAQUETE 10 UNI)\r\n', 20, 22, 'uploads/thumb_IMG-42 (4).png', '-', 0, 4, 2),
(468, '206130', NULL, 'PERILLA DE FRENO CON RESORTE UNIVERSAL\r\n', 13, 14.3, 'uploads/thumb_IMG-41 (4).png', '-', 79, 4, 2),
(469, '206009', NULL, 'HULE DE TANQUE DT 175 (PAQUETE 10 UNI )\r\n', 12, 13.2, 'uploads/thumb_IMG-40 (4).png', '-', 40, 4, 2),
(470, '206139', NULL, 'HULE DE ESTRIBO DELANTERO BOXER\r\n', 55, 60.5, 'uploads/thumb_IMG-39 (4).png', '-', 7, 4, 2),
(471, '206248', NULL, 'KIT DE REPARACION DE PRENSA DE CLUTCH PULSAR 200 NS\r\n', 80, 88, 'uploads/thumb_IMG-37 (4).png', '-', 7, 4, 2),
(472, '201208', NULL, 'RESORTE DE APOYO LATERAL  EN 125 (PAQUETE 10 UNI)\r\n', 8, 8.8, 'uploads/thumb_IMG-36 (4).png', '-', 0, 4, 2),
(473, '206260', NULL, 'KIT DE REPARACION DE PRENSA DE CLUTCH FZ-16 / R-15\r\n', 70, 77, 'uploads/thumb_IMG-35 (4).png', '-', 4, 4, 2),
(474, '206404', NULL, 'CODO PARA BUJIA CG PULSAR - 135 UNIVERSAL\r\n', 30, 33, 'uploads/thumb_IMG-34 (4).png', '-', 150, 4, 2),
(475, '201536', NULL, 'SUJETADOR DE CARETA M6 LARGA UNIVERSAL (PAQUETE 10 UNI)\r\n', 13.5, 14.85, 'uploads/thumb_IMG-33 (4).png', '-', 90, 4, 2),
(476, '101357', NULL, 'TORNILLO DE TAPADERA DE MOTOR ORIGINAL M6X20 (PAQUETE 10 UNI)\r\n', 5.5, 6.1, 'uploads/thumb_IMG-32 (4).png', '-', 328, 4, 2),
(477, '101082', NULL, 'TORNILLO DE TAPADERA DE  MOTOR ORIGINAL M6X25 (PAQUETE 10 UNI)\r\n', 6, 6.6, 'uploads/thumb_IMG-31 (4).png', '-', 298, 4, 2),
(478, '201537', NULL, 'SUJETADOR DE CARETA M5 LARGA UNIVERSAL (PAQUETE 10 UNI)\r\n', 12, 13.2, 'uploads/thumb_IMG-30 (4).png', '-', 90, 4, 2),
(479, '101644', NULL, 'KIT DE TORNILLO PASADO 3/8 COMPLETO ', 30, 33, 'uploads/thumb_IMG-29 (4).png', '-', 385, 4, 2),
(480, '105082', NULL, 'BUJE DE TIJERA CG 125 KB 150 - C/U\r\n', 40, 44, 'uploads/thumb_IMG-28 (4).png', '-', 130, 4, 2),
(481, '101085', NULL, 'TORNILLO DE TAPADERA DE MOTOR ORIGINAL M6X40 (PAQUETE 10 UNI)\r\n', 7.5, 8.3, 'uploads/thumb_IMG-27 (4).png', '-', 110, 4, 2),
(482, '201022', NULL, 'TORNILLO DE TENSOR DE CADENA DE TRACCION M8X50 (PAQUETE 10 UNI)\r\n', 4, 4.4, 'uploads/thumb_IMG-26 (4).png', '-', 800, 4, 2),
(483, '201538', NULL, 'SUJETADOR DE CARETA M5 CORTA UNIVERSAL (PAQUETE 10 UNI)\r\n', 12, 13.2, 'uploads/thumb_IMG-25 (4).png', '-', 65, 4, 2),
(484, '201394', NULL, 'RESORTE DE PEDAL DE FRENO GN125 - GS125 HJ-125-7 KMF/GXT-200\r\n', 9, 9.9, 'uploads/thumb_IMG-24 (4).png', '-', 120, 4, 2),
(485, '201017', NULL, 'TORNILLO DE CATARINA GXT/KMF 200 M8x25 (PAQUETE 10 UNI) \r\n', 2, 2.2, 'uploads/thumb_IMG-23 (4).png', '-', 120, 4, 2),
(486, '102003', NULL, 'TUERCA PARA AMORTIGUADOR DE LUJO M10 (PAQUETE 10 UNI)\r\n', 8, 8.8, 'uploads/thumb_IMG-22 (4).png', '-', 20, 4, 2),
(487, '102055', NULL, 'TUERCA DE CIGÜEÑAL PULSAR 180/200NS (PAQUETE 5 UNI)\r\n', 28, 30.8, 'uploads/thumb_IMG-21 (4).png', '-', 5, 4, 2),
(488, '206176', NULL, 'TORQUE DE CARBURADOR PULSAR 180\r\n', 40, 44, 'uploads/thumb_IMG-20 (4).png', '-', 84, 4, 2);
INSERT INTO `replacements` (`replacement_id`, `code`, `bar_code`, `description`, `wholesale_price`, `route_price`, `picture`, `location`, `quantity`, `cellar_id`, `provider_id`) VALUES
(489, '105220', NULL, 'KIT DE BUJES DE TIJERA YBR 125\r\n', 100, 110, 'uploads/thumb_IMG-19 (4).png', '-', 210, 4, 2),
(490, '201169', NULL, 'ARANDELA PLANA DE 10MM (PAQUETE DE 100 UNI)\r\n', 0.85, 0.95, 'uploads/thumb_IMG-18 (4).png', '-', 570, 4, 2),
(491, '105219', NULL, 'KIT DE BUJES DE CATARINA CG UNIVERSAL\r\n', 120, 132, 'uploads/thumb_IMG-17 (4).png', '-', 95, 4, 2),
(492, '101200', NULL, 'TORNILLO DE CALIBRACIÓN DE VALVULA (HONDA C90 HERO (TCA)) SCOOTER PULSAR 135/180  PLATINA/YBR 125\r\n', 14, 15.4, 'uploads/thumb_IMG-16 (4).png', '-', 388, 4, 2),
(493, '201024', NULL, 'TORNILLO PARA ESCAPE UNIVERSAL  M6x25 (PAQUETE 10 UNI)\r\n', 1.7, 1.9, 'uploads/thumb_IMG-14 (4).png', '-', 560, 4, 2),
(494, '201176', NULL, 'WASHA DE PRESIÓN 12MM (PAQUETE 100 UNI)\r\n', 1.25, 1.45, 'uploads/thumb_IMG-13 (4).png', '-', 600, 4, 2),
(495, '206223', NULL, 'KIT DE REPARACIÓN DE PRENSA DE CLUTCH XL 200', 50, 55, 'uploads/thumb_IMG-12 (4).png', '-', 6, 4, 2),
(496, '101358', NULL, 'TORNILLO DE TAPADERA DE MOTOR ORIGINAL M6X50\r\n (PAQUETE 10 UNI)', 10, 11, 'uploads/thumb_IMG-10 (4).png', '-', 108, 4, 2),
(497, '201175', NULL, 'WASHA DE PRESIÓN 10MM (PAQUETE 100 UNI)\r\n', 0.85, 0.95, 'uploads/thumb_IMG-9 (4).png', '-', 600, 4, 2),
(498, '206259', NULL, 'KIT DE REPARACIÓN DE PRENSA DE CLUTCH PULSAR 180/200\r\n', 60, 66, 'uploads/thumb_IMG-8 (4).png', '-', 7, 4, 2),
(499, '201053', NULL, 'TORNILLO DE SEGURO DE SPROCKET  M6x10 (PAQUETE 10 UNI)|\r\n', 0.8, 0.9, 'uploads/thumb_IMG-7 (4).png', '-', 30, 4, 2),
(500, '215002', NULL, 'CABLE PELADO DE ACELERADOR\r\n', 9, 9.9, 'uploads/thumb_IMG-6 (4).png', '-', 80, 4, 2),
(501, '215001', NULL, 'CABLE PELADO DE CLUTCH\r\n', 10, 11, 'uploads/thumb_IMG-6 (4).png', '-', 10, 4, 2),
(502, '208052', NULL, 'BOBINA DE ENCENDIDO AX 100-115\r\n', 100, 110, 'uploads/thumb_IMG-5 (4).png', '-', 18, 4, 2),
(503, '208037', NULL, 'CAPTADORA ESPECIAL DT 125-175\r\n', 90, 99, 'uploads/thumb_IMG-4 (4).png', '-', 0, 4, 2),
(504, '202022', NULL, 'TUERCA CORRIENTE M8x1.25\r\n', 0.85, 0.95, 'uploads/thumb_IMG-1 (4).png', '-', 1800, 4, 2),
(505, '206096', NULL, 'CORDÓN PARA SOCAR CARGA\r\n', 24, 26.4, 'uploads/thumb_IMG-48 (3).png', '-', 0, 4, 2),
(506, '104160', NULL, 'KIT DE TIJERA YBR 125\r\n', 200, 220, 'uploads/thumb_IMG-124.png', '-', 7, 4, 2),
(507, '104219', NULL, 'KIT DE TIJERA CRUX 110', 185, 203.5, 'uploads/thumb_IMG-126.png', '-', 7, 4, 2),
(508, '104226', NULL, 'KIT DE TIJERA FZ16 (4 CANASTILLAS)\r\n', 550, 605, 'uploads/thumb_IMG-118 (2).png', '-', 10, 4, 2),
(509, '104259', NULL, 'KIT DE TIJERA DISCOVER 125 ST 2014 UP (CANASTILLAS)\r\n', 500, 550, 'uploads/thumb_IMG-117 (2).png', '-', 6, 4, 2),
(510, '104227', NULL, 'KIT DE TIJERA YBR 125 (BUJES VULCANIZADOS)\r\n', 220, 242, 'uploads/thumb_IMG-121 (2).png', '-', 15, 4, 2),
(511, '104087', NULL, 'KIT DE TIJERA PULSAR 180\r\n', 275, 302.5, 'uploads/thumb_IMG-119 (2).png', '-', 10, 4, 2),
(512, '104125', NULL, 'KIT DE TIJERA XTZ 125\r\n', 300, 330, 'uploads/thumb_IMG-120 (2).png', '-', 25, 4, 2),
(513, '104099', NULL, 'KIT DE TIJERA GN 125\r\n', 215, 236.5, 'uploads/thumb_IMG-127.png', '-', 23, 4, 2),
(514, '104258', NULL, 'KIT DE TIJERA PULSAR 200 NS 2014 UP (CANASTILLAS)\r\n', 550, 605, 'uploads/thumb_IMG-125.png', '-', 5, 4, 2),
(515, '104269', NULL, 'KIT DE TIJERA XR 150L (2 CANASTILLAS)\r\n', 465, 511.5, 'uploads/thumb_IMG-122.png', '-', 5, 4, 2),
(516, '104011', NULL, 'KIT DE TIJERA DT/ DTK 125\r\n', 275, 302.5, 'uploads/thumb_IMG-123.png', '-', 6, 4, 2),
(517, '104143', NULL, 'EJE DE SPROCKET XTZ/YBR 125  \r\n', 650, 715, 'uploads/thumb_IMG-129.png', '-', 13, 4, 2),
(518, '104153', NULL, 'KIT DE TIJERA HJ 125-7\r\n', 210, 231, 'uploads/thumb_IMG-128.png', '-', 4, 4, 2),
(519, 'BOX002R', NULL, 'MANECILLA DE FRENO BAJAJ\r\n', 40, 44, 'uploads/thumb_IMG-83 (3).png', '-', 27, 4, 2),
(520, 'LIB001R', NULL, 'MANECILLA DE FRENO DE TAMBOR YBR 125 /DT 175/ CRUX 110', 40, 44, 'uploads/thumb_IMG-82 (3).png', '-', 10, 4, 2),
(521, 'LIB001L', NULL, 'MANECILLA DE CLUTCH YBR-125/CRUX-110', 40, 44, 'uploads/thumb_IMG-81 (4).png', '-', 0, 4, 2),
(522, 'DISCO1R', NULL, 'MANECILLA DE FRENO DISCOVER 125\r\n', 40, 44, 'uploads/thumb_IMG-80 (4).png', '-', 15, 4, 2),
(523, 'BOX002L', NULL, 'MANECILLA DE CLUTCH BOXER/CT 100 DISCOVER\r\n', 40, 44, 'uploads/thumb_IMG-79 (4).png', '-', 27, 4, 2),
(524, 'FZ001R', NULL, 'MANECILLA DE FRENO FZ16\r\n', 45, 49.5, 'uploads/thumb_IMG-78 (4).png', '-', 18, 4, 2),
(525, '104079-1', NULL, 'EJE DELANTERO PULSAR 180\r\n', 60, 66, 'uploads/thumb_IMG-116 (2).png', '-', 15, 4, 2),
(526, '104109-1', NULL, 'EJE DELANTERO SUPER SPLENDOR \r\n', 60, 66, 'uploads/thumb_IMG-116 (2).png', '-', 15, 4, 2),
(527, '104016-1', NULL, 'EJE TRASERO AX 100/115\r\n', 60, 66, 'uploads/thumb_IMG-116 (2).png', '-', 10, 4, 2),
(528, '104080-1', NULL, 'EJE TRASERO PULSAR 180 - DISCOVER 135 (TODAS)\r\n', 90, 99, 'uploads/thumb_IMG-116 (2).png', '-', 30, 4, 2),
(529, '104108-1', NULL, 'EJE TRASERO SUPER SPLENDOR\r\n', 60, 66, 'uploads/thumb_IMG-116 (2).png', '-', 6, 4, 2),
(530, '104150-1', NULL, 'EJE TRASERO GN125  - GS125\r\n', 80, 88, 'uploads/thumb_IMG-116 (2).png', '-', 5, 4, 2),
(531, '104005', NULL, 'EJE TRASERO  RX 100\r\n', 65, 71.5, 'uploads/thumb_IMG-116 (2).png', '-', 9, 4, 2),
(532, '104023', NULL, 'EJE TRASERO DT 125/175\r\n', 90, 99, 'uploads/thumb_IMG-116 (2).png', '-', 4, 4, 2),
(533, '104084', NULL, 'EJE DE TIJERA PULSAR 180\r\n', 80, 88, 'uploads/thumb_IMG-116 (2).png', '-', 10, 4, 2),
(534, '104124', NULL, 'EJE DE TIJERA XTZ 125\r\n', 65, 71.5, 'uploads/thumb_IMG-116 (2).png', '-', 12, 4, 2),
(535, '104141', NULL, 'EJE DE TIJERA PLATINA 100\r\n', 55, 60.5, 'uploads/thumb_IMG-116 (2).png', '-', 4, 4, 2),
(536, '104154', NULL, 'EJE DE TIJERA HJ 125-7, BYQ-150, EN/GN-125\r\n', 60, 66, 'uploads/thumb_IMG-116 (2).png', '-', 0, 4, 2),
(537, '104158', NULL, 'EJE DE TIJERA CGL125\r\n', 60, 66, 'uploads/thumb_IMG-116 (2).png', '-', 29, 4, 2),
(538, '104161', NULL, 'EJE DE TIJERA YBR 125\r\n', 75, 82.5, 'uploads/thumb_IMG-116 (2).png', '-', 0, 4, 2),
(539, '104203', NULL, 'EJE DE TIJERA DR 200\r\n', 75, 82.5, 'uploads/thumb_IMG-116 (2).png', '-', 10, 4, 2),
(540, '104225', NULL, 'EJE DE TIJERA  FZ16\r\n', 85, 93.5, 'uploads/thumb_IMG-116 (2).png', '-', 15, 4, 2),
(541, '104265', NULL, 'EJE TIJERA PULSAR 200 NS\r\n', 95, 104.5, 'uploads/thumb_IMG-116 (2).png', '-', 10, 4, 2),
(542, '213028', NULL, 'RETENEDOR DE CIGÜEÑAL 28-40-8  DT125/175  RX100/115 (PAQUETE 10 UNI) \r\n', 20, 22, 'uploads/thumb_IMG-181.png', '-', 10, 4, 2),
(543, '213023', NULL, 'RETENEDOR DE SPROCKET  25-44-7 TS 185 (PAQUETE 10 UNI)\r\n', 17, 18.7, 'uploads/thumb_IMG-181.png', '-', 30, 4, 2),
(544, '213021', NULL, 'RETENEDOR DE CIGÜEÑAL 25-40-8 DT125-175-RX100-115 (DOBLE) (PAQUETE 10 UNI)\r\n', 20, 22, 'uploads/thumb_IMG-181.png', '-', 30, 4, 2),
(545, '213059', NULL, 'RETENEDOR DE CIGUEÑAL 34-50-7 AK 125 (PAQUETE 10 UNI)\r\n', 23, 25.3, 'uploads/thumb_IMG-181.png', '-', 40, 4, 2),
(546, '213024', NULL, 'RETENEDOR DE SPROCKET 26-38-5 DT (PAQUETE 10 UNI)\r\n', 17, 18.7, 'uploads/thumb_IMG-181.png', '-', 30, 4, 2),
(547, '213010', NULL, 'RETENEDOR DE CRANK 16-28-7  XL 185 (PAQUETE 10 UNI)\r\n', 13, 14.3, 'uploads/thumb_IMG-181.png', '-', 40, 4, 2),
(548, '213012', NULL, 'RETENEDOR DE CRANK 17-25-4   DT-RX100-115 (PAQUETE 10 UNI)\r\n', 12, 13.2, 'uploads/thumb_IMG-181.png', '-', 47, 4, 2),
(549, '213019', NULL, 'RETENEDOR DE SPROCKET 20-38-5  DT100 (PAQUETE 10 UNI)\r\n', 16, 17.6, 'uploads/thumb_IMG-181.png', '-', 27, 4, 2),
(550, '213002', NULL, 'RETENEDOR DE CAMBIOS 12-21-4  DT-125-175 (PAQUETE 10 UNI)\r\n', 12, 13.2, 'uploads/thumb_IMG-181.png', '-', 42, 4, 2),
(551, '213007', NULL, 'RETENEDOR DE CAMBIOS 14-28-7 XL 185 (PAQUETE 10 UNI)\r\n', 13, 14.3, 'uploads/thumb_IMG-181.png', '-', 60, 4, 2),
(552, '213101', NULL, 'RETENEDOR DE EJE DE CAMBIOS 14-25-5 CBF 125 (PAQUETE 10 UNI)\r\n', 15, 16.5, 'uploads/thumb_IMG-181.png', '-', 20, 4, 2),
(553, '213058', NULL, 'RETENEDOR DE BARRA 30-42-10.5  YBR (PAQUETE 10 UNI)\r\n', 22, 24.2, 'uploads/thumb_IMG-181.png', '-', 60, 4, 2),
(554, '213026', NULL, 'RETENEDOR DE BARRA 27-37-10.5 CG 125 (PAQUETE 10 UNI)\r\n', 22, 24.2, 'uploads/thumb_IMG-181.png', '-', 60, 4, 2),
(555, '213095', NULL, 'RETENEDOR DE BARRA 32-44-10.5 GN 125 - GS  (PAQUETE 10 UNI)\r\n', 25, 27.5, 'uploads/thumb_IMG-181.png', '-', 49, 4, 2),
(556, '213056', NULL, 'RETENEDOR DE BARRA 31-43-10.5 XM 200 (PAQUETE 10 UNI)\r\n', 23, 25.3, 'uploads/thumb_IMG-181.png', '-', 60, 4, 2),
(557, '101398AZ', NULL, 'TORNILLO DE LUJO COLOR AZUL (PAQUETE 20 UNI)\r\n', 180, 198, 'uploads/thumb_IMG-1 (3).png', '-', 13, 4, 2),
(558, '101398DO', NULL, 'TORNILLO DE LUJO COLOR DORADO (PAQUETE 20 UNI)\r\n', 180, 198, 'uploads/thumb_IMG-4 (3).png', '-', 12, 4, 2),
(559, '101398NG', NULL, 'TORNILLO DE LUJO COLOR NEGRO (PAQUETE 20 UNI)\r\n', 180, 198, 'uploads/thumb_IMG-5 (3).png', '-', 0, 4, 2),
(560, '101398RO', NULL, 'TORNILLO DE LUJO COLOR ROJO (PAQUETE 20 UNI)\r\n', 180, 198, 'uploads/thumb_IMG-3 (3).png', '-', 12, 4, 2),
(561, '101398NT', NULL, 'TORNILLO DE LUJO COLOR CROMO (PAQUETE 20 UNI)\r\n', 180, 198, 'uploads/thumb_IMG-2 (3).png', '-', 12, 4, 2),
(562, '101398CV', NULL, 'TORNILLO DE LUJO COLORES VARIOS (PAQUETE 20 UNI)\r\n', 180, 198, 'uploads/thumb_IMG-28 (3).png', '-', 6, 4, 2),
(563, '201174', NULL, 'WASHA DE PRESIÓN 8MM (PAQUETE 100 UNI)', 0.37, 0.47, 'uploads/thumb_IMG-34 (3).png', '-', 600, 4, 2),
(564, '101348', NULL, 'TENSOR DE CADENA DE TRACCION CG 125 ', 16, 17.6, 'uploads/thumb_IMG-35 (3).png', '-', 62, 4, 2),
(565, '101100', NULL, 'PRISIONERO DE CLUTCH / SEGURO DE CABLE DE CLUTCH PELADO (PAQUETE 10 UNI)\r\n \r\n', 4.5, 5, 'uploads/thumb_IMG-36 (3).png', '-', 5630, 4, 2),
(566, '201012', NULL, 'TORNILLO PARA PLACA M6x20 (PAQUETE 10 UNI)\r\n', 0.85, 0.95, 'uploads/thumb_IMG-38 (3).png', '-', 7650, 4, 2),
(567, '102013', NULL, 'TUERCA DE VARILLA FRENO 6MM (PAQUETE 10 UNI)\r\n', 7, 7.7, 'uploads/thumb_IMG-39 (3).png', '-', 30, 4, 2),
(568, '201173', NULL, 'WASHA DE PRESIÓN 6MM (PAQUETE 100 UNI)\r\n', 0.35, 0.4, 'uploads/thumb_IMG-33 (3).png', '-', 600, 4, 2),
(569, '206324', NULL, 'VIA DERECHA YBR - 125\r\n', 36, 39.6, 'uploads/thumb_IMG-41 (3).png', '-', 6, 4, 2),
(570, '206323', NULL, 'VIA IZQUIERDA YBR - 125', 36, 39.6, 'uploads/thumb_IMG-41 (3).png', '-', 7, 4, 2),
(571, '101095', NULL, 'ARANDELA PLANA DE 8MM (PAQUETE 100 UNI)', 0.37, 0.41, 'uploads/thumb_IMG-52 (3).png', '-', 3800, 4, 2),
(572, '101263', NULL, 'TENSOR DE CADENA DE TRACCIÓN RX, CRUX 110, YBR', 33, 36.3, 'uploads/thumb_IMG-54 (3).png', '-', 376, 4, 2),
(573, '101010', NULL, 'TENSOR DE CADENA DE TRACCIÓN  GN 125H\r\n', 27, 29.7, 'uploads/thumb_IMG-56 (3).png', '-', 302, 4, 2),
(574, '101584', NULL, 'PATA DE BUFA YBR 125, CG-125, CG-200, UNIVERSAL\r\n', 35, 38.5, 'uploads/thumb_IMG-60 (3).png', '-', 0, 4, 2),
(575, '101118', NULL, 'TORNILLO DE BOMBA DE FRENO DELANTERO UNIVERSAL\r\n', 55, 60.5, 'uploads/thumb_IMG-64 (3).png', '-', 25, 4, 2),
(576, 'U009', NULL, 'FORRO PARA ASIENTO UNIVERSAL (80X54)\r\n', 230, 253, 'uploads/thumb_IMG-75 (3).png', '-', 468, 4, 2),
(577, 'U014', NULL, 'FORRO PARA ASIENTO BICOLOR SUPEREXTRALARGO (93X65)\r\n', 260, 286, 'uploads/thumb_IMG-74 (3).png', '-', 71, 4, 2),
(578, 'U018', NULL, 'FORRO PARA ASIENTO BICOLOR EXTRALARGO (88X54)', 250, 275, 'uploads/thumb_IMG-73 (3).png', '-', 185, 4, 2),
(579, 'P10052', NULL, 'PASTILLA DE FRENO TRASERA INVICTA\r\n', 90, 99, 'uploads/thumb_IMG-77 (3).png', '-', 5, 4, 2),
(580, 'P10012', NULL, 'PASTILLA DE FRENO TRASERA GS-150\r\n', 90, 99, 'uploads/thumb_IMG-78 (3).png', '-', 6, 4, 2),
(581, 'P10009', NULL, 'PASTILLA DE FRENO TRASERA BRONCO-250 XT-250 NUEVA, RAPTOR-700, DR 350, DR 650,   YZ-250\r\n', 100, 110, 'uploads/thumb_IMG-79 (3).png', '-', 0, 4, 2),
(582, 'P10015', NULL, 'PASTILLA DE FRENO TRASERA GENESIS HJ-150-30A, LONCIN SPITZER\r\n', 90, 99, 'uploads/thumb_IMG-80 (3).png', '-', 44, 4, 2),
(583, '101244', NULL, 'LAINA DE TORNILLOS DE CATARINA YAMAHA DT-175 (6 HOYOS) (PAQUETE 10 UNI)\r\n', 5, 5.5, 'uploads/thumb_IMG-0 (3).png', '-', 10, 4, 2),
(584, '201298', NULL, 'TORNILLO DE CATARINA BOXER-100, PULSAR (PAQUETE 10 UNI)\r\n', 10, 11, 'uploads/thumb_IMG-11 (3).png', '-', 0, 4, 2),
(585, '202034', NULL, 'TUERCA DE SEGURIDAD DE EJE TRASERO M 14 X 1.5 (PAQUETE 20 UNI)\r\n', 5, 5.5, 'uploads/thumb_IMG-25 (3).png', '-', 4760, 4, 2),
(586, '101006', NULL, 'TORNILLO REGULADOR DE CABLE DE CLUTCH UNIVERSAL (PAQUETE 5 UNI)\r\n\r\n', 12, 13.2, 'uploads/thumb_IMG-24 (3).png', '-', 5, 4, 2),
(587, '101250', NULL, 'TORNILLO DE BOMBA DE FRENO DELANTERO DISCO CG125 - CG150\r\n', 55, 60.5, 'uploads/thumb_IMG-70 (3).png', '-', 0, 4, 2),
(588, '101349', NULL, 'TENSOR DE CADENA DE TRACCIÓN BOXER 100\r\n', 20, 22, 'uploads/thumb_IMG-71 (3).png', '-', 0, 4, 2),
(589, '101350', NULL, 'VARILLA DE FRENO GN 125 - GS\r\n', 33, 36.3, 'uploads/thumb_IMG-77 (4).png', '-', 15, 4, 2),
(590, '101352', NULL, 'VARILLA DE FRENO CGL125\r\n', 35, 38.5, 'uploads/thumb_IMG-77 (4).png', '-', 24, 4, 2),
(591, '101444', NULL, 'TORNILLO PARA TAPADERAS LATERALES Y CARENAJES KMF/GXT-200, UNIVERSAL. M6X20 VERDE\r\n', 6, 6.6, 'uploads/thumb_IMG-68 (3).png', '-', 412, 4, 2),
(592, '104014', NULL, 'SOPORTE SUPERIOR MOTOR XL 125/185\r\n', 45, 49.5, 'uploads/thumb_IMG-66 (3).png', '-', 9, 4, 2),
(593, '201133', NULL, 'RESORTE DE PEDAL DE FRENO AVATAR 200 (PAQUETE 10 UNI)\r\n', 10, 11, 'uploads/thumb_IMG-108 (2).png', '-', 0, 4, 2),
(594, '201210', NULL, 'RESORTE DE PEDAL DE FRENO YBR-125, CRUX-110 (PAQUETE 10 UNI)\r\n', 15, 16.5, 'uploads/thumb_IMG-12 (3).png', '-', 0, 4, 2),
(595, '202020', NULL, 'TUERCA DE SEGURIDAD DE EJE DELANTERO M 12x1.25 (PAQUETE 20 UNI )', 3.75, 4.13, 'uploads/thumb_IMG-25 (3).png', '-', 1, 4, 2),
(596, '206008', NULL, 'HULE PARA TANQUE UNIVERSAL (PAQUETE 10 UNI)\r\n', 10, 11, 'uploads/thumb_IMG-16 (3).png', '-', 10, 4, 2),
(597, '206013', NULL, 'HULE DE TAPADERAS LATERALES UNIVERSAL (PAQUETE 10 UNI)\r\n', 5, 5.5, 'uploads/thumb_IMG-32 (3).png', '-', 90, 4, 2),
(598, '206014', NULL, 'POLVERAS DE BARRA DT-175, XM-200, AVATAR-200\r\n', 90, 99, 'uploads/thumb_IMG-40 (3).png', '-', 136, 4, 2),
(599, '206021', NULL, ' HULE PARA PEDAL DE CRANK  UNIVERSAL (PAQUETE 10 UNI)\r\n\r\n', 13, 14.3, 'uploads/thumb_IMG-15 (4).png', '-', 4, 4, 2),
(600, '206147', NULL, 'POLVERAS DE BARRA PULSAR 135/ YBR-125 / CG UNIVERSAL\r\n', 80, 88, 'uploads/thumb_IMG-17 (3).png', '-', 552, 3, 1),
(601, '206220', NULL, 'HULE DE CATARINA PULSAR 135/180, HAJOE HJ-150-9, DISCOVER 125ST\r\n', 55, 60.5, 'uploads/thumb_IMG-18 (3).png', '-', 7, 4, 2),
(602, '208000', NULL, 'PITO CONTRA EL AGUA PARA MOTO UNIVERSAL 12 V\r\n', 90, 99, 'uploads/thumb_IMG-14 (3).png', '-', 0, 4, 2),
(603, '210017', NULL, 'KIT COMPLETO DE EMPAQUES YBR 125\r\n', 65, 71.5, 'uploads/thumb_IMG-20 (3).png', '-', 73, 4, 2),
(604, '210157', NULL, 'EMPAQUE DE TAPADERA DE CLUTCH YBR 125\r\n', 15, 16.5, 'uploads/thumb_IMG-19 (3).png', '-', 200, 4, 2),
(605, '213032', NULL, 'RETENEDOR DE BARRA 35-48-11   DTK175, GXT/KMF-200, XT-225, XTM-200 35-48-11\r\n', 24, 26.4, 'uploads/thumb_IMG-181.png', '-', 120, 4, 2),
(606, '213033', NULL, 'KIT DE RETENEDORES YAMAHA DT 125/175\r\n', 85, 93.5, 'uploads/thumb_IMG-65 (4).png', '-', 0, 4, 2),
(607, '213034', NULL, 'KIT DE RETENEDORES YAMAHA RX115\r\n', 85, 93.5, 'uploads/thumb_IMG-73 (4).png', '-', 2, 4, 2),
(608, '213060', NULL, 'RETENEDOR DE BARRA 30-40.5-10.5  YBR-125, CRUX-110, EN-125, BYQ-150\r\n', 20, 22, 'uploads/thumb_IMG-181.png', '-', 160, 4, 2),
(609, '213070', NULL, 'SELLO DE VALVULA PULSAR 180/135/200/220, DISCOVER125/125ST, XCD\r\n', 12, 13.2, 'uploads/thumb_IMG-72 (3).png', '-', 0, 4, 2),
(610, '213104', NULL, 'KIT DE RETENEDORES HONDA NXR BROSS 125\r\n', 85, 93.5, 'uploads/thumb_IMG-70 (4).png', '-', 0, 4, 2),
(611, '216040', NULL, 'FILTRO DE AIRE FZ16\r\n', 115, 126.5, 'uploads/thumb_IMG-0 (4).png', '-', 0, 4, 2),
(612, '216053', NULL, 'FILTRO DE ACEITE YAMAHA R15/CRYPTON 110/115/XTZ250\r\n', 75, 82.5, 'uploads/thumb_IMG-9 (3).png', '-', 0, 4, 2),
(613, 'CIY10', NULL, 'EMPAQUE DE CILINDRO DT 175 (PAQUETE 10 UNI)\r\n', 9, 9.9, 'uploads/thumb_IMG-178.png', '-', 50, 4, 2),
(614, 'P10043', NULL, 'PASTILLA DE FRENO DELANTERA YAMAHA YBR 125/YBR 125TT  - F15\r\n', 90, 99, 'uploads/thumb_IMG-76 (4).png', '-', 0, 4, 2),
(615, '210031', NULL, 'KIT COMPLETO DE EMPAQUES GN 125\r\n', 80, 88, 'uploads/thumb_IMG-134.png', '-', 0, 4, 2),
(616, '210076', NULL, 'KIT MEDIO DE EMPAQUES DTK 175\r\n', 32, 35.2, 'uploads/thumb_IMG-160.png', '-', 0, 4, 2),
(617, '101376', NULL, 'VARILLA DE PRENSA DE CLUTCH YBR/XTZ 125\r\n', 100, 110, 'uploads/thumb_dest-30.png', '-', 26, 4, 2),
(618, '104241', NULL, 'KIT DE TIJERA HONDA XLR 125 (2 CANASTILLAS)\r\n', 400, 440, 'uploads/thumb_dest-32.png', '-', 5, 4, 2),
(619, '201373', NULL, 'TORNILLO DE CATARINA YBR 125, CRUX-110 (PAQUETE 10 UNI)\r\n', 8, 8.8, 'uploads/thumb_dest-35.png', '-', 80, 4, 2),
(620, '206003', NULL, 'HULE DE CATARINA HJ-125, XM-200, GN 125\r\n', 35, 38.5, 'uploads/thumb_dest-36.png', '-', 538, 4, 2),
(621, '206278', NULL, 'VIA IZQUIERDA PULSAR 200 NS\r\n', 55, 60.5, 'uploads/thumb_dest-38.png', '-', 78, 4, 2),
(622, '206279', NULL, 'VIA DERECHA PULSAR 200 NS\r\n', 55, 60.5, 'uploads/thumb_dest-38.png', '-', 78, 4, 2),
(623, '207064', NULL, 'MANUBRIO NEGRO XT 225, UNIVERSAL', 150, 165, 'uploads/thumb_dest-41.png', '-', 4, 4, 2),
(624, '207065', NULL, 'MANUBRIO NEGRO FZ16, UNIVERSAL\r\n', 150, 165, 'uploads/thumb_dest-41.png', '-', 37, 4, 2),
(625, '210333', NULL, 'SELLO DE ESCAPE DT-175, PULSAR 180, PULSAR 220, PULSAR 200 NS, DISCOVER 125\r\n', 7, 7.7, 'uploads/thumb_dest-42.png', '-', 40, 4, 2),
(626, 'A10007', NULL, 'FRICCIÓN DE FRENO DE BUFA UNIVERSAL DT-175 / YBR-125 / CG-125 / CG-200\r\n', 80, 88, 'uploads/thumb_dest-44.png', '-', 1912, 4, 2),
(627, 'KMA23', NULL, 'KIT MEDIO DE EMPAQUES XCD 125, PLATINA 125\r\n', 35, 38.5, 'uploads/thumb_dest-49.png', '-', 136, 4, 2),
(628, 'KMH33', NULL, 'KIT MEDIO DE EMPAQUES CBF 150\r\n', 45, 49.5, 'uploads/thumb_dest-48.png', '-', 10, 4, 2),
(629, 'MCP105', NULL, 'LODERA NEGRO ROJO\r\n', 45, 49.5, 'uploads/thumb_dest-46.png', '-', 1, 4, 2),
(630, 'P10030', NULL, 'PASTILLA DE FRENO DELANTERA HJ-125, GN 125H\r\n', 90, 99, 'uploads/thumb_dest-47.png', '-', 2, 4, 2),
(631, 'ACE-01', NULL, 'ACEITE MOTUL 3000 20W-50 4T\r\n', 117, 129, 'uploads/thumb_dest-0.png', '-', 240, 4, 6),
(632, 'ACE-02', NULL, 'ACEITE CASTROL ACTEVO 20W-50 4T \r\n', 110, 121, 'uploads/thumb_dest-1.png', '-', 824, 4, 5),
(633, '57391', NULL, 'LLANTA MOTO 300-18 K270 KENDA', 500, 550, 'uploads/thumb_dest-26.png', '-', 4, 4, 4),
(634, '57518', NULL, 'LLANTA MOTO 325-17 K270 KENDA', 600, 660, 'uploads/thumb_dest-28.png', '-', 4, 4, 4),
(635, '11521', NULL, 'LLANTA MOTO 460-17 K270 KENDA', 700, 770, 'uploads/thumb_dest-27.png', '-', 4, 4, 4),
(636, '10552', NULL, 'LLANTA MOTO 275-18 K270 KENDA', 380, 418, 'uploads/thumb_dest-25.png', '-', 4, 4, 4),
(637, '10557', NULL, 'LLANTA MOTO 410-18 K257 KENDA', 765, 841.5, 'uploads/thumb_dest-29.png', '-', 0, 4, 4),
(638, 'AC-03', NULL, 'LIQUIDO PARA FRENO POWER BRAKE DOT 3 ', 30, 33, 'uploads/thumb_dest-1 (2).png', '-', 3383, 4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `replacements_models`
--

CREATE TABLE `replacements_models` (
  `replacement_model` int(11) NOT NULL,
  `replacement_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `replacements_models`
--

INSERT INTO `replacements_models` (`replacement_model`, `replacement_id`, `model_id`) VALUES
(170, 76, 104),
(171, 77, 105),
(172, 77, 106),
(173, 77, 107),
(175, 78, 108),
(176, 78, 109),
(177, 78, 110),
(178, 78, 111),
(181, 80, 113),
(182, 80, 114),
(188, 86, 118),
(196, 92, 125),
(197, 93, 107),
(198, 94, 104),
(199, 94, 127),
(200, 94, 116),
(201, 95, 128),
(202, 95, 129),
(203, 96, 130),
(206, 97, 131),
(207, 98, 132),
(208, 99, 122),
(209, 100, 108),
(210, 101, 123),
(213, 102, 131),
(217, 104, 125),
(218, 105, 129),
(219, 106, 109),
(220, 107, 112),
(221, 108, 134),
(222, 108, 113),
(223, 108, 114),
(224, 109, 104),
(225, 110, 116),
(226, 110, 127),
(227, 111, 111),
(228, 112, 135),
(229, 112, 115),
(230, 113, 136),
(231, 114, 130),
(262, 125, 147),
(263, 126, 148),
(273, 131, 153),
(274, 131, 143),
(275, 132, 104),
(277, 134, 122),
(279, 135, 108),
(311, 154, 122),
(313, 156, 158),
(314, 156, 159),
(315, 157, 112),
(316, 158, 114),
(317, 158, 113),
(318, 159, 111),
(319, 160, 125),
(320, 161, 129),
(321, 162, 157),
(322, 163, 116),
(323, 164, 117),
(324, 165, 119),
(325, 165, 120),
(326, 166, 132),
(327, 167, 108),
(328, 167, 133),
(329, 168, 128),
(330, 168, 152),
(335, 171, 129),
(336, 172, 116),
(337, 172, 155),
(338, 173, 117),
(339, 174, 108),
(341, 176, 108),
(342, 176, 133),
(354, 181, 108),
(355, 181, 166),
(356, 181, 144),
(357, 181, 167),
(358, 181, 145),
(359, 181, 125),
(374, 187, 104),
(377, 190, 115),
(380, 193, 106),
(383, 196, 112),
(384, 197, 111),
(386, 199, 107),
(387, 200, 158),
(389, 201, 130),
(390, 202, 129),
(391, 203, 131),
(393, 205, 116),
(397, 208, 155),
(398, 206, 104),
(399, 204, 115),
(400, 195, 169),
(401, 209, 122),
(402, 210, 108),
(403, 211, 123),
(404, 212, 164),
(405, 213, 164),
(406, 214, 164),
(407, 215, 164),
(408, 216, 164),
(411, 219, 113),
(412, 219, 170),
(413, 219, 171),
(447, 233, 131),
(448, 233, 172),
(458, 239, 108),
(470, 245, 131),
(506, 261, 183),
(507, 261, 179),
(508, 261, 122),
(509, 262, 166),
(510, 262, 181),
(537, 282, 157),
(538, 283, 129),
(539, 284, 142),
(545, 287, 131),
(546, 287, 172),
(547, 288, 111),
(549, 289, 121),
(550, 290, 178),
(552, 292, 108),
(553, 293, 115),
(554, 294, 157),
(556, 296, 142),
(558, 298, 157),
(560, 300, 133),
(561, 301, 142),
(562, 302, 115),
(563, 302, 178),
(564, 303, 125),
(565, 304, 136),
(566, 305, 131),
(599, 75, 103),
(601, 79, 112),
(602, 81, 111),
(603, 82, 121),
(604, 83, 115),
(605, 84, 116),
(606, 85, 117),
(607, 87, 119),
(608, 87, 120),
(609, 88, 122),
(612, 91, 111),
(613, 103, 108),
(614, 103, 133),
(615, 115, 137),
(616, 115, 139),
(617, 117, 140),
(618, 117, 130),
(619, 117, 141),
(620, 117, 142),
(621, 118, 108),
(622, 119, 117),
(623, 119, 129),
(624, 119, 104),
(625, 119, 126),
(626, 119, 128),
(627, 120, 143),
(628, 120, 115),
(629, 121, 144),
(630, 121, 145),
(631, 121, 146),
(633, 122, 108),
(634, 128, 150),
(636, 136, 133),
(637, 136, 109),
(638, 137, 108),
(642, 139, 104),
(643, 139, 154),
(644, 139, 155),
(645, 140, 116),
(646, 140, 155),
(647, 141, 133),
(648, 141, 125),
(649, 142, 129),
(650, 143, 117),
(651, 143, 156),
(656, 324, 164),
(668, 327, 188),
(669, 325, 127),
(670, 326, 189),
(671, 328, 187),
(687, 330, 117),
(690, 332, 116),
(691, 333, 158),
(692, 334, 190),
(694, 335, 125),
(695, 336, 112),
(696, 337, 191),
(697, 338, 192),
(698, 339, 152),
(699, 340, 128),
(700, 341, 118),
(701, 342, 129),
(702, 343, 126),
(703, 344, 155),
(704, 345, 104),
(705, 346, 193),
(706, 347, 104),
(707, 348, 108),
(713, 351, 194),
(714, 351, 190),
(715, 352, 106),
(716, 353, 127),
(717, 354, 117),
(718, 355, 132),
(719, 356, 126),
(720, 357, 112),
(723, 359, 143),
(724, 360, 187),
(725, 361, 192),
(726, 362, 128),
(727, 363, 188),
(728, 364, 191),
(729, 365, 111),
(730, 366, 109),
(731, 367, 116),
(732, 368, 104),
(733, 368, 152),
(734, 368, 168),
(736, 369, 104),
(737, 369, 175),
(738, 370, 129),
(739, 371, 116),
(740, 372, 126),
(741, 372, 128),
(742, 373, 187),
(743, 374, 106),
(744, 374, 195),
(745, 375, 108),
(746, 376, 104),
(747, 376, 155),
(748, 377, 143),
(749, 378, 125),
(782, 390, 133),
(784, 392, 164),
(786, 394, 164),
(788, 396, 129),
(789, 397, 129),
(790, 398, 164),
(791, 399, 164),
(792, 400, 164),
(793, 401, 164),
(798, 405, 125),
(799, 404, 125),
(800, 406, 189),
(801, 407, 127),
(802, 408, 127),
(803, 409, 164),
(805, 411, 161),
(806, 412, 161),
(807, 413, 143),
(813, 415, 109),
(817, 419, 109),
(820, 422, 199),
(821, 423, 178),
(824, 426, 108),
(846, 432, 164),
(847, 433, 164),
(848, 434, 177),
(849, 435, 106),
(850, 436, 116),
(861, 441, 109),
(862, 441, 199),
(865, 443, 135),
(866, 444, 108),
(867, 444, 125),
(868, 442, 122),
(869, 442, 179),
(875, 429, 117),
(876, 429, 104),
(877, 429, 164),
(878, 430, 133),
(879, 430, 110),
(880, 430, 103),
(881, 430, 177),
(882, 430, 166),
(883, 430, 181),
(889, 403, 109),
(890, 403, 199),
(893, 414, 109),
(894, 437, 164),
(896, 439, 164),
(897, 446, 104),
(898, 446, 128),
(899, 447, 131),
(900, 447, 123),
(901, 448, 157),
(903, 450, 117),
(904, 450, 128),
(905, 451, 117),
(906, 451, 128),
(907, 452, 106),
(908, 453, 109),
(909, 454, 109),
(910, 454, 199),
(911, 455, 187),
(912, 456, 109),
(913, 456, 199),
(914, 457, 135),
(915, 457, 112),
(920, 459, 115),
(921, 459, 143),
(922, 416, 109),
(923, 416, 199),
(926, 461, 164),
(929, 462, 189),
(933, 431, 108),
(934, 431, 122),
(935, 431, 133),
(936, 431, 131),
(937, 431, 123),
(939, 465, 104),
(940, 465, 159),
(945, 468, 164),
(946, 467, 199),
(949, 469, 109),
(950, 470, 187),
(953, 472, 153),
(954, 472, 202),
(955, 473, 203),
(956, 473, 129),
(961, 475, 164),
(962, 476, 164),
(964, 477, 164),
(965, 478, 164),
(966, 479, 164),
(969, 481, 164),
(970, 480, 110),
(971, 480, 177),
(978, 486, 164),
(981, 488, 117),
(982, 489, 108),
(990, 492, 142),
(991, 492, 104),
(992, 492, 117),
(993, 492, 108),
(994, 492, 116),
(997, 493, 164),
(999, 495, 132),
(1000, 494, 164),
(1003, 496, 164),
(1004, 497, 164),
(1005, 498, 156),
(1006, 498, 117),
(1007, 499, 164),
(1010, 500, 164),
(1011, 501, 164),
(1012, 502, 106),
(1013, 502, 195),
(1014, 503, 199),
(1015, 503, 109),
(1016, 504, 164),
(1017, 383, 164),
(1018, 384, 164),
(1019, 385, 164),
(1020, 386, 164),
(1021, 387, 164),
(1022, 505, 164),
(1023, 506, 108),
(1024, 507, 125),
(1025, 508, 129),
(1026, 509, 127),
(1027, 510, 108),
(1028, 511, 117),
(1029, 512, 133),
(1030, 513, 143),
(1032, 515, 204),
(1033, 516, 199),
(1035, 518, 115),
(1043, 520, 108),
(1044, 520, 109),
(1045, 520, 125),
(1046, 519, 155),
(1047, 519, 126),
(1048, 519, 128),
(1049, 519, 187),
(1050, 519, 107),
(1051, 519, 116),
(1052, 519, 105),
(1059, 522, 128),
(1060, 522, 126),
(1061, 522, 104),
(1062, 522, 117),
(1063, 523, 187),
(1064, 523, 107),
(1065, 523, 116),
(1066, 523, 155),
(1067, 523, 126),
(1068, 524, 129),
(1069, 525, 117),
(1071, 527, 106),
(1072, 527, 195),
(1073, 528, 117),
(1074, 528, 128),
(1076, 530, 143),
(1077, 530, 205),
(1078, 531, 207),
(1079, 532, 199),
(1080, 532, 109),
(1084, 536, 115),
(1085, 536, 165),
(1086, 536, 153),
(1087, 536, 143),
(1089, 538, 108),
(1090, 537, 135),
(1091, 535, 105),
(1092, 533, 117),
(1095, 534, 133),
(1096, 539, 198),
(1097, 540, 129),
(1098, 541, 118),
(1159, 557, 164),
(1160, 556, 145),
(1161, 556, 182),
(1162, 556, 211),
(1163, 556, 140),
(1164, 556, 141),
(1165, 556, 181),
(1166, 556, 210),
(1167, 556, 126),
(1168, 556, 212),
(1169, 556, 122),
(1170, 556, 213),
(1171, 556, 179),
(1176, 554, 110),
(1177, 554, 177),
(1178, 554, 166),
(1179, 553, 108),
(1180, 552, 112),
(1181, 551, 193),
(1182, 550, 109),
(1183, 550, 199),
(1184, 549, 209),
(1185, 548, 109),
(1186, 548, 207),
(1187, 548, 208),
(1188, 547, 193),
(1189, 546, 109),
(1190, 545, 110),
(1191, 542, 199),
(1192, 542, 109),
(1193, 542, 207),
(1194, 542, 208),
(1195, 543, 191),
(1196, 544, 199),
(1197, 544, 109),
(1198, 544, 207),
(1199, 544, 208),
(1200, 558, 164),
(1201, 559, 164),
(1202, 560, 164),
(1204, 562, 164),
(1214, 566, 164),
(1216, 485, 164),
(1217, 567, 164),
(1218, 565, 164),
(1219, 568, 164),
(1220, 563, 164),
(1224, 490, 164),
(1225, 571, 164),
(1226, 460, 164),
(1227, 482, 164),
(1228, 564, 166),
(1229, 564, 177),
(1230, 564, 110),
(1231, 572, 108),
(1232, 572, 125),
(1233, 572, 207),
(1234, 573, 143),
(1235, 573, 115),
(1236, 574, 108),
(1237, 574, 110),
(1238, 574, 173),
(1239, 574, 164),
(1240, 358, 129),
(1241, 575, 164),
(1242, 576, 164),
(1243, 577, 164),
(1244, 578, 164),
(1245, 579, 149),
(1246, 379, 119),
(1247, 379, 112),
(1248, 379, 114),
(1249, 379, 129),
(1255, 580, 150),
(1256, 581, 192),
(1257, 581, 214),
(1258, 581, 215),
(1259, 581, 216),
(1260, 581, 217),
(1261, 581, 218),
(1262, 582, 124),
(1263, 582, 219),
(1265, 583, 109),
(1271, 585, 164),
(1273, 584, 187),
(1274, 584, 104),
(1275, 584, 105),
(1276, 584, 116),
(1277, 584, 158),
(1278, 586, 164),
(1281, 588, 187),
(1282, 589, 205),
(1283, 589, 143),
(1284, 590, 135),
(1287, 591, 164),
(1288, 591, 131),
(1289, 591, 172),
(1290, 592, 164),
(1291, 592, 193),
(1292, 592, 220),
(1294, 594, 108),
(1295, 594, 125),
(1296, 593, 212),
(1299, 597, 164),
(1300, 595, 164),
(1301, 596, 164),
(1305, 599, 164),
(1308, 598, 109),
(1309, 598, 122),
(1310, 598, 212),
(1311, 458, 131),
(1312, 458, 123),
(1316, 318, 126),
(1317, 318, 128),
(1322, 601, 104),
(1323, 601, 117),
(1324, 601, 202),
(1325, 601, 127),
(1326, 602, 164),
(1327, 603, 108),
(1328, 604, 108),
(1329, 605, 109),
(1330, 605, 131),
(1331, 605, 172),
(1332, 605, 190),
(1333, 605, 221),
(1334, 606, 199),
(1335, 606, 109),
(1336, 607, 208),
(1337, 608, 108),
(1338, 608, 125),
(1339, 608, 153),
(1340, 608, 165),
(1341, 609, 117),
(1342, 609, 104),
(1343, 609, 156),
(1344, 609, 160),
(1345, 609, 126),
(1346, 609, 127),
(1347, 609, 151),
(1348, 610, 213),
(1349, 611, 129),
(1353, 463, 122),
(1354, 463, 179),
(1355, 463, 183),
(1356, 612, 203),
(1357, 612, 222),
(1358, 612, 223),
(1359, 612, 224),
(1360, 613, 109),
(1363, 615, 143),
(1364, 616, 109),
(1365, 617, 108),
(1366, 617, 133),
(1367, 618, 226),
(1370, 620, 115),
(1371, 620, 122),
(1372, 620, 143),
(1373, 621, 118),
(1374, 622, 118),
(1375, 623, 190),
(1376, 623, 164),
(1377, 624, 164),
(1388, 626, 109),
(1389, 626, 108),
(1390, 626, 110),
(1391, 626, 173),
(1392, 626, 164),
(1393, 627, 155),
(1394, 627, 116),
(1395, 628, 114),
(1396, 629, 164),
(1397, 630, 115),
(1398, 630, 157),
(1399, 614, 223),
(1400, 614, 225),
(1401, 614, 108),
(1402, 614, 227),
(1405, 632, 164),
(1406, 631, 164),
(1407, 633, 164),
(1408, 634, 164),
(1409, 635, 164),
(1410, 636, 164),
(1411, 637, 164),
(1413, 638, 164),
(1414, 148, 118),
(1415, 149, 115),
(1416, 521, 109),
(1417, 521, 108),
(1418, 521, 125),
(1421, 150, 108),
(1422, 150, 125),
(1424, 151, 129),
(1427, 152, 157),
(1428, 153, 135),
(1429, 153, 115),
(1430, 155, 131),
(1431, 169, 125),
(1432, 175, 156),
(1433, 175, 160),
(1434, 175, 161),
(1435, 175, 162),
(1436, 175, 163),
(1437, 177, 131),
(1438, 178, 164),
(1440, 180, 122),
(1441, 180, 138),
(1442, 180, 165),
(1443, 182, 151),
(1444, 182, 104),
(1445, 182, 117),
(1446, 182, 126),
(1447, 182, 128),
(1448, 183, 131),
(1449, 184, 131),
(1451, 179, 164),
(1452, 587, 110),
(1453, 587, 111),
(1454, 587, 164),
(1455, 185, 132),
(1456, 561, 164),
(1457, 186, 129),
(1458, 188, 108),
(1459, 189, 131),
(1460, 191, 131),
(1461, 192, 106),
(1462, 194, 168),
(1463, 198, 125),
(1465, 217, 164),
(1466, 218, 131),
(1467, 218, 172),
(1468, 220, 107),
(1469, 220, 116),
(1470, 220, 104),
(1471, 220, 151),
(1472, 220, 158),
(1473, 221, 153),
(1474, 221, 143),
(1475, 223, 123),
(1476, 224, 110),
(1477, 225, 131),
(1478, 225, 172),
(1479, 226, 104),
(1480, 227, 131),
(1481, 227, 172),
(1482, 228, 108),
(1483, 228, 133),
(1484, 229, 111),
(1485, 229, 173),
(1486, 229, 115),
(1487, 230, 121),
(1488, 231, 131),
(1489, 231, 172),
(1490, 231, 122),
(1491, 231, 174),
(1492, 232, 104),
(1493, 234, 142),
(1494, 235, 110),
(1495, 236, 112),
(1496, 236, 114),
(1497, 236, 113),
(1498, 237, 129),
(1502, 238, 155),
(1503, 238, 176),
(1504, 238, 127),
(1508, 241, 131),
(1509, 242, 179),
(1510, 242, 122),
(1511, 242, 164),
(1512, 242, 180),
(1513, 243, 115),
(1514, 244, 108),
(1515, 244, 133),
(1516, 246, 125),
(1520, 251, 131),
(1525, 253, 131),
(1526, 255, 115),
(1527, 255, 164),
(1528, 256, 144),
(1529, 256, 111),
(1530, 256, 181),
(1535, 257, 121),
(1536, 257, 132),
(1540, 258, 166),
(1541, 258, 103),
(1542, 258, 177),
(1543, 259, 144),
(1544, 259, 181),
(1545, 259, 182),
(1550, 260, 179),
(1551, 260, 122),
(1552, 260, 174),
(1553, 260, 183),
(1555, 263, 115),
(1559, 267, 108),
(1560, 268, 135),
(1561, 269, 178),
(1562, 266, 131),
(1563, 266, 164),
(1572, 250, 131),
(1573, 250, 172),
(1574, 254, 131),
(1575, 270, 109),
(1576, 271, 106),
(1577, 272, 110),
(1578, 272, 166),
(1579, 273, 131),
(1580, 273, 172),
(1581, 274, 122),
(1582, 274, 184),
(1583, 275, 115),
(1584, 276, 178),
(1586, 277, 115),
(1587, 278, 108),
(1588, 279, 110),
(1589, 279, 173),
(1590, 280, 108),
(1593, 306, 116),
(1594, 307, 164),
(1595, 308, 164),
(1596, 309, 164),
(1597, 310, 164),
(1598, 311, 115),
(1599, 312, 115),
(1600, 313, 115),
(1601, 314, 108),
(1602, 315, 108),
(1603, 316, 108),
(1607, 317, 117),
(1608, 317, 156),
(1609, 317, 186),
(1611, 319, 129),
(1612, 320, 112),
(1615, 321, 104),
(1616, 321, 127),
(1618, 322, 187),
(1627, 89, 124),
(1628, 89, 111),
(1629, 90, 123),
(1630, 123, 131),
(1631, 124, 110),
(1632, 127, 149),
(1633, 129, 129),
(1634, 130, 151),
(1635, 130, 105),
(1636, 130, 107),
(1637, 130, 152),
(1638, 133, 108),
(1639, 138, 114),
(1640, 138, 113),
(1641, 138, 115),
(1642, 144, 131),
(1643, 145, 108),
(1644, 146, 115),
(1645, 147, 104),
(1646, 170, 126),
(1647, 170, 128),
(1648, 170, 104),
(1649, 207, 119),
(1650, 207, 120),
(1651, 329, 119),
(1653, 529, 119),
(1654, 526, 119),
(1655, 240, 177),
(1656, 240, 103),
(1657, 240, 178),
(1658, 449, 119),
(1660, 248, 108),
(1661, 247, 114),
(1662, 249, 131),
(1663, 252, 157),
(1664, 252, 131),
(1665, 264, 157),
(1666, 264, 131),
(1667, 264, 172),
(1668, 265, 157),
(1669, 265, 131),
(1670, 265, 123),
(1675, 281, 110),
(1676, 281, 111),
(1677, 285, 178),
(1678, 285, 121),
(1679, 285, 122),
(1680, 286, 110),
(1681, 286, 111),
(1683, 295, 131),
(1684, 295, 123),
(1685, 291, 142),
(1686, 299, 104),
(1687, 297, 115),
(1688, 297, 164),
(1689, 600, 108),
(1690, 600, 104),
(1691, 600, 178),
(1692, 600, 164),
(1696, 323, 108),
(1697, 323, 164),
(1698, 323, 178),
(1699, 349, 105),
(1700, 349, 187),
(1701, 350, 189),
(1702, 331, 132),
(1703, 380, 113),
(1704, 380, 196),
(1707, 381, 118),
(1714, 388, 164),
(1719, 382, 164),
(1720, 382, 153),
(1721, 389, 198),
(1722, 389, 123),
(1723, 389, 131),
(1724, 391, 108),
(1725, 393, 164),
(1726, 395, 129),
(1727, 402, 195),
(1728, 402, 178),
(1729, 410, 164),
(1731, 417, 164),
(1732, 418, 164),
(1733, 420, 178),
(1734, 420, 110),
(1735, 421, 131),
(1736, 421, 123),
(1737, 421, 164),
(1738, 424, 187),
(1739, 425, 125),
(1740, 427, 135),
(1741, 427, 108),
(1742, 427, 166),
(1743, 427, 164),
(1744, 428, 115),
(1745, 428, 153),
(1746, 428, 143),
(1747, 428, 201),
(1748, 440, 187),
(1749, 438, 143),
(1750, 438, 115),
(1751, 445, 164),
(1752, 222, 133),
(1753, 222, 108),
(1754, 466, 187),
(1755, 466, 152),
(1756, 466, 116),
(1757, 474, 104),
(1758, 474, 164),
(1759, 474, 178),
(1760, 484, 143),
(1761, 484, 153),
(1762, 484, 115),
(1763, 484, 131),
(1764, 487, 117),
(1765, 487, 118),
(1766, 491, 178),
(1767, 517, 133),
(1768, 517, 108),
(1769, 555, 143),
(1770, 555, 205),
(1771, 569, 108),
(1772, 570, 108),
(1775, 619, 108),
(1776, 619, 125),
(1778, 464, 118),
(1779, 471, 118),
(1780, 514, 118),
(1781, 625, 109),
(1782, 625, 117),
(1783, 625, 156),
(1784, 625, 118),
(1785, 625, 126),
(1788, 116, 118);

-- --------------------------------------------------------

--
-- Table structure for table `replacements_orders`
--

CREATE TABLE `replacements_orders` (
  `replacement_order` int(11) NOT NULL,
  `replacement_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '2',
  `active` tinyint(1) DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `user_type`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(7, 'Francisco Urrutia', 'francisco@imosa.hn', '$2y$10$1nJQeOMWuOjbZrSWHmAOEu.3XQhqOf5vr9SZXPrBkPJCRzGZsZaZ.', 1, 1, 'gYuhjFPbwnfmKlAkBAuP3uFSTjGFl895uGzo2yc5BLfVb1nQ1ukWEIohMm6p', '2016-09-01 05:01:50', '2016-09-22 22:29:05'),
(8, 'Juan Orellana ', 'jorellana@imosa.hn', '$2y$10$.ZXPiFDzusYEVPX1Ze7.n.XHxSeiw/IlyThbL6UVfP0F6e6m.SJgi', 3, 1, 'aIbiCRXAQ4jpNfhROYE3GDZnhFNKm0Kfy8Kb9laKd48Jgw7PnYvcnDgY5rkh', '2016-09-02 20:39:55', '2016-09-21 23:25:07'),
(9, 'Aldud Boquin', 'aldud@imosa.hn', '$2y$10$0wUZYwkXGkuRUTuCWRLfaOFfQ9iWRhJdl2MjMSVL07ad8Qfj7jd8C', 2, 1, '1H90hHdITYhF7RVBM4PHT4RsUct7hUUsvQf4yltsw5OC5ppYiKlavUXsbFGl', '2016-09-17 00:34:36', '2016-09-22 00:59:24'),
(10, 'Alexander Hernandez', 'calebherz@gmail.com', '$2y$10$PbqfrDUTG.iATu3T/7wVv.hU91eOTtjBKaWRHMIoz2pH6Ih0Kg/16', 1, 1, 'Kbyd90oY7bJ3mBtxgkHij4FUNSamLrgbAo4Ffml5NDrdnPA6eTS8SmJsqNmC', '2016-09-21 23:22:18', '2016-09-22 01:51:46'),
(11, 'Lennys Meza', 'lmeza@imosa.hn', '$2y$10$xZHPN0xt9/cWD1HUIyrXsOWTKA4LpJbNaJ.78Wwq0yd.sWcRT1s7O', 3, 1, NULL, '2016-09-21 23:28:45', '2016-09-21 23:29:04'),
(12, 'DEFAULT', 'default@imosa.hn', '$2y$10$n50wlPjAUeVF4k610DSLWOli5i2HUQvElcKwbFYcBiVVohxxlMepO', 3, 1, 'N23yW0vmN73mF2IJ1mBTQPoMgEDnQFgjKidrFDbKXgde6XGLY8OfAxhUqg55', '2016-09-22 01:57:32', '2016-09-22 20:46:28'),
(13, 'Ricardo Urrutia', 'ricardo@imosa.hn', '$2y$10$Su7j1V6BXy/1D2MkOLssr.KeciAlupnVYcpU7mKN3sxzf5wuToXRG', 1, 1, NULL, '2016-09-22 00:19:44', '2016-09-22 00:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `users_type`
--

CREATE TABLE `users_type` (
  `user_type` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_type`
--

INSERT INTO `users_type` (`user_type`, `description`) VALUES
(1, 'admin'),
(2, 'bodega'),
(3, 'vendedor');

-- --------------------------------------------------------

--
-- Table structure for table `vaucher`
--

CREATE TABLE `vaucher` (
  `vaucher_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cellars`
--
ALTER TABLE `cellars`
  ADD PRIMARY KEY (`cellar_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `company_invoice`
--
ALTER TABLE `company_invoice`
  ADD PRIMARY KEY (`company_invoice_id`),
  ADD KEY `fk_company` (`company_id`);

--
-- Indexes for table `credits`
--
ALTER TABLE `credits`
  ADD PRIMARY KEY (`credit_id`),
  ADD KEY `credits_customerFK` (`customer_id`);

--
-- Indexes for table `credits_replacement`
--
ALTER TABLE `credits_replacement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cronjob`
--
ALTER TABLE `cronjob`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD KEY `customersFK` (`customer_type`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `customers_type`
--
ALTER TABLE `customers_type`
  ADD PRIMARY KEY (`customer_type`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`),
  ADD KEY `fk_order_invoice` (`order_id`),
  ADD KEY `fk_company_invoice` (`company_invoice`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`);

--
-- Indexes for table `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`model_id`),
  ADD KEY `modelbrandFK` (`brand_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_customerFK` (`customer_id`);

--
-- Indexes for table `payments_history`
--
ALTER TABLE `payments_history`
  ADD PRIMARY KEY (`payments_history`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`provider_id`);

--
-- Indexes for table `pro_forma`
--
ALTER TABLE `pro_forma`
  ADD PRIMARY KEY (`proforma_id`),
  ADD KEY `fk_order_pro` (`order_id`),
  ADD KEY `fk_company_pro` (`company_id`);

--
-- Indexes for table `replacements`
--
ALTER TABLE `replacements`
  ADD PRIMARY KEY (`replacement_id`),
  ADD KEY `fk_cellar` (`cellar_id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `replacements_models`
--
ALTER TABLE `replacements_models`
  ADD PRIMARY KEY (`replacement_model`),
  ADD KEY `fk_model_repla` (`replacement_id`),
  ADD KEY `fk_repla_model` (`model_id`),
  ADD KEY `replacement_model` (`replacement_model`),
  ADD KEY `replacement_model_2` (`replacement_model`);

--
-- Indexes for table `replacements_orders`
--
ALTER TABLE `replacements_orders`
  ADD PRIMARY KEY (`replacement_order`),
  ADD KEY `fk_repla_order` (`replacement_id`),
  ADD KEY `fk_order_repla` (`order_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_type`
--
ALTER TABLE `users_type`
  ADD PRIMARY KEY (`user_type`);

--
-- Indexes for table `vaucher`
--
ALTER TABLE `vaucher`
  ADD PRIMARY KEY (`vaucher_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `cellars`
--
ALTER TABLE `cellars`
  MODIFY `cellar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `company_invoice`
--
ALTER TABLE `company_invoice`
  MODIFY `company_invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `credits`
--
ALTER TABLE `credits`
  MODIFY `credit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `credits_replacement`
--
ALTER TABLE `credits_replacement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `customers_type`
--
ALTER TABLE `customers_type`
  MODIFY `customer_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `models`
--
ALTER TABLE `models`
  MODIFY `model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=228;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payments_history`
--
ALTER TABLE `payments_history`
  MODIFY `payments_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `provider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pro_forma`
--
ALTER TABLE `pro_forma`
  MODIFY `proforma_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;
--
-- AUTO_INCREMENT for table `replacements`
--
ALTER TABLE `replacements`
  MODIFY `replacement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=639;
--
-- AUTO_INCREMENT for table `replacements_models`
--
ALTER TABLE `replacements_models`
  MODIFY `replacement_model` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1789;
--
-- AUTO_INCREMENT for table `replacements_orders`
--
ALTER TABLE `replacements_orders`
  MODIFY `replacement_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `users_type`
--
ALTER TABLE `users_type`
  MODIFY `user_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `company_invoice`
--
ALTER TABLE `company_invoice`
  ADD CONSTRAINT `fk_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customersFK` FOREIGN KEY (`customer_type`) REFERENCES `customers_type` (`customer_type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_company_invoice` FOREIGN KEY (`company_invoice`) REFERENCES `company_invoice` (`company_invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_invoice` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `models`
--
ALTER TABLE `models`
  ADD CONSTRAINT `modelbrandFK` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `order_customerFK` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pro_forma`
--
ALTER TABLE `pro_forma`
  ADD CONSTRAINT `fk_company_pro` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_pro` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `replacements`
--
ALTER TABLE `replacements`
  ADD CONSTRAINT `fk_cellar` FOREIGN KEY (`cellar_id`) REFERENCES `cellars` (`cellar_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `replacements_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`provider_id`);

--
-- Constraints for table `replacements_models`
--
ALTER TABLE `replacements_models`
  ADD CONSTRAINT `fk_model_repla` FOREIGN KEY (`replacement_id`) REFERENCES `replacements` (`replacement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_repla_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `replacements_orders`
--
ALTER TABLE `replacements_orders`
  ADD CONSTRAINT `fk_order_repla` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_repla_order` FOREIGN KEY (`replacement_id`) REFERENCES `replacements` (`replacement_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
