-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 24-08-2016 a las 16:53:23
-- Versión del servidor: 5.5.42
-- Versión de PHP: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `imosa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `brands`
--

INSERT INTO `brands` (`brand_id`, `description`) VALUES
(1, 'Yamaha'),
(2, 'Genesis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cellars`
--

CREATE TABLE `cellars` (
  `cellar_id` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cellars`
--

INSERT INTO `cellars` (`cellar_id`, `description`) VALUES
(1, 'Palma real'),
(2, 'Danli');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `rtn` varchar(255) NOT NULL,
  `web_site` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company_invoice`
--

CREATE TABLE `company_invoice` (
  `company_invoice_id` int(11) NOT NULL,
  `cai` varchar(255) NOT NULL,
  `range` varchar(255) NOT NULL,
  `limit_date` date NOT NULL,
  `isv` double NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `credits`
--

CREATE TABLE `credits` (
  `credit_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `balance` int(11) NOT NULL,
  `credit_condition` varchar(140) NOT NULL,
  `type` enum('Repuesto','Motocicleta') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `phone_number` varchar(140) NOT NULL,
  `address` varchar(255) NOT NULL,
  `full_name` varchar(140) NOT NULL,
  `RTN` varchar(14) DEFAULT NULL,
  `customer_type` int(11) NOT NULL,
  `customer_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `customers`
--

INSERT INTO `customers` (`customer_id`, `phone_number`, `address`, `full_name`, `RTN`, `customer_type`, `customer_id`) VALUES
(1, '97890174', 'Colonia san angel, Tegucigalpa, Honduras', 'Erick Zelaya', '07031992005239', 1, '0703199200523'),
(2, '23423423', 'Colonia palma real', 'Francisco Urrutia', '098098080808', 2, '0890924234234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers_type`
--

CREATE TABLE `customers_type` (
  `customer_type` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `customers_type`
--

INSERT INTO `customers_type` (`customer_type`, `description`) VALUES
(1, 'Mayorista'),
(2, 'Normal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `models`
--

CREATE TABLE `models` (
  `model_id` int(11) NOT NULL,
  `description` varchar(140) NOT NULL,
  `brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `models`
--

INSERT INTO `models` (`model_id`, `description`, `brand_id`) VALUES
(1, 'crux', 1),
(2, 'ybr', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `total` varchar(140) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`order_id`, `customer_id`, `total`, `date`, `active`) VALUES
(14, 2, '550', '2016-08-02 06:00:00', 1),
(16, 1, '200', '2016-08-01 06:00:00', 0),
(17, 1, '600', '2016-08-21 20:59:49', 0),
(18, 2, '850', '2016-08-22 16:49:45', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments_history`
--

CREATE TABLE `payments_history` (
  `payments_history` int(11) NOT NULL,
  `credit_id` int(11) NOT NULL,
  `payment_amount` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providers`
--

CREATE TABLE `providers` (
  `provider_id` int(11) NOT NULL,
  `name` varchar(140) NOT NULL,
  `country` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `providers`
--

INSERT INTO `providers` (`provider_id`, `name`, `country`) VALUES
(1, 'Viny', 'China'),
(2, 'Imbra', 'Colombia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_forma`
--

CREATE TABLE `pro_forma` (
  `proforma_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `replacements`
--

CREATE TABLE `replacements` (
  `replacement_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `bar_code` varchar(50) DEFAULT NULL,
  `brand` varchar(40) DEFAULT NULL,
  `description` varchar(140) DEFAULT NULL,
  `wholesale_price` double NOT NULL,
  `route_price` double NOT NULL,
  `picture` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `cellar_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `replacements`
--

INSERT INTO `replacements` (`replacement_id`, `code`, `bar_code`, `brand`, `description`, `wholesale_price`, `route_price`, `picture`, `location`, `quantity`, `cellar_id`) VALUES
(1, '123', NULL, 'Viny', 'Kit de cilindro', 120, 100, 'uploads/thumb_IMG-18.png', 'Estante 5', 1, 1),
(2, '12', NULL, 'Imbra', 'Catarinas', 100, 50, 'uploads/thumb_IMG-15.png', 'Estante 2', 5, 1),
(4, '123', NULL, 'Viny', 'Repuesto 4', 123, 123, 'uploads/thumb_IMG-81.png', 'estante 5', 94, 1);

-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `replacements_orders` (
  `replacement_order` int(11) NOT NULL AUTO_INCREMENT,
  `replacement_id` int(11) NOT NULL, 
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`replacement_order`),
  CONSTRAINT `fk_repla_order` FOREIGN KEY (`replacement_id`) REFERENCES `replacements` (`replacement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_order_repla` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Estructura de tabla para la tabla `replacements_models`
--

CREATE TABLE `replacements_models` (
  `replacement_model` int(11) NOT NULL,
  `replacement_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `replacements_models`
--

INSERT INTO `replacements_models` (`replacement_model`, `replacement_id`, `model_id`) VALUES
(31, 1, 1),
(32, 1, 2),
(33, 4, 1),
(34, 2, 2),
(35, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `replacements_orders`
--

CREATE TABLE `replacements_orders` (
  `replacement_order` int(11) NOT NULL,
  `replacement_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `replacements_orders`
--

INSERT INTO `replacements_orders` (`replacement_order`, `replacement_id`, `order_id`, `quantity`) VALUES
(9, 1, 16, 2),
(20, 2, 14, 1),
(23, 2, 18, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '2',
  `active` tinyint(1) DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `user_type`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Erick Alexander', 'erick@lynext.com', '$2y$10$OsTPufkDUAc3qtdAHsj2w.0sxmDLqDvdct.V9qcHoKR3c3i0RUOFe', 1, 1, 'nnfFRuXW1hRxaiW7dhAbXEzBNZSVwENzIbU84sJ7p9gNEm1XrHpmfwVGE3Ds', '2016-08-20 21:34:50', '2016-08-22 06:44:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_type`
--

CREATE TABLE `users_type` (
  `user_type` int(11) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vaucher`
--

CREATE TABLE `vaucher` (
  `vaucher_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indices de la tabla `cellars`
--
ALTER TABLE `cellars`
  ADD PRIMARY KEY (`cellar_id`);

--
-- Indices de la tabla `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indices de la tabla `company_invoice`
--
ALTER TABLE `company_invoice`
  ADD PRIMARY KEY (`company_invoice_id`),
  ADD KEY `fk_company` (`company_id`);

--
-- Indices de la tabla `credits`
--
ALTER TABLE `credits`
  ADD PRIMARY KEY (`credit_id`),
  ADD KEY `credits_customerFK` (`customer_id`);

--
-- Indices de la tabla `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD KEY `customersFK` (`customer_type`);

--
-- Indices de la tabla `customers_type`
--
ALTER TABLE `customers_type`
  ADD PRIMARY KEY (`customer_type`);

--
-- Indices de la tabla `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`),
  ADD KEY `fk_order_invoice` (`order_id`),
  ADD KEY `fk_company_invoice` (`company_id`);

--
-- Indices de la tabla `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`model_id`),
  ADD KEY `modelbrandFK` (`brand_id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_customerFK` (`customer_id`);

--
-- Indices de la tabla `payments_history`
--
ALTER TABLE `payments_history`
  ADD PRIMARY KEY (`payments_history`);

--
-- Indices de la tabla `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`provider_id`);

--
-- Indices de la tabla `pro_forma`
--
ALTER TABLE `pro_forma`
  ADD PRIMARY KEY (`proforma_id`),
  ADD KEY `fk_order_pro` (`order_id`),
  ADD KEY `fk_company_pro` (`company_id`);

--
-- Indices de la tabla `replacements`
--
ALTER TABLE `replacements`
  ADD PRIMARY KEY (`replacement_id`),
  ADD KEY `fk_cellar` (`cellar_id`);

--
-- Indices de la tabla `replacements_models`
--
ALTER TABLE `replacements_models`
  ADD PRIMARY KEY (`replacement_model`),
  ADD KEY `fk_model_repla` (`replacement_id`),
  ADD KEY `fk_repla_model` (`model_id`);

--
-- Indices de la tabla `replacements_orders`
--
ALTER TABLE `replacements_orders`
  ADD PRIMARY KEY (`replacement_order`),
  ADD KEY `fk_repla_order` (`replacement_id`),
  ADD KEY `fk_order_repla` (`order_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_type`
--
ALTER TABLE `users_type`
  ADD PRIMARY KEY (`user_type`);

--
-- Indices de la tabla `vaucher`
--
ALTER TABLE `vaucher`
  ADD PRIMARY KEY (`vaucher_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cellars`
--
ALTER TABLE `cellars`
  MODIFY `cellar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `company_invoice`
--
ALTER TABLE `company_invoice`
  MODIFY `company_invoice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `credits`
--
ALTER TABLE `credits`
  MODIFY `credit_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `customers_type`
--
ALTER TABLE `customers_type`
  MODIFY `customer_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `models`
--
ALTER TABLE `models`
  MODIFY `model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `payments_history`
--
ALTER TABLE `payments_history`
  MODIFY `payments_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `providers`
--
ALTER TABLE `providers`
  MODIFY `provider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pro_forma`
--
ALTER TABLE `pro_forma`
  MODIFY `proforma_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `replacements`
--
ALTER TABLE `replacements`
  MODIFY `replacement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `replacements_models`
--
ALTER TABLE `replacements_models`
  MODIFY `replacement_model` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `replacements_orders`
--
ALTER TABLE `replacements_orders`
  MODIFY `replacement_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `users_type`
--
ALTER TABLE `users_type`
  MODIFY `user_type` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `company_invoice`
--
ALTER TABLE `company_invoice`
  ADD CONSTRAINT `fk_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customersFK` FOREIGN KEY (`customer_type`) REFERENCES `customers_type` (`customer_type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_company_invoice` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_invoice` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `models`
--
ALTER TABLE `models`
  ADD CONSTRAINT `modelbrandFK` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `order_customerFK` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pro_forma`
--
ALTER TABLE `pro_forma`
  ADD CONSTRAINT `fk_company_pro` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_pro` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `replacements`
--
ALTER TABLE `replacements`
  ADD CONSTRAINT `fk_cellar` FOREIGN KEY (`cellar_id`) REFERENCES `cellars` (`cellar_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `replacements_models`
--
ALTER TABLE `replacements_models`
  ADD CONSTRAINT `fk_model_repla` FOREIGN KEY (`replacement_id`) REFERENCES `replacements` (`replacement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_repla_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `replacements_orders`
--
ALTER TABLE `replacements_orders`
  ADD CONSTRAINT `fk_order_repla` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_repla_order` FOREIGN KEY (`replacement_id`) REFERENCES `replacements` (`replacement_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
