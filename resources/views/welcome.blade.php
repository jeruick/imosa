<!DOCTYPE html>
<html>
    <head>
        <title>IMOSA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>IMOSA</title>
        <link rel="stylesheet" href="{{ asset('css/normalize.min.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    </head>
    <body>
        <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper">
                  <a href="#!" class="brand-logo"><img src="http://i.imgur.com/DTeoc5G.png" class="logo" alt=""></a>
                  <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                  <ul class="right hide-on-med-and-down">
                    <li><a href="#brands">Marcas</a></li>
                    <li><a href="#services">Servicios</a></li>
                    <li><a href="#contact">Contacto</a></li>
                    <li><a href="#motos">Motos</a></li>
                    <li><a href="#products">Productos</a></li>
                  </ul>
                  <ul class="side-nav" id="mobile-demo">
                    <li><a href="#brands">Marca</a></li>
                    <li><a href="#services">Servicios</a></li>
                    <li><a href="#contact">Contacto</a></li>
                    <li><a href="#motos">Motos</a></li>
                    <li><a href="#products">Productos</a></li>
                  </ul>
                </div>
              </nav>
        </div>
        

        <div class="slider">
           <ul class="slides">
             <li>
               <img src="{{ asset('images/banners/banner_vini.jpg') }}"> <!-- random image -->
               <div class="caption center-align">
                 <h3 class="red-text text-darken-1">Vini</h3>
                 <h5 class="light red-text text-darken-1">Oem parts</h5>
               </div>
             </li>
             <li>
               <img src="{{ asset('images/banners/banner2_vini.jpg') }}"> <!-- random image -->
               <div class="caption left-align">
                 <h3 class="red-text text-darken-1">Vini</h3>
                 <h5 class="light red-text text-darken-1">Fast enough</h5>
               </div>
             </li>
             <li>
               <img src="{{ asset('images/banners/imbra.jpg') }}"> <!-- random image -->
               <div class="caption left-align">
                 <h3 class="gray-text text-darken-1">Imbra</h3>
                 <h5 class="light gray-text text-darken-1">Producto 100% colombiano</h5>
               </div>
             </li>
           </ul>
         </div>

      

         <div class="row section scrollspy" id="products">
         <h4 class="title section scrollspy"><span>Nuestro productos</span></h4>
             <div style="width: 80%; margin: 0 auto;">
             <div class="col s12 l4 m6 who-whe-are">
                 <div class="card">
                     <div class="card-image waves-effect waves-block waves-light image">
                       <img class="activator" src="{{ asset('images/motor_parts.png') }}">
                     </div>
                     <div class="card-content">
                       <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                       <p><a href="#">This is a link</a></p>
                     </div>
                     <div class="card-reveal">
                       <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                       <p>Here is some more information about this product that is only revealed once clicked on.</p>
                     </div>
                   </div>
             </div>
             <div class="col l4 m6 s12">
                 <div class="card">
                     <div class="card-image waves-effect waves-block waves-light image">
                       <img class="activator" src="{{ asset('images/brakes.jpg') }}">
                     </div>
                     <div class="card-content">
                       <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                       <p><a href="#">This is a link</a></p>
                     </div>
                     <div class="card-reveal">
                       <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                       <p>Here is some more information about this product that is only revealed once clicked on.</p>
                     </div>
                   </div>
             </div>
             <div class="col l4 m6 s12">
                 <div class="card">
                     <div class="card-image waves-effect waves-block waves-light image">
                       <img class="activator" src="{{ asset('images/tires.jpg') }}">
                     </div>
                     <div class="card-content">
                       <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                       <p><a href="#">This is a link</a></p>
                     </div>
                     <div class="card-reveal">
                       <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                       <p>Here is some more information about this product that is only revealed once clicked on.</p>
                     </div>
                   </div>
             </div>
             <div class="col l4 m6 s12">
                 <div class="card">
                     <div class="card-image waves-effect waves-block waves-light image">
                       <img class="activator resize" src="{{ asset('images/chain.jpg') }}">
                     </div>
                     <div class="card-content">
                       <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                       <p><a href="#">This is a link</a></p>
                     </div>
                     <div class="card-reveal">
                       <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                       <p>Here is some more information about this product that is only revealed once clicked on.</p>
                     </div>
                   </div>
             </div>
             <div class="col l4 m6 s12">
                 <div class="card">
                     <div class="card-image waves-effect waves-block waves-light image">
                       <img class="activator resize" src="{{ asset('images/lub.png') }}">
                     </div>
                     <div class="card-content">
                       <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                       <p><a href="#">This is a link</a></p>
                     </div>
                     <div class="card-reveal">
                       <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                       <p>Here is some more information about this product that is only revealed once clicked on.</p>
                     </div>
                   </div>
             </div>
             <div class="col l4 m6 s12">
                 <div class="card">
                     <div class="card-image waves-effect waves-block waves-light image">
                       <img class="activator resize" src="{{ asset('images/heltment.jpg') }}">
                     </div>
                     <div class="card-content">
                       <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                       <p><a href="#">This is a link</a></p>
                     </div>
                     <div class="card-reveal">
                       <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                       <p>Here is some more information about this product that is only revealed once clicked on.</p>
                     </div>
                   </div>
             </div>
             <div class="col l4 m6 s12">
                 <div class="card">
                     <div class="card-image waves-effect waves-block waves-light image">
                       <img class="activator resize" src="{{ asset('images/gasket.jpg') }}">
                     </div>
                     <div class="card-content">
                       <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                       <p><a href="#">This is a link</a></p>
                     </div>
                     <div class="card-reveal">
                       <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                       <p>Here is some more information about this product that is only revealed once clicked on.</p>
                     </div>
                   </div>
             </div>
             <div class="col l4 m6 s12">
                 <div class="card">
                     <div class="card-image waves-effect waves-block waves-light image">
                       <img class="activator resize" src="{{ asset('images/batteries.jpg') }}">
                     </div>
                     <div class="card-content">
                       <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                       <p><a href="#">This is a link</a></p>
                     </div>
                     <div class="card-reveal">
                       <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                       <p>Here is some more information about this product that is only revealed once clicked on.</p>
                     </div>
                   </div>
             </div>
             <div class="col l4 m6 s12">
                 <div class="card">
                     <div class="card-image waves-effect waves-block waves-light image">
                       <img class="activator resize" src="{{ asset('images/cables.jpg') }}">
                     </div>
                     <div class="card-content">
                       <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
                       <p><a href="#">This is a link</a></p>
                     </div>
                     <div class="card-reveal">
                       <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                       <p>Here is some more information about this product that is only revealed once clicked on.</p>
                     </div>
                   </div>
             </div>
             </div>
         </div>

        <div class="parallax-container">
          <div class="parallax"><img src="images/banners/parallax1.jpg"></div>
        </div>

        <div class="row section scrollspy" id="services">
            <h4  class="title"><span>Servicios</span></h4>
            <div class="col s10 l3 service">
                <p class="center-align"><i class="material-icons large">settings</i></p>
                <p class="service-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus quidem eligendi explicabo. Aliquam doloremque neque dolore, laborum, sint blanditiis, deserunt dolorum magnam inventore expedita veniam dignissimos voluptate impedit cumque aliquid?  </p>
            </div>
            <div class="col s10 l3 service">
                <p class="center-align"><i class="fa fa-wrench large"></i></p>
                <p class="service-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id hic voluptates optio fugiat distinctio eos architecto, perferendis accusantium tempora blanditiis, numquam sed, est aperiam ullam. Adipisci, tempora officiis non. Exercitationem!</p>

            </div>
            <div class="col s10 l3 service">
                <p class="center-align"><i class="fa fa-motorcycle large"></i></p>
                <p class="service-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum earum hic ex repellendus quod libero, fugit, quos assumenda excepturi adipisci minus obcaecati totam et cupiditate omnis dolorum distinctio reiciendis est.</p>
            </div>
        </div>
        <div class="parallax-container">
          <div class="parallax"><img src="images/banners/parallax2.jpg"></div>
        </div>
        <div class="row section scrollspy" id="brands">
        <h4  class="title "><span>Marcas</span></h4>
            <div class="col s12 l4">
              <div class="card">
                <div class="card-image">
                  <img class="" src="{{ asset('images/marcas/vini.png') }}">
                  
                </div>
                <div class="card-content">
                  <p>I am a very simple card. I am good at containing small bits of information.
                  I am convenient because I require little markup to use effectively.</p>
                </div>
                <div class="card-action">
                  <a href="#">This is a link</a>
                </div>
              </div>
            </div>
            <div class="col s12 l4">
              <div class="card">
                <div class="card-image">
                  <img class="" src="{{ asset('images/marcas/imbra.png') }}">
                  
                </div>
                <div class="card-content">
                  <p>I am a very simple card. I am good at containing small bits of information.
                  I am convenient because I require little markup to use effectively.</p>
                </div>
                <div class="card-action">
                  <a href="#">This is a link</a>
                </div>
              </div>
            </div>
            <div class="col s12 l4">
              <div class="card">
                <div class="card-image">
                    <img style="margin:8.7em 0" src="{{ asset('images/marcas/w.png') }}">      
                  
                </div>
                <div class="card-content">
                  <p>I am a very simple card. I am good at containing small bits of information.
                  I am convenient because I require little markup to use effectively.</p>
                </div>
                <div class="card-action">
                  <a href="#">This is a link</a>
                </div>
              </div>
            </div>
          </div>
        <div class="parallax-container">
          <div class="parallax"><img src="images/banners/parallax4.jpg"></div>
        </div>
        <div class="row section scrollspy" id="contact">
            <h4  class="title"><span>Contactanos</span></h4>
            <div class="col s12 l6">
                <div class="contact">
                    <h4 class="title">Contacto</h4>
                    <div class="body">
                        <p>
                           <i class="material-icons">phone</i> Telefono: (+504)27631951
                        </p>
                        <p>
                            <i class="material-icons">phonelink_ring</i>Celular: 32516392
                        </p>
                        <p>
                            <i class="material-icons">language</i>Direccion: Contiguo a farmacia Kielsa #1. Danli, El paraiso
                        </p> 
                        
                    </div>   

                </div>
                
            </div>
            <div class="col s12 l6 map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61934.4369009583!2d-86.5912348908444!3d14.023804833905817!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f6e682d6789a60b%3A0x74cdede2bb676116!2sDanl%C3%AD%2C+Honduras!5e0!3m2!1ses!2s!4v1445490101861"  height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <footer class="page-footer">
          <div class="container">
            
          </div>
          <div class="footer-copyright">
            <div class="container">
            Derechos reservados © CVO 2015 
            <a class="grey-text text-lighten-4 right" href="http://lynext.com">Desarrollado por Lynext</a>
            </div>
          </div>
        </footer>
        
    </body>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>
    <script>

    jQuery(document).ready(function($) {
        $('.slider').slider();
        $(".button-collapse").sideNav();
        $('.scrollspy').scrollSpy();
        $('.parallax').parallax();
    });
    </script>

</html>
