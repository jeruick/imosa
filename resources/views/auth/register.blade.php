@extends('layouts.auth')

@section('title', 'Ingresar al sistema')

@section('content')


	<div class="row valign-wrapper register">

		<div class="small-12 medium-8 medium-offset-2 columns panel">

			<form action="register" method="POST">
				{!! csrf_field() !!}

				<div class="row">
					<div class="small-12 columns">
						<label for=""> Nombre Completo: 
							<input type="text" name="name" placeholder='nombre completo' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Email: 
							<input type="email" name="email" placeholder='me@example.com' required>
						</label>
					</div>
					@if (count($errors) > 0 && $errors->first('email'))
						<div class="small-12 columns">
							<span class="error">{{ $errors->first('email') }}</span>
						</div>
					@endif
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Contraseña: 
							<input type="password" name="password" placeholder='contraseña' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Repetir contraseña: 
							<input type="password" name="password_confirmation" placeholder='contraseña' required>
						</label>
					</div>
					@if (count($errors) > 0 && $errors->first('password'))
						<div class="small-12 columns error">
							<span class="error">{{ $errors->first('password') }}</span>
						</div>
					@endif
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Repetir contraseña: 
							<input type="password" name="password_confirmation" placeholder='contraseña' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<div class="clearfix">
							<button type="submit" class="button right">Ingresar</button>
						</div>	
					</div>
				</div>
			</form>
		</div>
	</div>
@stop