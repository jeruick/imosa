@extends('layouts.auth')


@section('content')
	<div class="row valign-wrapper">
		<div class="small-12 medium-8 medium-offset-2 columns panel">
			<form action="login" method="POST">
				{!! csrf_field() !!}
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Email: 
							<input type="email" name="email" placeholder='me@example.com' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Contraseña: 
							<input type="password" name="password" placeholder='contraseña' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<div class="clearfix">
							<a href="register">Solicitar cuenta al administrador</a>
							<button type="submit" class="button right"><i class="fa fa-sign-in"></i> Ingresar</button>
						</div>	
					</div>
					
				</div>
				@if ($errors->any())
					<span class="error">{{$errors->first()}}</span>
				@endif
			</form>
		</div>
	</div>
@stop