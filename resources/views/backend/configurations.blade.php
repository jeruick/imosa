@extends('layouts.master')

@section('title', 'Configuraciones')

@section('content')
	@parent

<section class="modules margin-top">
		<div class="row" >
			<div class="small-12 medium-4 columns" >
				<a class="button hollow  expanded" style="padding: 4em; font-size:1.5em;" href="{{route ('brands')}}">
				<p><i class="fa fa-motorcycle fa-2x"></i></p>
					Marcas
				</a>
			</div>
			<div class="small-12 medium-4 columns" >
				<a  class="button hollow expanded" style="padding: 4em; font-size:1.5em;" href="{{route ('company')}}">
				<p><i class="fa fa-building fa-2x"></i></p>
					Empresa
				</a>
			</div>	
			<div class="small-12 medium-4 columns" >
				<a  class="button hollow expanded" style="padding: 4em; font-size:1.5em;" href="{{ route ('companyinvoice')}}">
				<p><i class="fa fa-calculator fa-2x"></i></p>

					DEI
				</a>
			</div>
			<div class="small-12 medium-4 columns" >
				<a  class="button hollow expanded" style="padding: 4em; font-size:1.5em;" href="{{route ('cellars')}}">
				<p><i class="fa fa-object-ungroup  fa-2x"></i></p>
					Bodegas
				</a>
			</div>
			<div class="small-12 medium-4 columns" >
				<a  class="button hollow expanded" style="padding: 4em; font-size:1.5em;" href="{{route ('providers')}}">
				<p><i class="fa fa-truck fa-2x"></i></p>
					Proveedores
				</a>
			</div>
			<div class="small-12 medium-4 columns" >
				<a  class="button hollow expanded" style="padding: 4em; font-size:1.5em;" href="{{route ('providers')}}">
				<p><i class="fa fa-map-o fa-2x"></i></p>
					Ciudades
				</a>
			</div>
		</div>
	</section>
@stop