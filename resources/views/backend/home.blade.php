@extends('layouts.master')

@section('title', 'Bienvenido')

@section('content')
	@parent
	@if(Auth::user()->type->description == 'admin')
	<section class="modules margin-top">
		<div class="row">
			<div class="small-12 medium-4 columns" >
				<a class="button" href="{{ route('inventory') }}">
					<img src="images/inventory.png" alt="">
					Inventario
				</a>
			</div>
			<div class="small-12 medium-4 columns" >
				<a class="button" href="{{ route('getCustomerCredits') }}">
					<img src="images/debt.png" alt="">
					Creditos
				</a>
			</div>
			<div class="small-12 medium-4 columns" >
				<a class="button" href="{{route('orders')}}">
					<img src="images/order4.png" alt="">
					ordenes
				</a>
			</div>
			
			<div class="small-12 medium-4 columns" >
				<a class="button" href="{{ route('constancies') }}">
					<img src="images/document.png" alt="">
					Constancias
				</a>
			</div>
			
			<div class="small-12 medium-4 columns" >
				<a class="button" href="{{ route('providers') }}">
					<img src="images/provider.png" alt="">
					Proveedores
				</a>
			</div>
		</div>
	</section>
	@elseif(Auth::user()->type->description == 'vendedor')
	<section class="modules margin-top">
		<div class="row">
			<div class="small-12 medium-4 columns" >
				<a class="button" href="{{ route('inventory') }}">
					<img src="images/inventory.png" alt="">
					Inventario
				</a>
			</div>
			<div class="small-12 medium-4 columns" >
				<a class="button" href="{{route('orders')}}">
					<img src="images/order4.png" alt="">
					ordenes
				</a>
			</div>
		</div>
	</section>
	@else
	<section class="modules margin-top">
		<div class="row">
			<div class="small-12 medium-4 columns" >
				<a class="button" href="{{route('orders')}}">
					<img src="images/order4.png" alt="">
					ordenes
				</a>
			</div>
		</div>
	</section>
	@endif
@stop