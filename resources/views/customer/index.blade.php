@extends('layouts.master')	

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop

@section('content')
	@if ($errors->any())
		<ul>
			@foreach ($errors as $err)
				<li><span class="error">{{$err}}</span></li>
			@endforeach
		</ul>
	@endif
	<div class="row">
		<div class="small-12 columns">
			<a href="{{ route('home') }}" class="button fa fa-arrow-left radius left" >  Atras</a>
			<a class="button radius right" data-open="myModal"><i class="fa fa-user"></i> Nuevo Cliente</a>
		</div>
	
	<div class="row">
		<div class="small-12 columns">
			<label>Nombre/RTN de cliente</label>
			<input type="text" class="search-customers" placeholder="buscar por identidad,rtn, nombre...">
		</div>
	</div>
	
	<div class="row">
		<div class="small-12 columns">
			<table class="responsive" class="my-table">
				<thead>
					<th width="200">CODIGO</th>
					<th width="300">COMPAÑIA</th>
					<th width="200">CONTACTO</th>
					<th width="200">CIUDAD</th>
					<th width="200">TELEFONO</th>
					<th width="200">RTN</th>
					<th>ELIMINAR</th>
				</thead>
				<tbody class="customers-body">
					@foreach ($customers as $key => $element)
						<tr>
							<td style="text-align: center">{{$element->customer_id}}</td>
							<td><a href="{{ route('customer', $element->customer_id) }}">{{$element->full_name}}</a></td>
							<td>{{$element->contact}}</td>
							
							<td>{{$element->city->name}}</td>
							<td>{{$element->phone_number}}</td>
							<td>{{$element->RTN}}</td>
							<td><a href="{{ route('deleteCustomer', $element->customer_id) }}" class="button alert"><i class="fa fa-remove"></i></a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	{{-- Pagination --}}
	{!! str_replace('/?', '?',$customers->render(new LaravelFoundation\Pagination\SimpleFoundationFivePresenter($customers, ["pagination-centered"=>'true']))) !!}

	{{-- Customer Modal --}}
	<div id="myModal" class="full reveal" data-reveal style="z-index:10">
		<br><br>
		<h2>Nuevo Cliente</h2>
		<p>
			<form action="{{ route('newCustomer')}}" method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}

				<div class="row">
					<div class="medium-6 columns col-1">
						<div class="small-12 columns">
							<label for="">
								Nombre de la empresa
								<input  type="text" name="full_name" required>
							</label>
						</div>	
						
						<div class="small-12 columns">
							<label for="">
								RTN
								<input type="text"  onkeypress="return isNumber(event)" maxlength="14" name="RTN">
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Nombre del contacto
								<input type="text" name="contact" >
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Vendedor asignado
							
								<select  autocomplete="off" name="user_id" required="">
									@if (count($users) > 0)
										@foreach ($users as $user)
											<option value="{{$user->id}}">{{$user->name}}</option>
										@endforeach
									@else
										<option value="">No hay ningun vendedor registrado en el sistema</option>
									@endif
									
								</select>
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Departamento
							
								<select onchange="get_cities(this)" autocomplete="off" required="">
									<option selected="selected" disabled="">Selecciona un departamento</option>
									@if (isset($departments))
										@foreach ($departments as $element)
											<option value="{{$element->department_id}}">{{$element->name}}</option>
										@endforeach
									@else
										<option value="">No hay ningun departamento en el sistema</option>
									@endif
									
								</select>
							</label>
						</div>

						@include('customer.cities')
					</div>
					<div class="medium-6 columns col-2">

						<div class="small-12 columns">
							<label for="">
								Tipo de cliente
							
								<select name="customer_type" id="">
									@if (isset($customer_type))
										@foreach ($customer_type as $element)
											<option value="{{$element->customer_type}}">{{$element->description}}</option>
										@endforeach
									@else
										<option value="">No hay ningun tipo de cliente</option>
									@endif
									
								</select>
							</label>
						</div>
						
						<div class="small-12 columns">
							<label for="">
								Telefono Fijo
								<input type="text" onkeypress="return isNumber(event)" maxlength="9" name="telephone" >
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Telefono celular
								<input type="text" onkeypress="return isNumber(event)" maxlength="9" name="phone_number" >
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Telefono celular 2
								<input type="text" name="phone_number2" onkeypress="return isNumber(event)" maxlength="9">
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Correo electronico
								<input type="email" name="email" >
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Direccion
								<textarea name="address" id="" cols="10" rows="2" style="resize:none"></textarea>	
							</label>
						</div>

					</div>
					
				</div>

				<div class="row">
	  		     	<div class="small-12 columns">
	  		     		<button class="right button fa fa-floppy-o" type="submit">  Guardar</button>
	  		     	</div>
	  		     </div>
			</form>
		</p>	
		<button class="close-button" data-close aria-label="Close reveal" type="button">
		    <span aria-hidden="true">&times;</span>
		</button>
	</div>
@stop

@section('script')
	@parent
	<script src="{{ asset('js/responsive-tables.js') }}"></script>
	<script src="{{ asset('js/jquery.dynatable.js') }}"></script>
	<script>
		jQuery(document).ready(function($) {
			$('.search-customers').keyup(function(event) {
				if(event.keyCode == 13){
					var text = $(this).val() ? $(this).val() : 'blank';

					$.get('clientes/buscar/' + text, function(data) {

						if (data) {
							
							if(data.redirect)
							{
								window.location.href = data.redirect;
							}

							$('.customers-body').html('');
							
							console.log(data);
							$.each(data, function(index, val) {
								var markup = "<tr>" +
						  			"<td>" + val.customer_id + "</td>" +
						  			"<td><a href='cliente/" + val.customer_id + "'>" + val.full_name + "</a></td>" +
						  			"<td>" + val.contact + "</td>" +
						  			"<td>" + val.city_id + "</td>" +		
						  		  	"<td>" + val.phone_number + "</td>" +
						  		  	"<td>" + val.RTN + "</td>" +
						  		"</tr>";
						  		$('.customers-body').append(markup);
							})
						}

					})
				}
			});
		});

		function get_cities(element){
			$.ajax({
				url: "departments/" + $(element).val(),
				type: 'GET',
			})
			.done(function(data) {
				$('#cities').html(data);
			})
			.fail(function(error) {
				console.log('error');
			});
		}
	</script>
@stop