<div id="cities">
	<div class="small-12 columns" >
		<label for="">
			Ciudad
			<select name="city_id" required="">
				@if(isset($customer))
					@foreach ($cities as $element)
						<option value="{{$element->city_id}}" @if($customer->city_id == $element->city_id) selected="selected" @endif>{{$element->name}}</option>
					@endforeach
				@elseif (isset($cities))
					@foreach ($cities as $element)
						<option value="{{$element->city_id}}">{{$element->name}}</option>
					@endforeach
				@else
					<option value="">Seleccione una ciudad</option>
				@endif
			</select>
		</label>
	</div>	
</div>
