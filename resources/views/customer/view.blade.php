@extends('layouts.master')


@section('content')
	<form id="myform" action="{{ route('updateCustomer', $customer->customer_id)}}" method="POST" >
	<div class="row">
		<div class="small-12 columns">
			<a href="{{ URL::previous() }}" class="button"><i class="fa fa-arrow-left"></i> Atras</a>
			<button class="primary button right" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
		</div>
	</div>
	@if ($customer->first())

		<div class="row">
			<div class="small-12 columns">
				
					{!! csrf_field() !!}
					<div class="medium-6 columns">
						<div class="small-12 columns">
							<label for="">
								Nombre 
								<input type="text" name="full_name" value="{{$customer->full_name}}" required>
							</label>
						</div>
						
						<div class="small-12 columns">
							<label for="">
								Tipo de cliente
							
								<select name="customer_type" id="">
									@if (isset($customer_type))
										@foreach ($customer_type as $element)
											<option @if ($element->customer_type == $customer->customer_type)selected="selected" @endif value="{{$element->customer_type}}">{{$element->description}}</option>
										@endforeach
									@else
										<option value="">No hay ningun tipo de cliente</option>
									@endif
									
								</select>
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Vendedor asignado
							
								<select  autocomplete="off" name="user_id" required="">
									@if (count($users) > 0)
										@foreach ($users as $user)
											<option value="{{$user->id}}" @if($user->id == $customer->user_id) selected="selected" @endif>{{$user->name}}</option>
										@endforeach
									@else
										<option value="">No hay ningun vendedor registrado en el sistema</option>
									@endif
									
								</select>
							</label>
						</div>
						
						<div class="small-12 columns">
							<label for="">
								Telefono Fijo
								<input type="text" name="telephone" onkeypress="return isNumber(event)" maxlength="9" value="{{$customer->telephone}}">
							</label>
						</div>
						<div class="small-12 columns">
							<label for="">
								Telefono celular
								<input type="text" name="phone_number" onkeypress="return isNumber(event)" maxlength="9" value="{{$customer->phone_number}}">
							</label>
						</div>
						<div class="small-12 columns">
							<label for="">
								Telefono celular 2
								<input type="text" name="phone_number2" onkeypress="return isNumber(event)" maxlength="9" value="{{$customer->phone_number2}}">
							</label>
						</div>
						
					</div>

					<div class="medium-6 columns">
						<div class="small-12 columns">
							<label for="">
								Correo electronico
								<input type="email" name="email" value="{{$customer->email}}">
							</label>
						</div>
						<div class="small-12 columns">
							<label for="">
								Departamento
							
								<select onchange="get_cities(this)" autocomplete="off" required="">
									@if (isset($departments))
										@foreach ($departments as $element)
											<option value="{{$element->department_id}}" @if($element->department_id == $customer->city->department->department_id) selected="selected" @endif>{{$element->name}}</option>
										@endforeach
									@else
										<option value="">No hay ningun departamento en el sistema</option>
									@endif
									
								</select>
							</label>
						</div>

						@include('customer.cities')

						<div class="small-12 columns">
							<label for="">
								RTN
								<input onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" maxlength="14" type="text"  name="RTN" value="{{$customer->RTN}}">
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Nombre de contacto
								<input  maxlength="14" type="text"  name="contact" value="{{$customer->contact}}">
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Direccion
								<textarea name="address" cols="10" rows="2" style="resize:none">{{$customer->address}}</textarea>	
							</label>
						</div>
					</div>					
			</div>
		</div>
	@endif
	</form>
@stop

@section('script')
	@parent
	<script>
		function get_cities(element){
			$.ajax({
				url: "../departments/" + $(element).val(),
				type: 'GET',
			})
			.done(function(data) {
				$('#cities').html(data);
			})
			.fail(function(error) {
				console.log('error');
			});
		}
	</script>
@stop
