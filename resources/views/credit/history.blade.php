@extends('layouts.master')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{asset('css/print.css')}}" media='print'>
@stop


@section('content')
	<br>
	<br>
	<h2 class="panel">HISTORIAL DE PAGOS</h2>
	@if ($errors->any())
		<ul>
			@foreach ($errors->all() as $error)
				<li><span class="error">{{$error}}</span></li>
			@endforeach
		</ul>
	@endif
	<div class="full-width">
		<div class="small-12 columns">
			<a href="{{route('getCredits', $credit->customer_id) }}" class="fa fa-arrow-left button radius back-button"> Atras</a>
			@if ($credit->balance > 0)
				<a href="#" class="button success radius right pay" data-open="myModal"><i class="fa fa-money"></i> Abonar</a>
			@endif
			
		</div>
	</div>

	@if (session()->has('message'))
	
	<div class="row">
		<div class="small-12 colums">
			<div data-alert class="alert-box success radius">
			  {{session('message')}}
			  <a href="#" class="close">&times;</a>
			</div>	
		</div>
	</div>
	@endif
		<div class="row">
					<div class="small-12 columns">
						@if ($payment->first())
						<h4>CLIENTE: {{$credit->customer->full_name}}</h4>
						<table class="responsive">
							<thead>
						    	<tr>
							      	<th width="400">Abono</th>
							      	<th width="400">Fecha</th>
								</tr>
							</thead>
							<tbody>
									@foreach($payment as $element)
										<tr>
											<td>
												Lps. {{$element->payment_amount}}	
											</td>
											<td>
												{{ date('d-m-Y', strtotime($element->created_at)) }}
											</td>
										</tr>
									@endforeach
							</tbody>
						</table>
						<div class="small-6 columns">
							<div class="row">
								<div class="small-12 columns">
									<strong class="right">Total: L. {{$credit->amount}}</strong>
									<br>
									<strong class="right">Saldo Pendiente: L. {{$credit->balance}}</strong>
								</div>
							</div>
					</div>
				</div>
			</div>
			@else
				<div class="row">
					<h2 class="panel">No hay abonos todavía</h2>
				</div>
			@endif
			
		
		
			{{-- Modal --}}
			<div id="myModal" class="reveal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			  
			  <p>
			  	<form action="{{ route('payCredit', $credit->credit_id) }}" method="POST" >
			  		{!! csrf_field() !!}
		  		    <div class="row">
		  		    	<div class="small-12 columns">
		  		    		<label for=""><strong>Saldo Actual: {{$credit->balance}}</strong></label>
		  		    	</div>
		  		    </div>
		  		  	<div class="row">
		  		  	    <div class="small-12 columns">
		  		  	      <label>Monto
		  		  	        <input type="number" name="amount" step="0.01" required>
		  		  	      </label>
		  		  	    </div>
		  		  	</div>

		  		  	
		  		     <div class="row">
		  		     	<div class="small-12 columns">
		  		     		<button  class="button"	type="submit"><i class="fa fa-save"></i> Guardar</button>
		  		     	</div>
		  		     </div>
			  		</form>
			  </p>
			  <button class="close-button" data-close aria-label="Close modal" type="button">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>

		
	
@stop

@section('script')
	@parent
	<script src="{{ asset('js/responsive-tables.js') }}"></script>
@stop