@extends('layouts.master')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
@stop

@section('content')
	@if ($errors->any())
		<ul>
			@foreach ($errors->all() as $error)
				<li><span class="error">{{$error}}</span></li>
			@endforeach
		</ul>
	@endif

	<div class="full-width">
		<div class="small-12 columns">
			<a href="{{ route('home') }}" class="fa fa-arrow-left button radius left">  Atras</a>
		</div>
  	</div>
	<div class="row">
		<div class="small-12 colums">
			@if ($customers)
				<table class="responsive">
				  <thead>
				    <tr>
				      	<th width="100">DETALLE</th>
				      	<th width="100">CODIGO</th>
				      	<th width="300">COMPAÑIA</th>
				      	<th width="200">IMPORTE</th>
					</tr>
				  </thead>
				  <tbody class="credits-body">
					@foreach($customers as $customer)
						<tr>
							<td><a href="{{ route('getCredits', $customer->customer_id) }}" class="button radius"><i class="fa fa-search"></i></a></td>
							<td>{{$customer->customer_id}}</td>
							<td>{{$customer->full_name}}</td>
							<td>L. {{$customer->importe}}</td>
						</tr>
					@endforeach
				  </tbody>
				</table>
			@else
			<div class="row">
				<br>
				<h2>No hay creditos</h2>
			</div>
			@endif
			
			
		</div>
	</div>


			
@stop

@section('script')
	@parent
	<script src="{{ asset('js/responsive-tables.js') }}"></script>
	<script>
	jQuery(document).ready(function() {
		$('.search-motorcycles').keyup(function(event) {
			if (event.keyCode == 13) {
				$('.motorcycles-list').css('display', 'block');
				var $text = $(this).val() ? $(this).val() : 'blank';
				var url = "../../motos/buscar";
				$.ajax({
					url: url ,
					type: 'POST',
					dataType: 'json',
					data: {_token : $('meta[name=_token]').attr('content'),text: $text}
				})
				.done( function(data) {
					
					if(data.redirect)
					{
						console.log('blank');	
						$('.motorcycles-list').css('display', 'none'); 
					}
					else if (data.length != 0) {
						console.log(data);
						$('.motorcycles-list').html('');
						$.each(data, function(index, val) {
							var $markup = "<li id='" + val.motorcycle_id + "' onclick='select_motorcycle(this)' ><span class='description'>" + val.brand_description + " - " + val.model_description + " - " + val.plate_number + "</span><span class='right'>existencias: " + val.quantity +  "</span></li>"
							$('.motorcycles-list').append($markup)	 
						})
						
					}
					else
					{
						$('.motorcycles-list').css('display', 'none'); 
					}

				})
			} 
		});

		$('.search-replacements').keyup(function(event) {
			if(event.keyCode == 13)
			{
				$('.replacements-list').css('display', 'block');
				var $text = $(this).val() ? $(this).val() : 'blank';
				var url = "../../repuesto/buscar/" + $text;
				$.ajax({
					url: url ,
					type: 'GET',
					dataType: 'json'
				})
				.done( function(data) {
					
					if(data.redirect)
					{	
						$('.replacements-list').css('display', 'none'); 
					}
					else if (data.length != 0) {
						
						$('.replacements-list').html('');
						$.each(data, function(index, val) {
							var $markup = "<li id='" + val.replacement_id + "' onclick='select_replacement(this)' >" +
							"<span class='description'>" + val.description + "</span><span class='right'>existencias: " + val.quantity +   "</span></li>"
							$('.replacements-list').append($markup)	 
						})
						
					}
					else
					{
						$('.replacements-list').css('display', 'none'); 
					}

				})
			}
			
		})


	})
	function update_total () {
		
		if ($('.amount').length > 0) {
			
			if(localStorage.getItem('replacements')){
				var $replacements = JSON.parse(localStorage.getItem('replacements'))
				
				$.ajax({
					url: "{!! route('totalReplacements') !!}",
					type: 'POST',
					data: {_token: $('meta[name=_token]').attr('content'),replacements: $replacements, type: 'replacements'},
				})
				.done(function(data) {
					console.log(data);
					if (data) {
						$('.amount').val(data);
					}
				})
				
			}
			else
			{
				$('.amount').val("0.0");
			}
		} 
		
	}

	
	</script>
@stop