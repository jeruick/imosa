@extends('layouts.master')
@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{asset('css/print.css')}}" media='print'>
@stop

@section('content')
	<div class="full-width">
			<div class="small-12 columns">
				<a href="{{route('getCredits')}}" class="fa fa-arrow-left back button radius"> Atras</a>
				
			</div>
	</div>
	@if ($errors->any())
		<ul>
			@foreach ($errors->all() as $error)
				<li><span class="error">{{$error}}</span></li>
			@endforeach
		</ul>
	@endif
	
	@if (session()->has('message'))
	
	<div class="row">
		<div class="small-12 columns">
		    <div class="callout secondary">
		      <h5>{{session('message')}} </h5>
		    </div>
		</div>
	</div>
	@endif
	

	@if(isset($order))
		
		
		{{-- Update order --}}
		
		<form action="{{ route('updateOrder', $order->order_id) }}" method="POST">
			<input type="hidden" name="customer" value="{{$order->customer_id}}">
			<input type="hidden" name="order_id" class="order_id" value="{{$order->order_id}}">
		{!! csrf_field() !!}
		<div class="row">
			<div class="small-12 columns">
				<span class="left">Tipo de documento: @if($order->is_invoice) Factura @else Proforma  @endif</span>
				<span class="right">Correlativo: 
				@if($order->invoices->first()) 
					@foreach($order->invoices as $invoice)
						{{$invoice->invoice_id}}
					@endforeach
				@else
					@foreach($order->proformas as $proforma)
						{{$proforma->proforma_id}} - {{date('Y')}}
					@endforeach

				@endif</span>
			</div>
			<div class="small-12 columns">
				<table class="responsive">
					<thead>
						<th width="300">CLIENTE</th>
						<th width="200">CIUDAD</th>
						<th width="500">DIRECCION</th>
						<th width="200">RTN</th>
						<th width="200">TELEFONO</th>
					</thead>
					<tbody class="customers-body">
						<tr>
							<td>{{$order->customer->full_name}}</td>
							<td class= 'customer_id'>{{$order->customer->city->name}}</td>
							<td>{{$order->customer->address}}</td>
							<td>{{$order->customer->RTN}}</td>
							<td>{{$order->customer->phone_number}}</td>
						</tr>
					</tbody>
					
				</table>
			</div>
		
		</div>
		
		<div class="items">
@if($order)
<div class="row">
	<div class="small-12 columns">
		@if ($order->replacements)
		<table id="table" class="responsive">
			<thead>
				<th style="text-align: center">No.</th>
				<th style="text-align: center">CODIGO</th>
				<th style="text-align: center" >DESCRIPCION</th>
				@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'bodega')
				<th style="text-align: center" width="110px">BODEGA</th>
				<th style="text-align: center" >UBICACION</th>
				@endif
				@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
				<th style="text-align: center" >P.UNIDAD</th>
				@endif
				<th style="text-align: center" width="100">CANTIDAD</th>
				@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
				<th style="text-align: center">P.TOTAL</th>
				@endif
				
								
			</thead>
			<tbody class="replacements-body" >
				
				@foreach ($order->replacements as $key => $element)
					
					<tr id="{{$element->getReplacement->replacement_id}}">
						<td style="text-align: center">{{$key + 1}}</td>
						<td style="text-align: center">{{$element->getReplacement->code}}</td>
						<td style="text-align: center">
							{{$element->getReplacement->description}}
							<input type="hidden" name="replacements[{{$element->getReplacement->replacement_id}}][replacement_id]" value="{{$element->getReplacement->replacement_id}}">
						</td>
						@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'bodega')
						<td>
							{{$element->getReplacement->cellar->description}}
						</td>
						<td>
							{{$element->getReplacement->location}}
						</td>
						@endif
						@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
						<td style="text-align: center">
							@if ($order->customer->type->description == 'LOCAL')
								{{number_format($element->getReplacement->wholesale_price, 2, '.', ',')}}
							@else
								{{number_format($element->getReplacement->route_price, 2, '.', ',')}}	
							@endif
						</td>
						@endif
						<td style="text-align: center"> {{$element->quantity}}
						<input type="hidden" name="replacements[{{$element->getReplacement->replacement_id}}][quantity]" value="{{$element->quantity}}">
							
						</td>
						@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
						<td style="text-align: center">
							@if ($order->customer->type->description == 'LOCAL')
								{{number_format($element->getReplacement->wholesale_price*$element->quantity, 2, '.', ',')}}
							@else
								{{number_format($element->getReplacement->route_price*$element->quantity, 2, '.', ',')}}	
							@endif
						</td>
						@endif
						
						
					</tr>

				@endforeach
				
			</tbody>
		</table>
		@endif
	</div>
</div>
<div class="row">
	<div class="small-4 medium-2 small-offset-8 medium-offset-10 columns">
		<table>
			<tr>

				<td><strong>Total: L.</strong></td>
				<td><strong><span class="total">{{number_format((float)$order->total, 2, '.', ',')}}</span></strong></td>
			</tr>
			<tr @if($credit->balance > 0) style="background-color: #da3116" @else  style="background-color: #3adb76" @endif>

				<td><strong>Saldo: L.</strong></td>
				<td><strong><span  class="total">{{number_format((float)$credit->balance, 2, '.', ',')}}</span></strong></td>
			</tr>
		</table>
		<p class="right">
			</strong>
		</p>
		<p class="right">
			</strong>
		</p>
		<p class="right">
			
		</p>
	</div>
</div>

<div class="row">
	<div class="small-12 columns">
		<a class="button left" href="{{ route('paymentHistory', $credit->credit_id) }}" ><i class="fa fa-history"></i> HISTORIAL DE PAGOS</a>
	</div>
	
</div>


@if ($order->active && (isset($is_ready) && $is_ready ))
		<div class="row">
			<div class="small-12 columns">
				<button type="submit" class ="fa fa-check-circle button radius right" > Cerrar orden</button>		
			</div>
		</div>
		@endif

@endif

</div>
		
	</form>
	@endif
@stop