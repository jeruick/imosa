@extends('layouts.master')

@section('content')
<div class="full-width">
		<div class="small-12 columns">
			<a href="{{route('getCustomerCredits')}}" class="fa fa-arrow-left back button radius"> Atras</a>
		</div>
</div>

	@if (!empty($credits))
		<div class="row">
			<div class="small-12 columns orders">
				@if($credits && count($credits) > 0)
					<table>
						<thead>
							<th width="100">DETALLE</th>
							<th width="150">No. CREDITO</th>
							<th width="300">CLIENTE</th>
							<th width="100">TOTAL</th>
							<th width="100">SALDO</th>
							<th width="100">DIAS DE CREDITO</th>
							<th width="100">FECHA</th>
							<th width="50">ELIMINAR</th>
						</thead>
						<tbody>
							@foreach($credits as $credit)
							<tr>
								<td><a href="{{ route('getCredit', $credit->credit_id) }}" class="button radius"><i class="fa fa-search"></i></a></td>
								<td>{{$credit->credit_id}}</td>
								<td>{{$credit->customer->full_name}}</td>
								<td>{{$credit->amount}}</td>
								<td @if($credit->balance > 0) style="color: #da3116" @else  style="color: #3adb76" @endif>{{$credit->balance}}</td>
								<td>{{$credit->credit_condition}}</td>
								<td>{{date('d-m-Y', strtotime($credit->created_at))}}</td>
								<td><button id="{{ route('deleteCredit', $credit->credit_id) }}" class="button alert radius" onclick="check_admin_password(this)"><i class="fa fa-remove"></i></button></td>
							</tr>
							@endforeach
						</tbody>

					</table>

				@endif

			</div>
		</div>
		{{-- Pagination --}}
		
	@else
			<div class="row">
				<div class="small-12 columns">
					<h4 class="panel text-center">No hay ordenes al crédito</h4>
				</div>
			</div>
	@endif





@stop

@section('script')
	@parent
	<script>
		function check_admin_password(element)
		{	
			swal({ title: "Contraseña de administrador",  type: "input", inputType: 'password',  showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   inputPlaceholder: "Contraseña" }, function (inputValue){
				if (inputValue === false) return false;      
				if (inputValue === "") {     
					swal.showInputError("Escribe una contraseña");     
					return false   
				}    
				
				$.post("{{route('checkAdminPassword')}}",
				{
					'password': inputValue, 
					'_token': $('meta[name=_token]').attr('content')
				})
				.success(function(data) {
					if(data.id)
					{
						swal("Eliminada", "El crédito ha sido eliminada correctamente");
						window.location.replace($(element).attr('id'));
						
					}
					else
					{

						swal.showInputError("Contraseña incorrecta");     
					}
				})
				.fail(function() {
					console.log("error");
				});
			});
		}

	</script>
@stop
	