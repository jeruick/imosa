

@if ($orders)
<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Cliente</th>
			<th>Usuario</th>
			<th>Fecha</th>
			<th>Total</th>
			<th>Tipo</th>
			<th>Estado</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($orders as $key => $order)
			<tr>				
				<td style="text-align: center">{{$key + 1}}</td>
				<td style="text-align: center">{{$order->customer->full_name}}</td>
				<td style="text-align: center">{{$order->user->name}}</td>
				<td style="text-align: center">{{date('d-m-Y', strtotime($order->date))}}</td>
				<td style="text-align: center">{{$order->total}}</td>
				<td style="text-align: center">@if($order->is_invoice) Factura @else Proforma @endif</td>
				<td style="text-align: center">@if($order->active) Abierta @else Cerrada @endif</td>
			</tr>
		@endforeach
	</tbody>
</table>

@endif