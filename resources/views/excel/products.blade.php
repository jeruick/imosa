@if ($replacements)
	<table>
		<thead>
			<tr>
				<th>NUMERO</th>
				<th>CODIGO</th>
				<th>DESCRIPCION</th>
				<th>PROVEEDOR</th>
				<th>EXISTENCIA</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($replacements as $element)
				<tr>
					<td>
						{{$element->replacement_id}}
					</td>
					<td>
						{{$element->code}}
					</td>
					<td>
						{{$element->description}}
					</td>
					<td>
						{{$element->provider->name}}
					</td>
					<td>
						{{$element->quantity}}
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endif