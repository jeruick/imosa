<div class="row">
	<div class="small-12 columns">
		<span class="left">Tipo de documento: @if($order->is_invoice) Factura @else Proforma  @endif</span>
		<span class="right">Correlativo: 
		@if($order->invoices->first()) 
			@foreach($order->invoices as $invoice)
				{{$invoice->invoice_id}}
			@endforeach
		@else
			@foreach($order->proformas as $proforma)
				{{$proforma->proforma_id}} - {{date('Y')}}
			@endforeach

		@endif</span>
	</div>
	<div class="small-12 columns">
		<table class="responsive">
			<thead>
				<tr>
					<th style="text-align: center; border: 1px solid #ddd">CLIENTE</th>
					<th style="text-align: center; border: 1px solid #ddd">No. IDENTIDAD</th>
					<th style="text-align: center; border: 1px solid #ddd">DIRECCION</th>
					<th style="text-align: center; border: 1px solid #ddd">RTN</th>
					<th style="text-align: center; border: 1px solid #ddd">TELEFONO</th>
				</tr>
			</thead>
			<tbody class="customers-body">
				<tr>
					<td style="text-align: center; border: 1px solid #ddd">{{$order->customer->full_name}}</td>
					<td style="text-align: center; border: 1px solid #ddd" class= 'customer_id'>{{$order->customer->customer_id}}</td>
					<td style="text-align: center; border: 1px solid #ddd">{{$order->customer->address}}</td>
					<td style="text-align: center; border: 1px solid #ddd">{{$order->customer->RTN}}</td>
					<td style="text-align: center; border: 1px solid #ddd">{{$order->customer->phone_number}}</td>
				</tr>
			</tbody>
			
		</table>
	</div>

</div>
<br><br><br>
<div class="items">
@if($order)
<div class="row">
	<div class="small-12 columns">
		@if ($order->replacements)
		<table id="table" class="responsive">
			<thead>
				<tr>
					<th style="text-align: center; border: 1px solid #ddd">No.</th>
					<th style="text-align: center; border: 1px solid #ddd">DESCRIPCION</th>
					<th style="text-align: center; border: 1px solid #ddd">P.UNIDAD</th>
					<th style="text-align: center; border: 1px solid #ddd">CANTIDAD</th>
					<th style="text-align: center; border: 1px solid #ddd">P.TOTAL</th>
				</tr>
			</thead>
			<tbody class="replacements-body" >
				
				@foreach ($order->replacements as $key => $element)
					
					<tr id="{{$element->getReplacement->replacement_id}}">
						<td style="text-align: center; border: 1px solid #ddd">{{$key + 1}}</td>
						<td style="text-align: center; border: 1px solid #ddd">
							{{$element->getReplacement->description}}
							<input type="hidden" name="replacements[{{$element->getReplacement->replacement_id}}][replacement_id]" value="{{$element->getReplacement->replacement_id}}">
						</td>
						<td style="text-align: center; border: 1px solid #ddd">
							@if ($order->customer->type->description == 'LOCAL')
								{{number_format($element->getReplacement->wholesale_price, 2, '.', ',')}}
							@else
								{{number_format($element->getReplacement->route_price, 2, '.', ',')}}	
							@endif
						</td>
						<td style="text-align: center; border: 1px solid #ddd"> {{$element->quantity}}</td>
						<td style="text-align: center; border: 1px solid #ddd">
							@if ($order->customer->type->description == 'LOCAL')
								{{number_format($element->getReplacement->wholesale_price*$element->quantity, 2, '.', ',')}}
							@else
								{{number_format($element->getReplacement->route_price*$element->quantity, 2, '.', ',')}}	
							@endif
						</td>
					</tr>

				@endforeach

					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td style="text-align: center; border: 1px solid #ddd"><strong>TOTAL:</strong></td>
						<td style="text-align: center; border: 1px solid #ddd"><strong>{{number_format((float)$order->total, 2, '.', ',')}}</strong></td>
					</tr>
				
			</tbody>
		</table>
		@endif
	</div>
</div>
<br><br>
<div class="row">
	<div class="small-4 medium-2 small-offset-8 medium-offset-10 columns">
		<table>
			
		</table>
		<p class="right">
			</strong>
		</p>
		<p class="right">
			</strong>
		</p>
		<p class="right">
			
		</p>
	</div>
</div>


@endif

</div>

