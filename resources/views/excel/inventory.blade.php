
@if ($replacements)
<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Codigo</th>
			<th>Descripcion</th>
			<th>Marca</th>
			<th>Precio de mayoreo</th>
			<th>Precios de ruta</th>
			<th>Existencia</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($replacements as $key => $replacement)
			<tr>				
				<td style="text-align: center">{{$key + 1}}</td>
				<td style="text-align: center">{{$replacement->code}}</td>
				<td style="text-align: left">{{$replacement->description}}</td>
				<td style="text-align: center">{{$replacement->provider->name}}</td>
				<td style="text-align: center">{{$replacement->wholesale_price}}</td>
				<td style="text-align: center">{{$replacement->route_price}}</td>
				<td style="text-align: center">{{$replacement->quantity}}</td>
			</tr>
		@endforeach
	</tbody>
</table>
@endif

