
@if($replacements)
	
	<div style="width: 980px">
		@if ($price === 'wholesale_price' || $price === 'route_price')
		<div class="row">
			<center><h2>LISTA DE PRECIOS MAYOREO - IMOSA</h2></center>
		</div>
		@else
		<div class="row">
			<center><h2>LISTA DE PRODUCTOS AGOTADOS - IMOSA</h2></center>
		</div>
		@endif
		<div style="width: 980px">
			<table cellpadding="2" cellspacing="0" style="width: 980px">
			  <thead>
			    <tr>
			    	<th style="text-align: center; border: 1px solid #ddd; font-size:.8em">No</th>
			    	<th style="text-align: center; border: 1px solid #ddd; font-size:.8em">Codigo</th>
			      	<th style="text-align: center; border: 1px solid #ddd; font-size:.8em;">Descripcion</th>
			      	<th style="text-align: center; border: 1px solid #ddd; font-size:.8em">Marca</th>
			      	@if($price === 'wholesale_price')
			      	<th style="text-align: center; border: 1px solid #ddd; font-size:.8em">Precio de mayoreo</th>
			      	@elseif($price === 'route_price')
			      	<th style="text-align: center; border: 1px solid #ddd; font-size:.8em">Precio de Ruta</th>
			      	@endif
			      	<th style="text-align: center; border: 1px solid #ddd; font-size:.8em">Imagen</th>

				</tr>
			  </thead>
			  <tbody class="replacements-body">
				  	@foreach ($replacements as $key => $element)
				  		<tr id="{{$element->replacement_id}}" @if ($element->quantity < 1) class='sold-out' @endif>
				  			<td style="border: 1px solid #ddd; font-size: .8em; text-align: center">{{$key + 1}}</td>
				  			<td style="border: 1px solid #ddd; font-size: .8em; text-align: center">{{$element->code}}</td>
				  		  	<td style="border: 1px solid #ddd; font-size: .8em; text-align: center; " class="description" >{{$element->description}}</td>
				  		  	<td style="border: 1px solid #ddd; font-size: .8em; text-align: center;" class="provider">{{$element->provider->name}}</td>
				  		  	@if ($price === 'wholesale_price')
				  		  	<td style="border: 1px solid #ddd; font-size: .8em; text-align: center;" class="wholesale_price">{{$element->wholesale_price}}</td>
				  		  	@elseif($price === 'route_price')
				  		  	<td style="border: 1px solid #ddd; font-size: .8em; text-align: center;" class="route_price">{{$element->route_price}}</td>
				  		  	@endif
				  		  	</td>
							<td style="border: 1px solid #ddd; font-size: .8em; text-align: center" class="picture"><img src="{{asset($element->picture)}}" style="width: 100px"></td>
				  		</tr>
				  	@endforeach
			  </tbody>
			</table>	
		</div>
	</div>
	

@endif