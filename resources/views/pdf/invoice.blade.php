@extends('layouts.master')

@section('css')
	<link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}">
	
	<link rel="stylesheet" href="{{asset('css/print.css')}}" >

@stop

@section('content')


@foreach ($order->invoices as $code => $invoice)
<br><br>
<div>
	<div style="width: 1024px;border: 2px solid black; margin: 0 auto;" >
	<div class="row">
	 	<div class="col-10 columns">
	 		    <span style="font-size: 1.5em"><strong>{{$company->name}}</strong></span>
	 		    <p style="margin-bottom: 2px;">{{$company->address}}</p>
	 		    <p style="margin-bottom: 2px;">{{$company->web_site}} / Tel. {{$company->telephone}} | Email: {{$company->email}}</p>
	 		    <p style="margin-bottom: 2px;">R.T.N: {{$company->rtn}}</p>
	 		    <p style="margin-bottom: 2px;">CAI: {{$invoice->companyInvoice->cai}}</p>
	 		    <p style="margin-bottom: 2px;">RANGO AUTORIZADO: {{$invoice->CompanyInvoice->range_to}} - {{$invoice->CompanyInvoice->range_up}}</p>
	 	</div>
	 	<div class="col-2 columns">
	 		    <img src="{{asset($company->logo)}}" alt="" style="margin: 1em 2em 0 0">
	 		    
	 		    <p class="text-center">Fecha: {{date('d/m/Y', strtotime($order->date))}}</p>
	 	</div>
	</div>  
	<br>
	<div class="row">
		<div class="small-8 columns"><strong><span>CLIENTE:</span><span> {{$order->customer->full_name}}</span></strong></div>
		<div class="small-4 columns"><strong><span>RTN:</span><span> {{$order->customer->RTN}}</span></strong></div>
		<div class="small-8 columns"><strong><span>DIRECCION:</span><span> {{$order->customer->address}}</span></strong></div>
		<div class="small-4 columns"><strong><span>TELEFONO:</span><span> {{$order->customer->phone_number}}</span></strong></div>
		
	</div>
	<br>
	
	@foreach ($order->replacements->chunk(20) as $count => $products)	
		@if($count == $code)
	<div class="row">
		<div class="col-12 columns">
			<table class="table">
				<tr>
					<th style="text-align:center; width:20px;" >No.</th>
					<th style="text-align:center; width:100px;" >Codigo</th>
					<th style="text-align:center; width:300px;" >Descripcion</th>
					<th style="text-align:center; width:50px;" >Cantidad</th>
					<th style="text-align:center; width:50px;" >Costo Unitario</th>
					<th style="text-align:center; width:100px;" >Importe L</th>
				</tr>
				<tbody class="replacements-body">
				
				
					@foreach ($products as $key => $element)
						<tr>
							<td style="width:20px; padding:.5em !important; text-align:center">{{$key + 1}}</td>
							<td style="width:100px; padding:.5em !important; text-align:center">{{$element->getReplacement->code}}</td>
							<td style="width:300px; padding:.5em !important;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 500px">{{$element->getReplacement->description}}</td>
							<td style="width:50px; padding:.5em !important; text-align:center">{{$element->quantity}}</td>
							<td style="width:50px; padding:.5em !important; text-align:center" class="{{$element->quantity}}">
								@if ($order->customer->type->description == 'LOCAL')
									{{number_format(($element->getReplacement->wholesale_price/(1 + $invoice->companyInvoice->isv)), 2, '.', ',')}}
								@else
									{{number_format(($element->getReplacement->route_price/ (1 + $invoice->companyInvoice->isv)), 2, '.', ',')}}	
								@endif
							</td>
							<td style="width:100px; padding:0 !important; text-align:center" class="{{$element->getReplacement->route_price * $element->quantity}}" id="{{$element->getReplacement->wholesale_price * $element->quantity}}">
								@if ($order->customer->type->description == 'LOCAL')
									{{number_format(($element->getReplacement->wholesale_price/(1 + $invoice->companyInvoice->isv)), 2, '.', ',')*$element->quantity}}
								@else
									{{number_format(($element->getReplacement->route_price/(1 + $invoice->companyInvoice->isv)), 2, '.', ',')*$element->quantity}}
								@endif
							</td>
						</tr>
					
					@endforeach
					<tr>
						<td></td>
						<td></td>
						<td style="height:10px !important; padding: 0"><p style="text-align: center; font-size: 1em; margin: 0; font-weight: bold">*** ULTIMA LINEA ***</p></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					@for($i = 0; $i < 20 - count($products); $i++)
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					@endfor
					
					<tr>
						<td colspan="6" style="border: 0; background: white"></td>
					</tr>
					<tr >
						<td style="border: 0; padding: 0" colspan="4"><p>FECHA LIMITE DE EMISION: {{date('d/m/Y', strtotime($invoice->companyInvoice->limit_date))}}</p></td>
						<td style="background: white"><strong style="float: right">SUB-TOTAL</strong></td>
						<td style="background: white">{{number_format((float)$invoice->subtotal, 2, '.', ',')}}</td>
					</tr>
					<tr style="background: white">
						<td style="border: 0; padding: 0" colspan="4"><strong>La factura es beneficio de todos "EXIJALA"</strong>
						</td>
						<td><strong style="float: right">15% I.S.V</strong></td>
						<td>{{number_format((float)$invoice->isv, 2, '.', ',')}}</td>
					</tr>
					<tr style="background: white">
						<td style="border: 0; padding: 0" colspan="4"><p>Son: {{$invoice->textNumber}}</p></td>
						<td><strong style="float: right">TOTAL</strong></td>
						<td><strong>{{number_format((float)$invoice->total, 2, '.', ',')}}</strong></td>
					</tr>
					<tr style="background: white">
						<td style="border: 0; padding: 0" colspan="4"><p style="font-size: 25px">FACTURA No. 000-001-01-<span class="red">{{$invoice->invoice_id}}</span> </p></td>
					</tr>
				</tbody>
			</table>
		</div>
		
	</div>
	@endif
	@endforeach
	<br><br><br>

	<div class="row">
		<div class="col-6 columns">
			<div class="text-center">
				<p>________________________________________</p>
				<p>Firma y Sello</p>
			</div>
		</div>
		<div class="col-6 columns">
			<div class="text-center">
				<p>________________________________________</p>
				<p>IMOSA</p>
			</div>
		</div>

	</div>
</div>

<div  class="page-break"></div>
@endforeach



@stop

<script>
	document.addEventListener("DOMContentLoaded", function(event) { 
	  //do work
	  // window.print();
	  // document.location.href = "{{url('/ordenes')}}";
	});
	   
</script>