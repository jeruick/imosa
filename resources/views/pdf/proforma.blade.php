@extends('layouts.master')

@section('css')
	<link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}">
	
	<link rel="stylesheet" href="{{asset('css/print.css')}}" >

@stop

@section('content')


@foreach ($order->proformas as $code => $proforma)
<br><br><br>
<div>
	<div style="width: 1024px;border: 2px solid black; margin: 0 auto;">
	<div class="row">
		<div class="col-12 columns">
			    <h4 class="text-center"><strong>{{$company->name}}</strong></h4>
			    <h4 class="text-center">Comprobante de entrega de mercaderia</h4>
		</div>
		<div class="col-8 columns">
			
		</div>
	 	<div class="col-4 columns">
				<br>
			    <h4 class="text-center"><strong>{{$company->name}}</strong></h4>
			    <h4 class="text-center">Comprobante de entrega de mercaderia</h4>
		</div>
		
	 	<div class="col-4 columns">
				<br>
	 			<h4 class="text-center">No. <span>{{$proforma->proforma_id}} - {{date('Y')}}</span> </h4>
	 		    <p class="text-center" style="font-size: 1em" >Fecha: {{date('d/m/Y', strtotime($order->date))}}</p>
	 	</div>
	</div>  


	<div class="row">
		<div class="col-12 columns"><span>CLIENTE:</span><span> {{$order->customer->full_name}}</span></div>
		<div class="col-12 columns"><span>CONTACTO:</span><span> {{$order->customer->contact}}</span></div>
	</div>
	<br>
	<div class="row">
		<div class="col-12 columns">
			<table class="table">
				<tr>
					<th style="text-align:center" width="50">No.</th>
					<th style="text-align:center" width="100">Codigo</th>
					<th style="text-align:center" width="500">Descripcion</th>
					<th style="text-align:center" width="50">Cantidad</th>
					<th style="text-align:center" width="100">Costo Unitario</th>
					<th style="text-align:center" width="100">Importe L</th>
					<th style="text-align:center; width:50px;" >No.</th>
					<th style="text-align:center; width:100px;" >Codigo</th>
					<th style="text-align:center; width:500px;" >Descripcion</th>
					<th style="text-align:center; width:50px;" >Cantidad</th>
					<th style="text-align:center; width:100px;" >Costo Unitario</th>
					<th style="text-align:center; width:100px;" >Importe L</th>
				</tr>
				<tbody class="replacements-body">
				
				@foreach ($order->replacements->chunk(20) as $count => $products)	
					@if($count == $code)
					<tr></tr>
					@foreach ($products as $key => $element)
						<tr>
							<td style="padding:.5em !important; text-align:center">{{$key + 1}}</td>
							<td style="padding:.5em !important; text-align:center">{{$element->getReplacement->code}}</td>
							<td style="padding:0 !important; padding: .5em !important;max-width: 500px;white-space: nowrap;overflow: hidden;">{{$element->getReplacement->description}}</td>
							<td style="padding:.5em !important; text-align:center">{{$element->quantity}}</td>
							<td style="padding:.5em !important; text-align:center" class="{{$element->quantity}}">
							<td style="width:50px; padding:.5em !important; text-align:center">{{$key + 1}}</td>
							<td style="width:100px; padding:.5em !important; text-align:center">{{$element->getReplacement->code}}</td>
							<td style="width:500px; padding: .5em !important;max-width: 500px;white-space: nowrap;overflow: hidden;">{{$element->getReplacement->description}}</td>
							<td style="width:50px; padding:.5em !important; text-align:center">{{$element->quantity}}</td>
							<td style="width:100px; padding:.5em !important; text-align:center" class="{{$element->quantity}}">
								@if ($order->customer->type->description == 'LOCAL')
									{{number_format((float)$element->getReplacement->wholesale_price, 2, '.', ',')}}
								@else
									{{number_format((float)$element->getReplacement->route_price, 2, '.', ',')}}	
								@endif
							</td>
							<td style="padding:0 !important; text-align:center" class="{{$element->getReplacement->route_price * $element->quantity}}" id="{{$element->getReplacement->wholesale_price * $element->quantity}}">
							<td style="width:100px; padding:0 !important; text-align:center" class="{{$element->getReplacement->route_price * $element->quantity}}" id="{{$element->getReplacement->wholesale_price * $element->quantity}}">
								@if ($order->customer->type->description == 'LOCAL')
									{{number_format((float)$element->getReplacement->wholesale_price*number_format($element->quantity, 2, '.', ','), 2, '.', ',')}}
								@else
									{{number_format((float)$element->getReplacement->route_price*number_format($element->quantity, 2, '.', ','), 2, '.', ',')}}	
								@endif
							</td>
						</tr>
						
					@endforeach
					<tr>
						<td></td>
						<td></td>
						<td style="height:10px !important; padding: 0"><p style="text-align: center; font-size: 1em; margin: 0; font-weight: bold">*** ULTIMA LINEA ***</p></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					@for($i = 0; $i < 20 - count($products); $i++)
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					@endfor
					@endif
				@endforeach
					
					<tr>
						<td colspan="4"></td>
						
						<td width="140px"><strong>TOTAL</strong></td>
						<td width="60px"><strong>{{number_format((float)$proforma->total, 2, '.', ',')}}</strong></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="row">
		<div class="small-12 columns">
			<p style="font-size: 1em">VENDIDO POR: {{$order->user->name}}</p>
			@if(isset($order->checked_by))
			
			<p style="font-size: 1em">ENTREGADO POR: {{$order->checkedBy->name}}</p>
			@endif
		</div>
	</div>
	<br><br><br>
	<br><br><br>
	
	<div class="row">
		<div class="col-6 columns">
			<div class="text-center">
				<p>________________________________________</p>
				<p>Firma y Sello</p>
			</div>
		</div>
		<div class="col-6 columns">
			<div class="text-center">
				<p>________________________________________</p>
				<p>IMOSA</p>
			</div>
		</div>

	</div>
	</div>	
</div>

<div class="page-break"></div>
@endforeach



@stop

<script>
	document.addEventListener("DOMContentLoaded", function(event) { 
	  //do work
	  // window.print();
	  // document.location.href = "{{url('/ordenes')}}";
	});
	   
</script>