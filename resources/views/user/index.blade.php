@extends('layouts.master')


@section('content')

	@if ($errors->any())
		<ul>
			@foreach ($errors->all() as $err)
				<li><span class="error">{{$err}}</span></li>
			@endforeach
		</ul>
	@endif
	<div class="row">
		<div class="small-12 columns">
			<a href="{{ route('home') }}" class="fa fa-arrow-left button radius left" >  Atras</a>
			<a class="button radius right" data-open="myModal"><i class="fa fa-user"></i> Nuevo Usuario</a>
		</div>
	</div>
	
	<div class="full-width">
		<div class="small-12 medium-10 medium-offset-1 columns">
			<table class="responsive">
			  <thead>
			    <tr>
			      	<th width="300">Nombre</th>
			      	<th>Email</th>
			      	<th width="200">Fecha creacion</th>
					<th width="200">Tipo de usuario</th>
					<th width="200">Estado</th>
			      	<th>Eliminar</th>
			      	
			    </tr>
			  </thead>
			  <tbody class="providers-body">

			  	@foreach ($users as $element)
			  		<tr id="{{$element->id}}">
			  		  	<td>{{$element->name}}</td>
			  		  	<td>{{$element->email}}</td>
			  		  	<td>{{$element->created_at}}</td>
			  		  	<td>
			  		  		<select name="type" class="sel-type">
			  		  			@foreach($usertype as $type)
			  		  			<option value="{{$type->user_type}}" @if ($element->user_type == $type->user_type) selected="selected" @endif>{{$type->description}}
			  		  			</option>
			  		  			@endforeach
			  		  		</select>

			  		  	</td>
			  		  	<td>
			  		  		<select name="active" class="sel-status" >
			  		  			<option value="0" @if (!$element->active) selected @endif>Activo</option>
			  		  			<option value="1" @if (!$element->active) selected @endif>Desactivado</option>
			  		  		</select>
			  		  	</td>
						<td><a href="{{ route('deleteUser', $element->id) }}" class="button alert radius"><i class="fa fa-remove"></i></a></td>
						
			  		</tr>
			  	@endforeach
			    
			  </tbody>
			</table>
		</div>

		{{-- User Modal --}}

		<div id="myModal" class="full reveal" data-reveal style="z-index:10">
			<br><br><br>
			  <h2>Nuevo Usuario</h2>
			<p>
				<div class="row valign-wrapper register">

		<div class="small-12 medium-8 medium-offset-2 columns panel">

			<form action="{{ route('newUser') }}" method="POST">
				{!! csrf_field() !!}

				<div class="row">
					<div class="small-12 columns">
						<label for=""> Nombre Completo: 
							<input type="text" name="name" placeholder='nombre completo' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Email: 
							<input type="email" name="email" placeholder='me@example.com' required>
						</label>
					</div>
					@if (count($errors) > 0 && $errors->first('email'))
						<div class="small-12 columns">
							<span class="error">{{ $errors->first('email') }}</span>
						</div>
					@endif
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Contraseña: 
							<input type="password" name="password" placeholder='contraseña' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Repetir contraseña: 
							<input type="password" name="password_confirmation" placeholder='contraseña' required>
						</label>
					</div>
					@if (count($errors) > 0 && $errors->first('password'))
						<div class="small-12 columns error">
							<span class="error">{{ $errors->first('password') }}</span>
						</div>
					@endif
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label for=""> Repetir contraseña: 
							<input type="password" name="password_confirmation" placeholder='contraseña' required>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<div class="clearfix">
							<button type="submit" class="fa fa-save button right"> Guardar</button>
						</div>	
					</div>
				</div>
			</form>
		</div>
	</div>
			</p>
			
		  <button class="close-button" data-close aria-label="Close modal" type="button">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
		
@stop
