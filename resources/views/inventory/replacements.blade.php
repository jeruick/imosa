@extends('layouts.master')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
	<link media="screen" href="{{asset('css/jquery.msg.css')}}" rel="stylesheet" type="text/css">
@stop

@section('content')
	@if ($errors->any())
		<ul >
			@foreach ($errors->all() as $error)
				<li class="error" style="color:red">{{$error}}</li>
			@endforeach
		</ul>
	@endif
	<div class="row">
		<div class="small-12 columns">
			@if(Auth::user()->type->description == 'admin')
			<a class="success button left" href="{{route('exportInventory')}}" ><i class="fa fa-file-excel-o"></i> Exportar Inventario</a>
			<a  class="button radius right reveal-link" data-open="myModal"><i class="fa fa-cog"></i> Nuevo repuesto</a>
			@endif
		</div>
  	</div>
  	<div class="row">
  		<div class="small-12 columns">
  			<div class="small-12 medium-8 columns">
  				<input type="text" placeholder="buscar por: Codigo, Descripción del repuesto, Modelo de motos compatible ó codigo de barra" class="search-replacement" autofocus style="height: 3.5em">		
  			</div>
  		  
  		  <div class="small-12 medium-4 columns">
			
			<ul class="dropdown menu expanded" data-dropdown-menu>
				<li>
					<a class="alert hollow button expanded"><i class="fa fa-file-pdf-o fa-2x"></i> Exportar a pdf</a>
					 <ul class="menu">
					   <li><a class="export" href="{{route('exportWholesaler')}}">Productos en existencia(LOCAL)</a></li>
					   <li><a class="export" href="{{route('exportRoute')}}">Productos en existencia(Ruta)</a></li>
					   <li><a class="export" href="{{route('exportStockout')}}">Productos agotados</a></li>
					 </ul>
				</li>
			</ul>
  		  </div>
  		</div>  	
  	</div>
  	<br>
	<div class="row">
		<div class="small-12 colums table-scroll" id="replacements">
			@include('inventory.items')
		</div>
	</div>
	


	
	{{-- Modal --}}

	<div id="myModal" class="full reveal" data-reveal style="z-index:10">
	<br><br><br>
	  <h2>Nuevo Repuesto</h2>
	  <p>
	  	<form action="{{ route('replacements') }}" method="POST" enctype="multipart/form-data">
	  		{!! csrf_field() !!}
	  		<div class="row">
				<div class="small-12 medium-4 columns">
					<div class="small-12 columns">
					  <label>Codigo
					    <input type="text" name="code" required />
					  </label>
					</div>
					<div class="small-12 columns">
					  <label>Precio de mayoreo
					    <input type="text" onkeypress="return isDecimal(event)" name="wholesale_price"  required />
					  </label>
					</div>
					<div class="small-12 columns">
					  <label>Precio de ruta
					    <input type="text" onkeypress="return isDecimal(event)" name="route_price" required />
					  </label>
					</div>

					<div class="small-12 columns">
					  <label>Descripción
					    <textarea name="description" id="" cols="10" rows="2" style="resize:none" required></textarea>
					  </label>
					</div>

				</div>

				<div class="small-12 medium-4 columns">
					
					<div class="small-12 columns">
	  		        <label>Cantidad de unidades a ingresar
	  		          <input type="text" onkeypress="return isNumber(event)"  name="quantity"  required />
	  		        </label>
	  		      </div>
					
					<div class="small-12 columns">
						<div class="">
						  <label>Marca
						    <select name="provider" required>
						    	@foreach ($providers as $element)
						    		<option value="{{$element->provider_id}}">{{$element->name}}</option>
						    	@endforeach
						    </select>
						  </label>
						</div>
						
						
						<div class="">
						  <label>Bodega
						    <select name="cellar" required>
						    	@foreach ($cellars as $element)
						    		<option value="{{$element->cellar_id}}">{{$element->description}}</option>
						    	@endforeach
						    </select>
						  </label>
						</div>
					</div>
					<div class="small-12 columns">
					  <label>Ubicacion
					    <input type="text" name="location" required/>
					  </label>
					</div>
				</div>

				<div class="small-12 medium-4 columns">
					<div class="small-12 columns">
						<label for=""><input type="file" name="picture"></label>
					</div>

		  		     <div class="small-12 columns">
		  		        <label>Modelo
		  		        <div class="compatible-models">
							
		  		        	<input type="text" class="search-models">
				        		<ul class="list">
				        			
				        		</ul>		
						</div>
						<div class="brand-model row" style="display:none">
							<label for="" class="small-10 columns">
								Selecciona la marca a la que pertenece este modelo
								<select class="select-brand">
									@foreach ($brands as $element)
										<option value="{{$element->brand_id}}">{{$element->description}}</option>
									@endforeach
								</select>
								
							</label>
							<label for="" class="small-2 columns">
								<button class="button radius add-model"><i class="fa fa-check-circle"></i></button>
							</label>
							
						</div>
			  		    </label>
			  		    <div class="small-12 columns">
			  		     	<ul class="selected-models">
			  		     		
			  		     	</ul>
			  		    </div>
			  		 </div>
				</div>
				</div>
	  		     <div class="row">
	  		     	<div class="small-12 columns">
	  		     		<button class="button" style="margin-left:1em" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
	  		     	</div>
	  		     </div>
	  		</form>
	  </p>
	  
	  <button class="close-button" data-close aria-label="Close reveal" type="button">
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>
	
@stop

@section('script')
	@parent
	
	<script src="{{ asset('js/responsive-tables.js') }}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.center.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.msg.min.js')}}"></script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

	<script>
	jQuery(document).ready(function($) {
		
		$('.add-model').on('click',  function(event) {
			event.preventDefault();
			var $brand = $('.select-brand').val();
			var $model = $('.search-models').val();
			$.ajax({
				url: 'modelo',
				type: 'POST',
				data: {brand: $brand, model: $model, _token : $('meta[name=_token]').attr('content')},
			})
			.done(function(data) {
				$('.brand-model').css('display', 'none');

				/*Save model to localStorage*/
				var $model = {
					model_id : data.model_id,
					description: data.description
				}
				
				if(!localStorage.getItem('models'))
				{
					var $models = []
					$models.push($model);
					localStorage.setItem('models', JSON.stringify($models));
				}
				else
				{

					var $models = JSON.parse(localStorage.getItem('models'))
					var $exists = false;
					$.each($models, function(index, val) {
						 if (val.model_id == $model.model_id) {
						 	$exists = true
						 }
					});
					if (!$exists) {
						$models.push($model);
						localStorage.setItem('models', JSON.stringify($models));
					}
					
				}
				update_models();
			})

			
		});
		$('.search-models').keyup(function(event) {
			$('.compatible-models .list').css('display', 'block');
			var $text = $(this).val() ? $(this).val() : 'blank';

			var url = "{{ route('searchModels' ) }}";
			$.ajax({
				url: url ,
				type: 'POST',
				dataType: 'json',
				data: { text: $text, _token : $('meta[name=_token]').attr('content') } ,
			})
			.done( function(data) {
				
				if (data.length != 0) {
					
					$('.compatible-models .list').html('');
					$.each(data, function(index, val) {
						var $markup = "<li id='" + val.model_id + "' onclick='select_model(this)' >" + val.description + "</li>"
						$('.compatible-models .list').append($markup)	 
					});
					
				}
				else if($text != 'blank')
				{
					
					$('.compatible-models .list').html('');
					
					var $markup = "<li onclick='create_model(this)' style='width: 100%'>Agregar este modelo...</li>"
					$('.compatible-models .list').append($markup)	 
				}

			})
		});

	})
	function create_model (element) {
		$(element).parent().css('display', 'none');
		$('.brand-model').css('display', 'block');
	}

	function check_admin_password(element)
	{	
		swal({ title: "Contraseña de administrador",  type: "input", inputType: 'password',  showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   inputPlaceholder: "Contraseña" }, function (inputValue){
			if (inputValue === false) return false;      
			if (inputValue === "") {     
				swal.showInputError("Escribe una contraseña");     
				return false   
			}    
			
			$.post("{{route('checkAdminPassword')}}",
			{
				'password': inputValue, 
				'_token': $('meta[name=_token]').attr('content')
			})
			.success(function(data) {
				console.log(data);
				
				if(data)
				{
					swal("Eliminada", "La orden ha sido eliminada correctamente");
					window.location.replace($(element).attr('id'));
					
				}
				else
				{

					swal.showInputError("Contraseña incorrecta");     
				}
			})
			.fail(function() {
				console.log("error");
			});
		});
	}
	</script>
@stop



