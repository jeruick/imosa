@extends('layouts.master')

@section('css')
	@parent
	<link rel="stylesheet" href="{{asset('css/lightbox.min.css')}}">
@stop

@section('content')
	<form action="{{ route('updateReplacement', $replacement->replacement_id) }}" method="POST" enctype="multipart/form-data">
	<div class="row">

		<div class="small-12 columns">
			<a href="{{ route('replacements') }}" class="fa fa-arrow-left button">  Atras</a>
			@if(Auth::user()->type->description == 'admin')
			<button type="submit" class="fa fa-floppy-o right button">  Guardar</button>
			@endif
		</div>
	</div>
	
	@if (count($errors) > 0)
	    
        @foreach ($errors->all() as $error)
            <div class="row">
            	<span class="error">{{ $error }}</span> 
            </div>
        @endforeach
	    
	@endif

	
		{!! csrf_field() !!}
	  		  <div class="row">

				<div class="medium-4 columns col-1">
		  		    <div class="small-12 columns">
		  		      <label>Codigo
		  		        <input type="text" name="code" value="{{$replacement->code}}" required />
		  		      </label>
		  		    
		  		   </div>

					@if(Auth::user()->type->description == 'admin')
		  		    <div class="small-12 columns">
		  		       <label>Precio de mayoreo
		  		         <input type="text" name="wholesale_price"  onkeypress="return isDecimal(event)" value="{{$replacement->wholesale_price}}" required />
		  		       </label>
		  		    </div>
		  		    @endif

	  		      <div class="small-12 columns">
	  		        <label>Precio de ruta
	  		          <input type="text" name="route_price" onkeypress="return isDecimal(event)" value="{{$replacement->route_price}}" required />
	  		        </label>
	  		      </div>
	  		     
		  		  
		          <div class="small-12 columns">
		           <label>Descripción
		             <textarea name="description" id="" cols="10" rows="2" style="resize:none" required>{{$replacement->description}}</textarea>
		           </label>
		          </div>
	  		  		
				</div>

				<div class="medium-4 columns col-2">
					
				  <div class="small-12 columns">
				    <label>Cantidad disponible
				      <input type="text" name="quantity" onkeypress="return isNumber(event)"  value="{{$replacement->quantity}}" required />
				    </label>
				  </div>


			      <div class="small-12 columns">
			        <label>Proveedor
			          <select name="provider" required>
			          	@foreach ($providers as $element)
			          		<option value="{{$element->provider_id}}" @if ($element->provider_id == $replacement->provider_id) selected="selected"  @endif>{{$element->name}}</option>
			          	@endforeach
			          </select>
			        </label>
			      </div>
			  
			  
			      <div class="small-12 columns">
			        <label>Bodega
			          <select name="cellar" required>
			          	@foreach ($cellars as $element)
			          		<option value="{{$element->cellar_id}}" @if ($element->cellar_id == $replacement->cellar_id) selected="selected"  @endif>{{$element->description}}</option>
			          	@endforeach
			          </select>
			        </label>
			      </div>

			      <div class="small-12 columns">
			        <label>Ubicacion
			          <input type="text" name="location" value="{{$replacement->location}}" required/>
			        </label>
			      </div>

  	  		      <div class="small-12 columns">
  	  		        <label>Modelo
  	  		        <div class="compatible-models">
  						
  	  		        	<input type="text" class="search-models">
    		        		<ul class="list">
    		        			
    		        		</ul>		
  					</div>
  					<div class="brand-model row" style="display:none">
  						<label for="" class="small-10 columns">
  							Selecciona la marca a la que pertenece este modelo
  							<select class="select-brand">
  								@foreach ($brands as $element)
  									<option value="{{$element->brand_id}}">{{$element->description}}</option>
  								@endforeach
  							</select>
  							
  						</label>
  						<label for="" class="small-2 columns">
  							<button class="button radius add-model"><i class="fa fa-check-circle"></i></button>
  						</label>
  						
  					</div>
  	  		        </label>
  	  		        <ul class="selected-models">
  	  		        	@foreach ($replacement->models as $element)
  	  		        		<li>  {{$element->getModel->description}}  <input type='hidden' name='models[]' value='{{$element->getModel->model_id}} '/><span class='remove fa fa-close' onclick='remove_model(this)' value='  {{$element->getModel->model_id}}  '></span></li>
  	  		        	@endforeach
  	  		        </ul>
  	  		      </div>
				</div>
			
				<div class="medium-4 columns col-3">
					
					<div class="small-12 columns">
						<a href="{{asset($replacement->picture)}}" data-lightbox="image-1" ><img src="{{asset($replacement->picture)}}" class="product-image" alt="product-image"></a> <input type="file" name="picture">
					</div>
				</div>

	  		</div>
	</form>
@stop

@section('script')
	@parent
	<script src="{{asset('js/lightbox.min.js')}}"></script>
	<script>

	jQuery(document).ready(function($) {
		/* Search compatibles models for a replacement */
		$('.add-model').on('click',  function(event) {
			event.preventDefault();
			var $brand = $('.select-brand').val();
			var $model = $('.search-models').val();
			console.log($brand);
			console.log($model);
			$.ajax({
				url: "{{route('newModel')}}",
				type: 'POST',
				data: {brand: $brand, model: $model, _token : $('meta[name=_token]').attr('content')},
			})
			.done(function(data) {
				$('.brand-model').css('display', 'none');

				/*Save model to localStorage*/
				var $model = {
					model_id : data.model_id,
					description: data.description
				}
				
				if(!localStorage.getItem('models'))
				{
					var $models = []
					$models.push($model);
					localStorage.setItem('models', JSON.stringify($models));
				}
				else
				{

					var $models = JSON.parse(localStorage.getItem('models'))
					var $exists = false;
					$.each($models, function(index, val) {
						 if (val.model_id == $model.model_id) {
						 	$exists = true
						 }
					});
					if (!$exists) {
						$models.push($model);
						localStorage.setItem('models', JSON.stringify($models));
					}
					
				}
				update_models();
			})

			
		});

		if(!localStorage.getItem('models'))
		{
			var $models = []
			var $o = {!! $replacement->models !!}
			
			$.each($o, function(index, val) {
				 $models.push(val.get_model);
			});
			
			localStorage.setItem('models', JSON.stringify($models));
			
		}
		$('.search-models').keyup(function(event) {
			$('.compatible-models .list').css('display', 'block');
			var $text = $(this).val() ? $(this).val() : 'blank';
			var url = "{{ route('searchModels' ) }}";
			$.ajax({
				url: url ,
				type: 'POST',
				dataType: 'json',
				data: { text: $text, _token : $('meta[name=_token]').attr('content') } ,
			})
			.done( function(data) {
				
				if (data.length != 0) {
					
					$('.compatible-models .list').html('');
					$.each(data, function(index, val) {
						var $markup = "<li id='" + val.model_id + "' onclick='select_model(this)' >" + val.description + "</li>"
						$('.compatible-models .list').append($markup)	 
					});
					
				}
				else if($text != 'blank')
				{
					
					$('.compatible-models .list').html('');
					
					var $markup = "<li onclick='create_model(this)' style='width: 100%'>Agregar este modelo...</li>"
					$('.compatible-models .list').append($markup)	 
				}

			})
		});

	});
	
	function create_model (element) {
		$(element).parent().css('display', 'none');
		$('.brand-model').css('display', 'block');
	}
	</script>

	
@stop