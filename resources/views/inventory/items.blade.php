<table id="my-table">
  <thead>
    <tr>
    	<th>No.</th>
    	<th width="">Ver</th>
    	<th>Codigo</th>
      	<th width="300">Descripción</th>
      	<th width="100">Marca</th>
      	
      	@if(Auth::user()->type->description === 'admin')
      	<th width="100">Precio de mayoreo</th>
      	@endif
      	<th width="100">Precio de ruta</th>
      	<th width="100">Existencia</th>
      	<th width="200">Modelos</th>
      	
      	@if(Auth::user()->type->description === 'admin')
      	<th>Eliminar</th>
      	@endif
	    </tr>
  </thead>
  <tbody class="replacements-body">
	
  	@foreach ($replacements as $key => $element)
  		<tr id="{{$element->replacement_id}}" @if ($element->quantity < 1) class='sold-out' @endif>
  			<td>{{$key + 1}}</td>
  			<td class="show"><a href="{{ route('replacement', $element->replacement_id) }}" class="button"><i class="fa fa-search"></i></a></td>
  			<td>{{$element->code}}</td>
  		  	<td class="description">{{$element->description}}</td>
  		  	
  		  	<td class="provider">{{$element->provider->name}}</td>
  		  	@if(Auth::user()->type->description === 'admin')
  		  	<td class="wholesale_price">{{$element->wholesale_price}}</td>

  		  	@endif
  		  	<td class="route_price">{{$element->route_price}}</td>
  		  	<td class="quantity">{{$element->quantity}}</td>
  		  	<td class="type">
  		  	
  		  	@foreach ($element->models as $model)
  		  		  <span class="label">{{$model->getModel->description}}</span>
  		  	@endforeach</td>

  		  	@if(Auth::user()->type->description === 'admin')

    			<td class="delete"><button id="{{ route('deleteReplacement', $element->replacement_id) }}" class="button alert" onclick="check_admin_password(this)"><i class="fa fa-remove"></i></button></td>
    			@endif
  		</tr>
  	@endforeach
    
  </tbody>
</table>

@if(isset($paginate))
<center>

    {{-- Pagination --}}

    {!! str_replace('/?', '?',$replacements->render(new LaravelFoundation\Pagination\SimpleFoundationFivePresenter($replacements, ["pagination-centered"=>'true']))) !!}    
  </center>
  @endif