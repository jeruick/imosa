@extends('layouts.master')


@section('content')
<div class="row">
	<div class="row">
		<div class="small-12 columns">
			<a href="{{ route('configurations') }}" class="fa fa-arrow-left button">  Atras</a>
			<a class="button radius right" data-open="myModalNew"><i class="fa fa-user"></i> Nuevo Proveedor	</a>
		</div>
	</div>
</div>

<div class="row">
	<div class="small-12 medium-9 large-12 columns">
		<table class="responsive" style="width: 100%">
		  <thead>
		    <tr>
		    	<th width="200">Editar</th>
		    	<th width="200">Compañia</th>
		      	<th width="200">Marca</th>
		      	<th width="200">Pais</th>
		      	<th>Eliminar</th>
		      	<th>Ver Productos</th>
		    </tr>
		  </thead>
		  <tbody class="providers-body">

		  	@foreach ($providers as $element)
		  		<tr id="{{$element->provider_id}}">
		  			<td class="show"><a href="#" class="button radius show-modal" value="{{$element->provider_id}}" data-open="myModal"><i class="fa fa-edit"></i></a></td>
		  			<td class="description">{{$element->company}}</td>
		  		  	<td class="description">{{$element->name}}</td>
		  		  	<td class="wholesale_price">{{$element->country}}</td>
					<td class="delete"><a href="{{ route('deleteProvider', $element->provider_id) }}" class="button alert radius"><i class="fa fa-remove"></i></a></td>
					<td><a href="{{ route('providerProducts', $element->provider_id) }}" class="button radius fa fa-search"></a></td>
		  		</tr>
		  	@endforeach
		  </tbody>
		</table>
	</div>
</div>

	{{-- Provider Modal --}}

	<div id="myModalNew" class="reveal" data-reveal aria-labellebdy="modalTitle" aria-hidden="true" role="dialog"> 
		<h2>Nuevo Proveedor</h2>
		<p>
			<form action="{{ route('newProvider')}}" method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}

				<div class="row">
				    <div class="small-12 columns">
				      <label>Marca
				        <input type="text" name="name"  required />
				      </label>
				    </div>
				</div>
				<div class="row">
				    <div class="small-12 columns">
				      <label>Compañia
				        <input type="text" name="company" class="company"  required />
				      </label>
				    </div>
			   </div>
				<div class="row">
				     <div class="small-12 columns">
				       <label>Pais
				         <input type="text" name="country"  required />
				       </label>
				     </div>
				</div>
			     <div class="row">
			     	<div class="small-12 columns">
			     		<button class="button fa fa-floppy-o radius" type="submit">  Guardar</button>
			     	</div>
			     </div>
			</form>	
		</p>
	<button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
	</div>
		
	<div id="myModal" class="reveal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	  <p>
	  	<form action="" id="update-provider" method="POST">
			{!! csrf_field() !!}		
		  <div class="row">
		    <div class="small-12 columns">
		      <label>Marca
		        <input type="text" name="name" class="name"  required />
		      </label>
		    </div>
		   </div>
		   <div class="row">
		    <div class="small-12 columns">
		      <label>Compañia
		        <input type="text" name="company" class="company"  required />
		      </label>
		    </div>
		   </div>
		   <div class="row">
		     <div class="small-12 columns">
		       <label>Pais
		         <input type="text" name="country" class="country" required />
		       </label>
		     </div>
		    </div>
		     <div class="row">
		     	<div class="small-12 columns">
		     		<button class="button" type="submit">Guardar</button>
		     	</div>
		     </div>
		</form>
	  </p>
	 <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
	</div>
@stop

@section('script')
	@parent
	<script>
	jQuery(document).ready(function($) {
		$('.show-modal').on('click',  function(event) {
			var $url = 'proveedor/' + $(this).attr('value');
			console.log($url);
			$.get($url, function(data) {
				if (data) {
					$('#update-provider .name').val(data.name);
					$('#update-provider .country').val(data.country);
					$('#update-provider .company').val(data.company);
					$('#update-provider').attr('action', $url);
				}
			});
		});
	});
	</script>
@stop