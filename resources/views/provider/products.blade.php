@extends('layouts.master')

@section('css')
	@parent
	<link rel="stylesheet" media="print" href="{{ asset('css/print.css') }}">
@stop

@section('content')
	<div class="row">
		<div class="small-12 columns">
			<a href="{{ route('providers') }}" class="button radius back"><i class="fa fa-arrow-left"></i> Atras</a>
			@if($replacements)
			<a class="success button right" href="{{ route('exportProducts', $provider->provider_id) }}" ><i class="fa fa-file-excel-o"></i> Exportar Lista de Agotados</a>
			@endif
			<div class="small-12 columns"><h2>{{$provider->name}}</h2></div>
		</div>		
	</div>
	<br>
	@if ($provider)
		@if ($replacements->first())
			<div class="row">
				<div class="row">
					<div class="columns small-12">
						<h4 class="warning left" ><span>Productos agotados</span></h4>
						<a href="#" class="button radius print right"><i class="fa fa-print fa-2x"></i></a>
					</div>
				</div>
				<div class="small-12 columns ">
					<table>
						<thead>
							<th width="100">NUMERO</th>
							<th width="100">CODIGO</th>
							<th width="500">DESCRIPCION</th>
							<th width="200">PROVEEDOR</th>
							<th width="100">EXISTENCIA</th>
						</thead>
						<tbody>
							@foreach ($replacements as $element)
								<tr>
									<td>
										{{$element->replacement_id}}
									</td>
									<td>
										{{$element->code}}
									</td>
									<td>
										{{$element->description}}
									</td>
									<td>
										{{$provider->name}}
									</td>
									<td>
										{{$element->quantity}}
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="small-2 columns ">
					
				</div>
			</div>
			
		@endif
		<div class="row all-replacements">
			<div class="small-12 columns">
				<h4>Todos los productos</h4>
				<table>
					<thead>
						<th width="100">NUMERO</th>
						<th width="100">CODIGO</th>
						<th width="500">DESCRIPCION</th>
						<th width="200">PROVEEDOR</th>
						<th width="100">EXISTENCIA</th>
					</thead>
					<tbody>
						@foreach ($provider->replacements as $element)
							<tr>
								<td>
									{{$element->replacement_id}}
								</td>
								<td>
									{{$element->code}}
								</td>
								<td>
									{{$element->description}}
								</td>
								<td>
									{{$provider->name}}
								</td>
								<td>
									{{$element->quantity}}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		
	@endif
	
@stop