@extends('layouts.master')

@section('css')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive-tables.css') }}">
  <link rel="stylesheet" href="{{asset('css/print.css')}}" media='print'>
@stop

@section('title', 'Cotizacion Motos')


@section('content')
@include('print.header')


	<div class="full-width">
		<div class="small-12 columns">
			<a href="{{route('cotizacions')}}" class="fa fa-arrow-left back button radius"> Atras</a><a class ="fa fa-print print button right" href="#"> Imprimir</a>
		</div>
      
	</div>
<div class="row">
  <h2>Cotización</h2> 
  <h5 style="margin-right: 4%; margin-top: -4%;
;"class="right date">Fecha:</h5>
</div>

<div class="container">
<div class="row">
    <div class="small-12 columns">
      <input type="text" placeholder="buscar por placa, marca, modelo..." class="cotiz-motorcycle" autofocus>
    </div>
</div>

<div class="row">
  <div class="small-12 columns">
    
  </div>

</div>
<form class="form" action="{{route('motorcycle')}}" method="GET" >
  {!! csrf_field() !!}
    <div class="row">
      <div class="small-12 large-6 columns">
        <label>Número de placa
          <input type="text" name="plate_number" readonly="true">
        </label>
      </div>
      <div class="small-12 large-6 columns">
        <label for="">Marca
          <input type="text" name="brand" readonly="true">
        </label>
      </div>
    </div>

    <div class="row">
      <div class="small-12 large-6 columns">
        <label>Modelo
          <input type="text" name="model" readonly="true">
        </label>
      </div>
      <div class="small-12 large-6 columns">
        <label for="">Color
          <input type="text" name="color" readonly="true">
        </label>
      </div>
    </div>

     <div class="row">
      <div class="small-12 large-6 columns" >
        <label>Año
          <input type="text" name="year" readonly="true">
        </label>
      </div>

      <div class="small-12 large-6 columns">
        <label for="">Serie de chasis
          <input type="text" name="series" readonly="true">
        </label>
      </div>
    </div>

     <div class="row">
      <div class="small-12 large-6 columns" >
        <label>Serie de motor
          <input type="text" name="motor" readonly="true">
        </label>
      </div>

      <div class="small-12 large-6 columns">
        <label for="">Cilindraje
          <input type="text" name="cylindra" readonly="true">
        </label>
      </div>
    </div>
    
     <div class="row">
      <div class="small-12 large-6 columns" >
        <label>Tipo de Motocicleta
          <input type="text" name="motorcycle_type" readonly="true">
        </label>
      </div>

      <div class="small-12 large-6 columns">
        <label for="">Precio
          <input type="text" name="price" readonly="true">
        </label>
      </div>
    </div>
  
    
    <div class="row">
      <div class="small-12 columns">
      <img class="picture center-image" src="" alt="">
    </div>
    </div>
  
</form>	
</div>

@stop


@section('script')
  @parent
  <script>
    d = new Date()
    $('.date').html('Fecha: ' + d.toLocaleDateString())
  </script>
@stop


