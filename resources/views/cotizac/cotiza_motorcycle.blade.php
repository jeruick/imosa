@extends('layouts.master')

@section('css')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive-tables.css') }}">
  <link rel="stylesheet" href="{{asset('css/print.css')}}" media='print'>
@stop

@section('title', 'Cotizacion Motos')


@section('content')
@include('print.header')


	<div class="full-width">
		<div class="small-12 columns">
			<a href="{{route('cotizacion_motorcycle')}}" class="fa fa-arrow-left back button radius"> Atras</a><a class ="fa fa-print print button right" href="#"> Imprimir</a>
		</div>
      
	</div>
<div class="row">
  <h2>Cotización</h2> 
  <h5 style="margin-right: 4%; margin-top: -4%;"class="right date">Fecha:</h5>
</div>


<table class="table_cotiza" style="width: 80%; margin-left: 10%">
<tr>
  <td colspan="2">
    <img class="picture_cotiza center-image" src="{{$motorcycle->picture}}">
  </td>

</tr>
  <tr>
    <td>
      <label>MARCA:
      {{$motorcycle->brand}}
      </label>
    </td>
    <td>
      <label>MODELO:
        {{$motorcycle->model}}
      </label>
    </td>
  </tr>
  <tr>
    <td>
      <label>TRANSMISIÓN:
        {{$motorcycle->transmission}}
      </label>
    </td>
    <td>
      <label>IGNICIÓN:
        {{$motorcycle->ignition}}
      </label>
    </td>
  </tr>
  <tr>
    <td>
      <label>TIPO DE MOTOR:
        {{$motorcycle->engine_type}}
      </label>
    </td>
    <td>
      <label>NÚMERO DE VELOCIDADES:
        {{$motorcycle->speeds}}
      </label>
    </td>
  </tr>
  <tr>
    <td>
      <label>CILINDRAJE:
      {{$motorcycle->cyl}}        
      </label>
    </td>
    <td>
      <label>CABALLOS DE FUERZA:
        {{$motorcycle->horsepower}}
      </label>
    </td>
  </tr>
  <tr>
    <td>
      <label>TIPO DE COMBUSTIBLE:
      {{$motorcycle->fuel_type}}
      </label>
    </td>
    <td>
      <label>ECONOMÍA DE COMBUSTIBLE:
        {{$motorcycle->fuel_economy}}
      </label>
    </td>
  </tr>
  <tr>
    <td>
      <label>FRENOS (DELANTERO | TRASERO):
      {{$motorcycle->brakes}}
      </label>
    </td>
    <td>
      <label>SUSPENSIÓN DELANTERA:
      {{$motorcycle->front_suspension}}        
      </label>
    </td>
  </tr>
  <tr>
    <td>
      <label>SUSPENSIÓN TRASERA:
      {{$motorcycle->rear_suspension}}
      </label>
    </td>
    <td>
      <label>LLANTA-RIN DELANTERA:
      {{$motorcycle->front_wheel}}        
      </label>
    </td>
  </tr>
  <tr>
    <td>
      <label>LLANTA-RIN TRASERA:
      {{$motorcycle->rear_wheel}}        
      </label>
    </td>
    <td>
      <label>PESO:
      {{$motorcycle->weight}}        
      </label>
    </td>
  </tr>

</table>





@stop


@section('script')
  @parent
  <script>
    d = new Date()
    $('.date').html('Fecha: ' + d.toLocaleDateString())
  </script>
@stop


