@extends('layouts.master')

@section('title', 'Cotizaciones')

@section('content')
<section class="options margin-top">
    <div class="row">
       <div class="small-6 columns">
         <a href="{{route('cotizacion_replacements')}}"><img class="center-image" src="{{ asset('images/settings.png') }}" alt=""><h4 class="panel text-center">Repuestos</h4></a>
       </div>

       <div class="small-6 columns">
         <a href="{{route('cotizacion_motorcycle')}}"><img class="center-image" src="{{ asset('images/bike.png') }}" alt=""><h4 class="panel text-center">Motos</h4></a>
       </div>
       
     </div> 
</section>
@stop
