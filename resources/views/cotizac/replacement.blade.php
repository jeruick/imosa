
@extends('layouts.master')


@section('css')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{asset('css/print.css')}}" media='print'>
@stop

@section('title', 'Cotizacion Repuestos')


@section('content')

	@include('print.header')


	<div class="full-width">
		<div class="small-12 columns">
			<a href="{{route('cotizacions')}}" class="fa fa-arrow-left back button radius">Atras</a>
			<a class ="fa fa-print print button right" href="#"> Imprimir</a>
		</div>
	</div>
<div class="row">
  <h2>Cotización</h2> 
  <h5 style="margin-right: 4%; margin-top: -4%;
;"class="right date">Fecha:</h5>
</div>

<div class="container">
	<div class="cotizacion">
			<div class="row">
				<div class="small-12 columns">
				<label class="cover">Nombre/Número de indentidad de cliente</label>
					<input type="text" class="search-customer">
					<ul class="clients-list">
						
					</ul>
					</div>
				</div>
			<div class="row">
				<div class="small-12 columns">
					<table class="responsive">
						<thead>
							<th width="500">CLIENTE</th>
							<th width="300">No. IDENTIDAD</th>
							<th class="address"width="500">DIRECCION</th>
							<th width="300">RTN</th>
							<th width="300">TELEFONO</th>
						</thead>
						<tbody class="customers-body">
							
						</tbody>
						
					</table>
				</div>
			</div>

	</div>
	<br>
	<div class="cotizacion_prod">
		<div class="row">
			<div class="small-12 columns">
			<label class="cover">Nombre/Codigo de repuesto </label>
				<input type="text" class="search-replacements">
				<ul class="replacements-list">
					
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<table id="table" class="responsive">
					<thead>
						<th width="100">CANTIDAD</th>
						<th width="700">DESCRIPCION</th>
						<th width="200">P.UNIDAD</th>
						<th width="200">P.TOTAL</th>
					</thead>
					<tbody class="replacements-body">
						
					</tbody>
				</table>
				<span id= "total" class="bold right text-right">Total: 0.0</span>
				
			</div>
		</div>
	</div>
</div>

@stop


@section('script')
	@parent
	<script>
	var $sum_total = 0.0
	$(document).ready(function() {
			$('.search-replacements').keyup(function(event) {
				$('.replacements-list').css('display', 'block');
				var $text = $(this).val() ? $(this).val() : 'blank';
				var url = "../repuesto/buscar/" + $text;
				$.ajax({
					url: url ,
					type: 'GET',
					dataType: 'json'
				})
				.done( function(data) {
					console.log(data)
					if(data.redirect)
					{	
						$('.replacements-list').css('display', 'none'); 
					}
					else if (data.length != 0) {
						
						$('.replacements-list').html('');
						$.each(data, function(index, val) {
							var $markup = "<li id='" + val.replacement_id + "' onclick='create_row(this)' >" + val.description  +"</li>" 
							$('.replacements-list').append($markup)	 
						})
						
					}
					else
					{
						$('.replacements-list').css('display', 'none'); 
					}

				})

			})



			$('.search-customer').keyup(function(event) {
					var text = $(this).val() ? $(this).val() : 'blank';

					$.get('../clientes/buscar/' + text, function(data) {

						if (data) {
							console.log(data);
							if(data.redirect)
							{
								window.location.href = data.redirect;
							}

							$('.customers-body').html('');
							
							$.each(data, function(index, val) {

								var markup = "<tr>" +
						  			"<td>" + val.full_name + "</td>" +
						  			"<td class= 'customer_id'>" + val.customer_id + "</td>" +
						  		  	"<td class='address'>" + val.address + "</td>" +
						  		  	"<td>" + val.RTN + "</td>" +
						  		  	"<td>" + val.phone_number + "</td>" +
						  		  	
						  		"</tr>";
						  		$('.customers-body').append(markup);
							})
						}

					})

				})

})


		function create_row (element) 
		{
			var customer_id = $('.customers-body .customer_id').html();
			console.log(customer_id);
			if(customer_id != null) 


			{

				var $price = 0.0;
				var $quantity = prompt("Cantidad");
				if (!parseInt($quantity)) {
					return alert('valor no valido')
				}
				
				var $replacement = {
					replacement_id : $(element).attr('id'),
					description: $(element).html(),
					price: $price,
					quantity: $quantity
					
				}

				$.ajax({
					url: '../repuesto/precio/'+$replacement.replacement_id + '/' + customer_id,
					type: 'GET'
				})
				.done(function(price) {
					total = (parseInt($quantity) * parseInt(price));
					$replacement.price = price;
					$markup = '<tr>'+
					'<td>' + $replacement.quantity + '</td>'+
					'<td>' + $replacement.description + '</td>' +
					'<td>' + price + '</td>' +
					'<td class:"sub-total ">' + total + '</td>' +
					'</tr>'
					

					$('.replacements-body').append($markup)

				CalcularTotal(total);
							
				})
			}
			else
			{
				alert("debe seleccionar un cliente")
			}
		}	
			
		function CalcularTotal(total)
		{
			$sum_total += total;
			$('#total').html('Total: ' + $sum_total)
		}

		

    d = new Date()
    $('.date').html('Fecha: ' + d.toLocaleDateString())

	</script>
@stop