@extends('layouts.master')

@section('css')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive-tables.css') }}">
  <link rel="stylesheet" href="{{asset('css/print.css')}}" media='print'>
@stop

@section('title', 'Cotizacion Motos')


@section('content')
@include('print.header')

<div class="full-width">
	<div class="small-12 columns">
		<a href="{{route('cotizacions')}}" class="fa fa-arrow-left back button radius"> Atras</a>
	</div>
</div>
<div class="row">
  <h2>Motos</h2> 
  
</div>

<div class="row">
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details',1)}}">
			<div class="square-info" style=" background-image:url('{{asset('images/motos/bronco/thumbs/bronco-centaurus-250.jpg')}}')">
				<span>Bronco CENTAURUS 250</span>
			</div>
		</a>
	</div>

	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details', 2)}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/bronco/thumbs/bronco-et-200-bross.jpg')}}');">
				<span>Bronco ET-200 BROSS</span>
			</div>
		</a>
	</div>
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details', 3)}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/bronco/thumbs/bronco-fenix-125.jpg')}}');">
				<span>Bronco FENIX 125</span>
			</div>
		</a>
	</div>	
	
</div>
<div class="row">
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details', 4)}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/bronco/thumbs/bronco-vintage-150.jpg')}}');">
				<span>Bronco VINTAGE 150</span>
			</div>
		</a>
	</div>
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details', 5)}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/bronco/thumbs/bronco-et-150.jpg')}}');">
				<span>Bronco ET-150 CARGO</span>
			</div>
		</a>
	</div>
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details', 6)}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/bronco/thumbs/bronco-scout-150.jpg')}}');">
				<span>Bronco SCOUT 150</span>
			</div>
		</a>
	</div>	
		
</div>
<div class="row">
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details')}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/bronco/thumbs/bronco-thunder-j200.jpg')}}');">
				<span>Bronco THUNDER J200</span>
			</div>
		</a>
	</div>
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details')}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/bronco/thumbs/bronco-bolt-250.jpg')}}');">
				<span>Bronco BOLT 250 NINJA</span>
			</div>
		</a>
	</div>
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details')}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/bronco/thumbs/bronco-alfa-250.jpg')}}');">
				<span>Bronco ALFA 250 </span>
			</div>
		</a>
	</div>
</div>	


<div class="row">
	
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details')}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/haojin/thumbs/haojin-eagle-125.jpg')}}');">
				<span>Haojin EAGLE 125</span>
			</div>
		</a>
	</div>
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details')}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/haojin/thumbs/haojin-fabio-150.jpg')}}');">
				<span>Haojin FABIO 150 NINJA</span>
			</div>
		</a>
	</div>
	<div class=" large-4 small-12 columns">
		<a href="{{route('motorcycle_details')}}">
			<div class="square-info" style="background-image: url('{{asset('images/motos/haojin/thumbs/haojin-warrior-200.jpg')}}');">
				<span>Haojin WARRIOR 200 </span>
			</div>
		</a>
	</div>
</div>

@stop

