@extends('layouts.master')	

@section('content')

@if($company)

	<div class="row">
		<form action="{{route ('updateCompany', $company->company_id)}}" method="POST" enctype="multipart/form-data">
		<div class="row">
			<div class="small-12 columns">
				<a href="{{ route('configurations') }}" class="fa fa-arrow-left button">  Atras</a>
				<button type="submit" class="button fa fa-floppy-o right">  Guardar</button>
			</div>
		</div>

	@if (count($errors) > 0)
	    
        @foreach ($errors->all() as $error)
            <div class="row">
            	<span class="error">{{ $error }}</span> 
            </div>
        @endforeach
	    
	@endif
		{!! csrf_field() !!}
			<h2>Datos de Empresa</h2>
	  		  <div class="row">
				<div class="small-12 large-6  columns">
		  		    <div class="small-12 columns">
		  		      <label>Nombre
		  		        <input type="text" name="name" value="{{$company->name}}" required />
		  		      </label>
		  		   </div>
		  		    <div class="small-12 columns">
		  		       <label>Dirección
		  		         <input type="text" name="address" value="{{$company->address}}" required />
		  		       </label>
		  		    </div>

	  		      <div class="small-12 columns">
	  		        <label>Teléfono
	  		          <input type="text" name="telephone" value="{{$company->telephone}}" required />
	  		        </label>
	  		      </div>
	  		  		
	  		  		<div class="small-12 columns">
	  		  		    <label>Correo Eléctronico
	  		  		      <input type="text" name="email" value="{{$company->email}}" />
	  		  		    </label>
	  		  		</div>
				</div>
				<div class="small-12 large-6 columns">
				  <div class="small-12 columns">
				    <label>Página web
				      <input type="text" name="web_site"  value="{{$company->web_site}}" required />
				    </label>
				  </div>
					<div class="small-12 columns">
				    <label>RTN
				      <input type="text" name="rtn"  value="{{$company->rtn}}" required />
				    </label>
				  </div>
					<div class="small-12 columns">
						<label for=""><img src="{{$company->logo}}" class="company-image" alt="company-image"><input type="file" name="picture"></label>
					</div>
				</div>
	  		</div>
		</form>
	</div>
@else

	<div class="row">
		<form action="{{route ('newCompany')}}" method="POST" enctype="multipart/form-data">
		<div class="row">
			<div class="small-12 columns">
				<a href="{{ route('configurations') }}" class="fa fa-arrow-left button">  Atras</a>
				<button type="submit" class="fa fa-floppy-o right">  Guardar</button>
			</div>
		</div>

	@if (count($errors) > 0)
	    
        @foreach ($errors->all() as $error)
            <div class="row">
            	<span class="error">{{ $error }}</span> 
            </div>
        @endforeach
	    
	@endif
		{!! csrf_field() !!}
			<h2>Datos de Empresa</h2>
	  		  <div class="row">
				<div class="small-12 large-6  columns">
		  		    <div class="small-12 columns">
		  		      <label>Nombre
		  		        <input type="text" name="name" required />
		  		      </label>
		  		   </div>
		  		    <div class="small-12 columns">
		  		       <label>Dirección
		  		         <input type="text" name="address" required />
		  		       </label>
		  		    </div>

	  		      <div class="small-12 columns">
	  		        <label>Teléfono
	  		          <input type="text" name="telephone" required />
	  		        </label>
	  		      </div>
	  		  		
	  		  		<div class="small-12 columns">
	  		  		    <label>Correo Eléctronico
	  		  		      <input type="text" name="email" />
	  		  		    </label>
	  		  		</div>
				</div>
				<div class="small-12 large-6 columns">
				  <div class="small-12 columns">
				    <label>Página web
				      <input type="text" name="web_site"  required />
				    </label>
				  </div>
					<div class="small-12 columns">
				    <label>RTN
				      <input type="text" name="rtn"  required />
				    </label>
				  </div>
					<div class="small-12 columns">
						<label for=""><img  class="company-image" alt="company-image"><input type="file" name="picture"></label>
					</div>
				</div>
	  		</div>
		</form>
	</div>
@endif

@stop
