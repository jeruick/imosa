@extends('layouts.master')	
@section('css')
		@parent
		<link rel="stylesheet" href="{{ asset('css/foundation-datepicker.min.css') }}">		
	@show
@section('content')

@if($companyinvoice)
	<div class="row">
		<form action="{{route ('updateCompanyInvoice', $companyinvoice->company_invoice_id)}}" method="POST" enctype="multipart/form-data">
		<div class="row">
			<div class="small-12 columns">
				<a href="{{ route('configurations') }}" class="fa fa-arrow-left button">  Atras</a>
				<button type="submit" class="button fa fa-floppy-o right">  Guardar</button>
			</div>
		</div>
	
	@if (count($errors) > 0)
	    
        @foreach ($errors->all() as $error)
            <div class="row">
            	<span class="error">{{ $error }}</span> 
            </div>
        @endforeach
	    
	@endif
	
		{!! csrf_field() !!}
	  		  <div class="row">
				<h2>Datos de Factura</h2>
				<div class="small-12 large-6  columns">
		  		    <div class="small-12 columns">
		  		      <label>CAI
		  		        <input type="text" name="cai" value="{{$companyinvoice->cai}}" required />
		  		      </label>
		  		   </div>
		  		    <div class="small-12 columns">
		  		       <label>Rango de facturación - desde:
		  		         <input type="text" name="range_to" value="{{$companyinvoice->range_to}}" required />
		  		         <label>Rango de facturación - hasta:
		  		         <input type="text" name="range_up" value="{{$companyinvoice->range_up}}" required />
		  		         </label>
		  		       </label>
		  		    </div>
	  		  		
				</div>
				<div class="small-12 large-6 columns">
					
				  <div class="small-12 columns">
				    <label>ISV:
				      <input type="text" name="isv"  value="{{$companyinvoice->isv}}" required />
				    </label>
				  </div>
					
				  <div class="small-12 columns">
	  		  		    <label>Fecha limite de emisión:
	  		  		      <input type="date" class="span2" name="date" value="{{$companyinvoice->limit_date}}" id="dp2" data-date-format="dd-mm-yy" />
	  		  		    </label>
	  		  		</div>
	  		</div>
	</form>
	</div>


@else


	<div class="row">
		<form action="{{route ('newCompanyInvoice')}}" method="POST" enctype="multipart/form-data">
		<div class="row">
			<div class="small-12 columns">
				<a href="{{ route('configurations') }}" class="fa fa-arrow-left button">  Atras</a>
				<button type="submit" class="fa fa-floppy-o right">  Guardar</button>
			</div>
		</div>
	
	@if (count($errors) > 0)
	    
        @foreach ($errors->all() as $error)
            <div class="row">
            	<span class="error">{{ $error }}</span> 
            </div>
        @endforeach
	    
	@endif
	
		{!! csrf_field() !!}
	  		  <div class="row">
				<h2>Datos de Factura</h2>
				<div class="small-12 large-6  columns">
		  		    <div class="small-12 columns">
		  		      <label>CAI
		  		        <input type="text" name="cai" value="" required />
		  		      </label>
		  		   </div>
		  		    <div class="small-12 columns">
		  		       <label>Rango de facturación - desde:
		  		         <input type="text" name="range_to" value="" required />
		  		         <label>Rango de facturación - hasta:
		  		         <input type="text" name="range_up" value="" required />
		  		         </label>
		  		       </label>
		  		    </div>
	  		  		
				</div>
				<div class="small-12 large-6 columns">
					
				  <div class="small-12 columns">
				    <label>ISV:
				      <input type="text" name="isv"  value="" required />
				    </label>
				  </div>
					
					
				  <div class="small-12 columns">
	  		  		    <label>Fecha limite de emisión:
	  		  		      <input type="date" class="span2" name="date" value="" id="dp2" data-date-format="dd-mm-yyyy" />
	  		  		    </label>
	  		  		</div>
	  		</div>
	</form>
	</div>

@endif



@stop


@section('script')
	@parent
	<script src="{{ asset('js/foundation-datepicker.min.js 	') }}"></script>
	<script>
		$(function(){
		  $('#dp2').fdatepicker({
				closeButton: true,
				format: 'mm-dd-yyyy',
			});
		});
	</script>
@stop