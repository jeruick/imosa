@extends('layouts.master')	

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop

@section('content')
	@if ($errors->any())
		<ul>
			@foreach ($errors as $err)
				<li><span class="error">{{$err}}</span></li>
			@endforeach
		</ul>
	@endif

	<div class="row">
		<div class="small-12 columns">
			<a href="{{ route('configurations') }}" class="fa fa-arrow-left button radius left" >  Atras</a>
			<a href="#" class="button radius right" data-open="myModal"><i class="fa fa-user"></i> Nueva Bodega</a>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<table class="responsive" style="width: 100%" class="my-table">
			  <thead>
			    <tr>
			    	<th style="text-align: center;"  width="200">Numero</th>
			      	<th style="text-align: center;"  width="400">Nombre</th>
			      	<th style="text-align: center;" >Eliminar</th>
			    </tr>
			  </thead>
			  <tbody class="cellars-body">

			  	@foreach ($cellars as $element)
			  		<tr>

		  				<td style="text-align: center;">{{$element->cellar_id}}</td>
						<td style="text-align: center;">{{$element->description}}</td>
						<td style="text-align: center;"><a href="{{ route('deleteCellar', $element->cellar_id) }}" class="button alert"><i class="fa fa-remove"></i></a></td>
			  		</tr>
			  	@endforeach
			    
			  </tbody>
			</table>
		</div>



	{{-- Brand Modal --}}

	<div id="myModal" class="reveal" data-reveal aria-labellebdy="modalTitle" aria-hidden="true" role="dialog"> 
		<h2>Nueva Bodega</h2>
		<p>
			<form action="{{ route('newCellar')}}" method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}

				<div class="row">
					<div class="small-12 columns">
						<label for="">Nombre
							<input type="text" name="description" required>
						</label>
					</div>					
				</div>
				<div class="row">
	  		     	<div class="small-12 columns">
	  		     		<button class="button fa fa-floppy-o" type="submit">  Guardar</button>
	  		     	</div>
	  		     </div>
			</form>	
		</p>
		 <button class="close-button" data-close aria-label="Close modal" type="button">
   		 <span aria-hidden="true">&times;</span>
  		</button>
	</div>
@stop



