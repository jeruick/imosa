@extends('layouts.master')	

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.dynatable.css') }}">
@stop

@section('content')
	@if ($errors->any())
		<ul>
			@foreach ($errors->all() as $err)
				<li><span class="error">{{$err}}</span></li>
			@endforeach
		</ul>
	@endif
	<div class="row">
		<div class="small-12 columns">
			<a href="{{ route('configurations') }}" class="fa fa-arrow-left button radius left" >  Atras</a>
			<a class="button radius right" data-open="myModal"><i class="fa fa-user"></i> Nueva Marca</a>
		</div>
	</div>
	
	<div class="row">
		<div class="small-12 columns">
			<table class="responsive" style="width:100%" class="my-table">
				<thead>
					<th style="text-align: center;"  width="200">Número</th>
					<th style="text-align: center;" width="200">Nombre</th>
					<th style="text-align: center;" width="200">Eliminar</th>
				</thead>
				<tbody class="customers-body">
					@foreach ($brands as $element)
						<tr>
							<td style="text-align: center;">{{$element->brand_id}}</td>
							<td style="text-align: center;">{{$element->description}}</td>
							<td style="text-align: center;"><a href="{{ route('deletebrand', $element->brand_id) }}" class="button alert"><i class="fa fa-remove"></i></a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>	


	{{-- Brand Modal --}}

	<div id="myModal" class="reveal" data-reveal aria-labellebdy="modalTitle" aria-hidden="true" role="dialog"> 
		<h2>Nueva marca</h2>
		<p>
			<form action="{{ route('newBrand')}}" method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}

				<div class="row">
					<div class="small-12 columns">
						<label for="">Nombre
							<input type="text" name="description" required>
						</label>
					</div>					
				</div>
				<div class="row">
	  		     	<div class="small-12 columns">
	  		     		<button class="button fa fa-floppy-o" type="submit">  Guardar</button>
	  		     	</div>
	  		     </div>
			</form>	
		</p>
		
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
	</div>
@stop
	