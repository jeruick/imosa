<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="_token" content="{!! csrf_token() !!}"/>
	<title>@yield('title','IMOSA') </title>
	@section('css')
		<link rel="stylesheet" href="{{ asset('css/normalize.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
		<link rel="stylesheet" href="{{ asset('css/backend.css') }}">
		
	@show
</head>
<body>
	@section('header')
	
		<div class="top-bar">
			<div class="top-bar-title">
			   <span data-responsive-toggle="responsive-menu" data-hide-for="large">
			     <button class="menu-icon dark" type="button" data-toggle></button>
			   </span>
			</div>
			
			<div id="responsive-menu">

				<div class="top-bar-left">
					  <ul class="dropdown menu horizontal-scroll" data-dropdown-menu>
					     <!--<li class="menu-text">
					      <h4><a href="{{ route('home') }}">IMOSA</a></h4>
					    </li>-->
					    <li class="menu-text" style="padding:0">
					      <a href="{{ route('home') }}" style="padding:0 10px 0 10px"><img style="width:80px; padding:0!important;" src="{{asset('images/logo.jpg')}}" alt=""></a>
					    </li>
					     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
					    
					  	@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
				    	<li class="@if (isset($inventory_active)) active  @endif">
				    	  	<a href="{{ route('inventory') }}">Inventario</a>
				    	</li>
				    	@endif
				    	@if(Auth::user()->type->description == 'admin')
				    	<li @if (isset($customer_active)) class="active"  @endif><a href="{{ route('customers') }}">Clientes</a></li>

						@endif
						<li @if (isset($order_active)) class="active" @endif><a href="{{ route('orders') }}">Ordenes</a></li>
						@if(Auth::user()->type->description == 'admin')
						<li @if (isset($credit_active)) class="active"  @endif><a href="{{ route('getCustomerCredits') }}">Creditos</a></li>

						
						<li class="@if (isset($configuration_active)) active @endif">
						<a href="{{ route('configurations') }}">Configuración</a>
						</li>
						
						<li class="@if (isset($constancies_active)) active @endif">
							<a href="{{ route('constancies') }}">Documentación</a>
							
						</li>
						@endif
						@if (Auth::user()->user_type == 1)
							<li class="@if (isset($user_active)) active @endif">
							<a href="{{ route('users') }}">Usuarios</a>
							</li>	
						@endif

				    </ul>
				</div>
				<div class="top-bar-right">
					<ul class="dropdown menu" data-dropdown-menu>
						@if(Auth::user()->type->description === 'admin')
						<li>
							<a href="#" style="margin-top: -0.5em"><i class="fa fa-globe fa-2x" style="margin-top: 10px"></i></a>
							<ul class="dropdown notifications">
								
							</ul>
						</li>
						@endif
						<li>

						  	<a href="#" class="button">
						  		@if (Auth::check())
										{{Auth::user()->name}} ({{Auth::user()->type->description}})
								@endif
							</a>
						  	<ul class="menu vertical">
						  		@if (Auth::user()->user_type == 1)
						  			<li class="@if (isset($user_active)) active @endif">
						  			<a href="{{ route('users') }}">Usuarios</a>
						  			</li>	
						  		@endif
						    		<li><a href="{{url('auth/logout')}}" style="background: white; color: #c60f13; font-weight: bold"><i class="fa fa-sign-out hide-for-small-only" style="display: inline-block !important;"></i> Cerrar sesion</a></li>
						  	</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	@show
	@section('content')
		
	@show

	@section('script')
		<script src="{{asset('js/jquery.js')}}" ></script>		
		<script src="{{ asset('js/modernizr.js') }}"></script>
		<script src="{{ asset('js/foundation.min.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js"></script>
		<script src="{{asset('js/sweetalert.min.js')}}"></script>
		<script src="{{ asset('js/code.js') }}"></script>
	@show
</body>
</html>