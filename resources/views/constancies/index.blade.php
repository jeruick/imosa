@extends('layouts.master')

@section('title', 'Constancias')

@section('content')
<section class="options margin-top">
    
    <div class="row">
       <div class="small-6 columns ">
         <a href="{{route('vaucher')}}"><img class="center-image" src="{{ asset('images/recibo.png') }}" alt=""><h4 class="panel text-center">Recibos</h4></a>
       </div>
     </div> 
</section>
@stop
