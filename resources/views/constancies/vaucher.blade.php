@extends('layouts.master')

@section('title', 'Recibo')
@section('css')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive-tables.css') }}">
  <link rel="stylesheet" href="{{asset('css/print.css')}}" media='print'>
@stop

@section('content')

<div class="full-width">
	<div class="small-12 columns">
		<a href="{{route('constancies')}}" class="fa fa-arrow-left back button radius">Atras</a>
		<a class ="fa fa-print button right" onclick="number()" href="{{route('savevaucher')}}"> Imprimir</a>
	</div>
	<input name="nvau" style="display: none;"  id="nvau" type="text">
</div>
<div class="vaucher">
	<div class="row">
		<div class="image">
			<img src="{{asset('images/logo.jpg')}}" alt="">
		</div>
		<div class="text">
		      <span style="font-size: 20px;" class="tittle">IMPORTADORA Y DISTRIBUIDORA DE MOTOPARTES S.A.</span>
		      <h5 style="margin-bottom: 2px;">Res. Palma Real, Frente a Fuerza Aerea</h5>
		      <h5 style="margin-bottom: 2px;">Tel. 8943-0052 -- R.T.N. 08019015808210 </h5>
		      <h5 style="margin-bottom: 2px; text-align: center">Sitio web: www.imosa.com</h5>
		</div>

	</div>
		<form class="form_vaucher">
			<div class="row left">
				<h4 class="vaucher#">Recibo N°: </h4>
				<div style=" margin-left: 9%!important; margin-top: -4.5%!important; border: none!important; vertical-align: top" class="center"> <input style="border: none; box-shadow: none; font-size: 20px"  type="text" name="vnum" value="{{$vaucher->max('vaucher_id')}}">
				</div>


				<div style="margin-top: -40px; margin-bottom: -10px" class="right">
					<strong >Por Lps.</strong>
					<input name="mount" style="width: 60%;" id="numero" type="number" class="calculando" >
				</div>
			</div>
		<div class="inputs">
			<div class="row">
				<label >Recibí de: </label> 
				<input style="width: 86%; text-transform:uppercase;" type="text"> 
			</div>
			<div class="row">
				<label >La cantidad de: </label><input style="text-transform: uppercase;" type="text" class="quantity">
			</div>
			<div class="row">
			<label>Por concepto de: </label><input style ="text-transform:uppercase;" type="text">
			</div>
		</div>
			<br>
			<div class="foot row">
				<div class="negritas small-12 large-4 columns">
					
				<strong style="margin-bottom: 6%; width: 59%;" class="left">Saldo Anterior L: </strong><input style="height:35px; width: 40%; margin-right:1%; margin-bottom:1%;" class="sum" name="saldo" type="number">

				<strong style="margin-bottom: 6%; width: 59%;" class="left">Abono L: </strong><input style="height:35px; width: 40%; margin-right:1%; margin-bottom:1%;" class="sum" onblur="suma()" name="pay" type="number">
			
				<strong style="width: 59%;" class="left">Saldo L: </strong><input style="height:35px; width: 40%; margin-right:1%; margin-bottom:1%;" class="sum" id="total" name="nuevosaldo" type="number">		
				</div>

				<div style="margin-top: 0%; margin-right: 0%;"class="foot1 right  small-12 large-8 columns ">
					<div style="margin-left: 20%; width:44%; margin-bottom: 7%;" class="">
						<label class="right date"></label>
					</div>
					<div style="margin-right:10%" class="right">
						<div class="border small-12 columns">
							<strong> Firma y Sello </strong>
						</div>
					</div>	
				</div>
				
			</div>
		</form>
</div>


@stop


@section('script')
	@parent
	<script>

	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var f=new Date();
	date = (f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
	$('.date').html('Fecha: ' + date)

	    
	    $(".calculando").blur(function(){
		var numero = $("#numero").val();
		
		$.ajax({
					url: 'numero/' + numero ,
					type: 'GET'
				}).done(function(data)
				{
					$(".quantity").val(data)
				})

		
		// $("#resultado").load("ajax.php",{ numero : numero });
		// alert(numero);

		})
	
		 function suma()
		{
			var x = +$('input[name=mount]').val();

			var a = +$('input[name=saldo]').val();
			var b = +$('input[name=pay]').val();
			$total = a-b;
			// alert($total);
			$('#total').val($total);

		}

		function number()
		{
			var x = +$('input[name=vnum]').val();
			var y = x + 1;
			$('#nvau').val(y);
			console.log(y);
		}

	</script>
@stop
