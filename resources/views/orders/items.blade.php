
<div class="items">
@if($order)
<div class="row">
	<div class="small-12 columns table-scroll">
		@if ($order->replacements)
		<table id="table">
			<thead>
				<th style="text-align: center">No.</th>
				<th style="text-align: center">CODIGO</th>
				<th style="text-align: center" >DESCRIPCION</th>
				@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'bodega')
				<th style="text-align: center" width="110">BODEGA</th>
				<th style="text-align: center" >UBICACION</th>
				@endif
				@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
				<th style="text-align: center" >P.UNIDAD</th>
				@endif
				<th style="text-align: center" width="100">CANTIDAD</th>
				@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
				<th style="text-align: center">P.TOTAL</th>
				@endif
				@if(Auth::user()->type->description == 'admin')
				<th style="text-align: center">ELIMINAR</th>
				@endif
				
				@if(Auth::user()->type->description == 'bodega' || Auth::user()->type->description == 'admin')
				<th style="text-align: center">VERIFICAR</th>
				@endif
			</thead>
			<tbody class="replacements-body" >
				
				@foreach ($order->replacements->reverse() as $key => $element)
					
					<tr id="{{$element->getReplacement->replacement_id}}">
						<td style="text-align: center">{{$key + 1}}</td>
						<td style="text-align: center">{{$element->getReplacement->code}}</td>
						<td style="text-align: center">
							{{$element->getReplacement->description}}
							<input type="hidden" name="replacements[{{$element->getReplacement->replacement_id}}][replacement_id]" value="{{$element->getReplacement->replacement_id}}">
						</td>
						@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'bodega')
						<td>
							{{$element->getReplacement->cellar->description}}
						</td>
						<td>
							{{$element->getReplacement->location}}
						</td>
						@endif
						@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
						<td style="text-align: center">
							@if ($order->customer->type->description == 'LOCAL')
								{{number_format($element->getReplacement->wholesale_price, 2, '.', ',')}}
							@else
								{{number_format($element->getReplacement->route_price, 2, '.', ',')}}	
							@endif
						</td>
						@endif
						<td style="text-align: center"> <input type="text" class="quantity" onkeypress="return isNumber(event)" onkeyup="change_quantity(this)" value="{{$element->quantity}}" @if(Auth::user()->type->description == 'bodega') disabled @endif>
						<input type="hidden" name="replacements[{{$element->getReplacement->replacement_id}}][quantity]" value="{{$element->quantity}}">
							
						</td>
						@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
						<td style="text-align: center">
							@if ($order->customer->type->description == 'LOCAL')
								{{number_format($element->getReplacement->wholesale_price*$element->quantity, 2, '.', ',')}}
							@else
								{{number_format($element->getReplacement->route_price*$element->quantity, 2, '.', ',')}}	
							@endif
						</td>
						@endif
						@if(Auth::user()->type->description == 'admin')
						<td style="text-align: center"><span value="{{$element->replacement_order}}" class="button alert radius" onclick="remove_product_from_order(this)"><i class="fa fa-remove"></i></span></td>
						@endif
						@if(Auth::user()->type->description == 'bodega' || Auth::user()->type->description == 'admin')
						<td style="text-align: center">@if($element->status) <i class="fa fa-check-circle fa-2x" style="color: green"></i> @else <i class="fa fa-close fa-2x" style="color: red"></i> @endif</td>
						@endif
					</tr>

				@endforeach
				
			</tbody>
		</table>
		@endif
	</div>
</div>
@if(Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
<div class="row">
	<div class="small-12 medium-6  medium-offset-6 columns">
		<table >
			<tr>
				<td><strong>Total: </strong></td>
				<td><strong><span class="total">{{number_format((float)$order->total, 2, '.', ',')}}</span></strong></td>
			</tr>
		</table>
		<p class="right">
			</strong>
		</p>
		<p class="right">
			</strong>
		</p>
		<p class="right">
			
		</p>
	</div>
</div>
@endif


@if ($order->active && (isset($is_ready) && $is_ready ))
		<div class="row">
			<div class="small-12 columns">
				<button type="submit" class ="fa fa-check-circle button radius right" > Cerrar orden</button>		
			</div>
		</div>
		@endif

@endif

</div>

@section('script')
	@parent
	<script>
		function remove_product_from_order(element){
			swal({ title: "Contraseña de administrador",  type: "input", inputType: 'password', showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   inputPlaceholder: "Contraseña" }, function (inputValue){
				if (inputValue === false) return false;      
				if (inputValue === "") {     
					swal.showInputError("Escribe una contraseña");     
					return false   
				}    
				
				$.post("{{route('checkAdminPassword')}}",
				{
					'password': inputValue, 
					'_token': $('meta[name=_token]').attr('content')
				})
				.success(function(data) {
					console.log(data);
					
					if(data.id)
					{
						$.ajax({
							url: "{{route('removeOrderProduct')}}",
							type: 'DELETE',
							data: {
								'replacement_order': $(element).attr('value'), 
								'order_id': $('.order_id').val(), 
								'_token': $('meta[name=_token]').attr('content')},
						})
						.done(function(result) {
							swal("Eliminado", "El producto ha sido elimiado correctamente");
							$('.items').html(result);
						})
						.fail(function() {
							console.log("error");
						});
						
					}
					else
					{

						swal.showInputError("Contraseña incorrecta");     
					}
				})
				.fail(function() {
					console.log("error");
				});
			
			});
			
		}
	</script>
@stop

