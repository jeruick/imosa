
@if ($orders && count($orders) > 0)
<table>
	<thead>
		<tr>
			<th width="100">Detalles</th>
			<th width="200">No. Orden</th>
			<th width="300">Cliente</th>
			<th width="300">Usuario</th>
			<th width="200">Fecha</th>
			<th width="100">Total</th>
			<th width="50">Tipo</th>
			<th width="50">Estado</th>
			@if(Auth::user()->type->description === 'admin' )
			<th width="50">Imprimir</th>
			<th width="50">Enviar Correo</th>
			@endif

			@if(Auth::user()->type->description === 'admin' || Auth::user()->type->description === 'vendedor')
			<th width="50">Eliminar</th>
			@endif
		</tr>
	</thead>
	<tbody>
		@foreach ($orders as $order)
			<tr>
				<td><a href="{{ route('getOrder', $order->order_id) }}" class="button radius"><i class="fa fa-search"></i></a></td>
				<td>{{$order->order_id}}</td>
				@if(Auth::user()->type->description === 'admin')
					<td><a href="{{ route('customer', $order->customer_id) }}">{{$order->customer->full_name}}</a></td>
				@else
					<td>{{$order->customer->full_name}}</td>
				@endif
				<td>{{$order->user->name}}</td>
				<td>{{date('d-m-Y', strtotime($order->date))}}</td>
				<td>{{number_format($order->total, 2, '.', ','	)}}</td>
				<td>@if($order->is_invoice) Factura @else Proforma @endif</td>
				<td>@if($order->active) <i class="fa fa-check-square fa-2x" style="color: red" aria-hidden="true"></i> @else <i class="fa fa-check-square fa-2x" style="color: green" aria-hidden="true"></i> @endif</td>

				@if(Auth::user()->type->description === 'admin')

				<td><a href="{{route('printOrder', $order->order_id)}}"><i class="fa fa-print fa-2x" style="color: #00aaaa" aria-hidden="true"></i></a></td>
				<td><a href="{{route('sendEmail', $order->order_id)}}"><i class="fa fa-send fa-2x" style="color: #00aaaa" aria-hidden="true"></i></a></td>
				@endif
				
				@if(Auth::user()->type->description === 'admin' || Auth::user()->type->description === 'vendedor')
				<td><button id="{{ route('deleteOrder', $order->order_id) }}" class="button alert radius" onclick="check_admin_password(this)"><i class="fa fa-remove"></i></button></td>
				@endif
			</tr>
		@endforeach
	</tbody>
</table>
	

@else
	<br><br><br>
	<div class="callout">
	  <h1 class="text-center">No hay ordenes</h1>
	</div>
@endif