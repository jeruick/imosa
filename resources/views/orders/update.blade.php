@extends('layouts.master')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/responsive-tables.css') }}">
	<link rel="stylesheet" href="{{asset('css/print.css')}}" media='print'>
	<style>
		.label-container{
			position:fixed;
			bottom:48px;
			right:105px;
			display:table;
			visibility: hidden;
		}

		.label-text{
			color:#FFF;
			background:rgba(51,51,51,0.5);
			display:table-cell;
			vertical-align:middle;
			padding:10px;
			border-radius:3px;
		}

		.label-arrow{
			display:table-cell;
			vertical-align:middle;
			color:#333;
			opacity:0.5;
		}

		.float{
			position:fixed;
			width:60px;
			height:60px;
			bottom:40px;
			left:40px;
			background-color:#F33;
			color:#FFF;
			border-radius:50px;
			text-align:center;
			box-shadow: 2px 2px 3px #999;
			z-index:1000;
			animation: bot-to-top 2s ease-out;
		}

		.floating-message
		{
			background: rgba(0,0,0,.5);
			color: white;
			padding: .5em;
			position: absolute;
			width: 10em;
			right: -11em;
			top: .5em;
		}



		ul.floating-buttons{
			position:fixed;
			left:20px;
			padding-bottom:20px;
			bottom:80px;
			z-index:100;
		}

		ul.floating-buttons li{
			list-style:none;
			margin-bottom:10px;
			position: relative;
		}

		ul.floating-buttons li a{
			background-color:#F33;
			color:#FFF;
			border-radius:50px;
			text-align:center;
			box-shadow: 2px 2px 3px #999;
			width:60px;
			height:60px;
			display:block;
		}

		ul.floating-buttons:hover{
			visibility:visible!important;
			opacity:1!important;
		}


		.my-float{
			font-size:24px;
			margin-top:18px;
		}

		a#menu-share + ul{
		  visibility: hidden;
		}

		a#menu-share:hover + ul{
		  visibility: visible;
		  animation: scale-in 0.5s;
		}

		a#menu-share i{
			animation: rotate-in 0.5s;
		}

		a#menu-share:hover > i{
			animation: rotate-out 0.5s;
		}

		@keyframes bot-to-top {
		    0%   {bottom:-40px}
		    50%  {bottom:40px}
		}

		@keyframes scale-in {
		    from {transform: scale(0);opacity: 0;}
		    to {transform: scale(1);opacity: 1;}
		}

		@keyframes rotate-in {
		    from {transform: rotate(0deg);}
		    to {transform: rotate(360deg);}
		}

		@keyframes rotate-out {
		    from {transform: rotate(360deg);}
		    to {transform: rotate(0deg);}
		}
	</style>
@stop

@section('content')
	<div class="full-width">
		<div class="small-12 columns">
			<a href="{{ route('orders') }}" class="fa fa-arrow-left back button radius"> Atras</a>
			
			@if(isset($order) && Auth::user()->type->description === 'admin')
			<a class="success button right" href="{{route('exportOrder', $order->order_id)}}" ><i class="fa fa-file-excel-o"></i> Exportar orden</a>
			@endif
		</div>
	</div>
	
	@if ($errors->any())
		<ul>
			@foreach ($errors->all() as $error)
				<li><span class="error">{{$error}}</span></li>
			@endforeach
		</ul>
	@endif
	
	@if (session()->has('message'))
	
	<div class="row">
		<div class="small-12 columns">
		    <div class="callout secondary">
		      <h5>{{session('message')}} </h5>
		    </div>
		</div>
	</div>
	@endif
	
	

	@if(isset($order))
		
		
		{{-- Update order --}}
		
		<form action="{{ route('updateOrder', $order->order_id) }}" method="POST">
			<input type="hidden" name="customer" value="{{$order->customer_id}}">
			<input type="hidden" name="order_id" class="order_id" value="{{$order->order_id}}">
		{!! csrf_field() !!}
		<div class="row">
			<div class="small-12 columns">
				<span class="left">Tipo de documento: @if($order->is_invoice) Factura @else Proforma  @endif</span>
				<span class="right">Correlativo: 
				@if($order->invoices->first()) 
					@foreach($order->invoices as $invoice)
						{{$invoice->invoice_id}}
					@endforeach
				@else
					@foreach($order->proformas as $proforma)
						{{$proforma->proforma_id}} - {{date('Y')}}
					@endforeach

				@endif</span>
			</div>
			<div class="small-12 columns">
				<table class="responsive">
					<thead>
						<th width="300">CLIENTE</th>
						<th width="200">CIUDAD</th>
						<th width="500">DIRECCION</th>
						<th width="200">RTN</th>
						<th width="200">TELEFONO</th>
					</thead>
					<tbody class="customers-body">
						<tr>
							<td>{{$order->customer->full_name}}</td>
							<td class= 'customer_id'>{{$order->customer->city->name}}</td>
							<td>{{$order->customer->address}}</td>
							<td>{{$order->customer->RTN}}</td>
							<td>{{$order->customer->phone_number}}</td>
						</tr>
					</tbody>
					
				</table>
			</div>
		
		</div>
		@if (Auth::user()->type->description == 'admin' || Auth::user()->type->description == 'vendedor')
		<div class="row">
			<div class="small-12 columns">
				<label>Agregar repuesto a la orden</label>
				<input type="text" class="search-replacements" placeholder="Buscar repuesto...">
				<ul class="replacements-list">
					
				</ul>
			</div>
		</div>
		@endif
		
		@if (Auth::user()->type->description === 'bodega' || Auth::user()->type->description === 'admin')
		<div class="row">
			<div class="small-12 columns">
				<label>Chequear repuesto de la orden</label>
				<input type="text" autofocus placeholder="Chequear codigo del repuesto" id="replacement_code" onkeyup="check_item(event)" autocomplete="off"> 
			</div>
		</div>
			
		@endif
		@include('orders.items')
		
	</form>
	
	<a href="#" class="float" style="background: #00aaaa" id="menu-share"><i class="fa fa-plus my-float"></i></a>
	<ul class="floating-buttons">

		@if($order->is_invoice)
		<li><a href="{{route('printOrder', $order->order_id)}}" style="background: #00aaaa">
		<i class="fa fa-print my-float"></i>
		</a><span class="floating-message">Imprimir factura</span></li>
		@endif
		<li><a href="{{route('printProforma', $order->order_id)}}" style="background: #00aaaa">
		<i class="fa fa-print my-float"></i>
		</a><span class="floating-message">Imprimir proforma</span></li>
		
		<li><a href="{{route('sendEmail', $order->order_id)}}" style="background: #00aaaa">
		<i class="fa fa-send my-float"></i>
		</a><span class="floating-message">Enviar correo</span></li>

		
	</ul>


	@else

	{{-- Create order --}}
	<div class="row">
		<div class="small-12 columns">
			<h2 class="new_order">Nueva orden de repuestos</h2>		
		</div>
	</div>
	
	<form action="{{ route('saveOrder') }}" id="order" method="POST">
		<div class="row">
			<div class="small-12 columns">
			<label>Cliente</label>
				<input type="text" class="search-customer" placeholder='Buscar cliente...'>
				<ul class="clients-list">
					
				</ul>
			</div>
		</div>
		
		<div class="row">
			<div class="small-12 columns">
				<label class="left" style="margin-left:1em">
					Factura
					<input type="radio" name="order_type" value="invoice" checked="checked">
				</label>
				<label class="left" style="margin-left:1em">
					Proforma
					<input type="radio" name="order_type" value="proforma">
				</label>
				<label class="left" style="margin-left:1em;">
					Credito <input type="checkbox" name="credit" class="credit">
					
					
					<label style="position:relative; display:none" class="condition-wrapper">
						<input type="number" name="condition" class="condition" style="display:inline-block; width:50%;" >
						<span style="background: #0080a0;color:white;font-weight:bold;padding:8px; position:absolute; right:22px;">Dias</span>
					</label>
				</label>
				<label>
					<a href="#" class="button radius right" data-open="myModal"><i class="fa fa-user"></i> Nuevo Cliente</a>	
				</label>
				
			</div>
		</div>
		
			
		{!! csrf_field() !!}
		<input type="hidden" name="type" value="replacement">
		<div class="row">
			<div class="small-12 columns">
				<table class="responsive">
					<thead>
						<th width="300">CODIGO</th>
						<th width="500">CLIENTE</th>
						<th width="300">CONTACTO</th>
						<th class="address"width="500">DIRECCION</th>
						<th width="300">RTN</th>
						<th width="300">TELEFONO</th>
					</thead>
					<tbody class="customers-body">
						
					</tbody>
					
				</table>
			</div>
			
		</div>

		<div class="row">
			<div class="small-12 columns">
			<label>Codigo de repuesto</label>
				<input autofocus type="text" class="search-replacements" placeholder="Buscar repuesto...">
				<ul class="replacements-list">
					
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<table id="table" class="responsive">
					<thead>
						<th width="100">CANTIDAD</th>
						<th width="100">CODIGO</th>
						<th width="700">DESCRIPCION</th>
						<th width="200">P.UNIDAD</th>
						<th width="200">P.TOTAL</th>
						<th>Eliminar</th>
					</thead>
					<tbody class="replacements-body">
						
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="row">
			<div class="small-12 columns">
				<button type="submit" class="close-order button radius right add-order"><i class="fa fa-list-ul"></i> Guardar orden</button>
					
			</div>
		</div>
		
		</form>
		{{-- Customer Modal --}}
		<div id="myModal" class="full reveal" data-reveal style="z-index:10">
		<br><br>
		<h2>Nuevo Cliente</h2>
		<p>
			<form action="{{ route('newCustomer')}}" method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}

				<div class="row">
					<div class="medium-6 columns col-1">
						<div class="small-12 columns">
							<label for="">
								Nombre de la empresa
								<input  type="text" name="full_name" required>
							</label>
						</div>	
						
						<div class="small-12 columns">
							<label for="">
								RTN
								<input type="text"  onkeypress="return isNumber(event)" maxlength="14" name="RTN">
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Nombre del contacto
								<input type="text" name="contact" >
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Vendedor asignado
							
								<select  autocomplete="off" name="user_id" required="">
									@if (count($users) > 0)
										@foreach ($users as $user)
											<option value="{{$user->id}}">{{$user->name}}</option>
										@endforeach
									@else
										<option value="">No hay ningun vendedor registrado en el sistema</option>
									@endif
									
								</select>
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Departamento
							
								<select onchange="get_cities(this)" autocomplete="off" required="">
									<option selected="selected" disabled="">Selecciona un departamento</option>
									@if (isset($departments))
										@foreach ($departments as $element)
											<option value="{{$element->department_id}}">{{$element->name}}</option>
										@endforeach
									@else
										<option value="">No hay ningun departamento en el sistema</option>
									@endif
									
								</select>
							</label>
						</div>

						@include('customer.cities')
					</div>
					<div class="medium-6 columns col-2">

						<div class="small-12 columns">
							<label for="">
								Tipo de cliente
							
								<select name="customer_type" id="">
									@if (isset($customer_type))
										@foreach ($customer_type as $element)
											<option value="{{$element->customer_type}}">{{$element->description}}</option>
										@endforeach
									@else
										<option value="">No hay ningun tipo de cliente</option>
									@endif
									
								</select>
							</label>
						</div>
						
						<div class="small-12 columns">
							<label for="">
								Telefono Fijo
								<input type="text" onkeypress="return isNumber(event)" maxlength="9" name="telephone" >
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Telefono celular
								<input type="text" onkeypress="return isNumber(event)" maxlength="9" name="phone_number" >
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Telefono celular 2
								<input type="text" name="phone_number2" onkeypress="return isNumber(event)" maxlength="9">
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Correo electronico
								<input type="email" name="email" >
							</label>
						</div>

						<div class="small-12 columns">
							<label for="">
								Direccion
								<textarea name="address" id="" cols="10" rows="2" style="resize:none"></textarea>	
							</label>
						</div>

					</div>
					
				</div>

				<div class="row">
	  		     	<div class="small-12 columns">
	  		     		<button class="right button fa fa-floppy-o" type="submit">  Guardar</button>
	  		     	</div>
	  		     </div>
			</form>
		</p>	
		<button class="close-button" data-close aria-label="Close reveal" type="button">
		    <span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif
	
@stop

@section('script')
	@parent
	<script src="{{ asset('js/responsive-tables.js') }}"></script>
	@if (isset($order))
		<script>
		var $update_total = 0.0;

		jQuery(document).ready(function($) {
			
			$('.save-changes').on('click',  function(event) {

				var $input = '<input type="hidden" name="save_changes" value="1">';
				$('form').prepend($input);
			});
		});
		function check_item(event){
			// Ejecutar cuando es ENTER

			if(event.which == 13){
				var replacement_code = $('#replacement_code').val();
				var url = "{{route('getReplacementOrder', $order->order_id)}}" + '/' + replacement_code;
				
				$.get(url, function(data) {
					/*optional stuff to do after success */
					
					if(data.error){
						sweetAlert("Algo salio mal", data.error, "error");	
					}
					else{
						var description = data.description;
						var quantity = data.quantity;

						swal({
						  title: "Cantidad en la orden: " + quantity,
						  text: "Producto: " + description,
						  type: "input",
						  showCancelButton: true,
						  closeOnConfirm: false,
						  animation: "slide-from-top",
						  inputPlaceholder: "Cantidad"
						},
						function(quantity){
							
						  if (quantity === false) return false;
						  
						  if (quantity === "" || isNaN(parseInt(quantity))) {
						    swal.showInputError("Ingrese una cantidad valida");
						    return false
						  }
						  
						  $.post("{{route('changeItemStatus')}}", {
						  	replacement_code: replacement_code,
						  	replacement_quantity: quantity,
						  	order_id: $('.order_id').val(), 
						  	_token: $('meta[name=_token]').attr('content')
						  }, function(data, textStatus, xhr) {
						  	
						  	if(data.error)
						  	{
						  		sweetAlert("Algo salio mal", data.error, "error");	
						  	}
						  	else{
						  		sweetAlert("Completado", "Producto chequeado correctamente", "success");	
						  		$('.items').html(data);
						  	}
						  	window.location.reload();
						  });
						});
					}
				});
			}
			
		}
		</script>
	@endif
	<script>
	var $sum_total =  0.0;
	var products;

	$(document).ready(function() {

		$('.add-order').on('click',  function(event) {
			var $input = '<input type="hidden" name="add_order" value="1">';
			$('form').prepend($input);
		});

		$('.search-replacements').keyup(function(event) {
			if (event.keyCode == 13) {
				$('.replacements-list').css('display', 'block');

				var $text = $(this).val() ? $(this).val() : 'blank';
				var url = "../repuestos/buscar/" + $text;
				
				$.ajax({
					url: url ,
					type: 'GET',
					dataType: 'json'
				})
				.done( function(data) {
					
					if(data.redirect)
					{	
						$('.replacements-list').css("display", 'none'); 
					}
					else if(data.error)
					{
						$('.replacements-list').html("<span style='padding:.5em; color: gray'>" + data.error + "</span>"); 
					}
					else {

						
						products = data;
						
						$('.replacements-list').html('');
						$.each(products, function(index, val) {
							var $markup = "<li id='" + val.replacement_id + "' onclick='create_row(this)' name='" + val.quantity + "'>" +
						"<span class='code'>" + val.code + "</span>-<span class='description'>" + val.description + "</span><span class='right quantity'>existencias: " + val.quantity +   "</span></li>"
							$('.replacements-list').append($markup);	 
						})
						
					}
					

				})

			} 
			

		});



		$('.search-customer').keyup(function(event) {
				var text = $(this).val() ? $(this).val() : 'blank';

				$.get('../clientes/buscar/' + text, function(data) {
					
					if (data) {
						
						if(data.redirect)
						{
							window.location.href = data.redirect;
						}

						$('.customers-body').html('');
						
						$.each(data, function(index, val) {

							var markup = "<tr>" +
								"<td class= 'customer_id'>" + val.customer_id + "</td>" +
					  			"<td><input type ='hidden' name='customer' value='" + val.customer_id + "' >" + val.full_name + "</td>" +
					  		  	"<td>" + val.contact + "</td>" +
					  		  	"<td class='address'>" + val.address + "</td>" +
					  		  	"<td>" + val.RTN + "</td>" +
					  		  	"<td>" + val.phone_number + "</td>" +
					  		"</tr>";
					  		$('.customers-body').append(markup);
						})
					}

				});

			});

	});
		function get_cities(element){
			$.ajax({
				url: "../departments/" + $(element).val(),
				type: 'GET',
			})
			.done(function(data) {
				$('#cities').html(data);
			})
			.fail(function(error) {
				console.log('error');
			});
		}


		function create_row (element) 
		{
			var customer_id = $('.customers-body .customer_id').html();
			
			if(customer_id != null) 
			{
				/* Modificar orden */
				if($('.order_id').val())
				{
					console.log('Modificar orden');
					var $price;
					var stock = $(element).attr('name');
					var $quantity = prompt("Existencias: " + stock + "\nCantidad:");
					if (!parseInt($quantity)) {
						return sweetAlert("Algo salio mal", "La cantidad no es valida", "error");	
					}

					$.ajax({
						url: "{{route('addProductToOrder')}}",
						type: 'POST',
						data: {
							'replacement_id': $(element).attr('id'), 
							'order_id': $('.order_id').val(), 
							'quantity': $quantity,
							'_token': $('meta[name=_token]').attr('content')},
					})
					.done(function(result) {
						
						if(result.error)
							sweetAlert("Algo salio mal", result.error, "error");
						else
							$('.items').html(result);
					})
					.fail(function() {
						console.log("error");
					});
					
				}
				/* Nueva orden */
				else
				{
					console.log('Nueva orden');
					var $exists = false
						if (localStorage.getItem('replacements')){
							var $replacements = JSON.parse(localStorage.getItem('replacements'));
							$.each($replacements, function(index, val) {
								 if (val.replacement_id == $(element).attr('id')) {
								 	$exists = true
								 	return false;
								 } 
							});
						}
						if (!$exists) {
							var $price;
							var stock = $(element).attr('name');
							
							var $quantity = prompt("Existencias: " + stock + "\nCantidad:");
							if (!parseInt($quantity)) {
								return sweetAlert("Algo salio mal", "La cantidad no es valida", "error");
							}
							
							$.each(products, function(index, item){
								
								if(item.replacement_id == $(element).attr('id')){

									if(item.quantity >= $quantity)
									{
										var $replacement = {
											replacement_id : parseInt($(element).attr('id')),
											description: $(element).children('span.description').html(),
											quantity: parseInt($quantity),
											code: $(element).children('span.code').html()
											
										}

										console.log($replacement);

										$.ajax({
											url: '../repuesto/precio/'+$replacement.replacement_id + '/' + customer_id,
											type: 'GET'
										})
										.done(function(price) {

											total = (parseInt($quantity) * parseFloat(price));

											$replacement.price = parseFloat(price);

											$markup = '<tr id="' +  $replacement.replacement_id + '">'+
											'<td><input type="number" name="replacements[' + $replacement.replacement_id + '][quantity]" onchange="change_quantity(this)" value="' + $replacement.quantity + '" />' + 
											'<td class="code">' + $replacement.code + '</td>' +
											'</td>'+
											'<td><input type="hidden" name="replacements[' + $replacement.replacement_id + '][replacement_id]" value="' + $replacement.replacement_id + '">' + $replacement.description + '</td>' +
											'<td>' + price + '</td>' +
											'<td class:"sub-total ">' + total + '</td>' +
											"<td><span class='button alert radius' onclick='remove_product(this)' value='" + $replacement.replacement_id + "' ><i class='fa fa-remove'></i></span></td>"
											'</tr>'

											$('.replacements-body').append($markup)

											if(!localStorage.getItem('replacements'))
											{
												var $replacements = [];
												$replacements.push($replacement);
												localStorage.setItem('replacements', JSON.stringify($replacements));
											}
											else
											{
												var $replacements = JSON.parse(localStorage.getItem('replacements'))
												$replacements.push($replacement);
												localStorage.setItem('replacements', JSON.stringify($replacements));	
											}

											update_total(total);		
										});
									}
									else
									{
										sweetAlert("Algo salio mal", "No hay suficientes existencias de este producto", "error");	
									}
								}
							});
							
						} 
						else{
							sweetAlert("Algo salio mal", "Este producto ya se encuentra en la orden", "error");	
						}
						
				}

			}
			else
			{
				sweetAlert("Algo salio mal", "Debes seleccionar un cliente", "error");	
				
			}

			$(element).parent().hide()
			$('.search-replacements').val('')
			
		}	
			
		function update_total (subtotal) {
			
			$sum_total += subtotal;
			
			$('.total').html($sum_total)
		}
		function change_quantity (element) {

			if($('.order_id').val())
			{
				var $replacement_id = $(element).parent().parent().attr('id');
				if($(element).val() != '')
				{
					$.ajax({
							url: "{{route('updateOrderProduct')}}",
							type: 'POST',
							data: {
								'replacement_id': $replacement_id, 
								'order_id': $('.order_id').val(), 
								'quantity': $(element).val(),
								'_token': $('meta[name=_token]').attr('content')},
						})
						.done(function(result) {
							
							if(result.error)
							{
								sweetAlert("Oops...", result.error, "error");
								
							}
							else
							{
								$('.items').html(result);
							}
							
						})
						.fail(function(err) {
							console.log(err.responseText);
						});
						return false;
				}
			}
			else
			{
				var $replacement_id = $(element).parent().parent().attr('id');
					var $replacements = JSON.parse(localStorage.getItem('replacements'))
					$.each($replacements, function(index, val) {
						 if (val.replacement_id == $replacement_id) {
						 	val.quantity = $(element).val()
						 	var $price = parseFloat($($(element).parent().parent()[0].cells[3]).html())
						 	$($(element).parent().parent()[0].cells[4]).html($price*val.quantity)
						 	localStorage.setItem('replacements', JSON.stringify($replacements));	
						 	return false;
						 }
					})
					var $total = 0.0;
					$.each($replacements, function(index, val) {

						 $total += val.price*val.quantity;
					});
					$sum_total = $total;
					$('.total').html($total);
			}
			
			
		}

		function remove_product (element) {
			
			if(localStorage.getItem('replacements'))
			{
				var $replacement_id = $(element).attr('value');
				console.log($replacement_id);
				var $replacements = JSON.parse(localStorage.getItem('replacements'))
				
				if ($replacements.length <= 1) {
					localStorage.removeItem('replacements')
				} else{
					$.each($replacements, function(index, val) {
						
						 if (val.replacement_id == $replacement_id) {
						 	$replacements.splice(index, 1)
						 	return false;
						 } 
					})
					localStorage.setItem('replacements', JSON.stringify($replacements));
				}
				
				
				$(element).parent().parent().remove()
				recalculate();
			}

		}

		function remove_product_from_order(element){
			swal({ title: "Contraseña de administrador",  type: "input", inputType: 'password', showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   inputPlaceholder: "Contraseña" }, function (inputValue){
				if (inputValue === false) return false;      
				if (inputValue === "") {     
					swal.showInputError("Escribe una contraseña");     
					return false   
				}    
				
				$.post("{{route('checkAdminPassword')}}",
				{
					'password': inputValue, 
					'_token': $('meta[name=_token]').attr('content')
				})
				.success(function(data) {
					
					
					if(data.id)
					{
						$.ajax({
							url: "{{route('removeOrderProduct')}}",
							type: 'DELETE',
							data: {
								'replacement_order': $(element).attr('value'), 
								'order_id': $('.order_id').val(), 
								'_token': $('meta[name=_token]').attr('content')},
						})
						.done(function(result) {
							swal("Eliminado", "El producto ha sido elimiado correctamente");
							$('.items').html(result);
						})
						.fail(function() {
							console.log("error");
						});
						
					}
					else
					{

						swal.showInputError("Contraseña incorrecta");     
					}
				})
				.fail(function() {
					console.log("error");
				});
			
			});
			
		}

		function recalculate () {
			var $total = 0.0;
			var $replacements = JSON.parse(localStorage.getItem('replacements'));
			if(localStorage.getItem('replacements')){
				$.each($replacements, function(index, val) {
					 $total += val.price*val.quantity;
				})
				
				$sum_total = $total;
				$('.total').html($total);
			}	
			else
			{
				$('.total').html(0.0);
			}
			
		}
		

		/* Credit order */
		$('.credit').on('change',  function(event) {
			var $isChecked = $(this).is(':checked');
			if ($isChecked) {
				$('.close-order').html(' Guardar credito');
				$('#order').attr('action',"{!! route('saveOrder') !!}");
				$('.condition-wrapper').show();

			} else{
				$('.close-order').html('Guardar orden');
				$('#order').attr('action', "{!! route('saveOrder') !!}");
				$('.condition-wrapper').hide();
			}


		});
	    d = new Date()
	    $('.date').html('Fecha: ' + d.toLocaleDateString());

	</script>
@stop

