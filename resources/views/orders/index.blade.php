@extends('layouts.master')



@section('content')

@if(session('error'))
	<p  class="text-center" style="color:red">{{session('error')}}</p>
@endif

@if(session('message'))
	<p  class="text-center" style="color:green">{{session('message')}}</p>
@endif

@if(Auth::user()->type->description !== 'bodega')
<div class="row">
	<div class="small-12 columns">
			@if(Auth::user()->type->description === 'admin' && isset($orders))
			<a class="success button" href="{{route('exportOrders')}}" ><i class="fa fa-file-excel-o"></i> Exportar ordenes</a>
			@endif
			<a href="{{ route('replacementOrder') }}" class="button radius right"><i class="fa fa-file-text"></i> Nueva orden</a>	
	</div>
</div>
@endif

@if(isset($orders) && Auth::user()->type->description === 'admin')
	<div class="row">
		<div class="small-12 columns">

			<select name="orderType" id="orderType" style="float: left;width: 20%" >
				@if(Auth::user()->type->description !== 'bodega')
				<option value="all">Todas</option>
				@endif
				<option value="open">Abiertas</option>
				@if(Auth::user()->type->description !== 'bodega')
				<option value="close">Cerradas</option>
				@endif
			</select>
			<input type="text" name="searchOrders" id="hint" placeholder="Buscar ordenes.." style="float:left;width: 60%">
			<button id="search" class="button" style="width: 19%;padding: .5em 1em; height: 37px"><i class="fa fa-search"></i> Buscar</button>	
		</div>
	</div>
@endif

@if (!empty($orders))
<div class="row">
	<div class="small-12 columns orders table-scroll">
		@include('orders.orders')
	</div>
</div>
{{-- Pagination --}}
<center>{!! str_replace('/?', '?',$orders->render(new LaravelFoundation\Pagination\SimpleFoundationFivePresenter($orders, ["pagination-centered"=>'true']))) !!}</center>
@else
	<div class="row">
		<div class="small-12 columns">
			<h4 class="panel text-center">No hay ordenes</h4>
		</div>
	</div>
@endif


@stop

@section('script')
	@parent
	<script>
		jQuery(document).ready(function($) {
			$('#orderType').on('change', searchOrders);
			$('#search').on('click', searchOrders);
		});

		function searchOrders()
		{
			var value = $('#orderType').val();

			$.post( "{{route('filterOrders') }}",
			{
				filter: value, 
				hint: $('#hint').val(),
				'_token': $('meta[name=_token]').attr('content'),
			})
			.success(function (result) {
				console.log(result);
				if(result){
					$('.orders').html(result);
				}
				else
				{
					$('.orders').html("<h4>No se encontraron resultados</h4>");	
				}
			})
			.fail(function(e) {
				console.log('error');
			});
		}

		function check_admin_password(element)
		{	
			swal({ title: "Contraseña de administrador",  type: "input", inputType: 'password',  showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   inputPlaceholder: "Contraseña" }, function (inputValue){
				if (inputValue === false) return false;      
				if (inputValue === "") {     
					swal.showInputError("Escribe una contraseña");     
					return false   
				}    
				
				$.post("{{route('checkAdminPassword')}}",
				{
					'password': inputValue, 
					'_token': $('meta[name=_token]').attr('content')
				})
				.success(function(data) {
					if(data.id)
					{
						swal("Eliminada", "La orden ha sido eliminada correctamente");
						window.location.replace($(element).attr('id'));
						
					}
					else
					{

						swal.showInputError("Contraseña incorrecta");     
					}
				})
				.fail(function() {
					console.log("error");
				});
			});
		}

	</script>
@stop
