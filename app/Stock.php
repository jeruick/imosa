<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stock';

    protected $primaryKey = 'stock_id';

    public $timestamps = false;

    public function product()
    {
    	return $this->hasOne('App\Product', 'product_id');
    }

}
