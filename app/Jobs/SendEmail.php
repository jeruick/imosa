<?php

namespace App\Jobs;

use App\Jobs\Job;
use Mail;
use PDF;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $result = [];
        
        Mail::queue('emails.invoice', $result, function($message) use($data)
        {
            $pdf = PDF::loadView('emails.proforma', $data)->setPaper('Letter');
            $message->from("ventas@imosa.hn", "Ventas Imosa");
            $message->to($data['order']->customer->email)->subject('ORDEN DE COMPRA');
            $message->cc("ventas@imosa.hn");

            $message->attachData($pdf->output(), "orden de compra.pdf");
        }); 
    }
}
