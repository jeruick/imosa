<?php

namespace App\Jobs;

use App\Jobs\Job;
use PDF;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExportData extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $replacements;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($replacements)
    {
        $this->replacements = $replacements;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $replacements = $this->replacements;

        $pdf = PDF::loadView('pdf.replacements', ['replacements' => $replacements, 'price' => 'none'])->setPaper('Letter', 'landscape');
        
        return $pdf;
    }
}
