<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => 'auth'], function (){
	Route::get('/', ['as' => 'home' ,'uses' => 'HomeController@index']);
	Route::get('/inventario', ['as' => 'inventory' ,'uses' => 'InventoryController@replacements']);

	Route::post('/nuevo', ['as' => 'newUser' ,'uses' => 'HomeController@newUser']);

	/*constancias*/
	Route::group([], function(){
	Route::get('/constancias', ['as' => 'constancies' ,'uses' => 'ConstanciesController@index']);

	Route::post('/constancias/recibo', ['as' => 'savevaucher' ,'uses' => 'ConstanciesController@saveVaucher']);

	Route::get('/constancias/recibo', ['as' => 'vaucher' ,'uses' => 'ConstanciesController@createVaucher']);
	Route::get('/constancias/constancia', ['as' => 'constancy' ,'uses' => 'ConstanciesController@createConstancy']);
	Route::get('/constancias/numero/{number}', ['as' => 'number' ,'uses' => 'ConstanciesController@number']);
	});

	/*Replacements*/
	Route::group([], function(){
		Route::get('/repuestos', ['as' => 'replacements' ,'uses' => 'InventoryController@replacements']);
		Route::post('/repuestos', ['as' => 'replacements' ,'uses' => 'InventoryController@newReplacement']);
		Route::post('/repuestos/total', ['as' => 'totalReplacements' ,'uses' => 'InventoryController@totalProducts']);
		Route::get('/repuestos/buscar/{text}', ['as' => 'searchReplacements' ,'uses' => 'InventoryController@searchReplacements']);
		Route::get('/repuestos/{id}', ['as' => 'replacement' ,'uses' => 'InventoryController@getReplacement']);
		Route::get('/repuesto/buscar/{text}', ['as' => 'searchReplacement' ,'uses' => 'InventoryController@searchReplacement']);
		Route::get('/repuesto/eliminar/{id}', ['as' => 'deleteReplacement' ,'uses' => 'InventoryController@deleteReplacement']);
		Route::post('/repuesto/{id}', ['as' => 'updateReplacement' ,'uses' => 'InventoryController@updateReplacement']);
		Route::get('/repuesto/precio/{id}/{customer}', ['as' => 'replacements_price', 'uses' => 'InventoryController@getReplacementPrice']);

	});


	/*Providers*/
	Route::group([], function(){
		Route::get('/proveedores', ['as' => 'providers' ,'uses' => 'ProviderController@index']);
		Route::post('/proveedor', ['as' => 'newProvider' ,'uses' => 'ProviderController@newProvider']);
		Route::get('/proveedor/{id}', ['as' => 'provider' ,'uses' => 'ProviderController@getProvider']);
		Route::get('/proveedor/eliminar/{id}', ['as' => 'deleteProvider' ,'uses' => 'ProviderController@deleteProvider']);
		Route::post('/proveedor/{id}', ['as' => 'updateProvider' ,'uses' => 'ProviderController@updateProvider']);
		Route::get('/proveedor/{id}/productos/', ['as' => 'providerProducts' ,'uses' => 'ProviderController@providerProducts']);
	});
	

	Route::post('/modelos/buscar', ['as' => 'searchModels' ,'uses' => 'ModelController@searchModels']);
	Route::post('/modelo', ['as' => 'newModel' ,'uses' => 'ModelController@newModel']);
	Route::get('/modelos/marca/{id}', ['as' => 'getModels' ,'uses' => 'BrandController@getModels']);


	/*Credits*/
	Route::group([], function(){
		Route::get('/creditos/cliente/{id}', ['as' => 'getCredits' ,'uses' => 'CreditController@getCredits']);
		Route::post('/creditos', ['as' => 'newCredit' ,'uses' => 'CreditController@newCredit']);
		Route::get('/clientes/creditos', ['as' => 'getCustomerCredits' ,'uses' => 'CreditController@getCustomerCredits']);
		Route::get('/credito/{id}', ['as' => 'getCredit' ,'uses' => 'CreditController@getCredit']);

		Route::post('actualizar/credito/{id}', ['as' => 'updateCredit' ,'uses' => 'CreditController@updateCredit']);

		Route::get('/creditos/eliminar/{id}', ['as' => 'deleteCredit' ,'uses' => 'CreditController@deleteCredit']);
		Route::post('/abonos/{id}', ['as' => 'payCredit' ,'uses' => 'CreditController@payCredit']);
		Route::get('/credito/{id}/abonos', ['as' => 'historyCredit' ,'uses' => 'CreditController@historyCredit']);	
		Route::get('/credito/{id}/historial', ['as' => 'paymentHistory' ,'uses' => 'CreditController@paymentHistory']);	
		
	});
	
	/* Orders */
	Route::group([], function(){
		Route::get('/ordenes', ['as' => 'orders', 'uses' => 'OrderController@index']);
		Route::get('/ordenes/repuestos', ['as' => 'replacementOrder', 'uses' => 'OrderController@replacementOrder']);
		Route::get('/ordenes/{id}', ['as' => 'getOrder', 'uses' => 'OrderController@getOrder']);
		
		Route::get('/ordenes/imprimir/{id}', ['as' => 'printOrder', 'uses' => 'OrderController@printOrder']);
		Route::get('/ordenes/imprimir-proforma/{id}', ['as' => 'printProforma', 'uses' => 'OrderController@printProforma']);
		Route::get('/ordenes/eliminar/{id}', ['as' => 'deleteOrder', 'uses' => 'OrderController@deleteOrder']);
		Route::post('/orden', ['as' => 'saveOrder', 'uses' => 'OrderController@saveOrder']);
		
		Route::post('/orden/credito', ['as' => 'saveOrder', 'uses' => 'OrderController@saveOrder']);
		Route::post('/orden/{id}', ['as' => 'updateOrder', 'uses' => 'OrderController@updateOrder']);

		Route::post('/ordenes/filtradas', ['as' => 'filterOrders', 'uses' => 'OrderController@filterOrders']);

		Route::delete('/ordenes/replacement/delete', ['as' => 'removeOrderProduct', 'uses' => 'OrderController@removeOrderProduct']);

		Route::post('/ordenes/replacement/add', ['as' => 'addProductToOrder', 'uses' => 'OrderController@addProductToOrder']);

		Route::post('/ordenes/replacement/update', ['as' => 'updateOrderProduct', 'uses' => 'OrderController@updateOrderProduct']);

		Route::post('/ordenes/replacement/change-item-status', ['as' => 'changeItemStatus', 'uses' => 'OrderController@changeItemStatus']);

		Route::get('/ordenes/enviar-correo/{id}', ['as' => 'sendEmail', 'uses' => 'OrderController@sendEmail']);

		Route::get('/orden/{order}/repuesto/{code}', ['as' => 'getReplacementOrder', 'uses' => 'OrderController@getReplacementOrder']);		


	});

	/*Customers*/
	Route::group([] ,function(){
		Route::get('/clientes/buscar/{text}', ['as' => 'searchCustomer', 'uses' => 'CustomerController@searchCustomer']);
		Route::get('/clientes', ['as' => 'customers', 'uses' => 'CustomerController@getCustomers']);
		Route::get('/cliente/eliminar/{id}', ['as' => 'deleteCustomer', 'uses' => 'CustomerController@deleteCustomer']);
		Route::get('/cliente/{id}', ['as' => 'customer', 'uses' => 'CustomerController@getCustomer']);
		Route::post('/cliente', ['as' => 'newCustomer', 'uses' => 'CustomerController@newCustomer']);
		Route::post('/cliente/{id}', ['as' => 'updateCustomer', 'uses' => 'CustomerController@updateCustomer']);
		Route::get('/departments/{id}', ['as' => 'getCities', 'uses' => 'CustomerController@getCities']);
	});
	/*Usuarios*/
	Route::group(['middleware' => 'active'], function(){
		Route::get('/usuarios', ['as' => 'users', 'uses' => 'UserController@index']);
		Route::post('/usuario', ['as' => 'statusUser', 'uses' => 'UserController@changeStatus']);
		Route::post('/tipo', ['as' => 'changeType', 'uses' => 'UserController@changeType']);
		Route::get('/usuario/eliminar/{id}', ['as' => 'deleteUser', 'uses' => 'UserController@deleteUser']);
		Route::post('/check-password', ['as' => 'checkAdminPassword', 'uses' => 'UserController@checkAdminPassword']);


		Route::get('/load-notifications', ['as' => 'loadNotifications', 'uses' => 'UserController@loadNotifications']);
	});

	/*Cotizaciones*/
	Route::group([], function(){
		Route::get('/cotizaciones', ['as' => 'cotizacions' , 'uses' => 'CotizacionController@index']);
		Route::get('/cotizaciones/clientes', ['as' => 'cotizacionsCustomers' ,'uses' => 'CotizacionController@getCustomers']);
		Route::get('/cotizacions/repuesto', ['as' => 'cotizacion_replacements', 'uses' => 'CotizacionController@newCotizacion_replacement']);		
	});

	/*Configuraciones*/
	Route::group([], function(){

		Route::get('/configuraciones', ['as' => 'configurations' , 'uses' => 'ConfigurationController@configurations']);
		
		Route::get('/bodegas', ['as' => 'cellars', 'uses' => 'ConfigurationController@getCellar']);

		Route::get('/marcas', ['as' => 'brands' , 'uses' => 'ConfigurationController@getBrands']);

		
		Route::get('/empresa', ['as' => 'company' , 'uses' => 'ConfigurationController@getCompany']);


		Route::get('/dei', ['as' => 'companyinvoice', 'uses' => 'ConfigurationController@getCompanyInvoice']);
	});




	/*Marcas*/
	Route::group([], function(){
		Route::get('/marca/eliminar/{id}', ['as' => 'deletebrand', 'uses' => 'BrandController@deleteBrand']);

		Route::post('/marca', ['as' => 'newBrand' ,'uses' => 'BrandController@newBrand']);
	});



	/*Bodegas*/
	Route::group([], function(){
		
		Route::get('/bodega/eliminar/{id}', ['as' => 'deleteCellar', 'uses' => 'CellarController@deleteCellar']);

		Route::post('/bodega', ['as' => 'newCellar', 'uses'=> 'CellarController@newCellar']);
	});

	/*Company*/
	Route::group([], function(){
		Route::post('/empresas/{id}', ['as' => 'updateCompany' , 'uses' => 'CompanyController@updateCompany']);

		Route::post('/empresa/crear', ['as' => 'newCompany' , 'uses' => 'CompanyController@newCompany']);
	});
	
	/*Company Ivoice*/
	Route::group([], function(){

		Route::post('/empresa/factura/editar/{id}', ['as' => 'updateCompanyInvoice', 'uses' => 'CompanyController@updateCompanyInvoice']);

		Route::post('/empresa/factura/nueva', ['as' => 'newCompanyInvoice', 'uses' => 'CompanyController@newCompanyInvoice']);

	});

	//Exportar a pdf y excel
	Route::group([], function (){
		Route::get('/exportar/productos/mayorista', ['as' => 'exportWholesaler', 'uses' => 'PdfController@exportWholesaler']);
		Route::get('/exportar/productos/ruta', ['as' => 'exportRoute', 'uses' => 'PdfController@exportRoute']);
		Route::get('/exportar/productos/agotados', ['as' => 'exportStockout', 'uses' => 'PdfController@exportStockout']);

		Route::get('/exportar/ordenes', ['as' => 'exportOrders', 'uses' => 'OrderController@exportOrders']);
		Route::get('/exportar/orden/{id}', ['as' => 'exportOrder', 'uses' => 'OrderController@exportOrder']);
		Route::get('/exportar/inventario', ['as' => 'exportInventory', 'uses'=> 'InventoryController@exportInventory']);
		
		Route::get('exportar/productos-agotados/{id}', ['as' => 'exportProducts', 'uses'=> 'ProviderController@exportProducts']);
	});


});


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

