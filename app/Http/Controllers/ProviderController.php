<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Replacement;
use App\Provider;
use App\Product;
use Excel;

class ProviderController extends Controller
{
    public function index()
    {
    	$providers = Provider::all();
    	return view('provider.index', ['providers' => $providers ,'provider_active' => true]);
    }

    public function newProvider(Request $request)
    {
    	$provider = new Provider();
    	$provider->name = $request->input('name');
    	$provider->country = $request->input('country');
        $provider->country = $request->input('company');

    	if ($provider->save()) 
    	{
    		return redirect()->back();
    	}
    	else
    	{
    		return redirect()->back()->withErrors(['error' => 'No se pudo guardar el proveedor']);
    	}
    }

    public function deleteProvider($id)
    {
    	Provider::destroy($id);
    	return redirect()->back();
    }

    public function getProvider($id)
    {
    	$provider = Provider::find($id);
        return $provider;
    }

    public function updateProvider(Request $request,$id)
    {
    	$provider = Provider::find($id);
    	$provider->name = $request->input('name');
    	$provider->country = $request->input('country');

    	if ($provider->save()) 
    	{
    		return redirect()->back();
    	}
    	else
    	{
    		return redirect()->back()->withErrors(['error' => 'No se pudo guardar el proveedor']);
    	}
    }

    public function providerProducts($id)
    {
        $provider = Provider::find($id);
        $replacements = Replacement::where('provider_id', $id)->where('quantity', '<=', 5)->get();
        
        return view('provider.products', ['provider' => $provider, 'replacements' => $replacements,'provider_active' => true ]);
    }

    public function exportProducts($id)
    {
        Excel::create('Productos_Agotados', function($excel) use ($id){

            $excel->sheet('New sheet', function($sheet) use ($id) {
                $replacements = Replacement::where('provider_id', $id)->where('quantity', '<=', 5)->get();
                $sheet->loadView('excel.products', ['replacements' => $replacements]);

            });

        })->download('xls');
        
    }


    
}
