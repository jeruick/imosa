<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MotorcycleModel;
use App\Brand;


class ModelController extends Controller
{
   public function searchModels(Request $request)
   {
        $text = $request->input('text');
        $models = MotorcycleModel::where('description', 'LIKE', "%$text%")->get();
        return response()->json($models);
   }

   public function newModel(Request $request)
   {
      $brand = Brand::find($request->input('brand'));
      $model = new MotorcycleModel();
      $model->description = $request->input('model');
      $model->brand_id = $brand->brand_id;
      if ($model->save()) 
      {
        return $model;
      }

   }
}
