<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PdfController extends Controller
{
     public function exportWholesaler()
     {
        $replacements = \App\Replacement::where('quantity', '>', 0)->orderBy('provider_id')->get();
        
        return \PDF::loadView('pdf.replacements', [ 'replacements' => $replacements, 'price' => 'wholesale_price'])->download('LISTA DE PRECIOS IMOSA - MAYOREO.pdf');
     }

     public function exportRoute()
     {
		$replacements = \App\Replacement::where('quantity', '>', 0)->orderBy('provider_id')->get();
        
        return \PDF::loadView('pdf.replacements', [ 'replacements' => $replacements, 'price' => 'route_price'])->download('LISTA DE PRECIOS IMOSA - MAYOREO RUTA.pdf');
     }

     public function exportStockout()
 	{
 		$replacements = \App\Replacement::where('quantity', 0)->orderBy('provider_id')->get();
 	    
 	    return \PDF::loadView('pdf.replacements', [ 'replacements' => $replacements, 'price' => 'none'])->download('LISTA DE PRODUCTOS SIN EXISTENCIA IMOSA.pdf');
     }
}
