<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Brand;


class BrandController extends Controller
{
    public function newBrand(Request $request)
   {
     $brand = new Brand();
     return $this->saveBrand($brand, $request);
   }

   public function getModels($id)
   {
   		$brand = Brand::find($id);
   		return $brand->models;
   }

   public function saveBrand($brand, $request)
   {
      $rules = [
      'description' => 'required|unique:brands'
      ];

      $validator = Validator::make($request->all(), $rules);
      if($validator->fails()) {

          return redirect()->back()->withErrors($validator->errors());
      }


      $brand->description = $request->input('description');
      
      $brand->save();
      return redirect()->back();      
   }

    public function deleteBrand($id)
    {
        Brand::destroy($id);
        return redirect()->back();

    }

}