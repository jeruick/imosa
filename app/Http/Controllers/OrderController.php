<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Company;
use App\CompanyInvoice;
use App\Credit;
use App\CreditReplacement;
use App\Customer;
use App\CustomerType;
use App\Invoice;
use App\Jobs\SendEmail;
use App\Motorcycle;
use App\Order;
use App\Proforma;
use App\Replacement;
use App\Department;
use App\ReplacementOrder;
use App\User;
use Auth;
use DB;
use Excel;
use Mail;
use PDF;
use Session;
use Validator;

class OrderController extends Controller
{
    public function index()
    {
        if(Auth::user()->type->description == 'admin')
        {
            $orders = Order::orderBy('date', 'desc')->simplePaginate(5);
        }
        elseif(Auth::user()->type->description == 'vendedor')
        {
            $orders = Order::where('user_id', Auth::user()->id)->orderBy('date', 'desc')->simplePaginate(5);
        }
        elseif (Auth::user()->type->description == 'bodega') 
        {
            $orders = Order::where('active', 1)->orderBy('date', 'desc')->simplePaginate(5);
        }
        
        
    	return view('orders.index', ['orders' => $orders]);
    }

    public function getOrder($id)
    {
        $order = Order::find($id);
        
        
        return view('orders.update', ['order' => $order, 'is_ready' => $this->isOrderReady($order)]);
        
    }

    public function isOrderReady($order)
    {
        $is_ready = true;
        foreach ($order->replacements as $key => $value) 
        {
            if(!$value->status)
            {
                $is_ready = false;
                break;

            }
        }
        return $is_ready;
    }
    public function filterOrders(Request $request)
    {
        $orders = [];
        $hint = $request->hint;

        if($request->filter == 'open' || $request->filter == 'close')
        { 
            $active = ($request->filter == 'open') ? 1 : 0;

            $result = $this->getFilteredOrders($active, $hint);
            
            foreach ($result as $item) 
            {
                $order = (array)$item;
                array_push($orders, Order::find($order['order_id']));
            }
        }
        else
        { 

            $result = $this->getFilteredOrdersOnlyByHint($hint);

            foreach ($result as $item) 
            {
                $order = (array)$item;
                array_push($orders, Order::find($order['order_id']));
            }
        }

        return view('orders.orders', ['orders' => $orders]);
        
    }

    public function getFilteredOrdersOnlyByHint($hint)
    {
        
        if(Auth::user()->type->description == 'admin')
        {
            $response = DB::table('orders')
                            ->join('customers', 'customers.customer_id', '=', 'orders.customer_id')
                            ->select('orders.*')
                            ->where(function($query) use ($hint){
                                    $query->where('customers.full_name', 'LIKE', "%$hint%")
                                          ->orWhere('customers.RTN', "$hint")
                                          ->orWhere('customers.customer_id', "$hint");
                            })
                            ->orderBy('date', 'desc')
                            ->groupBy('order_id')
                            ->get();    
        }
        elseif(Auth::user()->type->description == 'vendedor')
        {
            $response = DB::table('orders')
                            ->join('customers', 'customers.customer_id', '=', 'orders.customer_id')
                            ->select('orders.*')
                            ->where('orders.user_id', Auth::user()->id)
                            ->where(function($query) use ($hint){
                                    $query->where('customers.full_name', 'LIKE', "%$hint%")
                                          ->orWhere('customers.RTN', "$hint")
                                          ->orWhere('customers.customer_id', "$hint");
                            })
                            ->orderBy('date', 'desc')
                            ->groupBy('order_id')
                            ->get();
        }
        elseif(Auth::user()->type->description == 'bodega')
        {   

            $response = DB::table('orders')
                            ->select('orders.*')
                            ->where('orders.active', 1)
                            ->where(function($query) use ($hint){
                                    $query->where('customers.full_name', 'LIKE', "%$hint%")
                                          ->orWhere('customers.RTN', "$hint")
                                          ->orWhere('customers.customer_id', "$hint");
                            })
                            ->orderBy('date', 'desc')
                            ->groupBy('order_id')
                            ->get();
        }

        return $response;
        
    }

    public function getFilteredOrders($active, $hint)
    {

        if(Auth::user()->type->description === 'vendedor')
        {
            $result = DB::table('orders')
                ->join('customers', 'customers.customer_id', '=', 'orders.customer_id')
                ->join('users', 'users.id', '=', 'orders.user_id')
                ->join('users_type', 'users.user_type', '=', 'users_type.user_type')
                ->select('orders.*')
                ->where('orders.active', $active)
                ->where('users.id', Auth::user()->id)
                ->where('users_type.description', 'vendedor')
                ->where(function($query) use ($hint){
                        $query->where('customers.full_name', 'LIKE', "%$hint%")
                              ->orWhere('customers.RTN', "$hint")
                              ->orWhere('customers.customer_id', "$hint");
                })
                ->orderBy('date', 'desc')
                ->groupBy('order_id')
                ->get();

        }
        else
        {
            $result = DB::table('orders')
                        ->join('customers', 'customers.customer_id', '=', 'orders.customer_id')
                        ->select('orders.*')
                        ->where('orders.active', $active)
                        ->where(function($query) use ($hint){
                                $query->where('customers.full_name', 'LIKE', "%$hint%")
                                      ->orWhere('customers.RTN', "$hint")
                                      ->orWhere('customers.customer_id', "$hint");
                        })
                        ->orderBy('date', 'desc')
                        ->groupBy('order_id')
                        ->get();             
        }
        
        return $result;
        
    }

    public function replacementOrder()
    {
        $customer_type = CustomerType::all();
        $users = $this->getVendorUsers();
        $departments = Department::all();
        return view('orders.update', ['customer_type' => $customer_type, 'users' => $users ,'departments'=> $departments , 'order_active' => true]);
    }

    public function getVendorUsers()
    {
        $users = [];

        $result = DB::table('users')
                    ->join('users_type', 'users.user_type', '=','users_type.user_type')
                    ->where('users_type.description', 'vendedor')
                    ->get();
        
        foreach ($result as $key => $value) 
        {
            array_push($users, User::find($value->id));
        }
        return $users;
    }

    public function SaveCreditOrder(Request $request, $order)
    {
        if ($request->input('condition'))
        {
            $rules = [
                'condition' => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                    
                return redirect()->back()->withErrors($validator->errors());
            }

            $credit = new Credit();
            $credit->customer_id = $order->customer_id;
            $credit->amount = $order->total;
            $credit->balance = $order->total;
            $credit->credit_condition = $request->input('condition');
            $credit->order_id = $order->order_id;
            $credit->save();
        }

    }
    


    public function saveOrder(Request $request)
    {
        
        $total = 0.0;

        
        if ($request->input('replacements')) 
        {
            $rules = [
                'customer' => 'required',
                'replacements' => 'required'
            ];
            
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                    
                return redirect()->back()->withErrors($validator->errors());
            }

            $total = $this->calculateTotal($request);
            $companyInvoice = CompanyInvoice::orderBy('company_invoice_id', 'desc')->take(1)->first();
            $order = new Order();
            $order->total = $total;
            $order->is_invoice = ($request->order_type == 'invoice') ? 1 : 0;
            $order->active = $request->input('add_order') ? 1 : 0;
            $order->customer_id = $request->input('customer');
            $order->user_id = Auth::user()->id;

            if ($order->save()) 
            {
                /* Guardar los repuestos de la orden */
                foreach ($request->input('replacements') as $key => $element) 
                {
                    $replacementOrder = new ReplacementOrder();
                    $replacement_id = $element['replacement_id'] ? $element['replacement_id'] : $element['motorcycle_id'];
                    $replacementOrder->order_id = $order->order_id;
                    $replacementOrder->replacement_id = $replacement_id;
                    $replacementOrder->quantity = $element['quantity'];
                    
                     $replacementOrder->save();

                     $product = Replacement::find($replacement_id);
                     $product->quantity -= $element['quantity'];

                     $product->save();    
                }
                
                if ($request->input('credit')) 
                {
                    $this->SaveCreditOrder($request, $order);
                }
                /*guardar orden*/
                if ($request->input('add_order')) 
                {
                     $this->saveProforma($order);    
                     return redirect()->back()->with(['message' => 'Orden guardada correctamente']);     
                }
                /*cerrar orden*/
                else
                {
                    if($order->is_invoice)
                    {
                        $this->saveInvoice($order);
                        $this->saveProforma($order);

                    }
                    else
                    {
                        $this->saveProforma($order);
                    }
                }
                return redirect()->route('printOrder', ['id' => $order->order_id]);
            } 
        }
    }

    public function saveInvoice($order)
    {   
        $companyInvoice = CompanyInvoice::orderBy('company_invoice_id', 'desc')->take(1)->first();

        if($order->invoices->first())
        {
            $invoices = Invoice::where('order_id', $order->order_id)->get();

            foreach ($order->replacements->chunk(20) as $key => $value) 
            {

                $invoice = $invoices[$key];

                if(!$invoice)
                {
                    $invoice = new Invoice();
                    $lastInvoice = Invoice::where('company_invoice', $companyInvoice->company_invoice_id)->orderBy('invoice_id', 'desc')->take(1)->first();

                    if($lastInvoice)
                    {
                        $invoice->invoice_id = str_pad(intval($lastInvoice->invoice_id) + 1, 8, '0', STR_PAD_LEFT);
                    }
                    else
                    {
                        $array = explode("-", $companyInvoice->range_to);
                        $invoiceNumber = $array[count($array) - 1];

                        $invoice->invoice_id = $invoiceNumber;
                    }
                }
                
                $invoice->order_id = $order->order_id;
                $total = $this->calculateInvoiceTotal($order, $value);
                $invoice->company_invoice = $companyInvoice->company_invoice_id;
                $invoice->total = $total;
                $invoice->subtotal = number_format($invoice->total/(1 + $companyInvoice->isv), 2, '.', '');
                $invoice->isv = number_format($invoice->subtotal*$companyInvoice->isv, 2,'.','');

                $invoice->save(); 
            }
        }
        else
        {
            
            foreach ($order->replacements->chunk(20) as $key => $value) 
            {
                $invoice = new Invoice();
                $lastInvoice = Invoice::where('company_invoice', $companyInvoice->company_invoice_id)->orderBy('invoice_id', 'desc')->take(1)->first();

                if($lastInvoice)
                {
                        
                    $invoice->invoice_id = str_pad(intval($lastInvoice->invoice_id) + 1, 8, '0', STR_PAD_LEFT);
                }
                else
                {
                    $array = explode("-", $companyInvoice->range_to);
                    $invoiceNumber = $array[count($array) - 1];

                    $invoice->invoice_id = $invoiceNumber;
                }

                $invoice->order_id = $order->order_id;
                $total = $this->calculateInvoiceTotal($order, $value);
                $companyInvoice = CompanyInvoice::orderBy('company_invoice_id', 'desc')->take(1)->first();
                $invoice->company_invoice = $companyInvoice->company_invoice_id;
                $invoice->total = $total;
                $invoice->subtotal = number_format($invoice->total/(1 + $companyInvoice->isv), 2, '.', '');
                $invoice->isv = number_format($invoice->subtotal*$companyInvoice->isv, 2,'.','');

                $invoice->save(); 
            }
        }
        

        
    }

    public function saveProforma($order)
    {
        Proforma::where('order_id', $order->order_id)->delete();

        foreach ($order->replacements->chunk(20) as $key => $value) 
        {
            $proforma = new Proforma();
            $proforma->order_id = $order->order_id;
            $company = Company::first();
            $proforma->company_id = $company->company_id;
            $proforma->total = $this->calculateInvoiceTotal($order, $value);
            $proforma->save(); 
        }    

    }

    public function updateOrder(Request $request, $id)
    {

        $rules = [
            'replacements' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $total = $this->calculateTotal($request);

        $order = Order::find($id);
        $order->total = $total;
        $companyInvoice = CompanyInvoice::orderBy('company_invoice_id', 'desc')->take(1)->first();
        $order->save();

        if ($request->input('save_changes')) 
        {
            $order->active = 1;
            $order->save();

            $replacementOrder = ReplacementOrder::where('order_id', $order->order_id)->get();

            /* Restaurar la cantidad de repuestos*/
            foreach ($replacementOrder as $key => $item) 
            {
                $replacement = Replacement::find($item->replacement_id);
                $replacement->quantity += $item->quantity;
                $replacement->save();
            }
            ReplacementOrder::where('order_id', $order->order_id)->delete();

            foreach ($request->input('replacements') as $key => $element) 
            {
                $replacementOrder = new ReplacementOrder();
                $replacementOrder->order_id = $order->order_id;
                $replacementOrder->replacement_id = $element['replacement_id'];
                $replacementOrder->quantity = $element['quantity'];

                 $replacement = Replacement::find($element['replacement_id']);
                 $replacement->quantity -= $element['quantity'];

                 $replacementOrder->save();
                 $replacement->save();
            }
            
            return redirect()->back();
        }
        else
        {
            $order->active = 0;
            $order->checked_by  = Auth::user()->id;
            $order->save();

            if($order->is_invoice)
            {
                
                if(!$this->checkInvoiceNumber($order))
                {
                    $order->active = 1;
                    $order->save();
                    return redirect()->back()->withErrors(['error' => 'Ha excedido el numero maximo de facturas o la fecha limite de emision']);
                }

                $this->saveInvoice($order);
                    
                $this->saveProforma($order);    
            }
            else
            {
                $this->saveProforma($order);       
            }
            

            return redirect()->route('printOrder', ['id' => $order->order_id]);
        }
    }

    public function deleteOrder($id)
    {
        $order = Order::find($id);
        
        if($order)
        {
            foreach ($order->replacements as $key => $item) 
            {
                $replacement = Replacement::find($item->replacement_id);
                $replacement->quantity += $item->quantity;
                $replacement->save();
            }
            ReplacementOrder::where('order_id', $id)->delete();
            $order->delete();    
        }
        
        return redirect()->back();

    }

    public function removeOrderProduct(Request $request)
    {

        $replacement_order = $request->replacement_order;
        $replacementOrder = ReplacementOrder::find($replacement_order);
        
        $order = Order::find($replacementOrder->order_id);
        $replacement = Replacement::find($replacementOrder->replacement_id);
        $replacement->quantity += $replacementOrder->quantity;
        $replacement->save();
        $replacementOrder->delete();
        
        $order->total = $this->calculateTotal($request);
        $order->save();

        if($order->proformas->first())
        {
            $this->saveProforma($order);
        }
        if($order->invoices->first())
        {
            $this->saveInvoice($order);
        }

        return view('orders.items', ['order' => $order, 'is_ready' => $this->isOrderReady($order)]);
    }

    public function addProductToOrder(Request $request)
    {
        if($request->order_id)
        {
            $isReplacementInOrder = ReplacementOrder::where('order_id', $request->order_id)->where('replacement_id', $request->replacement_id)->first();
            if($isReplacementInOrder)
            {
                return response()->json(['error' => 'Este producto ya se encuentra en la orden']);
            }
            $replacement_id = $request->replacement_id;

            $replacement = Replacement::find($replacement_id);

            if($replacement->quantity < $request->quantity)
            {
                return response()->json(['error' => "No hay suficientes $replacement->description"]);
            }
            $replacementOrder = new ReplacementOrder();
            $replacementOrder->order_id = $request->order_id;
            $replacementOrder->replacement_id = $request->replacement_id;
            $replacementOrder->quantity = $request->quantity;

            $replacement->quantity -= $request->quantity;
            $replacement->save();
            $replacementOrder->save();
            

            $order = Order::find($request->order_id);
            $request->replacement_order = $replacementOrder->replacement_order;
            $order->total = $this->calculateTotal($request);
            $order->save();

            if($order->proformas->first())
            {
                $this->saveProforma($order);
            }
            if($order->invoices->first())
            {
                $this->saveInvoice($order);
            }

            return view('orders.items', ['order' => $order, 'is_ready' => $this->isOrderReady($order)]);
        }

        return redirect()->back();

    }

    public function updateOrderProduct(Request $request)
    {
        $replacement_id = $request->replacement_id;
        $replacement = Replacement::find($replacement_id);
        $replacementOrder = ReplacementOrder::where('order_id', $request->order_id)->where('replacement_id', $request->replacement_id)->first();

        if($replacement->quantity < ($request->quantity - $replacementOrder->quantity))
        {
            return response()->json(['error' => "No hay suficientes $replacement->description"]);
        }
        $replacement->quantity -= ($request->quantity - $replacementOrder->quantity);

        $replacementOrder->quantity = $request->quantity;
        $replacement->save();
        $replacementOrder->save();
        
        $order = Order::find($request->order_id);
        $request->replacement_order = $replacementOrder->replacement_order;
        
        $order->total = $this->calculateTotal($request);
        $order->save();  

        if($order->proformas->first())
        {
            $this->saveProforma($order);
        }
        if($order->invoices->first())
        {
            $this->saveInvoice($order);
        }

        return view('orders.items', ['order' => $order, 'is_ready' => $this->isOrderReady($order)]);
    }

    public function changeItemStatus(Request $request)
    {
        $code = $request->replacement_code;
        $quantity = $request->replacement_quantity;
        $order_id = $request->order_id;
        
        $replacement = DB::table('replacements_orders')
                            ->join('replacements', 'replacements.replacement_id', '=', 'replacements_orders.replacement_id')
                            ->select('replacements_orders.*')
                            ->where('replacements_orders.order_id', $order_id)
                            ->where('replacements.code', $code)
                            ->first();

        if($replacement)
        {
            $replacement = ReplacementOrder::where('order_id', $replacement->order_id)->where('replacement_id', $replacement->replacement_id)->first();
            
            $order = Order::find($request->order_id);

            if($replacement->quantity == $quantity)
            {
                $replacement->status = 1;
                $replacement->save();
                return view('orders.items', ['order' => $order, 'is_ready' => $this->isOrderReady($order)]);
            }
            else
            {
                $replacement->status = 0;
                $replacement->save();    
                return response()->json(['error' => 'Las cantidades no coinciden']);
            }
                
            
        }
        else
        {
            return response()->json(['error' => 'Este codigo no se encuentra en la orden']);
        }
        
        

    }


    public function calculateTotal($request)
    {
        $total = 0.0;

        if($request->replacement_order)
        {
            $order = Order::find($request->order_id);
            $array = ReplacementOrder::where('order_id' , $request->order_id)->get();

            foreach ($array as $key => $item) 
            {
                $replacement = Replacement::find($item->getReplacement->replacement_id);

                if ($order->customer->type->description == 'LOCAL') 
                {
                    $total += ($replacement->wholesale_price * $item->quantity);
                }
                else
                {
                    $total += ($replacement->route_price * $item->quantity);
                }
            }
        } 
        else
        {

            $customer = Customer::find($request->input('customer'));

            foreach ($request->input('replacements') as $key => $element) 
            {
                $replacement = Replacement::find($key);

                if ($customer->type->description == 'LOCAL') 
                {
                    $total += ($replacement->wholesale_price * $element['quantity']);
                }
                else
                {
                    $total += ($replacement->route_price * $element['quantity']);
                }
                
            }    
        }
        
        return number_format((float)$total, 2, '.', '');
    }

    public function calculateInvoiceTotal($order, $products)
    {
        $total = 0.0;
        foreach ($products as $key => $item) 
        {
            $replacement = Replacement::find($item->getReplacement->replacement_id);
            if ($order->customer->type->description == 'LOCAL') 
            {
                $total += number_format($replacement->wholesale_price * $item->quantity, 2, '.', '');
            }
            else
            {
                $total += number_format($replacement->route_price * $item->quantity, 2, '.', '');
            }
        }

        return number_format((float)$total, 2, '.', '');
    }

    public function checkInvoiceNumber($order)
    {
        $companyInvoice = CompanyInvoice::orderBy('company_invoice_id', 'desc')->take(1)->first();
        $lastInvoice = Invoice::where('company_invoice', $companyInvoice->company_invoice_id)->orderBy('invoice_id', 'desc')->take(1)->first();

        if($lastInvoice)
        {
            $array = explode("-", $companyInvoice->range_up);
            $invoiceNumberLimit = $array[count($array) - 1];

            if($lastInvoice->invoice_id === $invoiceNumberLimit || date('Y-m-d') > $companyInvoice->limit_date)
            {

                return false;
            }

        }

        return true;

        
    }

    public function getReplacementOrder($order, $code)
    {
        $order = DB::table('replacements_orders')
                    ->join('replacements', 'replacements.replacement_id', '=', 'replacements_orders.replacement_id')
                    ->select('replacements.description', 'replacements_orders.quantity')
                    ->where('replacements_orders.order_id', $order)
                    ->where('replacements.code', $code)
                    ->first();
        if($order)
            return response()->json($order);
        else
            return response()->json(['error' => 'Este producto no se encuentra en la orden']);
    }
    public function printProforma($id)
    {
        $proforma = Proforma::where('order_id', $id)->first();
        $data = $this->getDataForPrintAndEmail($id);
        return view('pdf.proforma', $data);   
    }

    public function printOrder($id)
    {
        $order = Order::find($id);
        $invoice = Invoice::where('order_id', $id)->first();
        $proforma = Proforma::where('order_id', $id)->first();
        $data = $this->getDataForPrintAndEmail($id);
        

        if($data)
        {

            if($invoice)
            {
                
                return view('pdf.invoice', $data);
            }
            else
            {
                return view('pdf.proforma', $data);   
            }    
        }
        return redirect()->back()->with(['error' => 'No se ha creado la factura o proforma de esta orden']);
        
        
    }

    public function sendEmail($id)
    {
        $result = [];
        $data = $this->getDataForPrintAndEmail($id);

        if($data)
        {
            if($data['order']->customer->email)
            {
                $this->dispatch(new SendEmail($data));
                return redirect()->back()->with(['message' => 'El correo ha sido enviado']);
            }
            else
            {
                return redirect()->back()->with(['error' => 'Este cliente no tiene email']);
            }
        }
        else
        {
            return redirect()->back()->with(['error' => 'No se ha creado la factura o proforma de esta orden']);
        }
        
    }

    public function getDataForPrintAndEmail($id)
    {
        $order = Order::find($id);
        $invoice = Invoice::where('order_id', $id)->first();
        $proforma = Proforma::where('order_id', $id)->first();

        if(!$proforma)
        {
            $this->saveProforma($order);
        }
        
        $products = [];
        $replacements_array = [];
        
        for ($i=0; $i < count($order->replacements); $i = $i + 20) 
        { 
            $replacements_slice = array_slice($order->replacements->toArray(), $i, $i + 20);   
            
            array_push($products, $replacements_slice);
        }
        
        foreach ($products as $key => $value) 
        {
            $section = [];
            foreach ($value as $code => $item) 
            {   
                $replacement = ReplacementOrder::where('order_id', $order->order_id)->where('replacement_id', $item['replacement_id'])->first();
                if($replacement)
                    array_push($section, $replacement);
            }   
            array_push($replacements_array, $section);
        }
        
        $company = Company::first();
        foreach ($order->invoices as $key => &$invoice) 
        {
            $textNumber = $this->convertNumberToText($invoice->total);    
            $invoice['textNumber'] = $textNumber;
        }

        return $data = ['order' => $order, 'company' => $company, 'replacements' => $replacements_array];
    }

    public function exportOrders()
    {
        
        Excel::create('Ordenes', function($excel) {

            $excel->sheet('New sheet', function($sheet) {

                $orders = Order::all();
                $sheet->loadView('excel.orders', ['orders' => $orders]);

            });

        })->download('xls');
        
    }

    public function exportOrder($id)
    {
        Excel::create('Orden', function($excel) use ($id){

            $excel->sheet('New sheet', function($sheet) use ($id) {

                $order = Order::find($id);
                $sheet->loadView('excel.order', ['order' => $order]);

            });

        })->download('xls');
        
    }

    function convertNumberToText($num, $fem = false, $dec = true) { 
       $matuni[2]  = "dos"; 
       $matuni[3]  = "tres"; 
       $matuni[4]  = "cuatro"; 
       $matuni[5]  = "cinco"; 
       $matuni[6]  = "seis"; 
       $matuni[7]  = "siete"; 
       $matuni[8]  = "ocho"; 
       $matuni[9]  = "nueve"; 
       $matuni[10] = "diez"; 
       $matuni[11] = "once"; 
       $matuni[12] = "doce"; 
       $matuni[13] = "trece"; 
       $matuni[14] = "catorce"; 
       $matuni[15] = "quince"; 
       $matuni[16] = "dieciseis"; 
       $matuni[17] = "diecisiete"; 
       $matuni[18] = "dieciocho"; 
       $matuni[19] = "diecinueve"; 
       $matuni[20] = "veinte"; 
       $matunisub[2] = "dos"; 
       $matunisub[3] = "tres"; 
       $matunisub[4] = "cuatro"; 
       $matunisub[5] = "quin"; 
       $matunisub[6] = "seis"; 
       $matunisub[7] = "sete"; 
       $matunisub[8] = "ocho"; 
       $matunisub[9] = "nove"; 

       $matdec[2] = "veint"; 
       $matdec[3] = "treinta"; 
       $matdec[4] = "cuarenta"; 
       $matdec[5] = "cincuenta"; 
       $matdec[6] = "sesenta"; 
       $matdec[7] = "setenta"; 
       $matdec[8] = "ochenta"; 
       $matdec[9] = "noventa"; 
       $matsub[3]  = 'mill'; 
       $matsub[5]  = 'bill'; 
       $matsub[7]  = 'mill'; 
       $matsub[9]  = 'trill'; 
       $matsub[11] = 'mill'; 
       $matsub[13] = 'bill'; 
       $matsub[15] = 'mill'; 
       $matmil[4]  = 'millones'; 
       $matmil[6]  = 'billones'; 
       $matmil[7]  = 'de billones'; 
       $matmil[8]  = 'millones de billones'; 
       $matmil[10] = 'trillones'; 
       $matmil[11] = 'de trillones'; 
       $matmil[12] = 'millones de trillones'; 
       $matmil[13] = 'de trillones'; 
       $matmil[14] = 'billones de trillones'; 
       $matmil[15] = 'de billones de trillones'; 
       $matmil[16] = 'millones de billones de trillones'; 

       
       //Zi hack
       $float=explode('.',$num);
       $num=$float[0];

       $num = trim((string)@$num); 
       if ($num[0] == '-') { 
          $neg = 'menos '; 
          $num = substr($num, 1); 
       }else 
          $neg = ''; 
       while ($num[0] == '0') $num = substr($num, 1); 
       if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
       $zeros = true; 
       $punt = false; 
       $ent = ''; 
       $fra = ''; 
       for ($c = 0; $c < strlen($num); $c++) { 
          $n = $num[$c]; 
          if (! (strpos(".,'''", $n) === false)) { 
             if ($punt) break; 
             else{ 
                $punt = true; 
                continue; 
             } 

          }elseif (! (strpos('0123456789', $n) === false)) { 
             if ($punt) { 
                if ($n != '0') $zeros = false; 
                $fra .= $n; 
             }else 

                $ent .= $n; 
          }else 

             break; 

       } 
       $ent = '     ' . $ent; 
       if ($dec and $fra and ! $zeros) { 
          $fin = ' coma'; 
          for ($n = 0; $n < strlen($fra); $n++) { 
             if (($s = $fra[$n]) == '0') 
                $fin .= ' cero'; 
             elseif ($s == '1') 
                $fin .= $fem ? ' una' : ' un'; 
             else 
                $fin .= ' ' . $matuni[$s]; 
          } 
       }else 
          $fin = ''; 
       if ((int)$ent === 0) return 'Cero ' . $fin; 
       $tex = ''; 
       $sub = 0; 
       $mils = 0; 
       $neutro = false; 
       while ( ($num = substr($ent, -3)) != '   ') { 
          $ent = substr($ent, 0, -3); 
          if (++$sub < 3 and $fem) { 
             $matuni[1] = 'una'; 
             $subcent = 'as'; 
          }else{ 
             $matuni[1] = $neutro ? 'un' : 'uno'; 
             $subcent = 'os'; 
          } 
          $t = ''; 
          $n2 = substr($num, 1); 
          if ($n2 == '00') { 
          }elseif ($n2 < 21) 
             $t = ' ' . $matuni[(int)$n2]; 
          elseif ($n2 < 30) { 
             $n3 = $num[2]; 
             if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
             $n2 = $num[1]; 
             $t = ' ' . $matdec[$n2] . $t; 
          }else{ 
             $n3 = $num[2]; 
             if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
             $n2 = $num[1]; 
             $t = ' ' . $matdec[$n2] . $t; 
          } 
          $n = $num[0]; 
          if ($n == 1) { 
             $t = ' ciento' . $t; 
          }elseif ($n == 5){ 
             $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
          }elseif ($n != 0){ 
             $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 
          } 
          if ($sub == 1) { 
          }elseif (! isset($matsub[$sub])) { 
             if ($num == 1) { 
                $t = ' mil'; 
             }elseif ($num > 1){ 
                $t .= ' mil'; 
             } 
          }elseif ($num == 1) { 
             $t .= ' ' . $matsub[$sub] . '?n'; 
          }elseif ($num > 1){ 
             $t .= ' ' . $matsub[$sub] . 'ones'; 
          }   
          if ($num == '000') $mils ++; 
          elseif ($mils != 0) { 
             if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
             $mils = 0; 
          } 
          $neutro = true; 
          $tex = $t . $tex; 
       } 
       $tex = $neg . substr($tex, 1) . $fin; 
       $end_num=ucfirst($tex).' con '.$float[1].'/100 Lempiras';

       //Zi hack --> return ucfirst($tex);
       $end_num=ucfirst($tex).' con '.$float[1].'/100 Lempiras';
       return $end_num; 
    }
}
