<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Company;
use App\CompanyInvoice;


class CompanyController extends Controller
{
    public function updateCompany(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'rtn' => 'required',
            'web_site' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }

        $company = Company::find($id);

        $name = $request->input('name');
        $address = $request->input('address');
        $telephone = $request->input('telephone');
        $email = $request->input('email');
        $web_site = $request->input('web_site');
        $rtn = $request->input('rtn');

        $company->name = $name;
        $company->address = $address;
        $company->telephone = $telephone;
        $company->email = $email;
        $company->web_site = $web_site;
        $company->rtn = $rtn;

        if($file = $request->file('picture'))
        {
            $image = $request->file('picture');
            $thumbnailName  =   'thumb_'. $image->getClientOriginalName();
            $destinationPath = "uploads"; 

            $company->logo = $request->file('picture')->move($destinationPath, $thumbnailName);
        }

        if($company->save())
        {
            return redirect()->back();  
        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar los datos de la compañia']);     
        }
        
    }


     public function newCompany(Request $request)
    {
         $rules = [
            'name' => 'required',
            'address' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'rtn' => 'required',
            'web_site' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }

        $company = new Company();
        
        $name = $request->input('name');
        $address = $request->input('address');
        $telephone = $request->input('telephone');
        $email = $request->input('email');
        $web_site = $request->input('web_site');
        $rtn = $request->input('rtn');

        $company->name = $name;
        $company->address = $address;
        $company->telephone = $telephone;
        $company->email = $email;
        $company->web_site = $web_site;
        $company->rtn = $rtn;

        if($file = $request->file('picture'))
        {
            $image = $request->file('picture');
            $thumbnailName  =   'thumb_'. $image->getClientOriginalName();
            $destinationPath = "uploads"; 

            $company->logo = $request->file('picture')->move($destinationPath, $thumbnailName);
        }

        if($company->save())
        {
            return redirect()->back();  
        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar los datos de la compañia']);     
        }
        
    }


    public function newCompanyInvoice(Request $request)    
    {

        $rules = [
            'cai' => 'required',
            'range_to' => 'required',
            'range_up' => 'required',
            'date' => 'required',
            'isv' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }


        $companyinvoice = new CompanyInvoice();

        $cai = $request->input('cai');
        $range_to = $request->input('range_to');
        $range_up = $request->input('range_up');
        $limit_date = $request->input('date');
        $isv = $request->input('isv');
        $company = Company::take(1)->first();

        $companyinvoice->cai = $cai;
        $companyinvoice->company_id = $company->company_id;
        $companyinvoice->range_to = $range_to;
        $companyinvoice->range_up = $range_up;
        $companyinvoice->limit_date = $limit_date;
        $companyinvoice->isv = $isv;

        if($companyinvoice->save())
        {
            return redirect()->back();  
        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar los datos de la compañia']);     
        }


    }

    public function updateCompanyInvoice(Request $request, $id)
    {
        $companyinvoice = CompanyInvoice::find($id);


        $rules = [
            'cai' => 'required',
            'range_to' => 'required',
            'range_up' => 'required',
            'date' => 'required',
            'isv' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }

        $cai = $request->input('cai');
        $range_to = $request->input('range_to');
        $range_up = $request->input('range_up');
        $limit_date = $request->input('date');
        $isv = $request->input('isv');

        $companyinvoice->cai = $cai;
        $companyinvoice->range_to = $range_to;
        $companyinvoice->range_up = $range_up;
        $companyinvoice->limit_date = $limit_date;
        $companyinvoice->isv = $isv;

        if($companyinvoice->save())
        {
            return redirect()->back();  
        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar los datos de la compañia']);     
        }

    }

}
