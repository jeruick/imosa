<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserType;
use App\Order;
use App\Invoice;
use App\CompanyInvoice;
use Auth;
use DB;
use Hash;

class UserController extends Controller
{
    public function index()
    {
        $usertype = UserType::all();
    	$users = User::orderBy('active', 0)->get();
    	return view('user.index', ['users' => $users, 'usertype' => $usertype , 'user_active' => true]);
    }

    public function deleteUser($id)
    {
    	User::destroy($id);
    	return redirect()->back();
    }

    public function changeStatus(Request $request)
    {
    	$user = User::find($request->input('user_id'));
    	$user->active = $request->input('status');
    	$user->save();
    	return;
    }

    public function changeType(Request $request)
    {
        $user = User::find($request->input('user_id'));
        $user->user_type = $request->input('type');
        $user->save();
        return;
    }

    public function checkAdminPassword(Request $request)
    {
        $user = User::where('email','erick@lynext.com')->first();
        $found = Hash::check($request->password, $user->password);
        
        if($found)
        {
            return response()->json($user);
        }
        return null;
    }

    public function loadNotifications()
    {
        if(Auth::user()->type->description == 'admin')
        {
            $result = ['orders' => []];
            $orders = Order::where(DB::raw('datediff(NOW(), date)'), '=', 0)->get();

            if($orders)
            {
                foreach ($orders as $key => $value) 
                {
                    array_push($result['orders'], $value->user);
                }
                
            }
            if(!$this->checkInvoiceNumber())
            {
                $result['invoice'] = 'El limite de facturacion o el rango estan a punto de expirar';

            }
            $this->checkCreditLimitDate();
            
            return response()->json($result);
        }
        return null;
    }

    

    public function checkInvoiceNumber()
    {
        $companyInvoice = CompanyInvoice::orderBy('company_invoice_id', 'desc')->take(1)->first();
        $lastInvoice = Invoice::where('company_invoice', $companyInvoice->company_invoice_id)->orderBy('invoice_id', 'desc')->take(1)->first();

        if($lastInvoice)
        {
            $array = explode("-", $companyInvoice->range_up);
            $invoiceNumberLimit = $array[count($array) - 1];
            $diff = intval($invoiceNumberLimit) - intval($lastInvoice->invoice_id);
            $time1 = strtotime($companyInvoice->limit_date);
            $time2 = strtotime(date('Y-m-d'));
            $second_diff =  $time1 - $time2;

            if(($diff <= 10) || ($second_diff/60) < 43200)
            {
                return false;
            }

        }

        return true;

    }
}
