<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\CustomerType;
use App\Department;
use App\City;
use App\User;
use Validator;
use DB;

class CustomerController extends Controller
{
    public function searchCustomer($text)
    {
    	if ($text == 'blank') 
    	{
    		return Customer::all();
    	}

    	$customers = Customer::where('full_name', 'LIKE', "%$text%")->orWhere('RTN' , $text)->get();

    	return $customers;
    }

    public function getCustomer($id)
    {
        $customer = Customer::find($id);
        $users = $this->getVendorUsers();
        $departments = Department::all();
        $cities = City::all();
        $customer_type = CustomerType::all();

        return view('customer.view', ['customer' => $customer, 'customer_type' => $customer_type, 'customer_active' => true, 'departments' => $departments, 'users' => $users, 'cities' => $cities]);
    }

    public function newCustomer(Request $request)
    {
        $customer = new Customer();
        return $this->saveCustomer($customer, $request);
        
    }
    public function updateCustomer(Request $request, $id)
    {
        $customer = Customer::find($id);
        return $this->saveCustomer($customer, $request);
    }
    public function getCustomers()
    {
        $customers = Customer::paginate(10);
        $users = $this->getVendorUsers();
        $departments = Department::all();
        $customer_type = CustomerType::all();

        return view('customer.index', ['customers' => $customers, 'customer_type' => $customer_type, 'customer_active' => true, 'departments' => $departments, 'users' => $users]);
    }

    public function getVendorUsers()
    {
        $users = [];

        $result = DB::table('users')
                    ->join('users_type', 'users.user_type', '=','users_type.user_type')
                    ->where('users_type.description', 'vendedor')
                    ->get();
        
        foreach ($result as $key => $value) 
        {
            array_push($users, User::find($value->id));
        }
        return $users;
    }

    public function deleteCustomer($id)
    {
        Customer::destroy($id);
        return redirect()->back();
    }

    public function saveCustomer($customer, $request)
    {
        $rules = [
            'full_name' => 'required',
            'customer_type' => 'required',
            'address' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $customer->full_name = $request->input('full_name');
        $customer->address = $request->input('address');
        $customer->phone_number = $request->input('phone_number');
        $customer->phone_number2 = $request->input('phone_number2');
        $customer->customer_type = $request->input('customer_type');
        $customer->city_id = $request->input('city_id');
        $customer->telephone = $request->input('telephone');
        $customer->email = $request->input('email');
        $customer->user_id = $request->input('user_id');
        $customer->contact = $request->input('contact');
        $customer->RTN = $request->input('RTN');

        $customer->save();
        return redirect()->back();
    }

    public function getCities($id)
    {
        $cities = City::where('department_id', $id)->get();
        return view('customer.cities', ['cities' => $cities]);
    }
}





