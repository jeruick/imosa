<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Cellar;

class CellarController extends Controller
{

    public function newCellar(Request $request)
    {
        $cellar = new Cellar();
        return $this->saveCellar($cellar, $request);
    }

    public function saveCellar($cellar, $request)
    {
        $rules = [
        'description' => 'required|unique:cellars'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {

          return redirect()->back()->withErrors($validator->errors());
        }


        $cellar->description = $request->input('description');

        $cellar->save();
        return redirect()->back();
    }


    public function deleteCellar($id)
    {
        Cellar::destroy($id);
        return redirect()->back();
    }

    
}
