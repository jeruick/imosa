<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Cellar;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\ExportData;
use App\Motorcycle;
use App\MotorcycleModel;
use App\MotorcycleType;
use App\Provider;
use App\Replacement;
use App\ReplacementModel;
use Auth;
use DB;
use Excel;
use File;
use PDF;
use Storage;
use Validator;

class InventoryController extends Controller
{

    public function replacements()
    {        
    	$replacements = Replacement::simplePaginate(10);
        $brands = Brand::all();
        $models = MotorcycleModel::all();
        $cellars = Cellar::all();
        $providers = Provider::all();
    	
    	return view('inventory.replacements', ['replacements' => $replacements, 'models' => $models, 'cellars' => $cellars, 'providers' => $providers, 'brands' => $brands,'inventory_active' => true, 'paginate' => true]);
    }

    public function newReplacement(Request $request)
    {
        $replacement = new Replacement();
        $models = $request->models;
        
        $rules = [
            'wholesale_price' => 'required',
            'route_price' => 'required',
            'quantity' => 'required',
            'description' => 'required',
            'provider' => 'required',
            'cellar' => 'required',
            'code' => 'required|unique:replacements',
            'location' => 'required',

        ];
        
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) 
        {    
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $replacement = $this->saveReplacement($request, $replacement);

        if ($models && $replacement) 
        {
            $this->saveReplacementModels($replacement, $models);
        }

        return redirect()->back(); 
    	
    }

    public function getReplacement($id)
    {
        $brands = Brand::all();
        $replacement = Replacement::find($id);
        $cellars = Cellar::all();
        $providers = Provider::all();

        return view('inventory.replacement', ['replacement' => $replacement, 'cellars' => $cellars, 'providers' => $providers, 'brands' => $brands]);
    }

    public function getReplacementPrice($id, $customer)
    {
        $replacement = Replacement::find($id);
        $customer = Customer::where('customer_id', $customer)->first();
        
        if($customer->type->description == 'LOCAL')
        {
            return $replacement->wholesale_price;
        }
        else
        {
            return $replacement->route_price;
        }
        
    }

    public function searchReplacement($text)
    {
        
        if ($text == 'blank') 
        {
            return ['redirect' => 'repuestos'];
        }
        else
        {
            $replacements = $this->getSearchResult($text);

            if(count($replacements) > 0)
            {
                
                return view('inventory.items', ['replacements' => $replacements]);
            }
            else
            {
                return response()->json(['error' => 'No se encontraron resultados']);   
            }
        }
       
    }

    public function searchReplacements($text)
    {
        
        if ($text == 'blank') 
        {
            return ['redirect' => 'repuestos'];
        }
        else
        {
            $replacements = $this->getSearchResult($text);

            if(count($replacements) > 0)
            {
                
                return response()->json($replacements);
            }
            else
            {
                return response()->json(['error' => 'No se encontraron resultados']);   
            }
        }
       
    }


    public function exportInventory()
    {
        
        Excel::create('Inventario', function($excel) {

            $excel->sheet('New sheet', function($sheet) {

                $replacements = Replacement::all();
                $sheet->loadView('excel.inventory', ['replacements' => $replacements]);

            });

        })->download('xls');
        
    }

    public function getSearchResult($text)
    {
        $result = DB::table('replacements')
                    ->join('replacements_models', 'replacements_models.replacement_id', '=', 'replacements.replacement_id')
                    ->join('providers', 'providers.provider_id', '=', 'replacements.provider_id')
                    ->join('models', 'replacements_models.model_id', '=', 'models.model_id')
                    ->select('replacements.*','models.description as model_description','replacements.quantity', 'providers.name as provider')
                    ->where(function($query) use ($text){
                            $query->where('replacements.description', 'LIKE', "%$text%")
                                  ->orWhere('models.description', 'LIKE', "%$text%")
                                  ->orWhere('replacements.code', 'LIKE', "%$text%")
                                  ->orWhere('replacements.bar_code' , 'LIKE', "%$text%");    
                    })
                    ->groupBy('replacement_id')
                    ->get();

        $replacements = [];        
        if($result)
        {
            
            foreach ($result as $key => $item) 
            {
                $temp = Replacement::find($item->replacement_id);
                array_push($replacements, $temp);
            }

            
        }
        
        return $replacements;
    }

    public function deleteReplacement($id)
    {
        $replacement = Replacement::find($id);
        if($replacement->picture)
        {
            $picture_name = substr($replacement->picture, strrpos($replacement->picture, '/') + 1);
 
            if(Storage::disk('s3')->exists($picture_name))
            {
                Storage::disk('s3')->delete($picture_name);   
            }    
        }
        ReplacementModel::where('replacement_id', $id)->delete();
        Replacement::destroy($id);

        return redirect()->back();
    }

    public function updateReplacement(Request $request, $id)
    {

        $replacement = Replacement::find($id);

        $models = $request->input('models');

        $rules = [
            'wholesale_price' => 'required',
            'route_price' => 'required',
            'quantity' => 'required',
            'description' => 'required',
            'provider' => 'required',
            'cellar' => 'required',
            'code' => 'required',
            'location' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }

        $replacement = $this->saveReplacement($request, $replacement);       
        
        if ($models) 
        {
            $this->saveReplacementModels($replacement, $models);
        }
        return redirect()->back();

    }

    public function saveReplacement($request, $replacement)
    {
        
        /*replacement data*/    
        $wholesale_price = $request->input('wholesale_price');
        $route_price = $request->input('route_price');
        $description = $request->input('description');
        $code = $request->input('code');
        $cellar = $request->input('cellar');
        $location = $request->input('location');
        $provider_id = $request->input('provider');
        $quantity = $request->input('quantity');

        $replacement->wholesale_price = $wholesale_price;
        $replacement->route_price = $route_price;
        $replacement->description = $description;
        $replacement->code = $code;
        $replacement->location = $location;
        $replacement->quantity = $quantity;
        $replacement->cellar_id =$cellar;
        $replacement->provider_id = $provider_id;


        /* replacement image */
        if($file = $request->file('picture'))
        {
            $image = $request->file('picture');
            $thumbnailName  =   'thumb_'. $image->getClientOriginalName();
            $destinationPath = "uploads"; 
            
            /*save replacement image to s3*/
            // Storage::put($file->getFilename().'.'.$extension, File::get($file));    
            //$replacement->picture = Storage::getAdapter()->getClient()->getObjectUrl(env('AWS_BUCKET'), $file->getFilename().'.'.$extension);

            $replacement->picture = $request->file('picture')->move($destinationPath, $thumbnailName);
            
        }

        

        if($replacement->save())
        {
            return $replacement;  
        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar el repuesto']);     
        }


    }

    public function saveReplacementModels($replacement, $models)
    {   
        
        ReplacementModel::where('replacement_id',$replacement->replacement_id)->delete();
        foreach ($models as $key => $value) 
        {
            $o = new ReplacementModel();
            $o->replacement_id = $replacement->replacement_id;
            $o->model_id = $value;
            if (!$o->save()) 
            {
                return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar el repuesto']);     
            }
        }
    }

    public function newMotorcycleType(Request $request)
    {
        $type = new MotorcycleType();
        $type->description = $request->input('type');
        if ($type->save()) 
        {
            return $type;
        }
    }

    public function totalProducts(Request $request)
    {
        if($request->ajax()) 
        {
            $type = $request->input('type');

            if ($type == 'replacements' || $type == 'motorcycles') 
            {
                $products = $request->input('replacements') ? $request->input('replacements') : $request->input('motorcycles');
                
                if (isset($products)) 
                {
                    $total = 0.0;
                    foreach ($products as $key => $value) 
                    {
                        if ($type == 'replacements') 
                        {
                            $replacement = Replacement::find($value['replacement_id']);  
                            $total += $replacement->wholesale_price * $value['quantity'];  
                        }
                        else if ($type == 'motorcycles')
                        {
                            $motorcycle = Motorcycle::find($value['motorcycle_id']);       
                            $total += $motorcycle->price * $value['quantity'];
                        }
                        
                    }
                    return $total;
                } 
            }
            
        }
    }

    

}
