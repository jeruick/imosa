<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

use App\Brand;
use App\Cellar;
use App\Company;
use App\Provider;
use App\CompanyInvoice;



class ConfigurationController extends Controller
{
   
    public function configurations()
    {
        return view('backend.configurations', ['configuration_active' => true]);
    }
    
    public function getBrands(Request $request)
    {
        $brands = Brand::all();
        return view('configurations.brand', ['brands' => $brands]);
    }

    public function getCellar(Request $request)
    {
        $cellars = Cellar::all();
        return view('configurations.cellar', ['cellars' => $cellars]);
    }

    public function getCompany(Request $request)
    {
        $company = Company::first();
        return view('configurations.company', ['company' => $company]);
    }

    public function getCompanyInvoice(Request $request)
    {
        $company = Company::all();
        $companyinvoice = CompanyInvoice::first();
        return view('configurations.dei', ['companyinvoice' => $companyinvoice, 'company' => $company]);
    }

    public function getProviders(Request $request)
    {
        $providers = Provider::all();
        return view('provider.index', ['providers' => $providers ,'provider_active' => true]);
    }    
    
    
}