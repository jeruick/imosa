<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Credit;
use App\Customer;
use App\Replacement;
use App\Motorcycle;
use App\Order;
use App\CreditReplacement;
use App\PaymentHistory;
use DB;

class CreditController extends Controller
{
    
    public function index($id)
    {
        $credits = Credit::where('customer_id',$id)->where('balance', '>', 0)->orderBy('created_at', 'desc')->get();
        $customers = Customer::all();
        $creditreplacement = CreditReplacement::all();
        
        return view('credit.index', ['credits' => $credits,'customers' => $customers, 'creditreplacement' => $creditreplacement, 'id' => $id,'credit_active' => true]);
    }
        
    public function getCustomerCredits()
    {
        $customers = DB::table('credits')->join('customers', 'customers.customer_id', '=' , 'credits.customer_id' )->select('credits.customer_id', 'customers.full_name', DB::raw('SUM(credits.balance) as importe'))->groupBy('customer_id')->get();
        return view('credit.index', ['customers' => $customers, 'credit_active' => true]);
    }

    public function getCredits($id)
    {
        $customer = Customer::find($id);
        $credits = Credit::where('customer_id' ,$id)->get();    
        return view('credit.customers',['credits' => $credits, 'cutomer' => $customer ,'credit_active' => true]);
    }

    public function paymentHistory($id)
    {
        $credit = Credit::find($id);
        $payment = PaymentHistory::where('credit_id', $id)->orderBy('created_at', 'desc')->get();
        return view('credit.history', [ 'credit' => $credit, 'payment' => $payment, 'credit_ative' => true]);
    }

    public function newPayment($id)
    {
        $payment = new PaymentHistory();
        return view('credit.history', [ 'credit' => $credit, 'payment' => $payment, 'credit_ative' => true]);        
    }

    public function newCredit(Request $request)
    {
        
        $rules = [
            'customer' => 'required',
            'condition' => 'required'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
                
            return redirect()->back()->withErrors($validator->errors());
        }

        $amount = 0.0;
        if ($request->input('replacements')) 
        {
            $products = $request->input('replacements');
        }
        elseif ($request->input('motorcycles')) 
        {
            $products = $request->input('motorcycles');   
        }
        else
        {
            $products = $request->input('products');
        }
        
        foreach ($products as $key => $value) 
        {

            $product = Product::find($key);

            if ($product->quantity < $value['quantity']) 
            {
                return redirect()->back()->withErrors(['error' => "No hay suficiente existencias del $product->product_type"]);
            }
            if ($product->product_type == 'Repuesto') 
            {
                $amount += ($product->replacement->wholesale_price * $value['quantity']);
            }
            else if($product->product_type == 'Motocicleta')
            {
                $amount += ($product->motorcycle->price * $value['quantity']);   
            }
        }

        $credit = new Credit();
        $credit->customer_id = $request->input('customer');
        $credit->balance = $amount;
        $credit->amount = $amount;
        $credit->credit_condition = $request->input('condition');
        if ($request->input('type') == 'replacement') 
        {
            $credit->type = 'Repuesto';
        }
        else
        {
            $credit->type = 'Motocicleta';
        }

        if ($credit->save()) 
        {
            /*Reduce quantity from product*/
            
            foreach ($products as $key => $value) 
            {
                $creditreplacement = new CreditReplacement();
                $creditreplacement->credit_id = $credit->credit_id;
                $creditreplacement->product_id = $key;
                $creditreplacement->quantity = $value['quantity'];
                $creditreplacement->save();

                $product = Product::find($key);
                $product->quantity -= $value['quantity'];
                $product->save();

            }
            
            $products = CreditReplacement::where('credit_id', $credit->credit_id)->get();
            return redirect()->back()->with(['credit_completed' => $credit, 'products' => $products]);
        }
    }

    public function getCredit($id)
    {
        $credit = Credit::find($id);
        $customers = Customer::all();
        $order = Order::where('order_id', $credit->order_id)->first();
        return view('credit.credit', ['credit' => $credit, 'customers' => $customers , 'order' => $order , 'credit_active' => true]);
    }

    public function historyCredit($id)
    {
        $credit = Credit::find($id);
        $paymentHistory = PaymentHistory::where('credit_id', $id)->orderBy('created_at', 'desc')->get();
        return view('credit.history', ['credit' => $credit, 'paymentHistory' => $paymentHistory, 'credit_active' => true]);
    }

    public function updateCredit(Request $request, $id)
    {
        
        $credit = Credit::find($id);
        if (isset($credit)) 
        {
            
            $credit->amount = $request->input('amount');
            $credit->balance = $request->input('amount');
            $credit->credit_condition = $request->input('condition');

            if ($credit->save()) 
            {

                $creditreplacement = CreditReplacement::where('credit_id', $id)->get();

                foreach ($creditreplacement as $key => $element) 
                {
                    $product = Product::find($element->product_id);
                    $product->quantity += $element->quantity;
                    $product->save();
                }

                CreditReplacement::where('credit_id', $id)->delete();

                
                if ($request->input('products')) 
                {
                    foreach ($request->input('products') as $key => $value) 
                    {
                        $creditreplacement = new CreditReplacement();
                        $creditreplacement->credit_id = $id;
                        $creditreplacement->product_id = $key;
                        $creditreplacement->quantity = $value;
                        
                        if ($creditReplacement->save()) 
                        {
                            $product = Product::find($creditreplacement->product_id);
                            $product->quantity -= $creditreplacement->quantity;
                            $product->save();
                        }

                    }
                    
                }
                else
                {
                    $customer_id = $credit->customer_id;
                    $credit->delete(); 
                    return redirect()->route('credits', $customer_id);

                }
                
                return redirect()->back();
            }

            return redirect()->back()->withErrors(['error' => 'Ocurrio un error al guardar el credito']);  
        }
        return redirect()->back()->withErrors(['error' => 'El credito no existe']);  
        
    }

    public function deleteCredit($id)
    {
        $credit = Credit::find($id);
        
        foreach ($credit->credit_replacement as $key => $element) 
        {
            $element->product->quantity += $element->quantity;
            $element->product->save();
        }
        CreditReplacement::where('credit_id', $id)->delete();
        $credit->delete();
        return redirect()->back();
    }
    public function payCredit(Request $request, $id)
    {
        $credit = Credit::find($id);
        $amount = $request->input('amount');

        if ($credit->balance > 0 && isset($amount)) 
        {
            $paymentHistory = new PaymentHistory();  
            $paymentHistory->credit_id = $credit->credit_id;
            $paymentHistory->payment_amount = $amount;
            if ($paymentHistory->save()) 
            {
                $credit->balance -= $amount;
                if ($credit->save()) 
                {
                    if ($credit->balance <= 0) 
                    {
                        return redirect()->back()->with('message', 'El pago se completo correctamente');
                    }
                    return redirect()->back();
                }
                else
                {
                    return redirect()->back()->withErrors(['error' => 'Algo salio mal']);
                }
            }
        }
        else
        {
            return redirect()->back()->withErrors(['error' => 'Este credito ya se cancelo']);
        }
        
    }
}
