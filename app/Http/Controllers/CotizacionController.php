<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

use App\Customer;
use App\Replacement;
use App\Motorcycle;
use App\Product;
use App\InventoryMotorcycle;


class CotizacionController extends Controller
{
	
	public function index()
	{
		$customers = Customer::all();
		return view('cotizac.index', ['customers' => $customers,'cotizacion_active' => true]);
	}

	public function getCustomers()
	{
		$customers = Customer::all();
		return view('cotizac.replacement',['customers' => $customers]);
	}

	public function Cotizacion_motorcycle()
	{
		return view('cotizac.motorcycle');
	}

	public function GetMotorcycleDetails($id)
	{
		$motorcycle = InventoryMotorcycle::find($id);
		return view('cotizac.cotiza_motorcycle',['motorcycle' => $motorcycle]);
	}

	public function newCotizacion_replacement()
	{
		$customers = Customer::all();
		return view('cotizac.replacement', ['customers' => $customers]);
	}

}
