<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserType;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('backend.home', ['users' => $users]);
    }

    public function newUser(Request $request)
    {
        $user = New User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->user_type = '2';
        $user->save();
        $users = User::all();
        $usertype = UserType::all();
        return view('user.index', ['users' => $users , 'usertype' => $usertype]);
    }
}
