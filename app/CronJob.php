<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CronJob extends Model
{
    use SoftDeletes;

    protected $table = 'cronjob';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
