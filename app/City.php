<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $table = 'cities';
    public $primaryKey = 'city_id';
    
    public $timestamps = false;

    public function department()
    {
    	return $this->belongsTo('App\Department', 'department_id');
    }
}
