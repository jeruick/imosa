<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditReplacement extends Model
{

   protected $table = 'credits_replacement';

   protected $primaryKey = 'id';
   public $timestamps = false;

   public function credit()
   {
   		return $this->belongsTo('App\'Credit' , 'credit_id');
   }

   public function replacement()
   {
   		return $this->belongsTo('App\'Replacement' , 'replacement_id');
   }

}
