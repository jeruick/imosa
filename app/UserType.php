<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    protected $table = 'users_type';

    protected $primaryKey = 'user_type';

    public $timestamps = false;

    public function users()
    {
        return $this->hasMany('App\User', 'user_type');

    }
}
