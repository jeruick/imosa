<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = 'providers';

    protected $primaryKey = 'provider_id';
    public $timestamps = false;
    
    public function replacements()
    {
    	return $this->hasMany('App\Replacement', 'provider_id');
    }

}
