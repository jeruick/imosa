<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Replacement extends Model
{
    protected $table = 'replacements';

    protected $primaryKey = 'replacement_id';

    public $timestamps = false;

    public $incrementing = true;

    public function models()
    {
        return $this->hasMany('App\ReplacementModel', 'replacement_id');
    }

    public function provider()
    {
    	return $this->belongsTo('App\Provider', 'provider_id');
    }

    public function cellar()
    {
        return $this->belongsTo('App\Cellar', 'cellar_id');
    }
    
    public function credit_replacement()
    {
       return $this->hasMany('App\CreditReplacement', 'credit_id');
    }
    
}
