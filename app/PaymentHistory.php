<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    protected $table = 'payments_history';

    protected $primaryKey = 'payments_history';

    public $timestamps = false;

    public function credit()
   {
      return $this->belongsTo('App\Credit', 'credit_id');
   }
}
