<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $table = 'purchase_ordes';

    protected $primaryKey = 'order_id';

    public $timestamps = false;

}
