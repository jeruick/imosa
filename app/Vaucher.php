<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vaucher extends Model
{
    protected $table = 'vaucher';

    protected $primaryKey = 'id';

    public $timestamps = false;
}
