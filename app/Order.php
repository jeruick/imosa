<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'order_id';
    public $timestamps = false;

    public function customer()
    {
    	return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function replacements()
    {
    	return $this->hasMany('App\ReplacementOrder', 'order_id');
    }

    public function invoices()
    {
    	return $this->hasMany('App\Invoice', 'order_id');
    }

    public function proformas()
    {
    	return $this->hasMany('App\Proforma', 'order_id');
    }

    public function credit()
    {
        return $this->hasMany('App\Credit', 'credit_id');
    }    

    public function checkedBy()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}