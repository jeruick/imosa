<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proforma extends Model
{
  	protected $table = 'pro_forma';
    
    protected $primaryKey = 'proforma_id';
    
    public $timestamps = false;
    
    public $incrementing = true;
    
    public function order()
    {
    	return $this->belongsTo('App\Order', 'order_id');
    }
}
