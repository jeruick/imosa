<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public $table = 'departments';
    public $primaryKey = 'department_id';

    public $timestamps = false;

    public function cities()
    {
    	return $this->hasMany('App\City', 'department_id');
    }
}
