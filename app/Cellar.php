<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cellar extends Model
{
     protected $table = 'cellars';

    protected $primaryKey = 'cellar_id';

    public $timestamps = false;
}
