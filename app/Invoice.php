<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';
    
    protected $primaryKey = 'invoice_id';
    
    public $timestamps = false;
    
    public $incrementing = false;
    
    public function order()
    {
    	return $this->belongsTo('App\Order', 'order_id');
    }

    public function companyInvoice()
    {
    	return $this->belongsTo('App\CompanyInvoice', 'company_invoice');
    }
}
