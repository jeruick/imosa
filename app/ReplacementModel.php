<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplacementModel extends Model
{
    protected $table = 'replacements_models';

    protected $primaryKey = 'replacement_model';

    public $timestamps = false;

    public function getModel()
    {
    	return $this->belongsTo('App\MotorcycleModel', 'model_id');
    }
    public function getReplacement()
    {
    	return $this->belongsTo('App\Replacement', 'replacement_id');
    }

}
