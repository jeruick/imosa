<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
   protected $table = 'credits';

   protected $primaryKey = 'credit_id';
   public $timestamps = false;

   public function customer()
   {
      return $this->belongsTo('App\Customer', 'customer_id');
   }

   public function credit_replacement()
   {
      return $this->hasMany('App\CreditReplacement', 'credit_id');
   }

   public function payment_history()
   {
      return $this->hasMany('App\PaymentHistory', 'credit_id');
   }

   public function order()
   {
      return $this->belongsTo('App\Order', 'order_id');
   }

}
