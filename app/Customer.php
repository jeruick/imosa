<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	protected $table = 'customers';

	protected $primaryKey = 'customer_id';
	public $timestamps = false;

	public function credit()
	{
		return $this->hasMany('App\Credit', 'customer_id');
	}

	public function type()
	{
		return $this->belongsTo('App\CustomerType', 'customer_type');
	}

	public function city()
	{
		return $this->belongsTo('App\City', 'city_id');
	}
}
