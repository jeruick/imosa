<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_products';
    public $timestamps = false;
    

    public function product()
    {
    	return $this->belongsTo('App\Product', 'product_id');
    }

    public function order()
    {
    	return $this->belongsTo('App\Order', 'order_id');	
    }
}
