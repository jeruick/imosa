<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    protected $primaryKey = 'brand_id';
    public $timestamps = false;

    public function models()
    {
    	return $this->hasMany('App\Model', 'brand_id');
    }
}
