<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotorcycleModel extends Model
{
    protected $table = 'models';

    protected $primaryKey = 'model_id';

    public $timestamps = false;

    public function brand()
    {
    	return $this->belongsTo('App\Brand' , 'brand_id');
    }
    
    public function ReplacementModel()
    {
    	return $this->hasMany('App\ReplacementModel', 'model_id');
    }
}