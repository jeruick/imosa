<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInvoice extends Model
{
    protected $table = 'company_invoice';
    
    protected $primaryKey = 'company_invoice_id';
    
    public $timestamps = false;

    public function company()
    {
    	return $this->belongsTo('App\Company', 'company_id');
    }
}
