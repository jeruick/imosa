<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplacementOrder extends Model
{
    protected $table = 'replacements_orders';

    protected $primaryKey = 'replacement_order';

    public $timestamps = false;

    public function getOrder()
    {
    	return $this->belongsTo('App\Order', 'order_id');
    }

    public function getReplacement()
    {
    	return $this->belongsTo('App\Replacement', 'replacement_id');
    }
}
