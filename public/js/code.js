$(document).ready(function() {
	$('.export').on('click', function(event){
		  $.msg({ clickUnblock : false, timeOut: 10000, content: 'Espera mientras se genera el PDF...' });
	});
	$.get('/load-notifications', function(data, textStatus, xhr) {
		/*optional stuff to do after success */
		console.log(data);
		$('.notifications').html('');

		if(data.orders){
			$('.fa-globe').css('color', 'aqua');
			$.each(data.orders, function(index, val) {
				 /* iterate through array or object */
				 var markup = "<li><a href='/ordenes'>" + val.name + " creo una orden</a></li>";
				 $('.notifications').prepend(markup);
			});
		}

		if(data.invoice){
			$('.fa-globe').css('color', 'aqua');
			var markup = "<li><a href='/dei'>" + data.invoice + "</a></li>";
			$('.notifications').prepend(markup);
		}
	});

	$(document).foundation();
	// trigger by event
	/*Clear local storage*/
	clear_storage();

	/* Dynamic tables */
	if ($('#my-table').length > 0) {
		$('#my-table').DataTable({
			 paging: false,
			 searching: false,
		});
	} 
	/* refresh button */
	$('button.refresh').on('click', function(event) {
		event.preventDefault();
		window.location.reload();
	});
	/* reveal modal */
	$('a.reveal-link').click(function(event) {
		console.log("open");
		$('#myModal').foundation('open');

	});

	$(document).on("keypress", "form", function(event) { 
	    return event.keyCode != 13;
	});

	$('a.print').click(function() {
		window.print();
	});

	$('.sel-status').on('change', function(event){
		var $user_id = $(this).parent().parent().attr('id');
		var $status = parseInt($(this).val()) == 0 ? 1 : 0;
		console.log($user_id, $status);
		$.post('usuario', {user_id: $user_id, status: $status,  _token : $('meta[name=_token]').attr('content')}, function(data) {
			window.location.reload();
		});
	})
	$('.sel-type').on('change', function(event){
		var $user_id = $(this).parent().parent().attr('id');
		var $type = parseInt($(this).val());
		console.log($user_id, $type);
		$.post('tipo', {user_id: $user_id, type: $type,  _token : $('meta[name=_token]').attr('content')}, function(data) {
			window.location.reload();
		});
	})
	
	$('.compatible-models .list').css('display', 'none !important');

	$('input[type=checkbox]').on('change',  function(event) {
		event.preventDefault();
		var $check = $(this).
		val();
		$.each($('input[type=checkbox]'), function(index, val) {
    		if ($(val).val() != $check) {
    			$(val).prop('checked', false)
    		}
		});
		
	});


	$('.search-customer').keyup(function(event) {
		
		if(event.keyCode == 13){
			var text = $(this).val() ? $(this).val() : 'blank';

			$.get('../clientes/buscar/' + text, function(data) {

				if (data) {
					
					if(data.redirect)
					{
						window.location.href = data.redirect;
					}

					$('.customers-body').html('');
					
					$.each(data, function(index, val) {

						var markup = "<tr>" +
				  			"<td><a href='cliente/" + val.customer_id + "'>" + val.full_name + "</a></td>" +
				  			"<td>" + val.city.name + "</td>" +
				  		  	"<td>" + val.address + "</td>" +
				  		  	"<td>" + val.phone_number + "</td>" +
				  		  	"<td>" + val.RTN + "</td>" +
				  		"</tr>";
				  		$('.customers-body').append(markup);
					})
				}

			})
		}
		
	})

	$('.search-replacement').keyup(function(event) {
		
		;
		if (event.keyCode == 13) {
			var text = $(this).val() ? $(this).val() : 'blank';
			$.get('repuesto/buscar/' + text, function(data) {
				console.log(data);
				
				if(data.redirect)
				{
					window.location.href = data.redirect;
				}
				else if(data.error){
					$('#replacements').html('<h2>' + data.error + '</h2>');
				}
				else {
					
					$('#replacements').html(data);
				}
			
			});
		}

		
	})

	$('.cotiz-motorcycle').keyup(function(event) {
		
		var $text = $(this).val() ? $(this).val() : 'blank2';
		
		$.ajax({
			url: '../motos/buscar',
			type: 'POST',
			data: {text: $text, _token : $('meta[name=_token]').attr('content') },
		})
		.done(function(data) {
			
		
		if(data.redirect)
		{
			window.location.href = data.redirect;
		}
		if(data)
		{

			$('input[name=plate_number]').val(data[0].plate_number)
			$('input[name=brand]').val(data[0].brand_description)
			$('input[name=model]').val(data[0].model_description)
			$('input[name=color]').val(data[0].color)
			$('input[name=year]').val(data[0].year)
			$('input[name=series]').val(data[0].series)
			$('input[name=motor]').val(data[0].motor)
			$('input[name=cylindra]').val(data[0].cylindra)
			$('input[name=price]').val(data[0].price)
			$('input[name=motorcycle_type]').val(data[0].motorcycle_type)

			$('.picture').attr('src', data[0].picture);
			
		}

		})
	});

	$('.order-motorcycle').keyup(function(event) {
		if (event.keyCode == 13) {
			var $text = $(this).val() ? $(this).val() : 'blank';
			
			$.ajax({
				url: '../motos/buscar',
				type: 'POST',
				data: {text: $text, _token : $('meta[name=_token]').attr('content') },
			})
			.done(function(data) {
				
			
			if(data.redirect)
			{
				window.location.href = data.redirect;
			}
			if(data)
			{
				var $motorcycle_id = data[0].motorcycle_id;
				$('.motorcycle').val($motorcycle_id).attr('name', 'motorcycles['+ $motorcycle_id +'][motorcycle_id]')
				$('.quantity').val(1).attr('name', 'motorcycles['+ $motorcycle_id +'][quantity]')
				$('.plate_number').val(data[0].plate_number)
				$('.brand').val(data[0].brand_description)
				$('.model').val(data[0].model_description)
				$('.color').val(data[0].color)
				$('.year').val(data[0].year)
				$('.series').val(data[0].series)
				$('.motor').val(data[0].motor)
				$('.cylindra').val(data[0].cylindra)
				$('.price').val(data[0].price)
				$('.revision').val(data[0].revision)
				$('.permission').val(data[0].permission)
				$('.transfer').val(data[0].transfer)
				$('.plate_state').val(data[0].plate_state)
				$('.picture').attr('src', data[0].picture);
				
			}

			})
		} 
		
	});
	$('.search-motorcycle').keyup(function(event) {


		if (event.keyCode == 13) {
			var $text = $(this).val() ? $(this).val() : 'blank';
			var $filter = $('input[type=checkbox]:checked').val()
			
			$.ajax({
				url: 'motos/buscar',
				type: 'POST',
				data: { filter: $filter, text: $text, _token : $('meta[name=_token]').attr('content') } ,
			})
			.done(function(data) {
				
				if (data) {
					
					if(data.redirect)
					{
						window.location.href = data.redirect;
					}
					
					$('.motorcycles-body').html('');
					$.each(data, function(index, val) {

						var markup = "<tr>" +
								"<td><a href='motos/" + val.motorcycle_id +"' class='button'><i class='fa fa-search'></i></a></td>" +
								"<td>" + val.plate_number +"</td>" +
								"<td>" + val.brand_description +"</td>" +
								"<td>" + val.model_description +"</td>" +
								"<td>" + val.cylindra +"</td>" +
								"<td>" + val.year +"</td>" +
								"<td>" + val.price  +"</td>" +
								"<td>" + val.transfer +"</td>" +
								"<td>" + val.plate_state +"</td>" +
								"<td>" + val.quantity +"</td>" +
								"<td><a href='motos/eliminar/" + val.motorcycle_id + "' class='button alert'><i class='fa fa-remove'></i></a></td>" +

							"</tr>";
				  		$('.motorcycles-body').append(markup);
					});
				}
			})
		}
		
	})

	$('.brand-select').on('change', function(event) {
		event.preventDefault();
		/* Act on the event */
		var $brand_id = $(this).val();
		$.ajax({
			url: "modelos/marca/" + $brand_id ,
			type: 'GET'
		})
		.done(function(data) {
			$('.model-select').html('')
			if (data) {
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 var $markup = "<option value='" + val.model_id + "'>" + val.description +"</option>";
					 $('.model-select').append($markup)
				});
			} 
		})
		
	});

})

function select_model (element) {
	console.log(element);
	var $model = {
		model_id : $(element).attr('id'),
		description: $(element).html()
	}
	
	if(!localStorage.getItem('models'))
	{
		var $models = []
		$models.push($model);
		localStorage.setItem('models', JSON.stringify($models));
	}
	else
	{

		var $models = JSON.parse(localStorage.getItem('models'));

		var $exists = false;
		$.each($models, function(index, val) {
			 if (val.model_id == $model.model_id) {
			 	$exists = true
			 }
		});
		if (!$exists) {
			$models.push($model);
			localStorage.setItem('models', JSON.stringify($models));
		}
		
	}

	update_models();
}

function update_models () {
	$('.compatible-models .list').css('display', 'none');
	$('.search-models').val('')
	$('.selected-models').html('')
	if(localStorage.getItem('models'))
	{

		var $models = JSON.parse(localStorage.getItem('models'))
		
		$.each($models, function(index, val) {
			var $markup = "<li>" + val.description + "<input type='hidden' name='models[]' value='" + val.model_id+ "'/><span class='remove fa fa-close' onclick='remove_model(this)' value='" + val.model_id + "'></span></li>"
			$('.selected-models').append($markup)
		});
	}
}

function remove_model (element) {

	if(localStorage.getItem('models'))
	{
		var $models = JSON.parse(localStorage.getItem('models'))
		$.each($models, function(index, val) {
			 if (val.model_id == $(element).attr('value')) {
			 	$models.splice(index, 1)
			 	return false;
			 } 
		})
		localStorage.setItem('models', JSON.stringify($models));
		update_models()
	}

}
function select_motorcycle (element) {
	
	
	var $motorcycle = {
		motorcycle_id : $(element).attr('id'),
		description: $(element).children('span.description').html(),
		quantity:1
	}
	
	if(!localStorage.getItem('motorcycles'))
	{
		var $motorcycles = []
		$motorcycles.push($motorcycle);
		localStorage.setItem('motorcycles', JSON.stringify($motorcycles));
	}
	else
	{

		var $motorcycles = JSON.parse(localStorage.getItem('motorcycles'))
		var $exists = false;
		$.each($motorcycles, function(index, val) {
			 if (val.motorcycle_id == $motorcycle.motorcycle_id) {
			 	$exists = true
			 }
		});
		if (!$exists) {
			$motorcycles.push($motorcycle);
			localStorage.setItem('motorcycles', JSON.stringify($motorcycles));
		}
		
	}

	update_motorcycles();
}

function update_motorcycles () {
	$('.motorcycles-list').css('display', 'none');
	$('.search-motorcycles').val('')
	$('.selected-motorcycles').html('')

	if(localStorage.getItem('motorcycles'))
	{
		
		var $motorcycles = JSON.parse(localStorage.getItem('motorcycles'))
		
		$.each($motorcycles, function(index, val) {
			var $markup = "<li>" + val.description + " | " + val.quantity + "<input type='hidden' name='products["+val.motorcycle_id+"][motorcycle_id]' value='"+ val.motorcycle_id +"'><input type='hidden' name='products["+val.motorcycle_id+"][quantity]' value='" + val.quantity + "'/><span class='remove fa fa-close' onclick='remove_motorcycle(this)' value='" + val.motorcycle_id  + "'></span></li>"
			$('.selected-motorcycles').append($markup)
		});
	}

	update_moto_total();
}

function remove_motorcycle (element) {

	if(localStorage.getItem('motorcycles'))
	{
		var $motorcycles = JSON.parse(localStorage.getItem('motorcycles'))
		if ($motorcycles.length <= 1) {
			localStorage.removeItem('motorcycles')
		}
		else
		{
			$.each($motorcycles, function(index, val) {
				
				 if (val.motorcycle_id == $(element).attr('value')) {
				 	$motorcycles.splice(index, 1)
				 } 
			})
			localStorage.setItem('motorcycles', JSON.stringify($motorcycles));	
		}
		
		update_motorcycles()
	}

}



function select_replacement (element) {
	var $quantity = prompt("Cantidad");
	if (!parseInt($quantity)) {
		return alert('valor no valido')
	}
	var $replacement = {
		replacement_id : $(element).attr('id'),

		description: $(element).html(),
		

		description: $(element).children('span.description').html(),

		quantity: $quantity
	}
	
	if(!localStorage.getItem('replacements'))
	{
		var $replacements = []
		$replacements.push($replacement);
		localStorage.setItem('replacements', JSON.stringify($replacements));
	}
	else
	{
		
		var $replacements = JSON.parse(localStorage.getItem('replacements'))
		var $exists = false;
		$.each($replacements, function(index, val) {
			 if (val.replacement_id == $replacement.replacement_id) {
			 	$exists = true
			 }
		});
		if (!$exists) {
			$replacements.push($replacement);
			localStorage.setItem('replacements', JSON.stringify($replacements));
		}
		
	}

	update_replacements();
}

function update_replacements () {
	$('.replacements-list').css('display', 'none');
	$('.search-replacements').val('')
	$('.selected-replacements').html('')
	if(localStorage.getItem('replacements'))
	{

		var $replacements = JSON.parse(localStorage.getItem('replacements'))
		
		$.each($replacements, function(index, val) {
			var $markup = "<li>" + val.description + " | " + val.quantity + "<input type='hidden' name='products["+ val.replacement_id +"][replacement_id]' value='" + val.replacement_id + "'/>" + "<input type='hidden' name='products["+ val.replacement_id +"][quantity]' value='" + val.quantity + "'/><span class='remove fa fa-close' onclick='remove_replacement(this)' value='" + val.replacement_id  + "'></span></li>"
			$('.selected-replacements').append($markup)
		});
	}

	update_total();
}

function remove_replacement (element) {

	if(localStorage.getItem('replacements'))
	{
		var $replacements = JSON.parse(localStorage.getItem('replacements'))
		
		if ($replacements.length <= 1) {
			localStorage.removeItem('replacements')
		} else{
			$.each($replacements, function(index, val) {
				
				 if (val.replacement_id == $(element).attr('value')) {

				 	$replacements.splice(index, 1)
				 	return false;
				 } 
			})
			localStorage.setItem('replacements', JSON.stringify($replacements));
		}
		
		
		update_replacements()
	}

}

function clear_storage () {

	localStorage.removeItem('models')
	localStorage.removeItem('replacements')
	localStorage.removeItem('motorcycles')
}

function isNumber(evt){

	var charCode = (evt.which) ? evt.which : evt.keyCode;
  	if (charCode > 31  && (charCode < 48 || charCode > 57))
     return false;

  	return true;
}

function isDecimal(evt){
	var charCode = (evt.which) ? evt.which : evt.keyCode;
  	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
     return false;
  	return true;
}
    
 





